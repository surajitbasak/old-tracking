package com.example.sheet.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sheet.Activity.NewSpreadSheetActivity;
import com.example.sheet.R;
import com.example.sheet.Utils.BasicUtils;
import com.example.sheet.Utils.DatabaseHandler;
import com.example.sheet.databinding.ItemColumnBinding;
import com.example.sheet.databinding.ItemOptionsBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CreateSpreadsheetAdapter extends BaseAdapter {

    private static final String TAG = "CreateSpreadsheetAdapte";

    ArrayList<String> list;
    Context context;
    String accessToken, accessTokenType;
    BasicUtils basicUtils;
    DatabaseHandler db;

    public CreateSpreadsheetAdapter(ArrayList<String> list, Context context, String accessToken, String accessTokenType) {
        this.list = list;
        this.context = context;
        this.accessToken = accessToken;
        this.accessTokenType = accessTokenType;
        basicUtils = new BasicUtils(context);
        db = new DatabaseHandler(context);
    }

    /*@NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        ItemColumnBinding binding = ItemColumnBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        MyViewHolder holder = new MyViewHolder(binding);
        return holder;
    }*/

  /*  @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {

    }*/

   /* @Override
    public int getItemCount() {
        return list.size();
    }*/

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemColumnBinding b = ItemColumnBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        if (position > 1) {
            b.ivDelete.setVisibility(View.VISIBLE);
        } else {
            b.ivDelete.setVisibility(View.GONE);
        }

        b.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    list.remove(position);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onClick: " + e.toString());
                    basicUtils.showCustomAlert(e.toString());
                }
            }
        });

        ArrayList<String> inputTypes = new ArrayList<>();

        inputTypes.add("Input Type");
        inputTypes.add("Text");
        inputTypes.add("Large Text");
        inputTypes.add("Number");
        inputTypes.add("Date");
        inputTypes.add("Time");
        inputTypes.add("Barcode");
        inputTypes.add("Dropdown");
        inputTypes.add("Location");
        inputTypes.add("Mobile");
        inputTypes.add("Email");
        inputTypes.add("Website");
        inputTypes.add("Auto Timestamp");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.list_layout_column, inputTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        b.spInputType.setAdapter(adapter);

        b.spInputType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = b.spInputType.getSelectedItem().toString();
                if (value.equals("Dropdown")) {
                    b.llOptions.setVisibility(View.VISIBLE);
                } else {
                    b.llOptions.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayList<String> options = new ArrayList<>();
        OptionAdapter opAdapter = new OptionAdapter(options, b.rvOptions);
        b.rvOptions.setLayoutManager(new LinearLayoutManager(context));
        b.rvOptions.setAdapter(opAdapter);
        b.etOptions.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String value = b.etOptions.getText().toString().trim();
                if (value.length() == 0) {
                    basicUtils.showCustomAlert("No value entered!");
                } else {
                    b.etOptions.setText("");
                    options.add(value);
                    opAdapter.notifyDataSetChanged();
                    b.rvOptions.smoothScrollToPosition(list.size() - 1);
                }
                return true;
            }
        });
        b.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = b.etOptions.getText().toString().trim();
                if (value.length() == 0) {
                    basicUtils.showCustomAlert("No value entered!");
                } else {
                    b.etOptions.setText("");
                    options.add(value);
                    opAdapter.notifyDataSetChanged();
                    b.rvOptions.smoothScrollToPosition(list.size() - 1);
                }
            }
        });

        NewSpreadSheetActivity.b.llAddColumn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.add("");
                notifyDataSetChanged();
                //NewSpreadSheetActivity.b.lvColumns.smoothScrollToPosition(list.size() - 1);
                b.etColumnName.setText("");
            }
        });

        NewSpreadSheetActivity.b.tvCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<VerifyItem> vList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    VerifyItem item = new VerifyItem();
                    item.setColumnName(b.etColumnName.getText().toString().trim());
                    item.setInputType(b.spInputType.getSelectedItem().toString().trim());
                    item.setOptions(options);
                    vList.add(item);
                }
                if (isVerified(vList)) {
                    String spreadSheetName = NewSpreadSheetActivity.b.etName.getText().toString().trim();
                    if (spreadSheetName.length() == 0) {
                        basicUtils.showCustomAlert("Please enter a Spreadsheet Name!");
                    } else {
                        basicUtils.showCustomAlert("Success");
                    }
                } else {
                    basicUtils.showCustomAlert("Please fill all the fields!");
                }
            }
        });

        return b.getRoot();
    }

    /*public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemColumnBinding b;

        public MyViewHolder(@NonNull @NotNull ItemColumnBinding itemView) {
            super(itemView.getRoot());
            this.b = itemView;
        }
    }*/

    public class VerifyItem {
        String columnName;
        String inputType;
        ArrayList<String> options;

        public String getColumnName() {
            return columnName;
        }

        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        public String getInputType() {
            return inputType;
        }

        public void setInputType(String inputType) {
            this.inputType = inputType;
        }

        public ArrayList<String> getOptions() {
            return options;
        }

        public void setOptions(ArrayList<String> options) {
            this.options = options;
        }

        @Override
        public String toString() {
            return "VerifyItem{" +
                    "columnName='" + columnName + '\'' +
                    ", inputType='" + inputType + '\'' +
                    ", options=" + options +
                    '}';
        }
    }

    private boolean isVerified(ArrayList<VerifyItem> list) {
        boolean isVerified = true;
        for (VerifyItem item : list) {
            if (item.getColumnName().trim().length() == 0) {
                isVerified = false;
                break;
            }
            if (item.getInputType().equals("Input Type")) {
                isVerified = false;
                break;
            }
            if (item.getInputType().equals("Dropdown") && item.getOptions().size() == 0) {
                isVerified = false;
                break;
            }
        }
        return isVerified;
    }

    public static class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.MyViewHolder> {

        ArrayList<String> list;
        RecyclerView rvOptions;

        public OptionAdapter(ArrayList<String> list, RecyclerView rvOptions) {
            this.list = list;
            this.rvOptions = rvOptions;
        }

        @NonNull
        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            ItemOptionsBinding binding = ItemOptionsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            MyViewHolder holder = new MyViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {
            String value = list.get(position);
            h.b.tvCount.setText(String.valueOf(position + 1) + ".");
            h.b.tvText.setText(value);
            h.b.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ItemOptionsBinding b;

            public MyViewHolder(@NonNull @NotNull ItemOptionsBinding itemView) {
                super(itemView.getRoot());
                this.b = itemView;
            }
        }

        public ArrayList<String> getList() {
            return list;
        }
    }
}
