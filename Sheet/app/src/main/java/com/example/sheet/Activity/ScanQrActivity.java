package com.example.sheet.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.example.sheet.R;
import com.example.sheet.Utils.BasicUtils;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;


public class ScanQrActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    BarcodeReader barcodeReader;
    RelativeLayout rl;
    CardView cClear;
    CardView cScan;
    CardView cEdit;
    CardView cvTorch;
    String result;
    boolean isTorchOn = false;
    private CameraManager mCameraManager;
    private String mCameraId;
    BasicUtils basicUtils;

    public static final String TAG = "ScanQrActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);

        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() throws Exception{
        rl = findViewById(R.id.rl);
        cClear = findViewById(R.id.cClear);
        cScan = findViewById(R.id.cScan);
        cEdit = findViewById(R.id.cEdit);
        cvTorch = findViewById(R.id.cvTorch);
        basicUtils = new BasicUtils(ScanQrActivity.this);
        basicUtils.adjustScannerPreview(barcodeReader);

        cScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", result);
                returnIntent.putExtra("type", 1);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        cEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", result);
                returnIntent.putExtra("type", 2);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        cClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result = "";
                barcodeReader.pauseScanning();

            }
        });

        boolean isFlashAvailable = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (!isFlashAvailable) {
            Toast.makeText(this, "Flash not available in this device...", Toast.LENGTH_LONG).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                if (mCameraManager != null) {
                    mCameraId = mCameraManager.getCameraIdList()[0];
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //cvTorch.setVisibility(View.VISIBLE);
        }

        cvTorch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTorchOn) {
                    isTorchOn = false;
                } else {
                    isTorchOn = true;
                }
                switchFlashLight(isTorchOn);

            }
        });

    }

    @Override
    public void onScanned(final Barcode barcode) {
        barcodeReader.pauseScanning();

        basicUtils.vibrate();
        result = barcode.displayValue;
        if (result.matches(".*")) {
            barcodeReader.playBeep();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", result);
                    returnIntent.putExtra("type", 1);
                    setResult(Activity.RESULT_OK, returnIntent);
                    Toast.makeText(ScanQrActivity.this, "Scanned Successfully", Toast.LENGTH_SHORT).show();
                    finish();
//                editText.setText(""+barcode.displayValue);
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ScanQrActivity.this, "Wrong barcode. Please check your barcode", Toast.LENGTH_SHORT).show();
                    barcodeReader.resumeScanning();
                }
            });
        }
        Log.d(TAG, "onScanned: " + barcode.displayValue);
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }

    public void switchFlashLight(boolean status) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, status);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "switchFlashLight : " + e.toString());
        }
    }

}
