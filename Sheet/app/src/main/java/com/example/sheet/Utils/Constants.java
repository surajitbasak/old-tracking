package com.example.sheet.Utils;

public class Constants {
    public static final String APP_NAME                                                             = "Easy Sheet";

    public static final String API_KEY                                                              = "AIzaSyCmlzjCo7pK9eskETdfj96GBXZ9zMy-TNI";
    public static final String CLIENT_SECRET                                                        = "JtOO-7iGzIROFDyI1bxAgf_5";
    public static final String SERVER_CLIENT_ID                                                     = "536400324274-accv2dav3jil11r5jl5at8hmh6635ahq.apps.googleusercontent.com";
    public static final String WEB_CLIENT_ID                                                        = "536400324274-accv2dav3jil11r5jl5at8hmh6635ahq.apps.googleusercontent.com";
    public static final String OAUTH_CLIENT_ID                                                      = "536400324274-ojjcq4nij0t0k4tpv5sbgmjd5a77rhei.apps.googleusercontent.com";

    public static final String SHEETS_API                                                           = "https://sheets.googleapis.com/v4/spreadsheets/";
    public static final String DRIVE_API                                                            = "https://www.googleapis.com/drive/v3/files/";
    public static final String VERIFY_ID_TOKEN                                                      = "https://oauth2.googleapis.com/tokeninfo?id_token="; //add account.getIdToken();
    public static final String API_GET_ACCESS_TOKEN_INFO                                            = "https://oauth2.googleapis.com/tokeninfo?access_token="; // add Access token

    public static final String KEY_ACCOUNT_ID                                                       = "ACCOUNT_ID";
    public static final String KEY_ID_TOKEN                                                         = "ID_TOKEN";
    public static final String KEY_SUBSCRIPTION_STATUS                                              = "SUBSCRIPTION_STATUS";
    public static final String KEY_DATA                                                             = "DATA";



    public static final boolean SUPER_USER                                                          = false;


}
