package com.example.sheet.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.sheet.Item.RowContentItem;
import com.example.sheet.Item.RowFieldItem;
import com.example.sheet.Item.RowValueItem;
import com.example.sheet.R;
import com.example.sheet.Utils.API_Details;
import com.example.sheet.Utils.ApiProcessing;
import com.example.sheet.Utils.BasicUtils;
import com.example.sheet.Utils.Constants;
import com.example.sheet.Utils.DatabaseHandler;
import com.example.sheet.Utils.MySingleton;
import com.example.sheet.Utils.PostStringRequest;
import com.example.sheet.databinding.ActivityAddRowInfoBinding;
import com.example.sheet.databinding.ActivityNewSpreadSheetBinding;
import com.example.sheet.databinding.FieldAutoTimestampBinding;
import com.example.sheet.databinding.FieldBarcodeBinding;
import com.example.sheet.databinding.FieldDateBinding;
import com.example.sheet.databinding.FieldDropdownBinding;
import com.example.sheet.databinding.FieldEmailBinding;
import com.example.sheet.databinding.FieldLargeTextBinding;
import com.example.sheet.databinding.FieldLocationBinding;
import com.example.sheet.databinding.FieldMobileBinding;
import com.example.sheet.databinding.FieldNumberBinding;
import com.example.sheet.databinding.FieldTextBinding;
import com.example.sheet.databinding.FieldTimeBinding;
import com.example.sheet.databinding.FieldWebsiteBinding;
import com.example.sheet.databinding.ItemRowFieldsBinding;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddRowInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "AddRowInfoActivity";
    Context context;
    BasicUtils basicUtils;
    DatabaseHandler db;
    ActivityAddRowInfoBinding b;

    boolean isEdit;
    JSONArray array;
    ArrayList<RowValueItem> valueList;
    int Edit_position;
    String SHEET_NAME;
    int SHEET_ID;
    String ACCOUNT_ID;
    String SPREADSHEET_ID;

    ArrayList<RowFieldItem> rowFieldItemArrayList;
    MyAdapter adapter;
    LinearLayoutManager manager;

    int TEXT = 1,
            LARGE_TEXT = 2,
            NUMBER = 3,
            DATE = 4,
            TIME = 5,
            BARCODE = 6,
            DROPDOWN = 7,
            LOCATION = 8,
            MOBILE = 9,
            EMAIL = 10,
            WEBSITE = 11,
            AUTO_TIMESTAMP = 12;

    int ADD_ROW = 1;
    int EDIT_ROW = 2;

    boolean isLocationGot = false;

    public static final int REQUEST_CODE = 1;
    EditText tvBarcode;
    String ACCESS_TOKEN, ACCESS_TOKEN_TYPE;

    LocationManager locationManager;

    JSONArray updateArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BasicUtils.setStatusBarColor(this, R.color.colorAmber);
        b = ActivityAddRowInfoBinding.inflate(getLayoutInflater());
        setContentView(b.getRoot());
        try {
            init();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void init() throws JSONException {
        context = AddRowInfoActivity.this;
        basicUtils = new BasicUtils(context);
        db = new DatabaseHandler(context);
        manager = new LinearLayoutManager(context);
        valueList = new ArrayList<>();
        updateArray = new JSONArray();
        rowFieldItemArrayList = new ArrayList<>();
        adapter = new MyAdapter(rowFieldItemArrayList);
        b.lvRowFields.setAdapter(adapter);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Edit_position = getIntent().getIntExtra("edit_position", 1);
        SHEET_NAME = getIntent().getStringExtra("sheetName");
        SHEET_ID = getIntent().getIntExtra("sheetId", 0);
        isEdit = getIntent().getBooleanExtra("isEdit", false);
        array = new JSONArray(getIntent().getStringExtra("array"));
        if (isEdit)
            valueList = (ArrayList<RowValueItem>) getIntent().getExtras().get("valueList");
        Log.i(TAG, "init: ValueList = " + valueList.toString());
        Log.i(TAG, "init: Shared Array = " + array.toString());

        ACCOUNT_ID = db.getCurrentlyActiveAccountId();
        SPREADSHEET_ID = array.getJSONObject(0).getString("SpreadSheetId");

        b.tvSheetName.setText(SHEET_NAME);

        setBehaviours();
    }

    private void setBehaviours() {
        b.tvUpdate.setOnClickListener(this);
        b.ivBack.setOnClickListener(this);
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String title = object.getString("ColumnName");
                int type = getType(object.getString("InputType"));
                String body = getValue(title);
                JSONArray arr = object.getJSONArray("SpinnerOptions");
                ArrayList<String> options = new ArrayList<>();
                for (int j = 0; j < arr.length(); j++) {
                    options.add(arr.getJSONObject(j).getString("Option"));
                }

                RowFieldItem item = new RowFieldItem();
                item.setType(type);
                item.setTitle(title);
                item.setBody(body);
                item.setOptions(options);

                rowFieldItemArrayList.add(item);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //adapter.notifyDataSetChanged();
        setFields(rowFieldItemArrayList);

    }

    private String getValue(String fieldName) {
        String value = "";
        for (int i = 0; i < valueList.size(); i++) {
            if (valueList.get(i).getHeader().trim().equalsIgnoreCase(fieldName.trim())) {
                value = valueList.get(i).getValue();
                break;
            }
        }
        return value;
    }

    private int getType(String type) {
        int result = 1;
        if (type.equalsIgnoreCase("text"))
            result = TEXT;
        else if (type.equalsIgnoreCase("large text"))
            result = LARGE_TEXT;
        else if (type.equalsIgnoreCase("number"))
            result = NUMBER;
        else if (type.equalsIgnoreCase("date"))
            result = DATE;
        else if (type.equalsIgnoreCase("time"))
            result = TIME;
        else if (type.equalsIgnoreCase("barcode"))
            result = BARCODE;
        else if (type.equalsIgnoreCase("dropdown"))
            result = DROPDOWN;
        else if (type.equalsIgnoreCase("location"))
            result = LOCATION;
        else if (type.equalsIgnoreCase("mobile"))
            result = MOBILE;
        else if (type.equalsIgnoreCase("email"))
            result = EMAIL;
        else if (type.equalsIgnoreCase("website"))
            result = WEBSITE;
        else if (type.equalsIgnoreCase("auto timestamp"))
            result = AUTO_TIMESTAMP;

        return result;
    }

    private void setAutoTimeStamp(TextView tvAutoTimeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy hh:mm:ss a", Locale.ENGLISH);
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Calendar cal = Calendar.getInstance();
                tvAutoTimeStamp.setText(sdf.format(cal.getTime()));
            }

            @Override
            public void onFinish() {
                setAutoTimeStamp(tvAutoTimeStamp);
            }
        }.start();
    }

    private void setFields(ArrayList<RowFieldItem> list) {
        for (int i = 0; i < list.size(); i++) {

            RowFieldItem item = list.get(i);
            if (item.getType() == TEXT) {
                FieldTextBinding bi = FieldTextBinding.inflate(getLayoutInflater());
                bi.flText.setVisibility(View.VISIBLE);
                bi.tvText.setText(item.getTitle());
                bi.etText.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == LARGE_TEXT) {
                FieldLargeTextBinding bi = FieldLargeTextBinding.inflate(getLayoutInflater());
                bi.flLargeText.setVisibility(View.VISIBLE);
                bi.tvLargeText.setText(item.getTitle());
                bi.etLargeText.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == NUMBER) {
                FieldNumberBinding bi = FieldNumberBinding.inflate(getLayoutInflater());
                bi.flNumber.setVisibility(View.VISIBLE);
                bi.tvNumber.setText(item.getTitle());
                bi.etNumber.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == DATE) {
                FieldDateBinding bi = FieldDateBinding.inflate(getLayoutInflater());
                bi.flDate.setVisibility(View.VISIBLE);
                bi.tvDate.setText(item.getTitle());
                bi.etDate.setText(item.getBody());
                bi.flDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calen = Calendar.getInstance();
                        int year = calen.get(Calendar.YEAR);
                        int month = calen.get(Calendar.MONTH);
                        int day = calen.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog datePickerDialog = new DatePickerDialog(AddRowInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Calendar cal = Calendar.getInstance();
                                cal.set(year, month, dayOfMonth);
                                bi.etDate.setText(new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).format(cal.getTime()));
                            }
                        }, year, month, day);
                        datePickerDialog.show();
                    }
                });
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == TIME) {
                FieldTimeBinding bi = FieldTimeBinding.inflate(getLayoutInflater());
                bi.flTime.setVisibility(View.VISIBLE);
                bi.tvTime.setText(item.getTitle());
                bi.etTime.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
                bi.flTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar Time = Calendar.getInstance();
                        int hour = Time.get(Calendar.HOUR_OF_DAY);
                        int minute = Time.get(Calendar.MINUTE);

                        TimePickerDialog timePickerDialog = new TimePickerDialog(AddRowInfoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Calendar cal = Calendar.getInstance();
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                cal.set(Calendar.MINUTE, minute);
                                bi.etTime.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(cal.getTime()));
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                });
            } else if (item.getType() == BARCODE) {
                FieldBarcodeBinding bi = FieldBarcodeBinding.inflate(getLayoutInflater());
                bi.flBarcode.setVisibility(View.VISIBLE);
                bi.tvBarcode.setText(item.getTitle());
                bi.etBarcode.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
                bi.ivBarcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(AddRowInfoActivity.this, new String[]{Manifest.permission.CAMERA}, 30);
                            return;
                        }
                        tvBarcode = bi.etBarcode;
                        Intent i = new Intent(context, ScanQrActivity.class);
                        startActivityForResult(i, REQUEST_CODE);
                    }
                });
            } else if (item.getType() == DROPDOWN) {
                FieldDropdownBinding bi = FieldDropdownBinding.inflate(getLayoutInflater());
                bi.flDropDown.setVisibility(View.VISIBLE);
                bi.tvDropDown.setText(item.getTitle());
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.list_layout, item.getOptions());
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                bi.spDropDown.setAdapter(adapter);
                bi.spDropDown.setSelection(getPosition(item.getOptions(), item.getBody()));
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == LOCATION) {
                FieldLocationBinding bi = FieldLocationBinding.inflate(getLayoutInflater());
                bi.flLocation.setVisibility(View.VISIBLE);
                bi.tvLocation.setText(item.getTitle());
                if (item.getBody().contains(",")) {
                    String[] loc = item.getBody().split(",");
                    bi.etLatitude.setText(loc[0]);
                    bi.etLongitude.setText(loc[1]);
                } else {
                    bi.etLatitude.setText("");
                    bi.etLongitude.setText("");
                }
                bi.ivLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(AddRowInfoActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 20);
                            return;
                        }
                        if (!statusCheck()) {
                            return;
                        }
                        isLocationGot = false;
                        basicUtils.showSmallProgressDialog(true);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                            @Override
                            public void onLocationChanged(@NonNull Location location) {
                                if (!isLocationGot) {
                                    isLocationGot = true;
                                    basicUtils.showSmallProgressDialog(false);
                                    bi.etLatitude.setText(String.valueOf(location.getLatitude()));
                                    bi.etLongitude.setText(String.valueOf(location.getLongitude()));
                                }
                            }

                            @Override
                            public void onProviderEnabled(@NonNull String provider) {
                                basicUtils.showSmallProgressDialog(false);
                            }

                            @Override
                            public void onProviderDisabled(@NonNull String provider) {
                                basicUtils.showSmallProgressDialog(false);
                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                                basicUtils.showSmallProgressDialog(false);
                            }
                        });
                    }
                });
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == MOBILE) {
                FieldMobileBinding bi = FieldMobileBinding.inflate(getLayoutInflater());
                bi.flMobile.setVisibility(View.VISIBLE);
                bi.tvMobile.setText(item.getTitle());
                bi.etMobile.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == EMAIL) {
                FieldEmailBinding bi = FieldEmailBinding.inflate(getLayoutInflater());
                bi.flEmail.setVisibility(View.VISIBLE);
                bi.tvEmail.setText(item.getTitle());
                bi.etEmail.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == WEBSITE) {
                FieldWebsiteBinding bi = FieldWebsiteBinding.inflate(getLayoutInflater());
                bi.flWebsite.setVisibility(View.VISIBLE);
                bi.tvWebsite.setText(item.getTitle());
                bi.etWebsite.setText(item.getBody());
                b.llFields.addView(bi.getRoot());
            } else if (item.getType() == AUTO_TIMESTAMP) {
                FieldAutoTimestampBinding bi = FieldAutoTimestampBinding.inflate(getLayoutInflater());
                bi.flAutoTimeStamp.setVisibility(View.VISIBLE);
                bi.tvAutoTimeStamp.setText(item.getTitle());
                setAutoTimeStamp(bi.etAutoTimeStamp);
                b.llFields.addView(bi.getRoot());
            }
        }
    }

    public class MyAdapter extends BaseAdapter {
        ArrayList<RowFieldItem> list;

        public MyAdapter(ArrayList<RowFieldItem> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemRowFieldsBinding bi = ItemRowFieldsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            RowFieldItem item = list.get(position);

            if (item.getType() == TEXT) {
                bi.flText.setVisibility(View.VISIBLE);
                bi.tvText.setText(item.getTitle());
                bi.etText.setText(item.getBody());
            } else if (item.getType() == LARGE_TEXT) {
                bi.flLargeText.setVisibility(View.VISIBLE);
                bi.tvLargeText.setText(item.getTitle());
                bi.etLargeText.setText(item.getBody());
            } else if (item.getType() == NUMBER) {
                bi.flNumber.setVisibility(View.VISIBLE);
                bi.tvNumber.setText(item.getTitle());
                bi.etNumber.setText(item.getBody());
            } else if (item.getType() == DATE) {
                bi.flDate.setVisibility(View.VISIBLE);
                bi.tvDate.setText(item.getTitle());
                bi.etDate.setText(item.getBody());
            } else if (item.getType() == TIME) {
                bi.flTime.setVisibility(View.VISIBLE);
                bi.tvTime.setText(item.getTitle());
                bi.etTime.setText(item.getBody());
            } else if (item.getType() == BARCODE) {
                bi.flBarcode.setVisibility(View.VISIBLE);
                bi.tvBarcode.setText(item.getTitle());
                bi.etBarcode.setText(item.getBody());
            } else if (item.getType() == DROPDOWN) {
                bi.flDropDown.setVisibility(View.VISIBLE);
                bi.tvDropDown.setText(item.getTitle());
                ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.list_layout, item.getOptions());
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                bi.spDropDown.setAdapter(adapter);
                bi.spDropDown.setSelection(getPosition(item.getOptions(), item.getBody()));
            } else if (item.getType() == LOCATION) {
                bi.flLocation.setVisibility(View.VISIBLE);
                bi.tvLocation.setText(item.getTitle());
                if (item.getBody().contains(",")) {
                    String[] loc = item.getBody().split(",");
                    bi.etLatitude.setText(loc[0]);
                    bi.etLongitude.setText(loc[1]);
                } else {
                    bi.etLatitude.setText("");
                    bi.etLongitude.setText("");
                }
            } else if (item.getType() == MOBILE) {
                bi.flMobile.setVisibility(View.VISIBLE);
                bi.tvMobile.setText(item.getTitle());
                bi.etMobile.setText(item.getBody());
            } else if (item.getType() == EMAIL) {
                bi.flEmail.setVisibility(View.VISIBLE);
                bi.tvEmail.setText(item.getTitle());
                bi.etEmail.setText(item.getBody());
            } else if (item.getType() == WEBSITE) {
                bi.flWebsite.setVisibility(View.VISIBLE);
                bi.tvWebsite.setText(item.getTitle());
                bi.etWebsite.setText(item.getBody());
            } else if (item.getType() == AUTO_TIMESTAMP) {
                bi.flAutoTimeStamp.setVisibility(View.VISIBLE);
                bi.tvAutoTimeStamp.setText(item.getTitle());
                setAutoTimeStamp(bi.etAutoTimeStamp);
            }

            bi.flDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar calen = Calendar.getInstance();
                    int year = calen.get(Calendar.YEAR);
                    int month = calen.get(Calendar.MONTH);
                    int day = calen.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(AddRowInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Calendar cal = Calendar.getInstance();
                            cal.set(year, month, dayOfMonth);
                            bi.etDate.setText(new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).format(cal.getTime()));
                        }
                    }, year, month, day);
                    datePickerDialog.show();
                }
            });

            bi.flTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar Time = Calendar.getInstance();
                    int hour = Time.get(Calendar.HOUR_OF_DAY);
                    int minute = Time.get(Calendar.MINUTE);

                    TimePickerDialog timePickerDialog = new TimePickerDialog(AddRowInfoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            Calendar cal = Calendar.getInstance();
                            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            cal.set(Calendar.MINUTE, minute);
                            bi.etTime.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(cal.getTime()));
                        }
                    }, hour, minute, false);
                    timePickerDialog.show();
                }
            });

            bi.ivBarcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddRowInfoActivity.this, new String[]{Manifest.permission.CAMERA}, 30);
                        return;
                    }
                    tvBarcode = bi.etBarcode;
                    Intent i = new Intent(context, ScanQrActivity.class);
                    startActivityForResult(i, REQUEST_CODE);
                }
            });

            bi.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddRowInfoActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 20);
                        return;
                    }
                    basicUtils.showSmallProgressDialog(true);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                        @Override
                        public void onLocationChanged(@NonNull Location location) {
                            basicUtils.showSmallProgressDialog(false);
                            bi.etLatitude.setText(String.valueOf(location.getLatitude()));
                            bi.etLongitude.setText(String.valueOf(location.getLongitude()));
                        }

                        @Override
                        public void onProviderEnabled(@NonNull String provider) {
                            basicUtils.showSmallProgressDialog(false);
                        }

                        @Override
                        public void onProviderDisabled(@NonNull String provider) {
                            basicUtils.showSmallProgressDialog(false);
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {
                            basicUtils.showSmallProgressDialog(false);
                        }
                    });
                }
            });
            return bi.getRoot();
        }
    }

    private int getPosition(ArrayList<String> options, String value) {
        int position = 0;
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i).trim().equalsIgnoreCase(value.trim())) {
                position = i;
                break;
            }
        }
        return position;
    }

    public boolean statusCheck() {
        boolean isOk = false;
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            isOk = true;
        }
        return isOk;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean validateData(JSONArray UpdateArray) {
        boolean isOk = false;
        for (int i = 0; i < UpdateArray.length(); i++) {
            try {
                if (UpdateArray.getJSONObject(i).getString("value").trim().length() != 0) {
                    isOk = true;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isOk;
    }

    public void performWork(String spreadSheetId, int flag, String ACCESS_TOKEN, String ACCESS_TOKEN_TYPE) {
        if (flag == ADD_ROW) {
            volleyAddNewRow(spreadSheetId, SHEET_NAME, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }
        if (flag == EDIT_ROW) {
            volleyEditRow(spreadSheetId, SHEET_NAME, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }
    }

    private void getNewAccessToken(String accountId, String refreshToken, String spreadSheetId, int flag) {
        new Thread() {
            @Override
            public void run() {
                try {
                    GoogleTokenResponse tokenResponse =
                            new GoogleRefreshTokenRequest(
                                    new NetHttpTransport(),
                                    GsonFactory.getDefaultInstance(),
                                    refreshToken,
                                    Constants.WEB_CLIENT_ID,
                                    Constants.CLIENT_SECRET)
                                    .execute();

                    ACCESS_TOKEN = tokenResponse.getAccessToken();
                    long mExpiresInSeconds = tokenResponse.getExpiresInSeconds();
                    ACCESS_TOKEN_TYPE = tokenResponse.getTokenType();

                    Log.i(TAG, "volleyGetAccessTokenInfo: New Token Result = " +
                            "\n Access Token = " + ACCESS_TOKEN +
                            "\n Expires In = " + mExpiresInSeconds +
                            "\n Token Type = " + ACCESS_TOKEN_TYPE +
                            "\n Token Scope = " + tokenResponse.getScope());

                    db.storeNewAccessToken(accountId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);


                } catch (Exception e) {
                    Log.i(TAG, "exchangeCodeForToken: Results = " + e.toString());
                    Log.e(TAG, "Token exchange failed with " + e.getMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        performWork(spreadSheetId, flag, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                    }
                });
            }
        }.start();
    }

    private void volleyGetAccessTokenInfo(String accountId, String spreadsheetId, boolean showProgressBar, int flag) {

        ACCESS_TOKEN = db.getAccessToken(accountId);
        ACCESS_TOKEN_TYPE = db.getAccessTokenType(accountId);
        String refreshToken = db.getRefreshToken(accountId);

        if (showProgressBar)
            basicUtils.showSmallProgressDialog(true);
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetAccessTokenInfo");
        String url = ApiProcessing.UserInfo.API_VERIFY_ACCESS_TOKEN + ACCESS_TOKEN;
        Log.i(TAG, "volleyGetAccessTokenInfo: URL = " + url);
        details.setAPI_URL(url);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyGetAccessTokenInfo: Response = " + response.toString());
                        details.setResponse(response.toString());
                        try {
                            boolean isAccessTokenExpired = ApiProcessing.UserInfo.parseResponse(response);
                            if (isAccessTokenExpired) {
                                getNewAccessToken(accountId, refreshToken, spreadsheetId, flag);
                            } else {
                                Log.i(TAG, "volleyGetAccessTokenInfo: Expires in = " + ApiProcessing.UserInfo.getRemainingTime(response));
                                performWork(spreadsheetId, flag, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: JSONException = " + e.toString());
                            details.setException(e.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: Exception = " + e.toString());
                            details.setException(e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e(TAG, "volleyGetAccessTokenInfo: Error = " + error.toString());
                if (error instanceof ClientError) {
                    getNewAccessToken(accountId, refreshToken, spreadsheetId, flag);
                } else {
                    basicUtils.showSmallProgressDialog(false);
                    basicUtils.showCustomAlert("Failed to verify access token!");
                }
                details.setErrorResponse(error.toString());
                if (Constants.SUPER_USER)
                    details.show();
            }
        });
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AccessTokenInfo");
    }

    private boolean shouldResize() {
        boolean resize = false;
        for (int i = 0; i < updateArray.length(); i++) {
            try {
                if (updateArray.getJSONObject(i).getString("value").length() > 10) {
                    resize = true;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resize;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            String barcodeValue = data.getStringExtra("result");
            tvBarcode.setText(barcodeValue);
        } else if (resultCode == RESULT_CANCELED) {
            //basicUtils.showCustomAlert("Error scanning barcode! please try again");
        }
    }

    private void volleyFormatSheet(String spreadSheetId, int sheetId, boolean shouldResize, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyFormatSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.Operations.getFormatObject(sheetId, shouldResize);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyFormatSheet: URL = " + url);
        Log.i(TAG, "volleyFormatSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyFormatSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyFormatSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyFormatSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "FormatSheet");
    }

    private void volleyEditRow(String spreadSheetId, String sheetName, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyEditRow");

        String url = Constants.SHEETS_API + spreadSheetId + "/values/'" + sheetName + "'?valueInputOption=USER_ENTERED";
        JSONObject object = ApiProcessing.AddRow.createEditRowObject(sheetName, updateArray, Edit_position);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyEditRow: URL = " + url);
        Log.i(TAG, "volleyEditRow: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.PUT,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyEditRow: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        try {
                            RowContentItem item = new RowContentItem();
                            item.setTitle(updateArray.getJSONObject(0).getString("value"));
                            ArrayList<RowValueItem> vList = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                RowValueItem it = new RowValueItem();
                                it.setHeader(array.getJSONObject(i).getString("ColumnName"));
                                it.setValue(updateArray.getJSONObject(i).getString("value"));
                                vList.add(it);
                            }
                            item.setValueList(vList);

                            EditSpreadSheetActivity.rowContentList.remove(Edit_position);
                            EditSpreadSheetActivity.rowContentList.add(Edit_position, item);
                            EditSpreadSheetActivity.rowAdapter.notifyDataSetChanged();
                            basicUtils.showCustomAlert("Row Edited");
                            volleyFormatSheet(spreadSheetId, SHEET_ID, shouldResize(), ACCESS_TOKEN, TOKEN_TYPE);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.e(TAG, "volleyEditRow: Exception = " + e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showCustomAlert("Failed to Edit Row");
                basicUtils.showSmallProgressDialog(false);
                Log.e(TAG, "volleyEditRow: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyEditRow: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AddNewRow");
    }

    private void volleyAddNewRow(String spreadSheetId, String sheetName, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyAddNewRow");

        String url = Constants.SHEETS_API + spreadSheetId + "/values/'" + sheetName + "'" + ":append?valueInputOption=RAW";
        JSONObject object = ApiProcessing.AddRow.createAppendRowObject(sheetName, updateArray);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyAddNewRow: URL = " + url);
        Log.i(TAG, "volleyAddNewRow: OBJECT = " + object.toString());

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        basicUtils.showSmallProgressDialog(false);
                        Log.i(TAG, "volleyAddNewRow: Response = " + response);
                        details.setResponse(response.toString());
                        try {
                            RowContentItem item = new RowContentItem();
                            item.setTitle(updateArray.getJSONObject(0).getString("value"));
                            ArrayList<RowValueItem> vList = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                RowValueItem it = new RowValueItem();
                                it.setHeader(array.getJSONObject(i).getString("ColumnName"));
                                it.setValue(updateArray.getJSONObject(i).getString("value"));
                                vList.add(it);
                            }
                            item.setValueList(vList);

                            EditSpreadSheetActivity.rowContentList.add(item);
                            EditSpreadSheetActivity.rowAdapter.notifyDataSetChanged();
                            basicUtils.showCustomAlert("Row Added");
                            volleyFormatSheet(spreadSheetId, SHEET_ID, shouldResize(), ACCESS_TOKEN, TOKEN_TYPE);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.e(TAG, "volleyAddNewRow: Exception = " + e.toString());
                        }
                        //EditSpreadSheetActivity.rowContentList.add(Edit_position,);
                        basicUtils.showCustomAlert("Row Added");
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                basicUtils.showCustomAlert("Failed to add row!");
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyAddNewRow: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyAddNewRow: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AddNewRow");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvUpdate:
                updateArray = new JSONArray();
                try {
                    for (int i = 0; i < b.llFields.getChildCount(); i++) {
                        JSONObject object = new JSONObject();
                        View v = b.llFields.getChildAt(i);
                        if (rowFieldItemArrayList.get(i).getType() == TEXT) {
                            EditText value = v.findViewById(R.id.etText);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == LARGE_TEXT) {
                            EditText value = v.findViewById(R.id.etLargeText);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == NUMBER) {
                            EditText value = v.findViewById(R.id.etNumber);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == DATE) {
                            TextView value = v.findViewById(R.id.etDate);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == TIME) {
                            TextView value = v.findViewById(R.id.etTime);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == BARCODE) {
                            EditText value = v.findViewById(R.id.etBarcode);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == DROPDOWN) {
                            Spinner sp = v.findViewById(R.id.spDropDown);
                            object.put("value", sp.getSelectedItem().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == LOCATION) {
                            EditText value1 = v.findViewById(R.id.etLatitude);
                            EditText value2 = v.findViewById(R.id.etLongitude);
                            if (value1.length() == 0 && value2.length() == 0) {
                                object.put("value", "");
                            } else {
                                object.put("value", value1.getText().toString() + "," + value2.getText().toString());
                            }
                        } else if (rowFieldItemArrayList.get(i).getType() == MOBILE) {
                            EditText value = v.findViewById(R.id.etMobile);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == EMAIL) {
                            EditText value = v.findViewById(R.id.etEmail);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == WEBSITE) {
                            EditText value = v.findViewById(R.id.etWebsite);
                            object.put("value", value.getText().toString());
                        } else if (rowFieldItemArrayList.get(i).getType() == AUTO_TIMESTAMP) {
                            TextView value = v.findViewById(R.id.etAutoTimeStamp);
                            object.put("value", value.getText().toString());
                        }
                        updateArray.put(object);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "onClick: Update Values = " + updateArray.toString());
                Log.i(TAG, "onClick: Update Values Size = " + b.llFields.getChildCount() + " " + array.length());
                if (!validateData(updateArray)) {
                    basicUtils.showCustomAlert("Please fill at-least one field!");
                    return;
                }
                volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, isEdit ? EDIT_ROW : ADD_ROW);
                break;
            case R.id.ivBack:
                finish();
                break;
        }
    }
}