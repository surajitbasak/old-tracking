package com.example.sheet.Item;

public class SSItem {
    String title;
    String date;

    public String getId() {
        return title;
    }

    public void setId(String id) {
        this.title = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "SSItem{" +
                "title='" + title + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
