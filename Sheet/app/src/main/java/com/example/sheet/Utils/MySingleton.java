package com.example.sheet.Utils;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class MySingleton {
    private static MySingleton mySingleTon;
    private RequestQueue requestQueue;
    private static final String TAG = "MySingleton";
    private static Context mctx;

    private MySingleton(Context context) {
        this.mctx = context;
        this.requestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(mctx.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized MySingleton getInstance(Context context) {
        if (mySingleTon == null) {
            mySingleTon = new MySingleton(context);
        }
        return mySingleTon;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        requestQueue.add(request);
    }

    public <T> void setRetryPolicy(Request<T> req) {
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                        0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

    public <T> void setCustomRetryPolicy(Request<T> req, int retryTime) {
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * retryTime,
                        0,  //Since Multiple bids are being placed if retry is hit as response is delayed but DB captures the bid
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}