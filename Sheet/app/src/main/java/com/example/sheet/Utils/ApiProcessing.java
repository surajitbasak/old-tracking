package com.example.sheet.Utils;

import android.content.Intent;

import com.example.sheet.Item.RowContentItem;
import com.example.sheet.Item.RowValueItem;
import com.example.sheet.Item.SharedUserItem;
import com.example.sheet.Item.SheetItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApiProcessing {
    public static class UserInfo {
        public static final String API_VERIFY_ACCESS_TOKEN = Constants.API_GET_ACCESS_TOKEN_INFO;

        public static boolean parseResponse(JSONObject response) throws JSONException {
            boolean isExpired = false;
            if (response.has("expires_in")) {
                if (response.getString("expires_in").equalsIgnoreCase("0"))
                    isExpired = true;
                else
                    isExpired = false;
            } else {
                isExpired = true;
            }
            return isExpired;
        }

        public static String getRemainingTime(JSONObject response) throws JSONException {
            String time = "";
            if (response.has("expires_in")) {
                if (!response.getString("expires_in").equalsIgnoreCase("0")) {
                    int seconds = Integer.parseInt(response.getString("expires_in"));
                    int minutes = seconds / 60;
                    time = String.valueOf(minutes) + " Minutes";
                }
            } else {
                time = "";
            }
            return time;
        }

    }

    public static class SpreadSheetInfo {
        public static String getSpreadsheetTitle(JSONObject response) throws JSONException {
            String title = "";
            JSONObject properties = response.getJSONObject("properties");
            title = properties.getString("title");
            return title;
        }
    }

    public static class Operations {

        public static JSONObject getHeaderCreationObject(JSONArray array, String sheetName) {

            JSONObject object = new JSONObject();
            try {
                object.put("range", "'" + sheetName + "'");
                object.put("majorDimension", "ROWS");
                JSONArray values = new JSONArray();
                JSONArray rowArray = new JSONArray();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    String columnName = obj.getString("ColumnName");
                    rowArray.put(columnName);
                }
                values.put(rowArray);
                object.put("values", values);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject getMultipleHeaderCreationObject(JSONArray array, ArrayList<String> sheetNames) {
            JSONObject parentObject = new JSONObject();
            JSONArray data = new JSONArray();
            try {
                parentObject.put("valueInputOption", "USER_ENTERED");
                for (int j = 0; j < sheetNames.size(); j++) {
                    JSONObject object = new JSONObject();
                    object.put("range", "'" + sheetNames.get(j) + "'");
                    object.put("majorDimension", "ROWS");
                    JSONArray values = new JSONArray();
                    JSONArray rowArray = new JSONArray();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        String columnName = obj.getString("ColumnName");
                        rowArray.put(columnName);
                    }
                    values.put(rowArray);
                    object.put("values", values);
                    data.put(object);
                }
                parentObject.put("data", data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return parentObject;
        }

        public static JSONObject getFormatObject(int sheetId, boolean resize) {
            JSONObject object = new JSONObject();
            try {
                JSONArray requests = new JSONArray();
                JSONObject header = new JSONObject();
                JSONObject body = new JSONObject();
                JSONObject column = new JSONObject();
                JSONObject row = new JSONObject();

                header.put("repeatCell", new JSONObject()
                        .put("range", new JSONObject()
                                .put("sheetId", sheetId)
                                .put("startRowIndex", 0)
                                .put("endRowIndex", 1))
                        .put("cell", new JSONObject()
                                .put("userEnteredFormat", new JSONObject()
                                        .put("horizontalAlignment", "CENTER")
                                        .put("textFormat", new JSONObject()
                                                .put("fontSize", 12)
                                                .put("bold", true))))
                        .put("fields", "userEnteredFormat(textFormat,horizontalAlignment)"));

                body.put("repeatCell", new JSONObject()
                        .put("range", new JSONObject()
                                .put("sheetId", sheetId)
                                .put("startRowIndex", 1))
                        .put("cell", new JSONObject()
                                .put("userEnteredFormat", new JSONObject()
                                        .put("horizontalAlignment", "CENTER")
                                        .put("textFormat", new JSONObject()
                                                .put("fontSize", 12)
                                                .put("bold", false))))
                        .put("fields", "userEnteredFormat(textFormat,horizontalAlignment)"));

                column.put("autoResizeDimensions", new JSONObject()
                        .put("dimensions", new JSONObject()
                                .put("sheetId", sheetId)
                                .put("dimension", "COLUMNS")
                                .put("startIndex", 0)));

                row.put("autoResizeDimensions", new JSONObject()
                        .put("dimensions", new JSONObject()
                                .put("sheetId", sheetId)
                                .put("dimension", "ROWS")
                                .put("startIndex", 0)));

                requests.put(header)
                        .put(body);
                if (resize) {
                    requests.put(column)
                            .put(row);
                }

                object.put("requests", requests);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return object;
        }
    }

    public static class AddAccess {

        public static ArrayList<SharedUserItem> parseSharedUserResponse(JSONObject response) throws JSONException {
            ArrayList<SharedUserItem> list = new ArrayList<>();

            JSONArray array = response.getJSONArray("permissions");

            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                SharedUserItem item = new SharedUserItem();
                if (object.has("emailAddress")) {
                    item.setId(object.getString("id").trim());
                    if (object.has("displayName")) {
                        item.setName(object.getString("displayName").trim());
                    } else {
                        item.setName("Invalid User");
                    }
                    item.setEmail(object.getString("emailAddress").trim());
                    item.setRole(object.getString("role").trim());
                    if (object.has("photoLink")) {
                        item.setPicture(object.getString("photoLink").trim());
                    } else {
                        item.setPicture("");
                    }
                    list.add(item);
                }
            }

            return list;
        }

        public static JSONObject constructAccessObject(boolean isViewer) {
            JSONObject object = new JSONObject();
            try {
                object.put("role", isViewer ? "reader" : "writer");
                object.put("type", "anyone");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject constructRoleObject(boolean isViewer) {
            JSONObject object = new JSONObject();
            try {
                object.put("role", isViewer ? "reader" : "writer");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject constructShareObject(boolean isViewer, String email) {
            JSONObject object = new JSONObject();
            try {
                object.put("role", isViewer ? "reader" : "writer");
                object.put("type", "user");
                object.put("emailAddress", email);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }
    }

    public static class EditSpreadsheet {
        public static ArrayList<SheetItem> parseResponse(JSONObject response) throws JSONException {
            ArrayList<SheetItem> list = new ArrayList<>();

            JSONArray array = response.getJSONArray("sheets");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                SheetItem item = new SheetItem();
                JSONObject subObject = object.getJSONObject("properties");
                item.setSheetId(subObject.getInt("sheetId"));
                item.setSheetName(subObject.getString("title"));
                if (i == 0) {
                    item.setSelected(true);
                }
                list.add(item);
            }
            return list;
        }

        public static JSONObject constructCreateSheetObject(String name) {
            JSONObject object = new JSONObject();
            try {
                object.put("requests",
                        new JSONArray().put(
                                new JSONObject().put("addSheet",
                                        new JSONObject().put("properties",
                                                new JSONObject().put("title", name)))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static SheetItem parseCreateResponse(JSONObject response) throws JSONException {
            SheetItem item = new SheetItem();
            int sheetId = -1;
            String sheetName = "";
            JSONObject values = response
                    .getJSONArray("replies")
                    .getJSONObject(0)
                    .getJSONObject("addSheet")
                    .getJSONObject("properties");

            sheetId = values.getInt("sheetId");
            sheetName = values.getString("title");

            item.setSheetId(sheetId);
            item.setSheetName(sheetName);
            item.setSelected(true);

            return item;
        }

        public static JSONObject createRenameSheetObject(String name, int sheetId) {
            JSONObject object = new JSONObject();
            try {
                object.put("requests", new JSONArray()
                        .put(new JSONObject()
                                .put("updateSheetProperties", new JSONObject()
                                        .put("properties", new JSONObject()
                                                .put("sheetId", sheetId)
                                                .put("title", name))
                                        .put("fields", "title"))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject createDeleteSheetObject(int sheetId) {
            JSONObject object = new JSONObject();
            try {
                object.put("requests", new JSONArray()
                        .put(new JSONObject()
                                .put("deleteSheet", new JSONObject()
                                        .put("sheetId", sheetId))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static ArrayList<RowContentItem> parseContentResponse(JSONObject response) throws JSONException {
            ArrayList<RowContentItem> list = new ArrayList<>();
            ArrayList<String> header = new ArrayList<>();

            if (!response.has("values")) {
                return list;
            }
            JSONArray array = response.getJSONArray("values");
            JSONArray headers = array.getJSONArray(0);
            for (int i = 0; i < headers.length(); i++) {
                header.add(headers.getString(i));
            }
            for (int j = 1; j < array.length(); j++) {
                JSONArray body = array.getJSONArray(j);
                RowContentItem contentItem = new RowContentItem();
                ArrayList<RowValueItem> valueList = new ArrayList<>();
                for (int i = 0; i < body.length(); i++) {
                    RowValueItem item = new RowValueItem();
                    try {
                        item.setHeader(header.get(i));
                        item.setValue(body.getString(i));
                        valueList.add(item);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                contentItem.setTitle(body.getString(0));
                contentItem.setValueList(valueList);

                list.add(contentItem);
            }

            return list;
        }

        public static JSONObject createDeleteRowObject(int sheetId, int startIndex, int endIndex) {
            JSONObject object = new JSONObject();
            try {
                object.put("requests", new JSONArray()
                        .put(new JSONObject()
                                .put("deleteDimension", new JSONObject()
                                        .put("range", new JSONObject()
                                                .put("sheetId", sheetId)
                                                .put("dimension", "ROWS")
                                                .put("startIndex", startIndex)
                                                .put("endIndex", endIndex)))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject createRenameSpreadsheetObject(String spreadSheetName) {
            JSONObject object = new JSONObject();
            try {
                object.put("requests", new JSONArray()
                        .put(new JSONObject()
                                .put("updateSpreadsheetProperties", new JSONObject()
                                        .put("properties", new JSONObject()
                                                .put("title", spreadSheetName))
                                        .put("fields", "title"))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }
    }

    public static class AddRow {
        public static JSONObject createAppendRowObject(String sheetName, JSONArray updateArray) {
            JSONObject object = new JSONObject();
            try {
                object.put("range", "'" + sheetName + "'");
                object.put("majorDimension", "ROWS");
                JSONArray values = new JSONArray();
                JSONArray subValues = new JSONArray();
                for (int i = 0; i < updateArray.length(); i++) {
                    JSONObject obj = updateArray.getJSONObject(i);
                    subValues.put(obj.getString("value"));
                }
                values.put(subValues);
                object.put("values", values);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        public static JSONObject createEditRowObject(String sheetName, JSONArray updateArray, int edit_position) {
            JSONObject object = new JSONObject();
            int size = edit_position + 1;
            try {
                object.put("range", "'" + sheetName + "'");
                object.put("majorDimension", "ROWS");
                JSONArray values = new JSONArray();
                for (int j = 0; j <= size; j++) {
                    JSONArray subValues = new JSONArray();
                    if (j != size) {
                        values.put(subValues);
                    } else {
                        for (int i = 0; i < updateArray.length(); i++) {
                            JSONObject obj = updateArray.getJSONObject(i);
                            subValues.put(obj.getString("value"));
                        }
                        values.put(subValues);
                    }
                }
                object.put("values", values);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }
    }

}
