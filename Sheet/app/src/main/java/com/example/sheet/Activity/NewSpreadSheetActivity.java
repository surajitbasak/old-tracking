package com.example.sheet.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.sheet.Adapter.CreateSpreadsheetAdapter;
import com.example.sheet.Item.SSItem;
import com.example.sheet.Item.SpreadSheetItem;
import com.example.sheet.R;
import com.example.sheet.Utils.API_Details;
import com.example.sheet.Utils.ApiProcessing;
import com.example.sheet.Utils.BasicUtils;
import com.example.sheet.Utils.Constants;
import com.example.sheet.Utils.DatabaseHandler;
import com.example.sheet.Utils.MySingleton;
import com.example.sheet.Utils.PostStringRequest;
import com.example.sheet.databinding.ActivityNewSpreadSheetBinding;
import com.example.sheet.databinding.ItemColumnBinding;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class NewSpreadSheetActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "NewSpreadSheetActivity";
    Context context;
    BasicUtils basicUtils;
    DatabaseHandler db;
    public static ActivityNewSpreadSheetBinding b;

    ArrayList<String> arrayList;
    ArrayList<String> sheetNames;
    String SPREADSHEET_ID;
    CreateSpreadsheetAdapter adapter;
    LinearLayoutManager manager;

    boolean hasCredentials;
    String spreadsheetCredentials;
    JSONArray credArray;
    ArrayList<String> inputTypes;
    ArrayAdapter<String> spAdapter;

    ArrayList<View> columnsView;

    String ACCOUNT_ID;
    public String ACCESS_TOKEN;
    public String ACCESS_TOKEN_TYPE;

    public Sheets mService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BasicUtils.setStatusBarColor(this, R.color.colorAmber);
        b = ActivityNewSpreadSheetBinding.inflate(getLayoutInflater());
        setContentView(b.getRoot());
        init();
    }

    private void init() {
        context = NewSpreadSheetActivity.this;
        basicUtils = new BasicUtils(context);
        db = new DatabaseHandler(context);
        arrayList = new ArrayList<>();
        columnsView = new ArrayList<>();
        sheetNames = new ArrayList<>();
        manager = new LinearLayoutManager(context);
        ACCOUNT_ID = db.getCurrentlyActiveAccountId();
        hasCredentials = getIntent().getBooleanExtra("hasCreds", false);
        if (hasCredentials) {
            spreadsheetCredentials = getIntent().getExtras().getString("credentials");
            try {
                credArray = new JSONArray(spreadsheetCredentials);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sheetNames = (ArrayList<String>) getIntent().getExtras().get("sheetNames");
            SPREADSHEET_ID = getIntent().getStringExtra("spreadSheetId");
            b.tvSpreadsheetName.setText(EditSpreadSheetActivity.tvSpreadSheetName.getText().toString());
            b.etName.setText(EditSpreadSheetActivity.tvSpreadSheetName.getText().toString());
        }

        setBehaviours();
    }

    private void setBehaviours() {
        b.llAddColumn.setOnClickListener(this);
        b.tvCreate.setOnClickListener(this);
        b.ivBack.setOnClickListener(this);
        arrayList.add("");
        arrayList.add("");
        adapter = new CreateSpreadsheetAdapter(arrayList, context, db.getAccessToken(db.getCurrentlyActiveAccountId()), db.getAccessTokenType(db.getCurrentlyActiveAccountId()));
        //b.lvColumns.setAdapter(adapter);

        inputTypes = new ArrayList<>();

        inputTypes.add("Input Type *");
        inputTypes.add("Text");
        inputTypes.add("Large Text");
        inputTypes.add("Number");
        inputTypes.add("Date");
        inputTypes.add("Time");
        inputTypes.add("Barcode");
        inputTypes.add("Dropdown");
        inputTypes.add("Location");
        inputTypes.add("Mobile");
        inputTypes.add("Email");
        inputTypes.add("Website");
        inputTypes.add("Auto Timestamp");

        spAdapter = new ArrayAdapter<String>(context, R.layout.list_layout_column, inputTypes);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (!hasCredentials) {
            addColumn(null, null, null);
            addColumn(null, null, null);
            b.tvCreate.setText("Create Spreadsheet");
        } else {
            setPreviousColumns(spreadsheetCredentials);
            b.tvCreate.setText("Update Spreadsheet");
        }
    }

    private void addColumn(String columnName, String inputType, ArrayList<String> optionList) {
        ItemColumnBinding binding = ItemColumnBinding.inflate(LayoutInflater.from(b.llColumnContainer.getContext()), b.llColumnContainer, false);
        ArrayList<String> options = new ArrayList<>();
        CreateSpreadsheetAdapter.OptionAdapter opAdapter = new CreateSpreadsheetAdapter.OptionAdapter(optionList == null ? options : optionList, binding.rvOptions);
        binding.rvOptions.setLayoutManager(new LinearLayoutManager(context));
        binding.rvOptions.setAdapter(opAdapter);
        binding.spInputType.setAdapter(spAdapter);
        if (inputType != null) {
            binding.spInputType.setSelection(getPosition(inputType));
        }
        if (columnName != null) {
            binding.etColumnName.setText(columnName);
        }
        binding.spInputType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = binding.spInputType.getSelectedItem().toString();
                if (value.equals("Dropdown")) {
                    binding.llOptions.setVisibility(View.VISIBLE);
                } else {
                    binding.llOptions.setVisibility(View.GONE);
                    options.clear();
                    opAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        binding.etOptions.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String value = binding.etOptions.getText().toString().trim();
                if (value.length() == 0) {
                    basicUtils.showCustomAlert("No value entered!");
                } else {
                    binding.etOptions.setText("");
                    options.add(value);
                    opAdapter.notifyDataSetChanged();
                    binding.rvOptions.smoothScrollToPosition(options.size() - 1);
                }
                return true;
            }
        });
        binding.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = binding.etOptions.getText().toString().trim();
                if (value.length() == 0) {
                    basicUtils.showCustomAlert("No value entered!");
                } else {
                    if (optionList == null){
                        binding.etOptions.setText("");
                        options.add(value);
                        opAdapter.notifyDataSetChanged();
                        binding.rvOptions.smoothScrollToPosition(options.size() - 1);
                    }else{
                        binding.etOptions.setText("");
                        optionList.add(value);
                        opAdapter.notifyDataSetChanged();
                        binding.rvOptions.smoothScrollToPosition(optionList.size() - 1);
                    }
                }
            }
        });
        if (b.llColumnContainer.getChildCount() < 2) {
            binding.ivDelete.setVisibility(View.GONE);
        } else {
            if (!hasCredentials)
                binding.ivDelete.setVisibility(View.VISIBLE);
            else if (columnName != null)
                binding.ivDelete.setVisibility(View.GONE);
            else
                binding.ivDelete.setVisibility(View.VISIBLE);
        }

        binding.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeColumn(binding.getRoot());
            }
        });

        b.llColumnContainer.addView(binding.getRoot());
    }

    private void setPreviousColumns(String spreadsheetCredentials) {
        try {
            JSONArray array = new JSONArray(spreadsheetCredentials);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String columnName = object.getString("ColumnName");
                String inputType = object.getString("InputType");
                ArrayList<String> options = new ArrayList<>();
                JSONArray spinnerOptions = object.getJSONArray("SpinnerOptions");
                for (int j = 0; j < spinnerOptions.length(); j++) {
                    options.add(spinnerOptions.getJSONObject(j).getString("Option"));
                }
                addColumn(columnName, inputType, options);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void removeColumn(View view) {
        b.llColumnContainer.removeView(view);
    }

    private void createNewSpreadsheet(JSONArray array, String ACCESS_TOKEN, String ACCESS_TOKEN_TYPE) {
        new Thread() {
            @Override
            public void run() {
                try {
                    JSONObject object = array.getJSONObject(0);
                    String title = object.getString("Title");
                    //OnBackGround
                    Spreadsheet spreadsheet = new Spreadsheet().setProperties(new SpreadsheetProperties()
                            .setTitle(title));


                    spreadsheet = mService.spreadsheets().create(spreadsheet)
                            .setFields("spreadsheetId")
                            .execute();

                    Spreadsheet finalSpreadsheet1 = spreadsheet;
                    //OnPostExecution
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Spreadsheet ID: " + finalSpreadsheet1.getSpreadsheetId());
                            JSONArray newArray = new JSONArray();
                            for (int i = 0; i < array.length(); i++) {
                                try {
                                    JSONObject ob = array.getJSONObject(i);
                                    ob.put("SpreadSheetId", finalSpreadsheet1.getSpreadsheetId());
                                    newArray.put(ob);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.i(TAG, "run: New JSONArray = " + newArray.toString());
                            //todo Insert into Database
                            volleyAddSpreadSheet(finalSpreadsheet1.getSpreadsheetId(), "Sheet1", 0, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, array);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }.start();
    }

    private void create(boolean isNew) {
        columnsView.clear();
        for (int i = 0; i < b.llColumnContainer.getChildCount(); i++) {
            columnsView.add(b.llColumnContainer.getChildAt(i));
        }
        ArrayList<VerifyItem> vList = new ArrayList<>();
        for (int i = 0; i < columnsView.size(); i++) {
            VerifyItem item = new VerifyItem();
            View view = columnsView.get(i);
            EditText etName = view.findViewById(R.id.etColumnName);
            Spinner spInputType = view.findViewById(R.id.spInputType);
            LinearLayout llOptions = view.findViewById(R.id.llOptions);
            RecyclerView rvOptions = view.findViewById(R.id.rvOptions);
            EditText etOptions = view.findViewById(R.id.etOptions);
            CreateSpreadsheetAdapter.OptionAdapter adapter = (CreateSpreadsheetAdapter.OptionAdapter) rvOptions.getAdapter();
            item.setColumnName(etName.getText().toString());
            item.setInputType(spInputType.getSelectedItem().toString());
            item.setOptions(adapter.getList());
            vList.add(item);
        }
        if (isVerified(vList)) {
            String spreadSheetName = b.etName.getText().toString().trim();
            if (spreadSheetName.length() == 0) {
                basicUtils.showCustomAlert("Please enter a Spreadsheet Name!");
            } else {
                /* item.setAccountId(object.getString("AccountId"));
                item.setSpreadsheetId(object.getString("SpreadSheetId"));
                item.setTitle(object.getString("Title"));
                item.setDate(object.getString("Date"));
                item.setColumnName(object.getString("ColumnName"));
                item.setInputType(object.getString("InputType"));
                ArrayList<String> options = new ArrayList<>();
                JSONArray ops = object.getJSONArray("SpinnerOptions");
                for (int j = 0; j < ops.length(); j++) {
                    JSONObject ob = ops.getJSONObject(j);
                    options.add(ob.getString("Option"));
                }
                item.setSpinnerOptions(options);
                list.add(item);*/
                try {
                    JSONArray array = new JSONArray();
                    for (int i = 0; i < vList.size(); i++) {
                        JSONObject object = new JSONObject();
                        object.put("AccountId", ACCOUNT_ID);
                        object.put("Title", b.etName.getText().toString().trim());
                        object.put("Date", getTimeStamp());
                        object.put("ColumnName", vList.get(i).getColumnName());
                        object.put("InputType", vList.get(i).getInputType());
                        JSONArray ops = new JSONArray();
                        for (int j = 0; j < vList.get(i).getOptions().size(); j++) {
                            JSONObject ob = new JSONObject();
                            ob.put("Option", vList.get(i).getOptions().get(j));
                            ops.put(ob);
                        }
                        object.put("SpinnerOptions", ops);
                        array.put(object);
                    }
                    Log.i(TAG, "create: Json Array = " + array.toString());
                    volleyGetAccessTokenInfo(ACCOUNT_ID, true, array, isNew);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            basicUtils.showCustomAlert("Please fill all the fields!");
        }


    }

    public class VerifyItem {
        String columnName;
        String inputType;
        ArrayList<String> options;

        public String getColumnName() {
            return columnName;
        }

        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        public String getInputType() {
            return inputType;
        }

        public void setInputType(String inputType) {
            this.inputType = inputType;
        }

        public ArrayList<String> getOptions() {
            return options;
        }

        public void setOptions(ArrayList<String> options) {
            this.options = options;
        }

        @Override
        public String toString() {
            return "VerifyItem{" +
                    "columnName='" + columnName + '\'' +
                    ", inputType='" + inputType + '\'' +
                    ", options=" + options +
                    '}';
        }
    }

    private boolean isVerified(ArrayList<VerifyItem> list) {
        boolean isVerified = true;
        for (VerifyItem item : list) {
            if (item.getColumnName().trim().length() == 0) {
                isVerified = false;
                break;
            }
            if (item.getInputType().equals("Input Type *")) {
                isVerified = false;
                break;
            }
            if (item.getInputType().equals("Dropdown") && item.getOptions().size() == 0) {
                isVerified = false;
                break;
            }
        }
        return isVerified;
    }

    private String getTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        Calendar cal = Calendar.getInstance();
        return sdf.format(cal.getTime());
    }

    private boolean shouldResize(JSONArray array) {
        boolean resize = false;
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                String columnName = object.getString("ColumnName");
                if (columnName.length() > 10) {
                    resize = true;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resize;
    }

    private int getPosition(String inputType) {
        int pos = 0;
        for (int i = 0; i < inputTypes.size(); i++) {
            if (inputTypes.get(i).trim().equalsIgnoreCase(inputType.trim())) {
                pos = i;
            }
        }
        return pos;
    }

    private int getSpreadsheetPosition(String spreadSheetId) {
        int position = 0;
        for (int i = 0; i < HomeActivity.spreadsheetList.size(); i++) {
            if (HomeActivity.spreadsheetList.get(i).getId().trim().equals(spreadSheetId.trim())) {
                position = i;
                break;
            }
        }
        return position;
    }

    private void getNewAccessToken(String accountId, String refreshToken, JSONArray array, boolean isNew) {
        new Thread() {
            @Override
            public void run() {
                try {
                    GoogleTokenResponse tokenResponse =
                            new GoogleRefreshTokenRequest(
                                    new NetHttpTransport(),
                                    GsonFactory.getDefaultInstance(),
                                    refreshToken,
                                    Constants.WEB_CLIENT_ID,
                                    Constants.CLIENT_SECRET)
                                    .execute();

                    ACCESS_TOKEN = tokenResponse.getAccessToken();
                    long mExpiresInSeconds = tokenResponse.getExpiresInSeconds();
                    ACCESS_TOKEN_TYPE = tokenResponse.getTokenType();

                    Log.i(TAG, "volleyGetAccessTokenInfo: New Token Result = " +
                            "\n Access Token = " + ACCESS_TOKEN +
                            "\n Expires In = " + mExpiresInSeconds +
                            "\n Token Type = " + ACCESS_TOKEN_TYPE +
                            "\n Token Scope = " + tokenResponse.getScope());

                    db.storeNewAccessToken(accountId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);

                    GoogleCredential credential = db.getCredentials(accountId);
                    HttpTransport transport = new NetHttpTransport();
                    JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

                    mService = new Sheets
                            .Builder(transport, jsonFactory, credential)
                            .setApplicationName(Constants.APP_NAME)
                            .build();

                } catch (Exception e) {
                    Log.i(TAG, "exchangeCodeForToken: Results = " + e.toString());
                    Log.e(TAG, "Token exchange failed with " + e.getMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isNew) {
                            createNewSpreadsheet(array, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                        } else {
                            volleyEditSpreadSheet(SPREADSHEET_ID, sheetNames, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, array);
                        }
                    }
                });
            }
        }.start();
    }

    private void volleyGetAccessTokenInfo(String accountId, boolean showProgressBar, JSONArray array, boolean isNew) {

        ACCESS_TOKEN = db.getAccessToken(accountId);
        ACCESS_TOKEN_TYPE = db.getAccessTokenType(accountId);
        String refreshToken = db.getRefreshToken(accountId);

        if (showProgressBar)
            basicUtils.showSmallProgressDialog(true);
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetAccessTokenInfo");
        String url = ApiProcessing.UserInfo.API_VERIFY_ACCESS_TOKEN + ACCESS_TOKEN;
        Log.i(TAG, "volleyGetAccessTokenInfo: URL = " + url);
        details.setAPI_URL(url);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyGetAccessTokenInfo: Response = " + response.toString());
                        details.setResponse(response.toString());
                        try {
                            boolean isAccessTokenExpired = ApiProcessing.UserInfo.parseResponse(response);
                            if (isAccessTokenExpired) {
                                getNewAccessToken(accountId, refreshToken, array, isNew);
                            } else {
                                Log.i(TAG, "volleyGetAccessTokenInfo: Expires in = " + ApiProcessing.UserInfo.getRemainingTime(response));
                                GoogleCredential credential = db.getCredentials(accountId);
                                HttpTransport transport = new NetHttpTransport();
                                JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

                                mService = new Sheets
                                        .Builder(transport, jsonFactory, credential)
                                        .setApplicationName(getString(R.string.app_name))
                                        .build();

                                if (isNew) {
                                    createNewSpreadsheet(array, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                                } else {
                                    volleyEditSpreadSheet(SPREADSHEET_ID, sheetNames, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, array);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: JSONException = " + e.toString());
                            details.setException(e.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: Exception = " + e.toString());
                            details.setException(e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e(TAG, "volleyGetAccessTokenInfo: Error = " + error.toString());
                if (error instanceof ClientError) {
                    getNewAccessToken(accountId, refreshToken, array, isNew);
                } else {
                    basicUtils.showSmallProgressDialog(false);
                    basicUtils.showCustomAlert("Failed to verify access token!");
                }
                details.setErrorResponse(error.toString());
                if (Constants.SUPER_USER)
                    details.show();
            }
        });
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AccessTokenInfo");
    }

    private void volleyAddSpreadSheet(String spreadSheetId, String sheetName, int sheetId, String ACCESS_TOKEN, String TOKEN_TYPE, JSONArray array) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyAddSpreadSheet");

        String url = Constants.SHEETS_API + spreadSheetId + "/values/" + "'" + sheetName + "'" + "?valueInputOption=RAW";
        JSONObject object = ApiProcessing.Operations.getHeaderCreationObject(array, sheetName);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyAddSpreadSheet: URL = " + url);
        Log.i(TAG, "volleyAddSpreadSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.PUT,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyAddSpreadSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        basicUtils.showCustomAlert("Spreadsheet Created");
                        try {
                            Log.i(TAG, "onResponse: Stored Array = " + array.toString());
                            db.storeNewSpreadSheetInfo(array);
                            SSItem item = new SSItem();

                            item.setId(array.getJSONObject(0).getString("SpreadSheetId"));
                            item.setDate(array.getJSONObject(0).getString("Date"));

                            //Home List ====
                            SpreadSheetItem item1 = new SpreadSheetItem();

                            JSONObject object = array.getJSONObject(0);
                            String title = object.getString("Title");

                            item1.setId(spreadSheetId);
                            item1.setTitle(title);
                            item1.setDate(getTimeStamp());
                            HomeActivity.spreadsheetList.add(item1);
                            HomeActivity.sAdapter.notifyDataSetChanged();

                            db.storeNewSpreadSheetId(ACCOUNT_ID, item);
                            volleyFormatSheet(spreadSheetId, sheetId, shouldResize(array), ACCESS_TOKEN, ACCESS_TOKEN_TYPE);

                            Intent i = new Intent(context, EditSpreadSheetActivity.class);
                            i.putExtra("array", array.toString());
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyAddSpreadSheet: Error = " + error.toString());
                basicUtils.showCustomAlert("Spreadsheet Creation Failed");

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyAddSpreadSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AddUpdateSheet");
    }

    private void volleyEditSpreadSheet(String spreadSheetId, ArrayList<String> sheetNames, String ACCESS_TOKEN, String TOKEN_TYPE, JSONArray array) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyEditSpreadSheet");


        String url = Constants.SHEETS_API + spreadSheetId + "/values:batchUpdate";
        JSONObject object = ApiProcessing.Operations.getMultipleHeaderCreationObject(array, sheetNames);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyEditSpreadSheet: URL = " + url);
        Log.i(TAG, "volleyEditSpreadSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyEditSpreadSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        basicUtils.showCustomAlert("Spreadsheet Edited");
                        try {
                            Log.i(TAG, "onResponse: Stored Array = " + array.toString());

                            JSONArray newArray = new JSONArray();
                            for (int i = 0; i < array.length(); i++) {
                                try {
                                    JSONObject ob = array.getJSONObject(i);
                                    ob.put("SpreadSheetId", spreadSheetId);
                                    ob.remove("Title");
                                    ob.put("Title", b.etName.getText().toString());
                                    newArray.put(ob);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.i(TAG, "onResponse: New Array = " + newArray.toString());
                            Log.i(TAG, "onResponse: Old Array = " + array.toString());
                            db.deleteSpreadSheetInfo(ACCOUNT_ID, spreadSheetId);
                            db.storeNewSpreadSheetInfo(array);
                            SpreadSheetItem item1 = new SpreadSheetItem();

                            JSONObject object = array.getJSONObject(0);
                            String title = object.getString("Title");

                            item1.setId(spreadSheetId);
                            item1.setTitle(title);
                            item1.setDate(getTimeStamp());
                            HomeActivity.spreadsheetList.remove(getSpreadsheetPosition(spreadSheetId));
                            HomeActivity.spreadsheetList.add(item1);
                            HomeActivity.sAdapter.notifyDataSetChanged();

                            EditSpreadSheetActivity.array = array;
                            EditSpreadSheetActivity.tvSpreadSheetName.setText(title);
                            volleyRenameSpreadSheet(spreadSheetId, title, ACCESS_TOKEN, TOKEN_TYPE);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                            basicUtils.showCustomAlert("Exception = " + e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyEditSpreadSheet: Error = " + error.toString());
                basicUtils.showCustomAlert("Spreadsheet Edit Failed");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyEditSpreadSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AddUpdateSheet");
    }

    private void volleyRenameSpreadSheet(String spreadSheetId, String spreadSheetName, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyRenameSpreadSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.EditSpreadsheet.createRenameSpreadsheetObject(spreadSheetName);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyRenameSpreadSheet: URL = " + url);
        Log.i(TAG, "volleyRenameSpreadSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyRenameSpreadSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyRenameSpreadSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyRenameSpreadSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AddUpdateSheet");
    }

    private void volleyFormatSheet(String spreadSheetId, int sheetId, boolean shouldResize, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyFormatSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.Operations.getFormatObject(sheetId, shouldResize);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyFormatSheet: URL = " + url);
        Log.i(TAG, "volleyFormatSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyFormatSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyFormatSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyFormatSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "FormatSheet");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAddColumn:
                addColumn(null, null, null);
                break;
            case R.id.tvCreate:
                create(!hasCredentials);
                break;
            case R.id.ivBack:
                finish();
                break;
        }
    }
}