package com.example.sheet.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.sheet.Item.BackendItem;
import com.example.sheet.Item.PropertyItem;
import com.example.sheet.Item.SSItem;
import com.example.sheet.Item.UserItem;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";

    Context context;
    BasicUtils basicUtils;

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "EasySheet.db";

    private static final String TABLE_1 = "userInfo";
    private static final String TABLE_2 = "spreadSheetInfo";

    public static final String CREATE_TABLE_USER_INFO = "CREATE TABLE " + TABLE_1 +
            "(" + "AccountId" + " TEXT," +
            "DisplayName" + " TEXT," +
            "FamilyName" + " TEXT," +
            "GivenName" + " TEXT," +
            "EmailAddress" + " TEXT," +
            "ProfilePicURL" + " TEXT," +
            "IdToken" + " TEXT," +
            "ServerAuthCode" + " TEXT," +
            "RefreshToken" + " TEXT," +
            "AccessToken" + " TEXT," +
            "AccessTokenType" + " TEXT," +
            "SubscriptionStatus" + " TEXT," +
            "SpreadSheetIds" + " TEXT" + ")";


    public static final String CREATE_TABLE_SPREADSHEET_INFO = "CREATE TABLE " + TABLE_2 +
            "(" + "AccountId" + " TEXT," +
            "SpreadSheetId" + " TEXT," +
            "Title" + " TEXT," +
            "Date" + " TEXT," +
            "ColumnName" + " TEXT," +
            "InputType" + " TEXT," +
            "SpinnerOptions" + " TEXT" + ")";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        basicUtils = new BasicUtils(context);
    }

    SharedPreferences sheetDataDatabase;
    SharedPreferences.Editor editor;
    public static final String SDATABASE = "SharedDatabase.db";

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_USER_INFO);
            db.execSQL(CREATE_TABLE_SPREADSHEET_INFO);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onCreate: Exception = " + e.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_1);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_2);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onUpgrade: Exception = " + e.toString());
        }
    }

    //TODO Insert Operations =======================================================================

    public boolean insertUserInfo(ArrayList<UserItem> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;

        db.delete(TABLE_1, null, null);

        for (int i = 0; i < list.size(); i++) {
            UserItem item = list.get(i);
            ContentValues values = new ContentValues();

            values.put("AccountId", item.getAccountId());
            values.put("DisplayName", item.getDisplayName());
            values.put("FamilyName", item.getFamilyName());
            values.put("GivenName", item.getGivenName());
            values.put("EmailAddress", item.getEmailAddress());
            values.put("ProfilePicURL", item.getProfilePicUrl());
            values.put("IdToken", item.getIdToken());
            values.put("ServerAuthCode", item.getServerAuthCode());
            values.put("RefreshToken", item.getRefreshToken());
            values.put("AccessToken", item.getAccessToken());
            values.put("AccessTokenType", item.getAccessTokenType());
            values.put("SubscriptionStatus", item.getSubscriptionStatus());
            try {
                JSONArray array = new JSONArray();
                for (int j = 0; j < item.getSpreadSheetIds().size(); j++) {
                    JSONObject object = new JSONObject();
                    object.put("ID", item.getSpreadSheetIds().get(j).getId());
                    object.put("Date", item.getSpreadSheetIds().get(j).getDate());
                    array.put(object);
                }
                Log.i(TAG, "insertUserInfo: SpreadSheetIdArray = " + array.toString());
                values.put("SpreadSheetIds", array.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            result = db.insert(TABLE_1, null, values);
        }

        db.close();

        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertSpreadSheetInfo(ArrayList<PropertyItem> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = -1;
        db.delete(TABLE_2, null, null);

        for (int i = 0; i < list.size(); i++) {
            PropertyItem item = list.get(i);
            ContentValues values = new ContentValues();

            values.put("AccountId", item.getAccountId());
            values.put("SpreadSheetId", item.getSpreadsheetId());
            values.put("Title", item.getTitle());
            values.put("Date", item.getDate());
            values.put("ColumnName", item.getColumnName());
            values.put("InputType", item.getInputType());
            ArrayList<String> options = new ArrayList<>();
            try {
                options.addAll(item.getSpinnerOptions());
                JSONArray array = new JSONArray();
                for (int j = 0; j < options.size(); j++) {
                    JSONObject object = new JSONObject();
                    object.put("ITEM", options.get(j));
                    array.put(object);
                }
                Log.i(TAG, "insertSpreadSheetInfo: SpinnerItems = " + array.toString());
                values.put("SpinnerOptions", array.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            result = db.insert(TABLE_2, null, values);
        }

        db.close();

        if (result == -1)
            return false;
        else
            return true;

    }

    public void storeNewSpreadSheetInfo(JSONArray array) {
        ArrayList<PropertyItem> list = getAllSpreadSheetInfo();
        Log.i(TAG, "storeNewSpreadSheetInfo: Received Array = " + array.toString());
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                PropertyItem item = new PropertyItem();
                item.setAccountId(object.getString("AccountId"));
                item.setSpreadsheetId(object.getString("SpreadSheetId"));
                item.setTitle(object.getString("Title"));
                item.setDate(object.getString("Date"));
                item.setColumnName(object.getString("ColumnName"));
                item.setInputType(object.getString("InputType"));
                ArrayList<String> options = new ArrayList<>();
                JSONArray ops = object.getJSONArray("SpinnerOptions");
                for (int j = 0; j < ops.length(); j++) {
                    JSONObject ob = ops.getJSONObject(j);
                    options.add(ob.getString("Option"));
                }
                item.setSpinnerOptions(options);
                list.add(item);
            }
            insertSpreadSheetInfo(list);
            Log.i(TAG, "storeNewSpreadSheetInfo: Inserted Array = " + list.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            basicUtils.showCustomAlert("Error");
        }
    }

    public void storeNewSpreadSheetInfos(ArrayList<PropertyItem> list) {
        ArrayList<PropertyItem> lists = getAllSpreadSheetInfo();
        for (int i = 0; i < list.size(); i++) {
            lists.add(list.get(i));
        }
        insertSpreadSheetInfo(lists);
    }

    public void storeNewUser(UserItem item) {
        ArrayList<UserItem> list = getAllUserInfo();
        list.add(item);
        insertUserInfo(list);
    }

    public void storeNewSpreadSheetId(String accountId, SSItem item) {
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                ArrayList<SSItem> sheetIds = list.get(i).getSpreadSheetIds();
                sheetIds.add(item);
                list.get(i).setSpreadSheetIds(sheetIds);
                insertUserInfo(list);
                break;
            }
        }
    }

    public void storeNewAccessToken(String accountId, String accessToken, String accessTokenType) {
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                list.get(i).setAccessToken(accessToken);
                list.get(i).setAccessTokenType(accessTokenType);
                insertUserInfo(list);
                break;
            }
        }
    }

    public void setSubscriptionStatus(String accountId, boolean isSubscribed) {
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                list.get(i).setSubscriptionStatus(isSubscribed ? "true" : "false");
                insertUserInfo(list);
                break;
            }
        }
    }

    public void setBackendData(BackendItem item) {
        setSubscriptionStatus(item.getAccountId(), item.isSubscriptionStatus());
        Gson gson = new Gson();
        Type myType = new TypeToken<ArrayList<PropertyItem>>() {
        }.getType();
        ArrayList<PropertyItem> obj = gson.fromJson(item.getJSONData(), myType);
        Log.i(TAG, "setBackendData: PropertyList = " + obj.toString());
        storeNewSpreadSheetInfos(obj);
    }

    public void setUserSpreadsheetIds(String accountId, ArrayList<SSItem> spreadSheetIds) {
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equalsIgnoreCase(accountId.trim())) {
                list.get(i).setSpreadSheetIds(spreadSheetIds);
                insertUserInfo(list);
                break;
            }
        }
    }

    //TODO Fetch operations ========================================================================

    public ArrayList<BackendItem> getBackendData() {
        ArrayList<BackendItem> list = new ArrayList<>();
        int size = getAllSpreadSheetInfo().size();
        for (int i = 0; i < size; i++) {
            PropertyItem pItem = getAllSpreadSheetInfo().get(i);
            BackendItem item = new BackendItem();
            item.setAccountId(pItem.getAccountId());
            item.setIdToken(getIdToken(pItem.getAccountId()));
            item.setSubscriptionStatus(getSubscriptionStatus(pItem.getAccountId()).equals("true"));
            Gson gson = new Gson();
            String json = gson.toJson(getAUsersAllSpreadSheetInfo(pItem.getAccountId()));
            item.setJSONData(json);

            list.add(item);
        }

        Log.i(TAG, "getBackendData: String = " + list.toString());
        return list;
    }

    private ArrayList<PropertyItem> getAUsersAllSpreadSheetInfo(String accountId) {
        ArrayList<PropertyItem> list = new ArrayList<>();
        int size = getAllSpreadSheetInfo().size();
        for (int i = 0; i < size; i++) {
            if (getAllSpreadSheetInfo().get(i).getAccountId().trim().equals(accountId.trim())) {
                list.add(getAllSpreadSheetInfo().get(i));
            }
        }
        return list;
    }

    public GoogleCredential getCredentials(String accountId) {
        ArrayList<UserItem> list = getAllUserInfo();
        GoogleCredential credential = new GoogleCredential
                .Builder()
                .setTransport(new NetHttpTransport())
                .setJsonFactory(GsonFactory.getDefaultInstance())
                .setClientSecrets(Constants.WEB_CLIENT_ID, Constants.CLIENT_SECRET)
                .build();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                credential.setAccessToken(list.get(i).getAccessToken());
                credential.setRefreshToken(list.get(i).getRefreshToken());
                break;
            }
        }
        return credential;
    }

    public ArrayList<UserItem> getAllUserInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_1;
        Cursor c = db.rawQuery(selectQuery, null);

        ArrayList<UserItem> userList = new ArrayList<>();

        c.moveToFirst();
        while (c != null && !(c.isClosed()) && !c.isAfterLast()) {
            UserItem item = new UserItem();

            item.setAccountId(c.getString(c.getColumnIndex("AccountId")));
            item.setDisplayName(c.getString(c.getColumnIndex("DisplayName")));
            item.setFamilyName(c.getString(c.getColumnIndex("FamilyName")));
            item.setGivenName(c.getString(c.getColumnIndex("GivenName")));
            item.setEmailAddress(c.getString(c.getColumnIndex("EmailAddress")));
            item.setProfilePicUrl(c.getString(c.getColumnIndex("ProfilePicURL")));
            item.setIdToken(c.getString(c.getColumnIndex("IdToken")));
            item.setServerAuthCode(c.getString(c.getColumnIndex("ServerAuthCode")));
            item.setRefreshToken(c.getString(c.getColumnIndex("RefreshToken")));
            item.setAccessToken(c.getString(c.getColumnIndex("AccessToken")));
            item.setAccessTokenType(c.getString(c.getColumnIndex("AccessTokenType")));
            item.setSubscriptionStatus(c.getString(c.getColumnIndex("SubscriptionStatus")));
            ArrayList<SSItem> spreadSheetList = new ArrayList<>();
            try {
                JSONArray array = new JSONArray(c.getString(c.getColumnIndex("SpreadSheetIds")));
                for (int i = 0; i < array.length(); i++) {
                    String id = "";
                    String date = "";
                    JSONObject object = array.getJSONObject(i);
                    id = object.getString("ID");
                    date = object.getString("Date");
                    SSItem it = new SSItem();
                    it.setId(id);
                    it.setDate(date);
                    spreadSheetList.add(it);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            item.setSpreadSheetIds(spreadSheetList);

            userList.add(item);
            c.moveToNext();
        }
        c.close();
        db.close();
        return userList;
    }

    public ArrayList<PropertyItem> getAllSpreadSheetInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_2;
        Cursor c = db.rawQuery(selectQuery, null);

        ArrayList<PropertyItem> spreadSheetList = new ArrayList<>();

        c.moveToFirst();
        while (c != null && !(c.isClosed()) && !c.isAfterLast()) {
            PropertyItem item = new PropertyItem();

            item.setAccountId(c.getString(c.getColumnIndex("AccountId")));
            item.setSpreadsheetId(c.getString(c.getColumnIndex("SpreadSheetId")));
            item.setTitle(c.getString(c.getColumnIndex("Title")));
            item.setDate(c.getString(c.getColumnIndex("Date")));
            item.setColumnName(c.getString(c.getColumnIndex("ColumnName")));
            item.setInputType(c.getString(c.getColumnIndex("InputType")));
            ArrayList<String> spinnerOptions = new ArrayList<>();
            try {
                JSONArray array = new JSONArray(c.getString(c.getColumnIndex("SpinnerOptions")));
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    spinnerOptions.add(object.getString("ITEM"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            item.setSpinnerOptions(spinnerOptions);

            spreadSheetList.add(item);
            c.moveToNext();
        }
        c.close();
        db.close();
        Log.i(TAG, "getAllSpreadSheetInfo: List = " + spreadSheetList.toString());
        return spreadSheetList;
    }

    public JSONArray getSpreadSheetInfo(String accountId, String spreadSheetId) {
        JSONArray array = new JSONArray();
        ArrayList<PropertyItem> list = getAllSpreadSheetInfo();
        Log.i(TAG, "getSpreadSheetInfo: Test = " + list.toString());

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId) && list.get(i).getSpreadsheetId().trim().equals(spreadSheetId.trim())) {
                PropertyItem item = list.get(i);
                JSONObject object = new JSONObject();
                try {
                    object.put("AccountId", item.getAccountId());
                    object.put("SpreadSheetId", item.getSpreadsheetId());
                    object.put("Title", item.getTitle());
                    object.put("Date", item.getDate());
                    object.put("ColumnName", item.getColumnName());
                    object.put("InputType", item.getInputType());
                    JSONArray options = new JSONArray();
                    for (String op : item.getSpinnerOptions()) {
                        JSONObject ob = new JSONObject();
                        ob.put("Option", op);
                        options.put(ob);
                    }
                    object.put("SpinnerOptions", options);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                array.put(object);
                Log.i(TAG, "getSpreadSheetProperties: Array = " + array.toString());
            } else {
                Log.i(TAG, "getSpreadSheetProperties: Array = " + "Spreadsheet Not Found");
            }
        }
        Log.i(TAG, "getSpreadSheetInfo: Output Array = " + array.toString());
        return array;
    }

    public UserItem getUser(String accountId) {
        UserItem user = new UserItem();
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                user = list.get(i);
            }
        }
        return user;
    }

    public ArrayList<SSItem> getSpreadSheetIds(String accountId) {
        ArrayList<SSItem> sheetIds = new ArrayList<>();
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                sheetIds.addAll(list.get(i).getSpreadSheetIds());
                break;
            }
        }
        return sheetIds;
    }

    public String getIdToken(String accountId) {
        String value = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                value = list.get(i).getIdToken();
                break;
            }
        }
        return value;
    }

    public String getAccessToken(String accountId) {
        String accessToken = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                accessToken = list.get(i).getAccessToken();
                break;
            }
        }
        return accessToken;
    }

    public String getAccessTokenType(String accountId) {
        String accessTokenType = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                accessTokenType = list.get(i).getAccessTokenType();
                break;
            }
        }
        return accessTokenType;
    }

    public String getRefreshToken(String accountId) {
        String refreshToken = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                refreshToken = list.get(i).getRefreshToken();
                break;
            }
        }
        return refreshToken;
    }

    public String getEmailAddress(String accountId) {
        String value = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                value = list.get(i).getEmailAddress();
                break;
            }
        }
        return value;
    }

    public String getDisplayName(String accountId) {
        String value = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                value = list.get(i).getDisplayName();
                break;
            }
        }
        return value;
    }

    public String getProfilePicUrl(String accountId) {
        String value = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                value = list.get(i).getProfilePicUrl();
                break;
            }
        }
        return value;
    }

    public String getSubscriptionStatus(String accountId) {
        String value = "";
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                value = list.get(i).getSubscriptionStatus();
                break;
            }
        }
        return value;
    }

    public boolean isUserAlreadyLoggedIn(String accountId) {
        boolean exists = false;
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                exists = true;
            }
        }
        return exists;
    }

    //TODO Remove Operations =======================================================================

    public void deleteSpreadSheetInfo(String accountId, String spreadSheetId) {
        ArrayList<PropertyItem> list = getAllSpreadSheetInfo();
        Log.i(TAG, "deleteSpreadSheetInfo: Received = " + list.toString());
        int size = list.size();
        for (int i = size - 1; i >= 0; i--) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim()) && list.get(i).getSpreadsheetId().trim().equals(spreadSheetId.trim())) {
                list.remove(i);
            }
        }
        Log.i(TAG, "deleteSpreadSheetInfo: Output = " + list.toString());
        insertSpreadSheetInfo(list);
    }

    public void deleteSpreadSheetId(String accountId, String spreadSheetId) {
        ArrayList<UserItem> list = getAllUserInfo();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAccountId().trim().equals(accountId.trim())) {
                ArrayList<SSItem> sheetIds = list.get(i).getSpreadSheetIds();
                if (isExists(spreadSheetId, sheetIds)) {
                    sheetIds.remove(getId(spreadSheetId, sheetIds));
                } else
                    basicUtils.showCustomAlert("SpreadSheet not found!");
                list.get(i).setSpreadSheetIds(sheetIds);
                insertUserInfo(list);
                break;
            }
        }
    }

    public boolean isExists(String spreadSheetId, ArrayList<SSItem> list) {
        boolean isExists = false;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().trim().equals(spreadSheetId.trim())) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }

    public SSItem getId(String spreadSheetId, ArrayList<SSItem> list) {
        SSItem item = new SSItem();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().trim().equals(spreadSheetId.trim())) {
                item = list.get(i);
                break;
            }
        }
        return item;
    }

    //TODO Delete Operations =======================================================================

    public void clearAllUserData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_1);
        db.execSQL("delete from " + TABLE_2);
    }

    //TODO Shared Preference Database Operations ===================================================

    public String getCurrentlyActiveAccountId() {
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        String accountId = sheetDataDatabase.getString("AccountId", "");
        return accountId;
    }

    public void setCurrentlyActiveAccountId(String accountId) {
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        editor = sheetDataDatabase.edit();
        editor.putString("AccountId", accountId);
        editor.apply();
    }

    public String getRemainingUploadTime() {
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        String uploadTime = sheetDataDatabase.getString("UploadTime", "");
        if (uploadTime.equals(""))
            return "Not Set Yet";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        String DURATION = "";
        try {
            String s_years = " years ";
            String s_months = " months ";
            String s_days = " days ";
            String s_hours = " hours ";
            String s_minutes = " minutes ";
            String s_seconds = " seconds ";

            Date currentDate = cal.getTime();
            Date uploadDate = sdf.parse(uploadTime);
            long diff = uploadDate.getTime() - currentDate.getTime();

            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            long months = days / 30;
            long years = months / 12;

            DURATION = years + s_years + months + s_months + days + s_days + hours + s_hours + minutes + s_minutes + seconds + s_seconds;

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "Remaining Upload Time = " + e.toString());
        }
        return DURATION;
    }

    public boolean getUploadTime() {
        boolean upload = false;
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        String uploadTime = sheetDataDatabase.getString("UploadTime", "");
        if (uploadTime.equals(""))
            return false;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        try {
            Date currentDate = cal.getTime();
            Date uploadDate = sdf.parse(uploadTime);

            if (currentDate.compareTo(uploadDate) >= 0) {
                upload = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return upload;
    }

    public void setUploadTime(String time) {
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        editor = sheetDataDatabase.edit();
        editor.putString("UploadTime", time);
        editor.apply();
    }

    public void deleteCurrentlyActiveAccountId() {
        sheetDataDatabase = context.getSharedPreferences(SDATABASE, Context.MODE_PRIVATE);
        editor = sheetDataDatabase.edit();
        editor.putString("AccountId", "");
        editor.apply();
    }
}
