package com.example.sheet.Utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.sheet.databinding.ItemSharedUserBinding;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Templates {
    private static final String TAG = "Templates";
    Context context;
    BasicUtils basicUtils;
    
    //RecyclerView Template ============
    public class BookingImageAdapter extends RecyclerView.Adapter<BookingImageAdapter.MyViewHolder> {
        ArrayList<String> list;
        Context context;

        public BookingImageAdapter(ArrayList<String> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @NonNull
        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            ItemSharedUserBinding binding = ItemSharedUserBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
            MyViewHolder holder = new MyViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            ItemSharedUserBinding b;
            public MyViewHolder(@NonNull @NotNull ItemSharedUserBinding itemView) {
                super(itemView.getRoot());
                this.b = itemView;
            }
        }
    }

    //Volley Template ==================
    private void volleyGetBookingImageData(String sDate, String eDate, boolean showProgressBar) {
        final API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetBookingImageData");
        if (showProgressBar)
            basicUtils.showSmallProgressDialog(true);

        String url = "";
        details.setAPI_URL(url);
        Log.i(TAG, "volleyGetBookingImageData: URL = " + url);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        basicUtils.showSmallProgressDialog(false);
                        details.setResponse(response.toString());
                        Log.i(TAG, "volleyGetBookingImageData: Response = " + response.toString());
                        try {
                            JSONObject o = new JSONObject("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.i(TAG, "volleyGetBookingImageData: Exception = " + e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showSmallProgressDialog(false);
                basicUtils.showCustomAlert("Timed Out!");
                Log.i(TAG, "volleyGetBookingImageData: Error = " + error.toString());
            }
        });
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "GetBookingImage");
    }

    //Search Edittext =====================

}
