package com.example.sheet.Item;

import java.util.ArrayList;

public class RowFieldItem {
    int type;
    String title;
    String body;
    ArrayList<String> options;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "RowFieldItem{" +
                "type=" + type +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", options=" + options +
                '}';
    }
}
