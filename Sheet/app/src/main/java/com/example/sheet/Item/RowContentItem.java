package com.example.sheet.Item;

import java.util.ArrayList;

public class RowContentItem {
    String title;
    ArrayList<RowValueItem> valueList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<RowValueItem> getValueList() {
        return valueList;
    }

    public void setValueList(ArrayList<RowValueItem> valueList) {
        this.valueList = valueList;
    }

    @Override
    public String toString() {
        return "RowContentItem{" +
                "title='" + title + '\'' +
                ", valueList=" + valueList +
                '}';
    }
}
