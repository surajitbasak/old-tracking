package com.example.sheet.Item;

import java.io.Serializable;

public class RowValueItem implements Serializable {
    String header;
    String value;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "RowValueItem{" +
                "header='" + header + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
