package com.example.sheet.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.sheet.Item.RowContentItem;
import com.example.sheet.Item.RowValueItem;
import com.example.sheet.Item.SSItem;
import com.example.sheet.Item.SheetItem;
import com.example.sheet.Item.SpreadSheetItem;
import com.example.sheet.R;
import com.example.sheet.Utils.API_Details;
import com.example.sheet.Utils.ApiProcessing;
import com.example.sheet.Utils.BasicUtils;
import com.example.sheet.Utils.Constants;
import com.example.sheet.Utils.DatabaseHandler;
import com.example.sheet.Utils.MySingleton;
import com.example.sheet.Utils.PostStringRequest;
import com.example.sheet.databinding.ActivityEditSpreadSheetBinding;
import com.example.sheet.databinding.DialogAddSheetBinding;
import com.example.sheet.databinding.ItemRowBinding;
import com.example.sheet.databinding.ItemRowValueBinding;
import com.example.sheet.databinding.ItemSheetBinding;
import com.google.android.gms.vision.text.Line;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditSpreadSheetActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "EditSpreadSheetActivity";
    Context context;
    BasicUtils basicUtils;
    DatabaseHandler db;
    ActivityEditSpreadSheetBinding b;
    public static JSONArray array;
    int spreadSheetPosition = -1;

    SheetAdapter adapter;
    public static RowContentAdapter rowAdapter;
    LinearLayoutManager manager, rowManager;
    ArrayList<SheetItem> sheetList;
    public static ArrayList<RowContentItem> rowContentList;

    int FLAG = -1;
    int GET_SHEET_INFO = 0;
    int CREATE_SHEET = 1;
    int RENAME_SHEET = 2;
    int DELETE_SHEET = 3;
    int DELETE_SPREAD_SHEET = 4;
    int GET_SHEET_CONTENTS = 5;
    int DELETE_ROW = 6;
    int EDIT_SPREAD_SHEET = 7;

    String ACCESS_TOKEN, ACCESS_TOKEN_TYPE;

    String OPERATION_SHEET_ID;
    String SPREADSHEET_ID;
    String ACCOUNT_ID;
    String SHEET_NAME;
    String SELECTED_SHEET;
    int SHEET_ID;
    int POSITION;
    int ROW_POSITION;
    int START_INDEX;
    int END_INDEX;
    TextView TV_NOTE;
    LinearLayout PB_SHEET;
    CardView CV_SHEET;

    Dialog DIALOG;
    public static TextView tvSpreadSheetName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BasicUtils.setStatusBarColor(this, R.color.colorAmber);
        b = ActivityEditSpreadSheetBinding.inflate(getLayoutInflater());
        setContentView(b.getRoot());
        init();
    }

    private void init() {
        context = EditSpreadSheetActivity.this;
        basicUtils = new BasicUtils(context);
        db = new DatabaseHandler(context);
        manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rowManager = new LinearLayoutManager(context);
        tvSpreadSheetName = findViewById(R.id.tvSpreadsheetName);
        sheetList = new ArrayList<>();
        rowContentList = new ArrayList<>();
        adapter = new SheetAdapter(sheetList);
        array = new JSONArray();
        String s = getIntent().getStringExtra("array");
        try {
            array = new JSONArray(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setBehaviour();
    }

    private void setBehaviour() {
        b.ivMenuSpreadsheet.setOnClickListener(this);
        b.ivBack.setOnClickListener(this);
        b.cvAddSheet.setOnClickListener(this);
        b.ivClearSearch.setOnClickListener(this);
        b.cvAddRow.setOnClickListener(this);
        b.rvSheets.setLayoutManager(manager);
        b.rvSheets.setAdapter(adapter);
        b.rvRows.setLayoutManager(rowManager);
        Log.i(TAG, "setBehaviour: Array = " + array.toString());
        try {
            SPREADSHEET_ID = array.getJSONObject(0).getString("SpreadSheetId");
            tvSpreadSheetName.setText(array.getJSONObject(0).getString("Title"));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "setBehaviour: Array Exception = " + array.toString());
        }
        ACCOUNT_ID = db.getCurrentlyActiveAccountId();

        b.etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                basicUtils.hideKeyboard();
                return true;
            }
        });

        b.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.trim().length() == 0) {
                    b.ivClearSearch.setVisibility(View.GONE);
                } else {
                    b.ivClearSearch.setVisibility(View.VISIBLE);
                }
                filterList(text);
            }
        });

        b.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SHEET_NAME = SELECTED_SHEET;
                PB_SHEET = null;
                TV_NOTE = b.tvNoRow;
                volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, false, GET_SHEET_CONTENTS);
            }
        });

        volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, GET_SHEET_INFO);
    }

    private int getPosition(String spreadsheetId) {
        int pos = -1;
        for (int i = 0; i < HomeActivity.spreadsheetList.size(); i++) {
            if (HomeActivity.spreadsheetList.get(i).getId().equals(spreadsheetId)) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    private void filterList(String filterText) {
        String text = filterText.toLowerCase().trim();
        ArrayList<RowContentItem> filterList = new ArrayList<>();
        if (text.length() > 0) {
            for (RowContentItem item : rowContentList) {
                if (item.getTitle().toLowerCase().contains(text)) {
                    filterList.add(item);
                }
            }
        } else {
            filterList.addAll(rowContentList);
        }
        b.tvCount.setText(String.valueOf(filterList.size()));
        rowAdapter = new RowContentAdapter(filterList);
        b.rvRows.setAdapter(rowAdapter);
    }

    private boolean shouldResize(JSONArray array) {
        boolean resize = false;
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                String columnName = object.getString("ColumnName");
                if (columnName.length() > 10) {
                    resize = true;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resize;
    }

    public class SheetAdapter extends RecyclerView.Adapter<SheetAdapter.MyViewHolder> {
        private static final String TAG = "SheetAdapter";
        ArrayList<SheetItem> list;

        public SheetAdapter(ArrayList<SheetItem> list) {
            this.list = list;
        }

        @NonNull
        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            ItemSheetBinding binding = ItemSheetBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            MyViewHolder holder = new MyViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {
            SheetItem item = list.get(position);
            h.b.tvSheetName.setText(item.getSheetName());

            if (position == 0) {
                CV_SHEET = h.b.cvRoot;
            }

            h.b.ivMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu pop = new PopupMenu(context, h.b.ivMenu);
                    pop.getMenuInflater().inflate(R.menu.menu_sheet, pop.getMenu());
                    pop.show();

                    pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item1) {
                            switch (item1.getItemId()) {
                                case R.id.menu_rename:
                                    Dialog dialog = new Dialog(context);
                                    DialogAddSheetBinding bi = DialogAddSheetBinding.inflate(getLayoutInflater());
                                    basicUtils.setDialogAttributes(dialog, true, true, bi.getRoot(), 16, 17);
                                    bi.tvTitle.setText("Rename Sheet");
                                    bi.tvAdd.setText("Update");
                                    bi.etSheetName.setText(item.getSheetName());

                                    bi.tvAdd.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String name = bi.etSheetName.getText().toString().trim();
                                            if (name.length() == 0) {
                                                bi.etSheetName.setError("Please Enter a name");
                                                bi.etSheetName.requestFocus();
                                                return;
                                            }
                                            if (isSheetExists(name)) {
                                                basicUtils.showLongCustomAlert(name + " already exist to this Spreadsheet!");
                                                return;
                                            }
                                            SHEET_NAME = name;
                                            SHEET_ID = item.getSheetId();
                                            POSITION = position;
                                            DIALOG = dialog;
                                            volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, RENAME_SHEET);
                                        }
                                    });

                                    bi.ivClose.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();
                                    break;
                                case R.id.menu_delete:
                                    if (list.size() == 1) {
                                        basicUtils.showLongCustomAlert("You can't delete all the sheets from a Spreadsheet!");
                                        return true;
                                    }
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Are you sure you want to delete " + item.getSheetName() + "?");
                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            SHEET_ID = item.getSheetId();
                                            POSITION = position;
                                            volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, DELETE_SHEET);
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.show();
                                    break;
                            }
                            return true;
                        }
                    });
                }
            });

            if (item.isSelected()) {
                h.b.cvRoot.setBackgroundColor(getResources().getColor(R.color.colorAmber));
                h.b.tvSheetName.setTextColor(getResources().getColor(R.color.white));
                h.b.ivMenu.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN);
                h.b.cvRoot.setEnabled(false);
                SHEET_ID = item.getSheetId();
            } else {
                h.b.cvRoot.setBackgroundColor(getResources().getColor(R.color.white));
                h.b.tvSheetName.setTextColor(getResources().getColor(R.color.black));
                h.b.ivMenu.setColorFilter(ContextCompat.getColor(context, R.color.black), PorterDuff.Mode.SRC_IN);
                h.b.cvRoot.setEnabled(true);
            }

            h.b.cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(position);
                    SHEET_ID = item.getSheetId();
                    SHEET_NAME = item.getSheetName();
                    PB_SHEET = b.pbSheet;
                    TV_NOTE = b.tvNoRow;
                    volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, false, GET_SHEET_CONTENTS);
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ItemSheetBinding b;

            public MyViewHolder(@NonNull @NotNull ItemSheetBinding itemView) {
                super(itemView.getRoot());
                this.b = itemView;
            }
        }

        public void setSelection(int position) {
            for (int i = 0; i < list.size(); i++) {
                if (i == position) {
                    list.get(i).setSelected(true);
                } else {
                    list.get(i).setSelected(false);
                }
            }
            notifyDataSetChanged();
        }

        public boolean isSheetExists(String name) {
            boolean isExists = false;
            for (SheetItem item : list) {
                if (item.getSheetName().equals(name)) {
                    isExists = true;
                    break;
                }
            }
            return isExists;
        }
    }

    public class RowContentAdapter extends RecyclerView.Adapter<RowContentAdapter.MyViewHolder> {
        private static final String TAG = "SheetAdapter";

        ArrayList<RowContentItem> list;

        public RowContentAdapter(ArrayList<RowContentItem> list) {
            this.list = list;

        }

        @NonNull
        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            ItemRowBinding binding = ItemRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            MyViewHolder holder = new MyViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {
            RowContentItem item = list.get(position);
            h.b.tvFirstRowName.setText(item.getTitle());
            ArrayList<RowValueItem> valueList = item.getValueList();
            RowValueAdapter adapter = new RowValueAdapter(valueList);
            h.b.rvRowContents.setLayoutManager(new LinearLayoutManager(context));
            h.b.rvRowContents.setAdapter(adapter);

            h.b.cvRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (h.b.llRowContents.getVisibility() == View.VISIBLE) {
                        h.b.llRowContents.setVisibility(View.GONE);
                        h.b.ivDown.animate().rotation(0).start();
                    } else {
                        h.b.llRowContents.setVisibility(View.VISIBLE);
                        h.b.ivDown.animate().rotation(180).start();
                    }
                }
            });

            h.b.ivMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu pop = new PopupMenu(context, h.b.ivMenu);
                    pop.getMenuInflater().inflate(R.menu.menu_row, pop.getMenu());
                    pop.show();

                    pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem ite) {
                            switch (ite.getItemId()) {
                                case R.id.menu_edit_row:
                                    Intent it = new Intent(context, AddRowInfoActivity.class);

                                    it.putExtra("edit_position", position);
                                    it.putExtra("sheetName", SELECTED_SHEET);
                                    it.putExtra("sheetId", SHEET_ID);
                                    it.putExtra("isEdit", true);
                                    it.putExtra("array", array.toString());
                                    it.putExtra("valueList", valueList);

                                    startActivity(it);
                                    break;
                                case R.id.menu_delete_row:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Do you want to delete this row?");
                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            START_INDEX = position + 1;
                                            END_INDEX = START_INDEX + 1;
                                            ROW_POSITION = position;
                                            volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, DELETE_ROW);
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    builder.show();
                                    break;
                            }
                            return true;
                        }
                    });
                }
            });

            if (position == list.size() - 1) {
                h.b.space.setVisibility(View.VISIBLE);
            } else {
                h.b.space.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            b.tvCount.setText(String.valueOf(list.size()));
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ItemRowBinding b;

            public MyViewHolder(@NonNull @NotNull ItemRowBinding itemView) {
                super(itemView.getRoot());
                this.b = itemView;
            }
        }
    }

    public class RowValueAdapter extends RecyclerView.Adapter<RowValueAdapter.MyViewHolder> {
        private static final String TAG = "SheetAdapter";

        ArrayList<RowValueItem> list;

        public RowValueAdapter(ArrayList<RowValueItem> list) {
            this.list = list;
        }

        @NonNull
        @NotNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            ItemRowValueBinding binding = ItemRowValueBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            MyViewHolder holder = new MyViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull MyViewHolder h, int position) {
            RowValueItem item = list.get(position);
            h.b.tvTitle.setText(item.getHeader());
            h.b.tvValue.setText(item.getValue());

            if (position == (list.size() - 1)) {
                h.b.view.setVisibility(View.GONE);
            } else {
                h.b.view.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ItemRowValueBinding b;

            public MyViewHolder(@NonNull @NotNull ItemRowValueBinding itemView) {
                super(itemView.getRoot());
                this.b = itemView;
            }
        }

    }

    public void performWork(String spreadSheetId, int flag, String ACCESS_TOKEN, String ACCESS_TOKEN_TYPE) {

        if (flag == GET_SHEET_INFO) {
            volleyGetAllSheetInfo(spreadSheetId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }

        if (flag == CREATE_SHEET) {
            volleyCreateNewSheet(spreadSheetId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, SHEET_NAME, DIALOG);
        }

        if (flag == RENAME_SHEET) {
            volleyRenameSheet(spreadSheetId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, SHEET_NAME, SHEET_ID, POSITION, DIALOG);
        }

        if (flag == DELETE_SHEET) {
            volleyDeleteSheet(spreadSheetId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE, SHEET_ID, POSITION);
        }

        if (flag == DELETE_SPREAD_SHEET) {
            volleyDeleteSpreadSheet(spreadSheetId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }

        if (flag == GET_SHEET_CONTENTS) {
            volleyGetSheetContents(spreadSheetId, SHEET_NAME, TV_NOTE, PB_SHEET, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }

        if (flag == DELETE_ROW) {
            volleyDeleteRow(spreadSheetId, SHEET_ID, START_INDEX, END_INDEX, ROW_POSITION, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
        }
    }

    private void getNewAccessToken(String accountId, String refreshToken, String spreadSheetId, int flag) {
        new Thread() {
            @Override
            public void run() {
                try {
                    GoogleTokenResponse tokenResponse =
                            new GoogleRefreshTokenRequest(
                                    new NetHttpTransport(),
                                    GsonFactory.getDefaultInstance(),
                                    refreshToken,
                                    Constants.WEB_CLIENT_ID,
                                    Constants.CLIENT_SECRET)
                                    .execute();

                    ACCESS_TOKEN = tokenResponse.getAccessToken();
                    long mExpiresInSeconds = tokenResponse.getExpiresInSeconds();
                    ACCESS_TOKEN_TYPE = tokenResponse.getTokenType();

                    Log.i(TAG, "volleyGetAccessTokenInfo: New Token Result = " +
                            "\n Access Token = " + ACCESS_TOKEN +
                            "\n Expires In = " + mExpiresInSeconds +
                            "\n Token Type = " + ACCESS_TOKEN_TYPE +
                            "\n Token Scope = " + tokenResponse.getScope());

                    db.storeNewAccessToken(accountId, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);


                } catch (Exception e) {
                    Log.i(TAG, "exchangeCodeForToken: Results = " + e.toString());
                    Log.e(TAG, "Token exchange failed with " + e.getMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        performWork(spreadSheetId, flag, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                    }
                });
            }
        }.start();
    }

    private void volleyGetAccessTokenInfo(String accountId, String spreadsheetId, boolean showProgressBar, int flag) {

        ACCESS_TOKEN = db.getAccessToken(accountId);
        ACCESS_TOKEN_TYPE = db.getAccessTokenType(accountId);
        String refreshToken = db.getRefreshToken(accountId);

        if (showProgressBar)
            basicUtils.showSmallProgressDialog(true);
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetAccessTokenInfo");
        String url = ApiProcessing.UserInfo.API_VERIFY_ACCESS_TOKEN + ACCESS_TOKEN;
        Log.i(TAG, "volleyGetAccessTokenInfo: URL = " + url);
        details.setAPI_URL(url);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyGetAccessTokenInfo: Response = " + response.toString());
                        details.setResponse(response.toString());
                        try {
                            boolean isAccessTokenExpired = ApiProcessing.UserInfo.parseResponse(response);
                            if (isAccessTokenExpired) {
                                getNewAccessToken(accountId, refreshToken, spreadsheetId, flag);
                            } else {
                                Log.i(TAG, "volleyGetAccessTokenInfo: Expires in = " + ApiProcessing.UserInfo.getRemainingTime(response));
                                performWork(spreadsheetId, flag, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: JSONException = " + e.toString());
                            details.setException(e.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAccessTokenInfo: Exception = " + e.toString());
                            details.setException(e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e(TAG, "volleyGetAccessTokenInfo: Error = " + error.toString());
                if (error instanceof ClientError) {
                    getNewAccessToken(accountId, refreshToken, spreadsheetId, flag);
                } else {
                    basicUtils.showSmallProgressDialog(false);
                    basicUtils.showCustomAlert("Failed to verify access token!");
                }
                details.setErrorResponse(error.toString());
                if (Constants.SUPER_USER)
                    details.show();
            }
        });
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "AccessTokenInfo");
    }

    private void volleyGetAllSheetInfo(String spreadSheetId, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetAllSheetInfo");

        String url = Constants.SHEETS_API + spreadSheetId + "?&fields=sheets.properties";

        details.setAPI_URL(url);

        Log.i(TAG, "volleyGetAllSheetInfo: URL = " + url);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyGetAllSheetInfo: Response = " + response);
                        details.setResponse(response.toString());
                        basicUtils.showSmallProgressDialog(false);
                        sheetList.clear();
                        try {
                            sheetList = ApiProcessing.EditSpreadsheet.parseResponse(response);
                            adapter = new SheetAdapter(sheetList);
                            b.rvSheets.setAdapter(adapter);
                            Log.i(TAG, "onResponse: List = " + sheetList.toString());
                            volleyGetSheetContents(spreadSheetId, sheetList.get(0).getSheetName(), b.tvNoRow, b.pbSheet, ACCESS_TOKEN, ACCESS_TOKEN_TYPE);
                            SHEET_ID = sheetList.get(0).getSheetId();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "volleyGetAllSheetInfo: Exception = " + e.toString());
                            details.setException(e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                basicUtils.showCustomAlert("Timed Out!");
                finish();
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyGetAllSheetInfo: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyGetAllSheetInfo: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "GetFullSpreadSheetInfo");
    }

    private void volleyGetSheetContents(String spreadSheetId, String sheetName, TextView tvNote, LinearLayout pbSheet, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyGetSheetContents");

        tvNote.setVisibility(View.GONE);
        if (pbSheet != null)
            pbSheet.setVisibility(View.VISIBLE);

        String url = Constants.SHEETS_API + spreadSheetId + "/values/'" + sheetName + "'";

        details.setAPI_URL(url);
        Log.i(TAG, "volleyGetSheetContents: URL = " + url);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyGetSheetContents: Response = " + response);
                        details.setResponse(response.toString());
                        basicUtils.showSmallProgressDialog(false);
                        SELECTED_SHEET = sheetName;
                        if (b.swipeRefresh.isRefreshing())
                            b.swipeRefresh.setRefreshing(false);
                        if (pbSheet != null)
                            pbSheet.setVisibility(View.GONE);
                        rowContentList.clear();
                        try {
                            rowContentList = ApiProcessing.EditSpreadsheet.parseContentResponse(response);
                            Log.i(TAG, "onResponse: Content List = " + rowContentList.toString());
                            rowAdapter = new RowContentAdapter(rowContentList);
                            b.rvRows.setAdapter(rowAdapter);
                            if (rowContentList.size() == 0) {
                                tvNote.setVisibility(View.VISIBLE);
                            } else {
                                tvNote.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.e(TAG, "volleyGetSheetContents: Exception = " + e.toString());
                            basicUtils.showCustomAlert("Something Unexpected Happened!");
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                SELECTED_SHEET = sheetName;
                if (b.swipeRefresh.isRefreshing())
                    b.swipeRefresh.setRefreshing(false);
                if (pbSheet != null)
                    pbSheet.setVisibility(View.GONE);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyGetSheetContents: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyGetSheetContents: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "GetFullSpreadSheetContents");
    }

    private void volleyCreateNewSheet(String spreadSheetId, String ACCESS_TOKEN, String TOKEN_TYPE, String sheetName, Dialog dialog) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyCreateNewSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.EditSpreadsheet.constructCreateSheetObject(sheetName);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyCreateNewSheet: URL = " + url);
        Log.i(TAG, "volleyGetAllSheetInfo: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyCreateNewSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        dialog.dismiss();
                        try {
                            SheetItem item = ApiProcessing.EditSpreadsheet.parseCreateResponse(new JSONObject(response));
                            for (int i = 0; i < sheetList.size(); i++) {
                                sheetList.get(i).setSelected(false);
                            }
                            sheetList.add(item);
                            adapter.notifyDataSetChanged();
                            b.rvSheets.smoothScrollToPosition(sheetList.size());
                            volleySetHeaderInSheet(spreadSheetId, sheetName, item.getSheetId(), ACCESS_TOKEN, TOKEN_TYPE, array);
                            volleyGetSheetContents(spreadSheetId, sheetName, b.tvNoRow, b.pbSheet, ACCESS_TOKEN, TOKEN_TYPE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.e(TAG, "volleyCreateNewSheet: Exception = " + e.toString());
                        }
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                dialog.dismiss();
                basicUtils.showCustomAlert("Failed to create sheet");
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyCreateNewSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyCreateNewSheet: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "CreateNewSheet");
    }

    private void volleySetHeaderInSheet(String spreadSheetId, String sheetName, int sheetId, String ACCESS_TOKEN, String TOKEN_TYPE, JSONArray array) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleySetHeaderInSheet");

        String url = Constants.SHEETS_API + spreadSheetId + "/values/" + "'" + sheetName + "'" + "?valueInputOption=RAW";
        JSONObject object = ApiProcessing.Operations.getHeaderCreationObject(array, sheetName);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleySetHeaderInSheet: URL = " + url);
        Log.i(TAG, "volleySetHeaderInSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.PUT,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleySetHeaderInSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        volleyFormatSheet(spreadSheetId, sheetId, shouldResize(array), ACCESS_TOKEN, TOKEN_TYPE);
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleySetHeaderInSheet: Error = " + error.toString());
                basicUtils.showCustomAlert("Spreadsheet Creation Failed");

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleySetHeaderInSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "SetHeaderInSheet");
    }

    private void volleyFormatSheet(String spreadSheetId, int sheetId, boolean shouldResize, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyFormatSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.Operations.getFormatObject(sheetId, shouldResize);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyFormatSheet: URL = " + url);
        Log.i(TAG, "volleyFormatSheet: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyFormatSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                basicUtils.showSmallProgressDialog(false);
                details.setErrorResponse(error.toString());
                Log.e(TAG, "volleyFormatSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                map.put("Content-Length", String.valueOf(contentLength));
                Log.i(TAG, "volleyFormatSheet: Header = " + "Authorization = " + authorization);
                details.setHeader(authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "FormatSheet");
    }

    private void volleyRenameSheet(String spreadSheetId, String ACCESS_TOKEN, String TOKEN_TYPE, String name, int sheetId, int position, Dialog dialog) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyRenameSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.EditSpreadsheet.createRenameSheetObject(name, sheetId);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyRenameSheet: URL = " + url);
        Log.i(TAG, "volleyGetAllSheetInfo: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyRenameSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        dialog.dismiss();
                        sheetList.get(position).setSheetName(name);
                        adapter.notifyDataSetChanged();
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showSmallProgressDialog(false);
                dialog.dismiss();
                basicUtils.showCustomAlert("Failed to Rename!");
                Log.e(TAG, "volleyRenameSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyRenameSheet: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "RenameSheet");
    }

    private void volleyDeleteSheet(String spreadSheetId, String ACCESS_TOKEN, String TOKEN_TYPE, int sheetId, int position) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyDeleteSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.EditSpreadsheet.createDeleteSheetObject(sheetId);

        details.setAPI_URL(url);
        details.setObject(object);
        Log.i(TAG, "volleyDeleteSheet: URL = " + url);
        Log.i(TAG, "volleyGetAllSheetInfo: OBJECT = " + object.toString());

        final PostStringRequest request = new PostStringRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyDeleteSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        sheetList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, sheetList.size());
                        CV_SHEET.performClick();
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showCustomAlert("Failed to delete!");
                basicUtils.showSmallProgressDialog(false);
                Log.e(TAG, "volleyDeleteSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyDeleteSheet: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "DeleteSheet");
    }

    private void volleyDeleteSpreadSheet(String spreadSheetId, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyDeleteSheet");

        String url = Constants.DRIVE_API + spreadSheetId;

        details.setAPI_URL(url);
        Log.i(TAG, "volleyDeleteSheet: URL = " + url);

        final StringRequest request = new StringRequest(Request.Method.DELETE,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "volleyDeleteSheet: Response = " + response);
                        details.setResponse(response);
                        basicUtils.showSmallProgressDialog(false);
                        basicUtils.showLongCustomAlert("Spreadsheet Deleted");
                        db.deleteSpreadSheetId(ACCOUNT_ID, spreadSheetId);
                        db.deleteSpreadSheetInfo(ACCOUNT_ID, spreadSheetId);
                        try {
                            HomeActivity.spreadsheetList.remove(getPosition(spreadSheetId));
                            HomeActivity.sAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                            details.setException(e.toString());
                            Log.e(TAG, "volleyDeleteSheet: Exception = " + e.toString());
                        }
                        finish();
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showCustomAlert("Failed to delete Spreadsheet!");
                basicUtils.showSmallProgressDialog(false);
                Log.e(TAG, "volleyDeleteSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyDeleteSheet: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "DeleteSheet");
    }

    private void volleyDeleteRow(String spreadSheetId, int sheetId, int startIndex, int endIndex, int rowPosition, String ACCESS_TOKEN, String TOKEN_TYPE) {
        API_Details details = new API_Details(context);
        details.setAPI_Name("volleyDeleteSheet");

        String url = Constants.SHEETS_API + spreadSheetId + ":batchUpdate";
        JSONObject object = ApiProcessing.EditSpreadsheet.createDeleteRowObject(sheetId, startIndex, endIndex);
        details.setAPI_URL(url);
        details.setObject(object.toString());
        Log.i(TAG, "volleyDeleteSheet: URL = " + url);
        Log.i(TAG, "volleyDeleteSheet: OBJECT = " + object.toString());

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                url,
                object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "volleyDeleteSheet: Response = " + response);
                        details.setResponse(response.toString());
                        basicUtils.showSmallProgressDialog(false);
                        basicUtils.showLongCustomAlert("Row Deleted");
                        rowContentList.remove(rowPosition);
                        rowAdapter.notifyItemRemoved(rowPosition);
                        rowAdapter.notifyItemRangeChanged(rowPosition, rowContentList.size());
                        if (Constants.SUPER_USER)
                            details.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                details.setErrorResponse(error.toString());
                basicUtils.showCustomAlert("Failed to delete Spreadsheet!");
                basicUtils.showSmallProgressDialog(false);
                Log.e(TAG, "volleyDeleteSheet: Error = " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                String authorization = TOKEN_TYPE + " " + ACCESS_TOKEN;
                //int contentLength = object.toString().getBytes().length;
                map.put("Authorization", authorization);
                //map.put("Content-Length",String.valueOf(contentLength));
                Log.i(TAG, "volleyDeleteSheet: Header = " + "Authorization = " + authorization);
                return map;
            }
        };
        MySingleton.getInstance(context).setRetryPolicy(request);
        MySingleton.getInstance(context).addToRequestQueue(request, "DeleteSheet");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMenuSpreadsheet:
                PopupMenu pop = new PopupMenu(context, b.ivMenuSpreadsheet);
                pop.getMenuInflater().inflate(R.menu.menu_spreadsheet, pop.getMenu());
                pop.show();

                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_edit_spreadsheet:
                                Intent i = new Intent(context, NewSpreadSheetActivity.class);

                                ArrayList<String> sheetNames = new ArrayList<>();
                                for (int j = 0; j < sheetList.size(); j++) {
                                    sheetNames.add(sheetList.get(j).getSheetName());
                                }
                                i.putExtra("hasCreds", true);
                                i.putExtra("credentials", array.toString());
                                i.putExtra("sheetNames", sheetNames);
                                i.putExtra("spreadSheetId", SPREADSHEET_ID);

                                startActivity(i);
                                break;
                            case R.id.menu_delete_spreadhseet:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage("Are you sure you want to delete " + b.tvSpreadsheetName.getText().toString() + "?");
                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, DELETE_SPREAD_SHEET);
                                        dialog.dismiss();
                                    }
                                });
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                builder.show();

                                break;
                        }
                        return true;
                    }
                });
                break;
            case R.id.ivBack:
                finish();
                break;
            case R.id.cvAddSheet:
                Dialog dialog = new Dialog(context);
                DialogAddSheetBinding bi = DialogAddSheetBinding.inflate(getLayoutInflater());
                basicUtils.setDialogAttributes(dialog, true, true, bi.getRoot(), 16, 17);
                bi.tvTitle.setText("Add Sheet");
                bi.tvAdd.setText("Add");

                bi.tvAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name = bi.etSheetName.getText().toString().trim();
                        if (name.length() == 0) {
                            bi.etSheetName.setError("Please Enter a name");
                            bi.etSheetName.requestFocus();
                            return;
                        }
                        SHEET_NAME = name;
                        DIALOG = dialog;
                        volleyGetAccessTokenInfo(ACCOUNT_ID, SPREADSHEET_ID, true, CREATE_SHEET);
                    }
                });

                bi.ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.ivClearSearch:
                b.etSearch.setText("");
                break;

            case R.id.cvAddRow:
                Intent it = new Intent(context, AddRowInfoActivity.class);

                it.putExtra("edit_position", rowContentList.size() - 1);
                it.putExtra("sheetName", SELECTED_SHEET);
                it.putExtra("sheetId", SHEET_ID);
                it.putExtra("array", array.toString());

                startActivity(it);
                break;
        }
    }
}