package com.example.sheet.Utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.sheet.R;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.ArrayList;

public class DataManager {

    public static final String UNDEF = "undefined";
    private static final String TAG = "DataManager";

    private com.google.api.services.sheets.v4.Sheets mService = null;
    // this is the play copy
    private static String mSheetID = "SHEET_ID";

    private static final String PREF_ACCESS_TOKEN = "accessToken";
    private static final String PREF_REFRESH_TOKEN = "refreshToken";
    private static final String PREF_EXPIRES_IN_SECONDS = "expiresInSec";

    private Context mContext;
    private String mAccessToken;
    private String mRefreshToken;
    private Long mExpiresInSeconds;
    private String mAuthCode;

    public DataManager(Context context) {
        mContext = context;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        mAccessToken =  prefs.getString(PREF_ACCESS_TOKEN, UNDEF);
        mRefreshToken = prefs.getString(PREF_REFRESH_TOKEN, UNDEF);
        mExpiresInSeconds = prefs.getLong(PREF_EXPIRES_IN_SECONDS, 0);
    }

    private void exchangeCodeForToken(String authCode) {
        try {
            GoogleTokenResponse tokenResponse =
                    new GoogleAuthorizationCodeTokenRequest(
                            new NetHttpTransport(),
                            GsonFactory.getDefaultInstance(),
                            "https://www.googleapis.com/oauth2/v4/token",
                            Constants.WEB_CLIENT_ID,
                            Constants.CLIENT_SECRET,
                            authCode,
                            "")
                            .execute();

            mAccessToken = tokenResponse.getAccessToken();
            mRefreshToken = tokenResponse.getRefreshToken();
            mExpiresInSeconds = tokenResponse.getExpiresInSeconds();

            // TODO: do I really need to store and pass the three values individually?
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(PREF_ACCESS_TOKEN, mAccessToken);
            editor.putString(PREF_REFRESH_TOKEN, mRefreshToken);
            editor.putLong(PREF_EXPIRES_IN_SECONDS, mExpiresInSeconds);
            editor.apply();

        } catch (Exception e) {
            Log.e(TAG, "Token exchange failed with " + e.getMessage());
        }
    }

    private void refreshAccessToken(String refreshToken) {
        try {
            // TODO: what to do here?
            throw new Exception("TBD");
        } catch (Exception e) {
            Log.e(TAG, "Token refresh failed with " + e.getMessage());
        }
    }

    private GoogleCredential getCredential() {

        if (mAuthCode != UNDEF) {
            exchangeCodeForToken(mAuthCode);
        }

        // TODO: handle missing or expired token
        if (mRefreshToken !=  UNDEF && mExpiresInSeconds < 30) {
        refreshAccessToken(mRefreshToken);
        }
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(new NetHttpTransport())
                .setJsonFactory(GsonFactory.getDefaultInstance())
                .build();
        credential.setAccessToken(mAccessToken);
        if (mRefreshToken !=  UNDEF) {
            credential.setRefreshToken(mRefreshToken);
            credential.setExpiresInSeconds(mExpiresInSeconds);
        }

        return credential;
    }

    // Set up credential and service object, then issue api call.
    public ArrayList<String> getFooListFromServer() throws IOException {
        try {
            GoogleCredential credential = getCredential();

            HttpTransport transport = new NetHttpTransport();
            JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
            mService = new com.google.api.services.sheets.v4.Sheets.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName(mContext.getString(R.string.app_name))
                    .build();

            return getDataFromServer();
        } catch (IOException exception) {
            // ...
            throw exception;
        } catch (Exception e) {
            Log.e(TAG, "something else is going on " + e.toString());
            throw e;
        }
    }

    /**
     * Actually fetch the data from google
     *
     * @return List of Foos
     * @throws IOException
     */
    private ArrayList<String> getDataFromServer() throws IOException {

        ArrayList<String> foos = new ArrayList<String>();

        ValueRange response = this.mService.spreadsheets().values()
                .get(mSheetID, "Sheet1")
                .setValueRenderOption("UNFORMATTED_VALUE")
                .setDateTimeRenderOption("FORMATTED_STRING")
                .execute();
        //...
        return foos;
    }
}