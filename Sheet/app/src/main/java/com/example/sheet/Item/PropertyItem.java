package com.example.sheet.Item;

import java.util.ArrayList;

public class PropertyItem {

    String accountId;
    String spreadsheetId;
    String title;
    String date;
    String columnName;
    String inputType;
    ArrayList<String> spinnerOptions;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getSpreadsheetId() {
        return spreadsheetId;
    }

    public void setSpreadsheetId(String spreadsheetId) {
        this.spreadsheetId = spreadsheetId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public ArrayList<String> getSpinnerOptions() {
        return spinnerOptions;
    }

    public void setSpinnerOptions(ArrayList<String> spinnerOptions) {
        this.spinnerOptions = spinnerOptions;
    }

    @Override
    public String toString() {
        return "PropertyItem{" +
                "accountId='" + accountId + '\'' +
                ", spreadsheetId='" + spreadsheetId + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", columnName='" + columnName + '\'' +
                ", inputType='" + inputType + '\'' +
                ", spinnerOptions=" + spinnerOptions +
                '}';
    }
}
