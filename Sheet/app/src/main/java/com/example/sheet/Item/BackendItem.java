package com.example.sheet.Item;

public class BackendItem {
    String AccountId;
    String IdToken;
    boolean SubscriptionStatus = false;
    String JSONData;

    public String getAccountId() {
        return AccountId;
    }

    public void setAccountId(String accountId) {
        AccountId = accountId;
    }

    public String getIdToken() {
        return IdToken;
    }

    public void setIdToken(String idToken) {
        IdToken = idToken;
    }

    public boolean isSubscriptionStatus() {
        return SubscriptionStatus;
    }

    public void setSubscriptionStatus(boolean subscriptionStatus) {
        SubscriptionStatus = subscriptionStatus;
    }

    public String getJSONData() {
        return JSONData;
    }

    public void setJSONData(String JSONData) {
        this.JSONData = JSONData;
    }

    @Override
    public String toString() {
        return "BackendItem{" +
                "AccountId='" + AccountId + '\'' +
                ", IdToken='" + IdToken + '\'' +
                ", SubscriptionStatus=" + SubscriptionStatus +
                ", JSONDate='" + JSONData + '\'' +
                '}';
    }
}
