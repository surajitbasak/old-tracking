package com.example.sheet.Item;

public class SheetItem {
    int sheetId;
    String sheetName;
    boolean isSelected = false;

    public int getSheetId() {
        return sheetId;
    }

    public void setSheetId(int sheetId) {
        this.sheetId = sheetId;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "SheetItem{" +
                "sheetId=" + sheetId +
                ", sheetName='" + sheetName + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
