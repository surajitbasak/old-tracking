package com.spreadsheet.app.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.List;
import p153e.p174d.p240d.C7182e;

/* renamed from: com.spreadsheet.app.Utils.h */
public class C6879h {

    /* renamed from: d */
    private static C6879h f24052d = new C6879h();

    /* renamed from: a */
    SharedPreferences f24053a;

    /* renamed from: b */
    SharedPreferences.Editor f24054b;

    /* renamed from: c */
    Context f24055c;

    /* renamed from: c */
    public static C6879h m33499c() {
        return f24052d;
    }

    /* renamed from: a */
    public boolean mo24221a(String str) {
        return this.f24053a.getBoolean(str, false);
    }

    /* renamed from: b */
    public String mo24222b(String str) {
        return this.f24053a.getString(str, "");
    }

    /* renamed from: d */
    public int mo24223d(String str) {
        return this.f24053a.getInt(str, 0);
    }

    /* renamed from: e */
    public long mo24224e(String str) {
        return this.f24053a.getLong(str, 0);
    }

    /* renamed from: f */
    public void mo24225f(Context context) {
        this.f24055c = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(C6873b.f24022m, 0);
        this.f24053a = sharedPreferences;
        this.f24054b = sharedPreferences.edit();
    }

    /* renamed from: g */
    public void mo24226g(String str, boolean z) {
        this.f24054b.putBoolean(str, z);
        this.f24054b.commit();
    }

    /* renamed from: h */
    public void mo24227h(String str, String str2) {
        this.f24054b.putString(str, str2);
        this.f24054b.commit();
    }

    /* renamed from: i */
    public void mo24228i(String str, int i) {
        this.f24054b.putInt(str, i);
        this.f24054b.commit();
    }

    /* renamed from: j */
    public void mo24229j(String str, List<HashMap<String, String>> list) {
        this.f24054b.putString(str, new C7182e().mo24739l(list));
        this.f24054b.commit();
    }

    /* renamed from: k */
    public void mo24230k(String str, long j) {
        this.f24054b.putLong(str, j);
        this.f24054b.commit();
    }
}
