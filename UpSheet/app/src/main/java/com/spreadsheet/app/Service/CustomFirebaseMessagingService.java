package com.spreadsheet.app.Service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;
import androidx.core.app.C0345h;
import com.google.firebase.messaging.C6742b;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6879h;
import java.util.Map;

public class CustomFirebaseMessagingService extends FirebaseMessagingService {

    /* renamed from: i */
    C6879h f23965i = C6879h.m33499c();

    /* renamed from: j */
    String f23966j = "com.spreadsheet.app";

    /* renamed from: k */
    C0345h.C0349d f23967k = null;

    /* renamed from: l */
    NotificationChannel f23968l = null;

    static {
        Class<CustomFirebaseMessagingService> cls = CustomFirebaseMessagingService.class;
    }

    /* renamed from: i */
    public void mo23838i(C6742b bVar) {
        C0345h.C0349d dVar;
        super.mo23838i(bVar);
        this.f23965i.mo24225f(this);
        if (Build.VERSION.SDK_INT >= 26) {
            if (this.f23968l == null) {
                NotificationChannel notificationChannel = new NotificationChannel(this.f23966j, "spreadsheetapp", 0);
                this.f23968l = notificationChannel;
                notificationChannel.setLightColor(-16776961);
                this.f23968l.setLockscreenVisibility(0);
                ((NotificationManager) getSystemService("notification")).createNotificationChannel(this.f23968l);
            }
            dVar = new C0345h.C0349d(this, this.f23966j);
        } else {
            dVar = new C0345h.C0349d(this);
        }
        this.f23967k = dVar;
        Map<String, String> r0 = bVar.mo23845r0();
        if (r0.containsKey(C6873b.f23997Y) && !r0.get(C6873b.f23997Y).equals("")) {
            mo24191m(this, r0);
        }
    }

    /* renamed from: k */
    public void mo23840k(String str) {
        Log.e("NEW_TOKEN", str);
        this.f23965i.mo24225f(this);
        this.f23965i.mo24227h(C6873b.f24033x, str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:34|35|36|37) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x00b6 */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24191m(android.content.Context r13, Map<String, String> r14) {
        /*
            r12 = this;
            r0 = 2
            android.net.Uri r0 = android.media.RingtoneManager.getDefaultUri(r0)     // Catch:{ Exception -> 0x01ee }
            java.util.Random r1 = new java.util.Random     // Catch:{ Exception -> 0x01ee }
            r1.<init>()     // Catch:{ Exception -> 0x01ee }
            r2 = 1000(0x3e8, float:1.401E-42)
            int r1 = r1.nextInt(r2)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23997Y     // Catch:{ Exception -> 0x01ee }
            boolean r2 = r14.containsKey(r2)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r3 = ""
            if (r2 == 0) goto L_0x0023
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23997Y     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r2 = r14.get(r2)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x01ee }
            goto L_0x0024
        L_0x0023:
            r2 = r3
        L_0x0024:
            java.lang.String r4 = com.spreadsheet.app.Utils.C6873b.f24002b0     // Catch:{ Exception -> 0x01ee }
            boolean r4 = r14.containsKey(r4)     // Catch:{ Exception -> 0x01ee }
            if (r4 == 0) goto L_0x0035
            java.lang.String r4 = com.spreadsheet.app.Utils.C6873b.f24002b0     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r4 = r14.get(r4)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x01ee }
            goto L_0x0036
        L_0x0035:
            r4 = r3
        L_0x0036:
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f23998Z     // Catch:{ Exception -> 0x01ee }
            boolean r5 = r14.containsKey(r5)     // Catch:{ Exception -> 0x01ee }
            if (r5 == 0) goto L_0x0047
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f23998Z     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r5 = r14.get(r5)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x01ee }
            goto L_0x0048
        L_0x0047:
            r5 = r3
        L_0x0048:
            java.lang.String r6 = com.spreadsheet.app.Utils.C6873b.f24004c0     // Catch:{ Exception -> 0x01ee }
            boolean r6 = r14.containsKey(r6)     // Catch:{ Exception -> 0x01ee }
            if (r6 == 0) goto L_0x0059
            java.lang.String r6 = com.spreadsheet.app.Utils.C6873b.f24004c0     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r6 = r14.get(r6)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x01ee }
            goto L_0x005a
        L_0x0059:
            r6 = r3
        L_0x005a:
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f24000a0     // Catch:{ Exception -> 0x01ee }
            boolean r7 = r14.containsKey(r7)     // Catch:{ Exception -> 0x01ee }
            if (r7 == 0) goto L_0x006b
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f24000a0     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r7 = r14.get(r7)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ Exception -> 0x01ee }
            goto L_0x006c
        L_0x006b:
            r7 = r3
        L_0x006c:
            java.lang.String r8 = com.spreadsheet.app.Utils.C6873b.f24006d0     // Catch:{ Exception -> 0x01ee }
            boolean r8 = r14.containsKey(r8)     // Catch:{ Exception -> 0x01ee }
            if (r8 == 0) goto L_0x007d
            java.lang.String r8 = com.spreadsheet.app.Utils.C6873b.f24006d0     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r14 = r14.get(r8)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ Exception -> 0x01ee }
            goto L_0x007e
        L_0x007d:
            r14 = r3
        L_0x007e:
            boolean r8 = r6.equals(r3)     // Catch:{ Exception -> 0x01ee }
            if (r8 == 0) goto L_0x0088
            java.lang.String r6 = r12.getPackageName()     // Catch:{ Exception -> 0x01ee }
        L_0x0088:
            r8 = 0
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24008e0     // Catch:{ Exception -> 0x01ee }
            boolean r9 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r10 = "https://play.google.com/store/apps/details?id="
            java.lang.String r11 = "android.intent.action.VIEW"
            if (r9 == 0) goto L_0x00cf
            android.content.Intent r8 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            r14.<init>()     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            java.lang.String r2 = "market://details?id="
            r14.append(r2)     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            r14.append(r6)     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            java.lang.String r14 = r14.toString()     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            android.net.Uri r14 = android.net.Uri.parse(r14)     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            r8.<init>(r11, r14)     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            r14 = 67108864(0x4000000, float:1.5046328E-36)
            r8.setFlags(r14)     // Catch:{ ActivityNotFoundException -> 0x00b6 }
            goto L_0x013a
        L_0x00b6:
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ee }
            r14.<init>()     // Catch:{ Exception -> 0x01ee }
            r14.append(r10)     // Catch:{ Exception -> 0x01ee }
            r14.append(r6)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x01ee }
            android.net.Uri r14 = android.net.Uri.parse(r14)     // Catch:{ Exception -> 0x01ee }
            r8.<init>(r11, r14)     // Catch:{ Exception -> 0x01ee }
            goto L_0x013a
        L_0x00cf:
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24010f0     // Catch:{ Exception -> 0x01ee }
            boolean r9 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            if (r9 == 0) goto L_0x00df
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            java.lang.Class<com.spreadsheet.app.activities.ActivitySplash> r14 = com.spreadsheet.app.activities.ActivitySplash.class
            r8.<init>(r13, r14)     // Catch:{ Exception -> 0x01ee }
            goto L_0x013a
        L_0x00df:
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24014h0     // Catch:{ Exception -> 0x01ee }
            boolean r9 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            if (r9 == 0) goto L_0x00f4
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            java.lang.Class<com.spreadsheet.app.activities.ActivitySplash> r14 = com.spreadsheet.app.activities.ActivitySplash.class
            r8.<init>(r13, r14)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r14 = com.spreadsheet.app.Utils.C6873b.f24012g0     // Catch:{ Exception -> 0x01ee }
            r8.putExtra(r14, r2)     // Catch:{ Exception -> 0x01ee }
            goto L_0x013a
        L_0x00f4:
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24012g0     // Catch:{ Exception -> 0x01ee }
            boolean r9 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            if (r9 == 0) goto L_0x0104
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            java.lang.Class<com.spreadsheet.app.activities.ActivitySplash> r14 = com.spreadsheet.app.activities.ActivitySplash.class
            r8.<init>(r13, r14)     // Catch:{ Exception -> 0x01ee }
            goto L_0x013a
        L_0x0104:
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24016i0     // Catch:{ Exception -> 0x01ee }
            boolean r9 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            if (r9 == 0) goto L_0x0114
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            java.lang.Class<com.spreadsheet.app.activities.ActivitySplash> r14 = com.spreadsheet.app.activities.ActivitySplash.class
            r8.<init>(r13, r14)     // Catch:{ Exception -> 0x01ee }
            goto L_0x013a
        L_0x0114:
            java.lang.String r9 = com.spreadsheet.app.Utils.C6873b.f24018j0     // Catch:{ Exception -> 0x01ee }
            boolean r2 = r2.equals(r9)     // Catch:{ Exception -> 0x01ee }
            if (r2 == 0) goto L_0x013a
            boolean r2 = r14.equals(r3)     // Catch:{ Exception -> 0x01ee }
            if (r2 == 0) goto L_0x0131
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ee }
            r14.<init>()     // Catch:{ Exception -> 0x01ee }
            r14.append(r10)     // Catch:{ Exception -> 0x01ee }
            r14.append(r6)     // Catch:{ Exception -> 0x01ee }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x01ee }
        L_0x0131:
            android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x01ee }
            android.net.Uri r14 = android.net.Uri.parse(r14)     // Catch:{ Exception -> 0x01ee }
            r8.<init>(r11, r14)     // Catch:{ Exception -> 0x01ee }
        L_0x013a:
            r14 = 100
            r2 = 0
            android.app.PendingIntent r14 = android.app.PendingIntent.getActivity(r13, r14, r8, r2)     // Catch:{ Exception -> 0x01ee }
            boolean r3 = r4.equals(r3)     // Catch:{ Exception -> 0x01ee }
            r6 = 1
            r8 = 2131099710(0x7f06003e, float:1.781178E38)
            r9 = 2131230937(0x7f0800d9, float:1.807794E38)
            if (r3 == 0) goto L_0x018a
            androidx.core.app.h$d r3 = r12.f23967k     // Catch:{ Exception -> 0x01ee }
            r3.mo2155k(r5)     // Catch:{ Exception -> 0x01ee }
            r3.mo2166w(r0)     // Catch:{ Exception -> 0x01ee }
            r3.mo2165v(r9)     // Catch:{ Exception -> 0x01ee }
            android.content.res.Resources r0 = r13.getResources()     // Catch:{ Exception -> 0x01ee }
            r4 = 2131623936(0x7f0e0000, float:1.8875038E38)
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeResource(r0, r4)     // Catch:{ Exception -> 0x01ee }
            r3.mo2158o(r0)     // Catch:{ Exception -> 0x01ee }
            android.content.res.Resources r0 = r13.getResources()     // Catch:{ Exception -> 0x01ee }
            int r0 = r0.getColor(r8)     // Catch:{ Exception -> 0x01ee }
            r3.mo2152h(r0)     // Catch:{ Exception -> 0x01ee }
            r3.mo2150f(r6)     // Catch:{ Exception -> 0x01ee }
            androidx.core.app.h$c r0 = new androidx.core.app.h$c     // Catch:{ Exception -> 0x01ee }
            r0.<init>()     // Catch:{ Exception -> 0x01ee }
            r0.mo2144h(r5)     // Catch:{ Exception -> 0x01ee }
            r0.mo2143g(r7)     // Catch:{ Exception -> 0x01ee }
            r3.mo2167x(r0)     // Catch:{ Exception -> 0x01ee }
            r3.mo2153i(r14)     // Catch:{ Exception -> 0x01ee }
        L_0x0185:
            android.app.Notification r14 = r3.mo2148b()     // Catch:{ Exception -> 0x01ee }
            goto L_0x01d0
        L_0x018a:
            androidx.core.app.h$d r3 = r12.f23967k     // Catch:{ Exception -> 0x01ee }
            r3.mo2155k(r5)     // Catch:{ Exception -> 0x01ee }
            r3.mo2154j(r7)     // Catch:{ Exception -> 0x01ee }
            r3.mo2166w(r0)     // Catch:{ Exception -> 0x01ee }
            r3.mo2165v(r9)     // Catch:{ Exception -> 0x01ee }
            android.content.res.Resources r0 = r13.getResources()     // Catch:{ Exception -> 0x01ee }
            int r0 = r0.getColor(r8)     // Catch:{ Exception -> 0x01ee }
            r3.mo2152h(r0)     // Catch:{ Exception -> 0x01ee }
            r3.mo2150f(r6)     // Catch:{ Exception -> 0x01ee }
            r3.mo2153i(r14)     // Catch:{ Exception -> 0x01ee }
            androidx.core.app.h$b r14 = new androidx.core.app.h$b     // Catch:{ Exception -> 0x01ee }
            r14.<init>()     // Catch:{ Exception -> 0x01ee }
            com.bumptech.glide.i r0 = com.bumptech.glide.C0703b.m4012t(r13)     // Catch:{ Exception -> 0x01ee }
            com.bumptech.glide.h r0 = r0.mo4315f()     // Catch:{ Exception -> 0x01ee }
            r0.mo4311s0(r4)     // Catch:{ Exception -> 0x01ee }
            com.bumptech.glide.p.c r0 = r0.mo4312w0()     // Catch:{ Exception -> 0x01ee }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x01ee }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ Exception -> 0x01ee }
            r14.mo2140h(r0)     // Catch:{ Exception -> 0x01ee }
            r14.mo2141i(r5)     // Catch:{ Exception -> 0x01ee }
            r14.mo2142j(r7)     // Catch:{ Exception -> 0x01ee }
            r3.mo2167x(r14)     // Catch:{ Exception -> 0x01ee }
            goto L_0x0185
        L_0x01d0:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01ee }
            r3 = 26
            if (r0 < r3) goto L_0x01e3
            androidx.core.app.h$d r13 = r12.f23967k     // Catch:{ Exception -> 0x01ee }
            android.app.Notification r13 = r13.mo2148b()     // Catch:{ Exception -> 0x01ee }
            r12.startForeground(r1, r13)     // Catch:{ Exception -> 0x01ee }
            r12.stopForeground(r2)     // Catch:{ Exception -> 0x01ee }
            goto L_0x01ee
        L_0x01e3:
            java.lang.String r0 = "notification"
            java.lang.Object r13 = r13.getSystemService(r0)     // Catch:{ Exception -> 0x01ee }
            android.app.NotificationManager r13 = (android.app.NotificationManager) r13     // Catch:{ Exception -> 0x01ee }
            r13.notify(r1, r14)     // Catch:{ Exception -> 0x01ee }
        L_0x01ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.Service.CustomFirebaseMessagingService.mo24191m(android.content.Context, java.util.Map):void");
    }
}
