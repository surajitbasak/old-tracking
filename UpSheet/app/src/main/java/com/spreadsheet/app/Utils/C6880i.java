package com.spreadsheet.app.Utils;

import java.util.Comparator;
import p153e.p300e.p301a.p302a.C7566k;

/* renamed from: com.spreadsheet.app.Utils.i */
public class C6880i implements Comparator<C7566k> {
    /* renamed from: a */
    public int compare(C7566k kVar, C7566k kVar2) {
        return String.valueOf(kVar.getTaskDate()).compareTo(String.valueOf(kVar2.getTaskDate()));
    }
}
