package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.C0065b;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.billingclient.api.C0660a;
import com.android.billingclient.api.C0663b;
import com.android.billingclient.api.C0665c;
import com.android.billingclient.api.C0671e;
import com.android.billingclient.api.C0673f;
import com.android.billingclient.api.C0676g;
import com.android.billingclient.api.C0679h;
import com.android.billingclient.api.C0681i;
import com.android.billingclient.api.C0684j;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.C4524a;
import com.google.firebase.database.C4527b;
import com.google.firebase.database.C4529d;
import com.google.firebase.database.C4534g;
import com.google.firebase.database.C4549p;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomTextBold;
import com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7077a;
import com.spreadsheet.app.adapters.C7099f;
import com.spreadsheet.app.adapters.SheetAccessAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import p153e.p168c.p169a.p170a.C5326a;
import p153e.p174d.p176b.p190c.p217h.C6386c;
import p153e.p174d.p176b.p190c.p217h.C6394h;
import p153e.p300e.p301a.p302a.C7556a;
import p153e.p300e.p301a.p302a.C7559d;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p302a.C7568m;
import p153e.p300e.p301a.p304c.C7571a;
import p153e.p300e.p301a.p304c.C7575e;
import p153e.p300e.p301a.p304c.C7577g;
import p153e.p300e.p301a.p304c.C7580j;
import p153e.p300e.p301a.p305d.C7581a;
import p153e.p300e.p301a.p305d.C7582b;
import p153e.p300e.p301a.p305d.C7583c;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7595f;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p305d.C7597h;
import p153e.p300e.p301a.p306e.C7598a;
import p153e.p307f.p314b.C7822t;
import p153e.p307f.p314b.C7838x;

public class MainActivity extends C0067c implements C7575e, C7577g, C7598a, NavigationView.C3788c, View.OnClickListener, C7580j, C7571a, C0679h {

    /* renamed from: A */
    C4529d f24419A;

    /* renamed from: A0 */
    ArrayList<String> f24420A0;

    /* renamed from: B */
    C4529d f24421B;

    /* renamed from: B0 */
    ArrayList<String> f24422B0;

    /* renamed from: C */
    C4529d f24423C;

    /* renamed from: C0 */
    AppCompatSpinner f24424C0;

    /* renamed from: D */
    List<C7560e> f24425D = new ArrayList();

    /* renamed from: D0 */
    AppCompatSpinner f24426D0;

    /* renamed from: E */
    C6872a f24427E = C6872a.m33458a();

    /* renamed from: E0 */
    EditText f24428E0;

    /* renamed from: F */
    C6875d f24429F = C6875d.m33482b();

    /* renamed from: F0 */
    Dialog f24430F0;

    /* renamed from: G */
    C7596g f24431G = C7596g.m35606z();

    /* renamed from: G0 */
    TextView f24432G0;

    /* renamed from: H */
    C7590d f24433H = C7590d.m35558a();

    /* renamed from: H0 */
    Button f24434H0;

    /* renamed from: I */
    private List<String> f24435I;

    /* renamed from: I0 */
    Button f24436I0;

    /* renamed from: J */
    String f24437J;

    /* renamed from: J0 */
    Button f24438J0;

    /* renamed from: K */
    C7562g f24439K = C7562g.getInstance();

    /* renamed from: K0 */
    ImageView f24440K0;

    /* renamed from: L */
    CircularImageView f24441L;

    /* renamed from: L0 */
    C7568m f24442L0;

    /* renamed from: M */
    CustomTextView f24443M;

    /* renamed from: N */
    CustomTextView f24444N;

    /* renamed from: O */
    ConstraintLayout f24445O;

    /* renamed from: P */
    ConstraintLayout f24446P;

    /* renamed from: Q */
    ConstraintLayout f24447Q;

    /* renamed from: R */
    ImageView f24448R;

    /* renamed from: S */
    RecyclerView f24449S;

    /* renamed from: T */
    Dialog f24450T;

    /* renamed from: U */
    EditText f24451U;

    /* renamed from: V */
    CustomTextView f24452V;

    /* renamed from: W */
    CustomTextSemiBold f24453W;

    /* renamed from: X */
    ImageView f24454X;

    /* renamed from: Y */
    Button f24455Y;

    /* renamed from: Z */
    String f24456Z;

    /* renamed from: a0 */
    int f24457a0;
    @BindView(2131296320)
    AdView adView;

    /* renamed from: b0 */
    C7560e f24458b0;
    @BindView(2131296344)
    CustomTextView btnTryAgain;

    /* renamed from: c0 */
    int f24459c0 = 0;
    @BindView(2131296374)
    CardView cardBarcodeSheet;
    @BindView(2131296376)
    CardView cardContact;
    @BindView(2131296377)
    CardView cardCustomised;
    @BindView(2131296387)
    CardView cardTodo;

    /* renamed from: d0 */
    List<String> f24460d0;
    @BindView(2131296569)
    DrawerLayout drawerLayout;

    /* renamed from: e0 */
    Purchase f24461e0 = null;

    /* renamed from: f0 */
    private String f24462f0 = "";

    /* renamed from: g0 */
    C7099f f24463g0;

    /* renamed from: h0 */
    private C7597h f24464h0 = C7597h.m35641o();

    /* renamed from: i0 */
    String[] f24465i0 = {"android.permission.READ_CONTACTS"};
    @BindView(2131296538)
    ImageView imageSearch;

    /* renamed from: j0 */
    private C7582b f24466j0;

    /* renamed from: k0 */
    private String f24467k0;

    /* renamed from: l0 */
    private int f24468l0;
    @BindView(2131296570)
    ConstraintLayout layoutEmptyScreen;
    @BindView(2131296591)
    RelativeLayout layoutProgress;
    @BindView(2131296601)
    HorizontalScrollView layoutSheetTypes;
    @BindView(2131296608)
    RelativeLayout layoutTryAgain;

    /* renamed from: m0 */
    private String f24469m0;

    /* renamed from: n0 */
    private boolean f24470n0;
    @BindView(2131296677)
    NavigationView navigationView;
    @BindView(2131296679)
    NestedScrollView nestedScrollView;

    /* renamed from: o0 */
    private List<HashMap<String, String>> f24471o0;

    /* renamed from: p0 */
    boolean f24472p0;

    /* renamed from: q0 */
    List<HashMap<String, String>> f24473q0;

    /* renamed from: r0 */
    C5326a f24474r0;
    @BindView(2131296730)
    RecyclerView recyclerviewSheets;

    /* renamed from: s0 */
    C7591e f24475s0;
    @BindView(2131296759)
    SearchView searchView;

    /* renamed from: t0 */
    C7560e f24476t0;
    @BindView(2131296835)
    CustomTextView textCreateSpreadsheet;
    @BindView(2131296899)
    CustomTextSemiBold textSpreadsheet;
    @BindView(2131296904)
    CustomTextSemiBold textTemplate;
    @BindView(2131296930)
    Toolbar toolbarHome;
    @BindView(2131296931)
    CustomTextBold toolbarHomeGoPremium;
    @BindView(2131296933)
    CustomTextBold toolbarHomeTitlePremium;

    /* renamed from: u */
    C6879h f24477u = C6879h.m33499c();

    /* renamed from: u0 */
    C7560e f24478u0;

    /* renamed from: v */
    C7583c f24479v = C7583c.m35532f();

    /* renamed from: v0 */
    String f24480v0;

    /* renamed from: w */
    String f24481w;

    /* renamed from: w0 */
    String f24482w0;

    /* renamed from: x */
    FirebaseAuth f24483x;

    /* renamed from: x0 */
    C0665c f24484x0;

    /* renamed from: y */
    C4529d f24485y;

    /* renamed from: y0 */
    C7595f f24486y0;

    /* renamed from: z */
    C4529d f24487z;

    /* renamed from: z0 */
    RecyclerView f24488z0;

    /* renamed from: com.spreadsheet.app.activities.MainActivity$a */
    class C7008a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24489c;

        C7008a(Dialog dialog) {
            this.f24489c = dialog;
        }

        public void onClick(View view) {
            this.f24489c.dismiss();
            MainActivity.this.f24433H.mo25638c("NAV_RATE_YES", "NAV_RATE_YES");
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + MainActivity.this.getPackageName()));
                intent.addFlags(268435456);
                MainActivity.this.startActivity(intent);
            } catch (ActivityNotFoundException unused) {
                MainActivity mainActivity = MainActivity.this;
                MainActivity.m33736F1(mainActivity, "https://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName());
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$a0 */
    class C7009a0 implements View.OnClickListener {
        C7009a0() {
        }

        public void onClick(View view) {
            MainActivity.this.f24430F0.dismiss();
            MainActivity.this.f24426D0.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) null);
            MainActivity.this.f24424C0.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) null);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$b */
    class C7010b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24492c;

        C7010b(Dialog dialog) {
            this.f24492c = dialog;
        }

        public void onClick(View view) {
            this.f24492c.dismiss();
            MainActivity.this.mo24401S0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$b0 */
    class C7011b0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24494c;

        C7011b0(MainActivity mainActivity, Dialog dialog) {
            this.f24494c = dialog;
        }

        public void onClick(View view) {
            this.f24494c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$c */
    class C7012c implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24495c;

        C7012c(MainActivity mainActivity, Dialog dialog) {
            this.f24495c = dialog;
        }

        public void onClick(View view) {
            this.f24495c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$c0 */
    class C7013c0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24496c;

        /* renamed from: d */
        final /* synthetic */ RadioGroup f24497d;

        /* renamed from: e */
        final /* synthetic */ C7560e f24498e;

        /* renamed from: f */
        final /* synthetic */ String f24499f;

        C7013c0(Dialog dialog, RadioGroup radioGroup, C7560e eVar, String str) {
            this.f24496c = dialog;
            this.f24497d = radioGroup;
            this.f24498e = eVar;
            this.f24499f = str;
        }

        public void onClick(View view) {
            this.f24496c.dismiss();
            RadioGroup radioGroup = this.f24497d;
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
            String str = radioButton.getText().toString().equalsIgnoreCase("Viewer") ? "reader" : radioButton.getText().toString().equalsIgnoreCase("Editor") ? "writer" : "";
            MainActivity.this.f24429F.mo24213d("");
            MainActivity.this.f24428E0.setText("");
            MainActivity mainActivity = MainActivity.this;
            mainActivity.f24486y0.mo25655k(mainActivity, mainActivity);
            MainActivity.this.f24486y0.mo25658n(this.f24498e.getSheetId(), MainActivity.this.f24486y0.mo25652h(str, this.f24499f));
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$d */
    class C7014d implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24501c;

        C7014d(Dialog dialog) {
            this.f24501c = dialog;
        }

        public void onClick(View view) {
            this.f24501c.dismiss();
            MainActivity.this.f24433H.mo25638c("NAV_RATE_YES", "NAV_RATE_YES");
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + MainActivity.this.getPackageName()));
                intent.addFlags(268435456);
                MainActivity.this.startActivity(intent);
            } catch (ActivityNotFoundException unused) {
                MainActivity mainActivity = MainActivity.this;
                MainActivity.m33736F1(mainActivity, "https://play.google.com/store/apps/details?id=" + MainActivity.this.getPackageName());
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$d0 */
    class C7015d0 implements View.OnClickListener {
        C7015d0() {
        }

        public void onClick(View view) {
            MainActivity.this.f24479v.mo25615a();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$e */
    class C7016e implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24504c;

        C7016e(Dialog dialog) {
            this.f24504c = dialog;
        }

        public void onClick(View view) {
            this.f24504c.dismiss();
            MainActivity.this.mo24401S0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$e0 */
    class C7017e0 implements AdapterView.OnItemSelectedListener {
        C7017e0() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            MainActivity.this.f24429F.mo24213d("");
            MainActivity mainActivity = MainActivity.this;
            mainActivity.f24486y0.mo25655k(mainActivity, mainActivity);
            MainActivity mainActivity2 = MainActivity.this;
            C7595f fVar = mainActivity2.f24486y0;
            String str = mainActivity2.f24480v0;
            if (i == 0) {
                fVar.mo25649d(str);
            } else {
                fVar.mo25648c(str, fVar.mo25651g("reader"));
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$f */
    class C7018f implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24507c;

        C7018f(MainActivity mainActivity, Dialog dialog) {
            this.f24507c = dialog;
        }

        public void onClick(View view) {
            this.f24507c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$f0 */
    class C7019f0 implements AdapterView.OnItemSelectedListener {
        C7019f0() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            MainActivity.this.f24429F.mo24213d("");
            MainActivity mainActivity = MainActivity.this;
            mainActivity.f24486y0.mo25655k(mainActivity, mainActivity);
            MainActivity mainActivity2 = MainActivity.this;
            C7595f fVar = mainActivity2.f24486y0;
            fVar.mo25648c(mainActivity2.f24480v0, fVar.mo25651g(i == 0 ? "reader" : "writer"));
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$g */
    class C7020g implements C6386c<Void> {
        C7020g() {
        }

        /* renamed from: b */
        public void mo7656b(C6394h<Void> hVar) {
            if (hVar.mo23043r()) {
                MainActivity.this.m33748Z0();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$g0 */
    class C7021g0 implements TabLayout.C3824d {

        /* renamed from: a */
        final /* synthetic */ CardView f24510a;

        /* renamed from: b */
        final /* synthetic */ CardView f24511b;

        /* renamed from: c */
        final /* synthetic */ CardView f24512c;

        C7021g0(MainActivity mainActivity, CardView cardView, CardView cardView2, CardView cardView3) {
            this.f24510a = cardView;
            this.f24511b = cardView2;
            this.f24512c = cardView3;
        }

        /* renamed from: a */
        public void mo14469a(TabLayout.C3829g gVar) {
        }

        /* renamed from: b */
        public void mo14470b(TabLayout.C3829g gVar) {
            if (gVar.mo14488f() == 0) {
                this.f24510a.setVisibility(0);
                this.f24511b.setVisibility(8);
                this.f24512c.setVisibility(8);
                return;
            }
            this.f24510a.setVisibility(8);
            this.f24511b.setVisibility(0);
            this.f24512c.setVisibility(0);
        }

        /* renamed from: c */
        public void mo14471c(TabLayout.C3829g gVar) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$h */
    class C7022h implements Runnable {
        C7022h() {
        }

        public void run() {
            AppCompatSpinner appCompatSpinner;
            MainActivity.this.f24429F.mo24211a();
            int i = 0;
            MainActivity.this.f24426D0.setVisibility(View.VISIBLE);
            String str = MainActivity.this.f24439K.getPublicAccessMap().get("role");
            if (str.equalsIgnoreCase("reader")) {
                appCompatSpinner = MainActivity.this.f24426D0;
            } else if (str.equalsIgnoreCase("writer")) {
                appCompatSpinner = MainActivity.this.f24426D0;
                i = 1;
            } else {
                return;
            }
            appCompatSpinner.setSelection(i);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$h0 */
    class C7023h0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24514c;

        C7023h0(MainActivity mainActivity, Dialog dialog) {
            this.f24514c = dialog;
        }

        public void onClick(View view) {
            this.f24514c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$i */
    class C7024i implements View.OnClickListener {
        C7024i() {
        }

        public void onClick(View view) {
            MainActivity mainActivity = MainActivity.this;
            mainActivity.drawerLayout.mo2350G(mainActivity.navigationView);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$i0 */
    class C7025i0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24516c;

        C7025i0(Dialog dialog) {
            this.f24516c = dialog;
        }

        public void onClick(View view) {
            this.f24516c.dismiss();
            MainActivity.this.mo24410j1(C6873b.f24009f);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$j */
    class C7026j implements Runnable {
        C7026j() {
        }

        public void run() {
            MainActivity.this.f24429F.mo24211a();
            MainActivity.this.f24426D0.setVisibility(8);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$j0 */
    class C7027j0 implements C0663b {

        /* renamed from: a */
        final /* synthetic */ Purchase f24519a;

        C7027j0(Purchase purchase) {
            this.f24519a = purchase;
        }

        /* renamed from: a */
        public void mo4199a(C0676g gVar) {
            if (gVar.mo4230a() == 0) {
                MainActivity.this.m33733D0(this.f24519a);
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$k */
    class C7028k implements C6386c<Void> {

        /* renamed from: a */
        final /* synthetic */ C7560e f24521a;

        C7028k(C7560e eVar) {
            this.f24521a = eVar;
        }

        /* renamed from: b */
        public void mo7656b(C6394h<Void> hVar) {
            if (hVar.mo23043r()) {
                MainActivity mainActivity = MainActivity.this;
                mainActivity.f24476t0 = this.f24521a;
                mainActivity.m33740O0();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$k0 */
    class C7029k0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24523c;

        C7029k0(MainActivity mainActivity, Dialog dialog) {
            this.f24523c = dialog;
        }

        public void onClick(View view) {
            this.f24523c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$l */
    class C7030l implements C6386c<Void> {

        /* renamed from: a */
        final /* synthetic */ C7560e f24524a;

        C7030l(C7560e eVar) {
            this.f24524a = eVar;
        }

        /* renamed from: b */
        public void mo7656b(C6394h<Void> hVar) {
            if (hVar.mo23043r()) {
                MainActivity mainActivity = MainActivity.this;
                mainActivity.f24476t0 = this.f24524a;
                mainActivity.m33743R0();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$l0 */
    class C7031l0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24526c;

        C7031l0(MainActivity mainActivity, Dialog dialog) {
            this.f24526c = dialog;
        }

        public void onClick(View view) {
            this.f24526c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$m */
    class C7032m implements Runnable {
        C7032m() {
        }

        public void run() {
            MainActivity.this.m33752h1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$m0 */
    class C7033m0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24528c;

        C7033m0(Dialog dialog) {
            this.f24528c = dialog;
        }

        public void onClick(View view) {
            this.f24528c.dismiss();
            MainActivity.this.mo24405b1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$n */
    class C7034n implements C6386c<Void> {

        /* renamed from: a */
        final /* synthetic */ C7562g f24530a;

        /* renamed from: b */
        final /* synthetic */ C7560e f24531b;

        C7034n(C7562g gVar, C7560e eVar) {
            this.f24530a = gVar;
            this.f24531b = eVar;
        }

        /* renamed from: b */
        public void mo7656b(C6394h<Void> hVar) {
            MainActivity.this.f24429F.mo24211a();
            if (hVar.mo23043r()) {
                this.f24530a.getSheetsList().add(this.f24531b);
                MainActivity.this.f24475s0.mo25641g();
                Intent intent = new Intent(MainActivity.this, ActivityBarcodeSheet.class);
                intent.putExtra("spreadsheet", this.f24531b);
                intent.putExtra("newSheet", "true");
                MainActivity.this.startActivityForResult(intent, 7);
                return;
            }
            Toast.makeText(MainActivity.this, "Failed To Create Sheet!", 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$n0 */
    class C7035n0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24533c;

        C7035n0(MainActivity mainActivity, Dialog dialog) {
            this.f24533c = dialog;
        }

        public void onClick(View view) {
            this.f24533c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$o */
    class C7036o implements C0671e {
        C7036o() {
        }

        /* renamed from: a */
        public void mo4215a(C0676g gVar) {
            MainActivity.this.m33754n1();
        }

        /* renamed from: b */
        public void mo4216b() {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$o0 */
    class C7037o0 implements Runnable {
        C7037o0() {
        }

        public void run() {
            MainActivity.this.m33752h1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$p */
    class C7038p implements C0684j {
        C7038p() {
        }

        /* renamed from: a */
        public void mo4244a(C0676g gVar, List<SkuDetails> list) {
            if (list != null) {
                for (SkuDetails a : list) {
                    MainActivity.this.f24482w0 = a.mo4185a();
                }
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$p0 */
    class C7039p0 implements C4549p {

        /* renamed from: com.spreadsheet.app.activities.MainActivity$p0$a */
        class C7040a implements C0671e {
            C7040a() {
            }

            /* renamed from: a */
            public void mo4215a(C0676g gVar) {
                MainActivity.this.mo24404a1();
            }

            /* renamed from: b */
            public void mo4216b() {
            }
        }

        C7039p0() {
        }

        /* renamed from: a */
        public void mo18610a(C4527b bVar) {
        }

        /* renamed from: b */
        public void mo18611b(C4524a aVar) {
            if (aVar.mo18572c()) {
                MainActivity.this.f24442L0 = (C7568m) aVar.mo18577h(C7568m.class);
                long longValue = Long.valueOf(MainActivity.this.f24442L0.getPurchaseDate()).longValue();
                Calendar instance = Calendar.getInstance();
                instance.setTimeInMillis(longValue);
                instance.add(2, 12);
                if (instance.getTime().after(Calendar.getInstance().getTime())) {
                    MainActivity.this.f24477u.mo24226g(C6873b.f24017j, true);
                } else {
                    MainActivity.this.f24477u.mo24226g(C6873b.f24017j, false);
                    MainActivity.this.mo24409i1();
                }
                MainActivity.this.mo24416r1();
                return;
            }
            MainActivity.this.f24484x0.mo4206g(new C7040a());
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$q */
    class C7041q implements C0671e {

        /* renamed from: a */
        final /* synthetic */ String f24539a;

        /* renamed from: com.spreadsheet.app.activities.MainActivity$q$a */
        class C7042a implements C0684j {
            C7042a() {
            }

            /* renamed from: a */
            public void mo4244a(C0676g gVar, List<SkuDetails> list) {
                for (SkuDetails z1 : list) {
                    MainActivity.this.mo24420z1(z1);
                }
            }
        }

        C7041q(String str) {
            this.f24539a = str;
        }

        /* renamed from: a */
        public void mo4215a(C0676g gVar) {
            if (gVar.mo4230a() == 0) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this.f24539a);
                C0681i.C0682a c = C0681i.m3985c();
                c.mo4241b(arrayList);
                c.mo4242c("subs");
                MainActivity.this.f24484x0.mo4205f(c.mo4240a(), new C7042a());
            }
        }

        /* renamed from: b */
        public void mo4216b() {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$q0 */
    class C7043q0 implements Runnable {

        /* renamed from: com.spreadsheet.app.activities.MainActivity$q0$a */
        class C7044a implements C4549p {

            /* renamed from: a */
            final /* synthetic */ String f24543a;

            C7044a(String str) {
                this.f24543a = str;
            }

            /* renamed from: a */
            public void mo18610a(C4527b bVar) {
            }

            /* renamed from: b */
            public void mo18611b(C4524a aVar) {
                if (!aVar.mo18572c()) {
                    C7559d dVar = new C7559d();
                    dVar.setOrderId(MainActivity.this.f24461e0.mo4171a());
                    dVar.setUserEmail(MainActivity.this.f24477u.mo24222b(C6873b.f24025p));
                    dVar.setUserId(MainActivity.this.f24477u.mo24222b(C6873b.f24024o));
                    dVar.setPurchaseToken(MainActivity.this.f24461e0.mo4175e());
                    dVar.setPurchaseDate(String.valueOf(MainActivity.this.f24461e0.mo4174d()));
                    dVar.setUpsheetPremiumActive(true);
                    MainActivity.this.f24423C.mo18590g(this.f24543a).mo18595k(dVar);
                }
            }
        }

        C7043q0() {
        }

        public void run() {
            Purchase.C0658a e = MainActivity.this.f24484x0.mo4204e("subs");
            if (e.mo4184c() == 0) {
                boolean z = false;
                for (Purchase next : e.mo4183b()) {
                    if (next.mo4178g().equals(C6873b.f24009f)) {
                        MainActivity.this.f24461e0 = next;
                        z = true;
                    }
                }
                if (z) {
                    Calendar instance = Calendar.getInstance();
                    MainActivity mainActivity = MainActivity.this;
                    mainActivity.f24477u.mo24227h(C6873b.f24011g, mainActivity.f24461e0.mo4171a());
                    MainActivity mainActivity2 = MainActivity.this;
                    mainActivity2.f24477u.mo24227h(C6873b.f24013h, mainActivity2.f24461e0.mo4175e());
                    MainActivity mainActivity3 = MainActivity.this;
                    mainActivity3.f24477u.mo24230k(C6873b.f24015i, mainActivity3.f24461e0.mo4174d());
                    MainActivity.this.f24477u.mo24226g(C6873b.f24017j, true);
                    instance.add(2, 12);
                    MainActivity.this.f24477u.mo24230k(C6873b.f24019k, instance.getTimeInMillis());
                    String replace = MainActivity.this.f24461e0.mo4171a().replace(".", "-");
                    MainActivity.this.f24423C.mo18590g(replace).mo18606b(new C7044a(replace));
                } else {
                    MainActivity.this.f24477u.mo24226g(C6873b.f24017j, false);
                }
                MainActivity.this.mo24416r1();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$r */
    class C7045r implements Runnable {
        C7045r() {
        }

        public void run() {
            int i;
            MenuItem menuItem;
            if (MainActivity.this.f24477u.mo24221a(C6873b.f24023n)) {
                MainActivity.this.navigationView.getMenu().findItem(R.id.menu_login).setTitle("Logout");
                menuItem = MainActivity.this.navigationView.getMenu().findItem(R.id.menu_login);
                i = R.drawable.logout;
            } else {
                MainActivity.this.navigationView.getMenu().findItem(R.id.menu_login).setTitle("Login");
                menuItem = MainActivity.this.navigationView.getMenu().findItem(R.id.menu_login);
                i = R.drawable.login;
            }
            menuItem.setIcon(i);
            MainActivity.this.mo24394F0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$r0 */
    class C7046r0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24546c;

        C7046r0(MainActivity mainActivity, Dialog dialog) {
            this.f24546c = dialog;
        }

        public void onClick(View view) {
            this.f24546c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$s */
    class C7047s implements View.OnClickListener {
        C7047s() {
        }

        public void onClick(View view) {
            MainActivity mainActivity;
            boolean z;
            if (MainActivity.this.f24447Q.getVisibility() == 0) {
                MainActivity.this.f24447Q.setVisibility(8);
                MainActivity.this.f24448R.setImageResource(R.drawable.ic_arrow_drop_down);
                mainActivity = MainActivity.this;
                z = true;
            } else {
                z = false;
                MainActivity.this.f24447Q.setVisibility(0);
                MainActivity.this.f24448R.setImageResource(R.drawable.ic_arrow_drop_up);
                mainActivity = MainActivity.this;
            }
            mainActivity.m33765y1(z);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$s0 */
    class C7048s0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24548c;

        C7048s0(Dialog dialog) {
            this.f24548c = dialog;
        }

        public void onClick(View view) {
            this.f24548c.dismiss();
            MainActivity.this.mo24410j1(C6873b.f24009f);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$t */
    class C7049t implements DialogInterface.OnClickListener {
        C7049t() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            MainActivity.this.mo24407f1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$t0 */
    class C7050t0 implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24551c;

        C7050t0(MainActivity mainActivity, Dialog dialog) {
            this.f24551c = dialog;
        }

        public void onClick(View view) {
            this.f24551c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$u */
    class C7051u implements DialogInterface.OnClickListener {
        C7051u(MainActivity mainActivity) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$u0 */
    class C7052u0 implements SearchView.C0182l {
        C7052u0() {
        }

        /* renamed from: a */
        public boolean mo1180a() {
            MainActivity.this.imageSearch.setVisibility(0);
            MainActivity.this.textSpreadsheet.setVisibility(0);
            MainActivity.this.searchView.setVisibility(8);
            return false;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$v */
    class C7053v implements Runnable {
        C7053v() {
        }

        public void run() {
            MainActivity.this.mo24411k1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$v0 */
    class C7054v0 implements SearchView.C0183m {
        C7054v0() {
        }

        /* renamed from: a */
        public boolean mo1181a(String str) {
            MainActivity.this.f24463g0.getFilter().filter(str);
            return false;
        }

        /* renamed from: b */
        public boolean mo1182b(String str) {
            return false;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$w */
    class C7055w implements Runnable {
        C7055w() {
        }

        public void run() {
            MainActivity.this.mo24412l1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$w0 */
    class C7056w0 implements C4549p {
        C7056w0() {
        }

        /* renamed from: a */
        public void mo18610a(C4527b bVar) {
        }

        /* renamed from: b */
        public void mo18611b(C4524a aVar) {
            if (aVar != null && aVar.mo18572c()) {
                String str = (String) aVar.mo18571b("AlertMessage").mo18576g();
                String str2 = (String) aVar.mo18571b("RedirectPackage").mo18576g();
                if (!str.equals("") && !str2.equals("")) {
                    MainActivity.this.m33760v1(str, str2);
                }
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$x */
    class C7057x implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ C7560e f24557c;

        C7057x(C7560e eVar) {
            this.f24557c = eVar;
        }

        public void onClick(View view) {
            MainActivity mainActivity;
            String str;
            String obj = MainActivity.this.f24428E0.getText().toString();
            if (obj.equalsIgnoreCase("")) {
                mainActivity = MainActivity.this;
                str = "Enter email address";
            } else if (!MainActivity.this.mo24398L0(obj)) {
                MainActivity.this.mo24392E0(this.f24557c, obj);
                return;
            } else {
                mainActivity = MainActivity.this;
                str = "Email already has access to this spreadsheet";
            }
            Toast.makeText(mainActivity, str, 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$x0 */
    class C7058x0 implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ String f24559c;

        C7058x0(String str) {
            this.f24559c = str;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.f24559c));
                intent.addFlags(268435456);
                MainActivity.this.startActivity(intent);
            } catch (ActivityNotFoundException unused) {
                MainActivity mainActivity = MainActivity.this;
                MainActivity.m33736F1(mainActivity, "https://play.google.com/store/apps/details?id=" + this.f24559c);
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$y */
    class C7059y implements View.OnClickListener {
        C7059y() {
        }

        public void onClick(View view) {
            MainActivity.this.mo24417s1();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity$z */
    class C7060z implements View.OnClickListener {
        C7060z() {
        }

        public void onClick(View view) {
            ((ClipboardManager) MainActivity.this.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("", "https://docs.google.com/spreadsheets/d/" + MainActivity.this.f24480v0 + "/edit?usp=sharing"));
            Toast.makeText(MainActivity.this, "Link copied to clipboard", 0).show();
        }
    }

    public MainActivity() {
        C7581a.m35508c();
        this.f24466j0 = C7582b.m35511n();
        this.f24467k0 = "";
        this.f24468l0 = 0;
        this.f24469m0 = "";
        new ArrayList();
        new ArrayList();
        C7556a.getInstance();
        this.f24471o0 = new ArrayList();
        this.f24472p0 = false;
        this.f24473q0 = new ArrayList();
        this.f24475s0 = C7591e.m35564f();
        this.f24480v0 = "";
        this.f24482w0 = "";
        this.f24486y0 = C7595f.m35578f();
        this.f24420A0 = new ArrayList<>();
        this.f24422B0 = new ArrayList<>();
    }

    /* renamed from: A1 */
    private void m33728A1() {
        Calendar instance = Calendar.getInstance();
        String str = instance.getTimeInMillis() + "";
        int size = this.f24435I.size();
        this.f24459c0 = size;
        this.f24458b0.setColCount(size);
        C7560e eVar = new C7560e(this.f24467k0, this.f24469m0, this.f24459c0, str, "0", "Contact", false, "", "", "");
        this.f24487z.mo18590g(this.f24481w).mo18590g(this.f24467k0).mo18595k(eVar).mo23027b(new C7028k(eVar));
    }

    /* renamed from: B1 */
    private void m33730B1() {
        String str = this.f24481w;
        if (str != null) {
            this.f24485y.mo18590g(str).mo18590g("deviceToken").mo18595k(this.f24477u.mo24222b(C6873b.f24033x));
            Calendar instance = Calendar.getInstance();
            C4529d g = this.f24485y.mo18590g(this.f24481w).mo18590g("lastVisitedDate");
            g.mo18595k(instance.getTimeInMillis() + "");
        }
    }

    /* renamed from: C1 */
    private void m33732C1() {
        String str;
        if (this.f24477u.mo24221a(C6873b.f24023n) && !this.f24477u.mo24222b(C6873b.f24031v).equals("") && (str = this.f24481w) != null) {
            this.f24485y.mo18590g(str).mo18590g("refreshToken").mo18595k(this.f24477u.mo24222b(C6873b.f24031v));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: D0 */
    public void m33733D0(Purchase purchase) {
        CustomTextBold customTextBold;
        Calendar instance = Calendar.getInstance();
        this.f24477u.mo24227h(C6873b.f24011g, purchase.mo4171a());
        this.f24477u.mo24227h(C6873b.f24013h, purchase.mo4175e());
        this.f24477u.mo24230k(C6873b.f24015i, purchase.mo4174d());
        this.f24477u.mo24226g(C6873b.f24017j, true);
        instance.add(2, 12);
        this.f24477u.mo24230k(C6873b.f24019k, instance.getTimeInMillis());
        int i = 0;
        this.toolbarHomeTitlePremium.setVisibility(0);
        if (this.f24477u.mo24221a(C6873b.f24017j)) {
            customTextBold = this.toolbarHomeGoPremium;
            i = 8;
        } else {
            customTextBold = this.toolbarHomeGoPremium;
        }
        customTextBold.setVisibility(i);
        C7568m mVar = new C7568m();
        mVar.setOrderId(purchase.mo4171a());
        mVar.setUserId(this.f24477u.mo24222b(C6873b.f24024o));
        mVar.setPurchaseToken(purchase.mo4175e());
        mVar.setPurchaseDate(String.valueOf(purchase.mo4174d()));
        mVar.setUpsheetPremiumActive(true);
        C7559d dVar = new C7559d();
        dVar.setOrderId(purchase.mo4171a());
        dVar.setUserEmail(this.f24477u.mo24222b(C6873b.f24025p));
        dVar.setUserId(this.f24477u.mo24222b(C6873b.f24024o));
        dVar.setPurchaseToken(purchase.mo4175e());
        dVar.setPurchaseDate(String.valueOf(purchase.mo4174d()));
        dVar.setUpsheetPremiumActive(true);
        if (this.f24477u.mo24222b(C6873b.f24024o) != null) {
            this.f24421B.mo18590g(this.f24477u.mo24222b(C6873b.f24024o)).mo18595k(mVar);
        }
        this.f24423C.mo18590g(purchase.mo4171a().replace(".", "-")).mo18595k(dVar);
        m33758u1();
    }

    /* renamed from: D1 */
    private void m33734D1() {
        if (this.f24477u.mo24223d(C6873b.f24034y) != this.f24425D.size()) {
            this.f24477u.mo24228i(C6873b.f24034y, this.f24425D.size());
            String str = this.f24481w;
            if (str != null) {
                C4529d g = this.f24485y.mo18590g(str).mo18590g("sheetCount");
                g.mo18595k(this.f24425D.size() + "");
            }
        }
    }

    /* renamed from: E1 */
    private void m33735E1() {
        Calendar instance = Calendar.getInstance();
        String str = instance.getTimeInMillis() + "";
        int size = this.f24460d0.size();
        this.f24459c0 = size;
        this.f24458b0.setColCount(size);
        C7560e eVar = new C7560e(this.f24456Z, this.f24462f0, this.f24459c0, str, "0", "ToDo", false, "", "", "");
        this.f24487z.mo18590g(this.f24481w).mo18590g(this.f24456Z).mo18595k(eVar).mo23027b(new C7030l(eVar));
    }

    /* renamed from: F1 */
    public static void m33736F1(Context context, String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /* renamed from: I0 */
    private void m33737I0() {
        this.f24419A.mo18606b(new C7056w0());
    }

    /* renamed from: J0 */
    private void m33738J0() {
        int d = this.f24477u.mo24223d("dayCount");
        if (d == 2 && d == 6) {
            if (!this.f24477u.mo24221a("feedback") && !this.f24477u.mo24221a("isFeedback")) {
                this.f24477u.mo24226g("isFeedback", true);
                m33762w1();
            }
        } else if ((d == 4 || d == 8 || d == 10) && !this.f24477u.mo24221a("rateUs") && !this.f24477u.mo24221a("isRate")) {
            this.f24477u.mo24226g("isRate", true);
            m33764x1();
        }
    }

    /* renamed from: N0 */
    private void m33739N0() {
        C7562g instance = C7562g.getInstance();
        this.f24433H.mo25638c("CREATE_SPREADSHEET", "CREATE_SPREADSHEET");
        Calendar instance2 = Calendar.getInstance();
        C7560e eVar = new C7560e(instance.spreadsheetId, instance.spreadSheetTitle, 3, instance2.getTimeInMillis() + "", "0", "Barcode", false, "", "", "");
        this.f24487z.mo18590g(this.f24481w).mo18590g(instance.spreadsheetId).mo18595k(eVar).mo23027b(new C7034n(instance, eVar));
    }

    /* access modifiers changed from: private */
    /* renamed from: O0 */
    public void m33740O0() {
        this.f24470n0 = true;
        C7582b bVar = this.f24466j0;
        String str = this.f24467k0;
        bVar.mo25604d(str, "sheet1!A1:E1", bVar.mo25609i(str, "sheet1!A1:E1", this.f24435I));
    }

    /* renamed from: P0 */
    private void m33741P0(String str) {
        this.f24429F.mo24213d("");
        C7582b bVar = this.f24466j0;
        bVar.mo25605e(bVar.mo25611k(str));
    }

    /* renamed from: Q0 */
    private void m33742Q0(String str) {
        this.f24429F.mo24213d("");
        C7597h hVar = this.f24464h0;
        hVar.mo25691d(hVar.mo25698k(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: R0 */
    public void m33743R0() {
        m33747Y0("sheet1");
        C7597h hVar = this.f24464h0;
        hVar.mo25694g(this.f24456Z, hVar.mo25701n(this.f24457a0, this.f24460d0));
    }

    /* renamed from: T0 */
    private void m33744T0() {
        this.f24429F.mo24213d("");
        this.f24466j0.mo25610j(this.f24467k0, "Sheet1!A1:E");
    }

    /* renamed from: U0 */
    private void m33745U0() {
        C7582b bVar = this.f24466j0;
        bVar.mo25606f(this.f24467k0, bVar.mo25612l(this.f24468l0));
    }

    /* renamed from: V0 */
    private void m33746V0() {
        C7582b bVar = this.f24466j0;
        bVar.mo25607g(this.f24467k0, bVar.mo25613m(this.f24468l0));
    }

    /* renamed from: Y0 */
    private String m33747Y0(String str) {
        String str2;
        StringBuilder sb;
        switch (this.f24460d0.size()) {
            case 2:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:B1";
                break;
            case 3:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:C1";
                break;
            case 4:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:D1";
                break;
            case 5:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:E1";
                break;
            case 6:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:F1";
                break;
            case 7:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:G1";
                break;
            case 8:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:H1";
                break;
            case 9:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:I1";
                break;
            case 10:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:J1";
                break;
            default:
                return "";
        }
        sb.append(str2);
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: Z0 */
    public void m33748Z0() {
        if (this.f24427E.mo24207d()) {
            this.f24475s0.mo25640e();
            return;
        }
        this.layoutProgress.setVisibility(8);
        this.f24429F.mo24211a();
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
        this.layoutTryAgain.setVisibility(0);
    }

    /* renamed from: c1 */
    private void m33749c1(Purchase purchase) {
        C7027j0 j0Var = new C7027j0(purchase);
        if (purchase.mo4173c() != 1) {
            return;
        }
        if (!purchase.mo4179h()) {
            C0660a.C0661a b = C0660a.m3901b();
            b.mo4198b(purchase.mo4175e());
            this.f24484x0.mo4201a(b.mo4197a(), j0Var);
            return;
        }
        m33733D0(purchase);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x028a  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x02f5  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x035d  */
    /* renamed from: d1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m33750d1() {
        /*
            r9 = this;
            e.c.a.a.a r0 = new e.c.a.a.a
            r0.<init>(r9)
            r9.f24474r0 = r0
            e.c.a.a.m.b r1 = p153e.p168c.p169a.p170a.p171m.C5344b.DIALOG
            r0.mo20960A(r1)
            e.c.a.a.a r0 = r9.f24474r0
            e.c.a.a.m.d r1 = p153e.p168c.p169a.p170a.p171m.C5346d.GOOGLE_PLAY
            r0.mo20961B(r1)
            e.c.a.a.a r0 = r9.f24474r0
            java.lang.String r1 = ""
            r0.mo20963z(r1)
            com.google.firebase.auth.FirebaseAuth r0 = com.google.firebase.auth.FirebaseAuth.getInstance()
            r9.f24483x = r0
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23978F
            com.google.firebase.database.d r0 = r0.mo18599d(r2)
            r9.f24487z = r0
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23979G
            r0.mo18599d(r2)
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23976D
            com.google.firebase.database.d r0 = r0.mo18599d(r2)
            r9.f24485y = r0
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23977E
            com.google.firebase.database.d r0 = r0.mo18599d(r2)
            r9.f24419A = r0
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23981I
            r0.mo18599d(r2)
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23982J
            com.google.firebase.database.d r0 = r0.mo18599d(r2)
            r9.f24421B = r0
            com.google.firebase.database.g r0 = com.google.firebase.database.C4534g.m24100b()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23983K
            com.google.firebase.database.d r0 = r0.mo18599d(r2)
            r9.f24423C = r0
            e.e.a.d.d r0 = r9.f24433H
            r0.mo25637b(r9)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            r0.setTitle((java.lang.CharSequence) r1)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            android.content.res.Resources r2 = r9.getResources()
            r3 = 2131099968(0x7f060140, float:1.7812304E38)
            int r2 = r2.getColor(r3)
            r0.setTitleTextColor((int) r2)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            android.content.res.Resources r2 = r9.getResources()
            r3 = 2131099976(0x7f060148, float:1.781232E38)
            int r2 = r2.getColor(r3)
            r0.setBackgroundColor(r2)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            r9.mo276r0(r0)
            android.widget.ImageView r0 = r9.imageSearch
            r2 = 8
            r0.setVisibility(r2)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            r3 = 2131230926(0x7f0800ce, float:1.8077919E38)
            r0.setNavigationIcon((int) r3)
            androidx.appcompat.widget.Toolbar r0 = r9.toolbarHome
            com.spreadsheet.app.activities.MainActivity$i r3 = new com.spreadsheet.app.activities.MainActivity$i
            r3.<init>()
            r0.setNavigationOnClickListener(r3)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r3 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r0 = r0.mo24221a(r3)
            r3 = 0
            if (r0 == 0) goto L_0x00dc
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r4 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r0 = r0.mo24221a(r4)
            if (r0 == 0) goto L_0x00d6
            com.spreadsheet.app.Utils.Fonts.CustomTextBold r0 = r9.toolbarHomeGoPremium
            r0.setVisibility(r2)
            com.spreadsheet.app.Utils.Fonts.CustomTextBold r0 = r9.toolbarHomeTitlePremium
            r0.setVisibility(r3)
            goto L_0x00e6
        L_0x00d6:
            com.spreadsheet.app.Utils.Fonts.CustomTextBold r0 = r9.toolbarHomeGoPremium
            r0.setVisibility(r3)
            goto L_0x00e1
        L_0x00dc:
            com.spreadsheet.app.Utils.Fonts.CustomTextBold r0 = r9.toolbarHomeGoPremium
            r0.setVisibility(r2)
        L_0x00e1:
            com.spreadsheet.app.Utils.Fonts.CustomTextBold r0 = r9.toolbarHomeTitlePremium
            r0.setVisibility(r2)
        L_0x00e6:
            com.google.android.material.navigation.NavigationView r0 = r9.navigationView
            r0.setNavigationItemSelectedListener(r9)
            com.google.android.material.navigation.NavigationView r0 = r9.navigationView
            android.view.View r0 = r0.mo14296f(r3)
            r4 = 2131296875(0x7f09026b, float:1.821168E38)
            android.view.View r4 = r0.findViewById(r4)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r4 = (com.spreadsheet.app.Utils.Fonts.CustomTextView) r4
            r9.f24443M = r4
            r4 = 2131296874(0x7f09026a, float:1.8211677E38)
            android.view.View r4 = r0.findViewById(r4)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r4 = (com.spreadsheet.app.Utils.Fonts.CustomTextView) r4
            r9.f24444N = r4
            r4 = 2131296545(0x7f090121, float:1.821101E38)
            android.view.View r4 = r0.findViewById(r4)
            com.mikhaellopez.circularimageview.CircularImageView r4 = (com.mikhaellopez.circularimageview.CircularImageView) r4
            r9.f24441L = r4
            r4 = 2131296557(0x7f09012d, float:1.8211034E38)
            android.view.View r4 = r0.findViewById(r4)
            androidx.constraintlayout.widget.ConstraintLayout r4 = (androidx.constraintlayout.widget.ConstraintLayout) r4
            r9.f24447Q = r4
            r4 = 2131296558(0x7f09012e, float:1.8211036E38)
            android.view.View r4 = r0.findViewById(r4)
            androidx.constraintlayout.widget.ConstraintLayout r4 = (androidx.constraintlayout.widget.ConstraintLayout) r4
            r9.f24446P = r4
            r4 = 2131296598(0x7f090156, float:1.8211117E38)
            android.view.View r4 = r0.findViewById(r4)
            androidx.constraintlayout.widget.ConstraintLayout r4 = (androidx.constraintlayout.widget.ConstraintLayout) r4
            r9.f24445O = r4
            r4 = 2131296521(0x7f090109, float:1.8210961E38)
            android.view.View r4 = r0.findViewById(r4)
            android.widget.ImageView r4 = (android.widget.ImageView) r4
            r9.f24448R = r4
            r4 = 2131296725(0x7f0901d5, float:1.8211375E38)
            android.view.View r0 = r0.findViewById(r4)
            androidx.recyclerview.widget.RecyclerView r0 = (androidx.recyclerview.widget.RecyclerView) r0
            r9.f24449S = r0
            androidx.recyclerview.widget.LinearLayoutManager r0 = new androidx.recyclerview.widget.LinearLayoutManager
            r4 = 1
            r0.<init>(r9, r4, r3)
            androidx.recyclerview.widget.RecyclerView r5 = r9.f24449S
            r5.setLayoutManager(r0)
            androidx.constraintlayout.widget.ConstraintLayout r0 = r9.f24445O
            com.spreadsheet.app.activities.MainActivity$s r5 = new com.spreadsheet.app.activities.MainActivity$s
            r5.<init>()
            r0.setOnClickListener(r5)
            androidx.constraintlayout.widget.ConstraintLayout r0 = r9.f24446P
            com.spreadsheet.app.activities.MainActivity$d0 r5 = new com.spreadsheet.app.activities.MainActivity$d0
            r5.<init>()
            r0.setOnClickListener(r5)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r9.f24435I = r0
            java.lang.String r5 = "Contact Name"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24435I
            java.lang.String r5 = "Contact No1"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24435I
            java.lang.String r5 = "Contact No2"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24435I
            java.lang.String r5 = "Contact No3"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24435I
            java.lang.String r5 = "Email"
            r0.add(r5)
            java.util.ArrayList<java.lang.String> r0 = r9.f24420A0
            java.lang.String r5 = "Restricted access"
            r0.add(r5)
            java.util.ArrayList<java.lang.String> r0 = r9.f24420A0
            java.lang.String r5 = "Anyone with link"
            r0.add(r5)
            java.util.ArrayList<java.lang.String> r0 = r9.f24422B0
            java.lang.String r5 = "Viewer"
            r0.add(r5)
            java.util.ArrayList<java.lang.String> r0 = r9.f24422B0
            java.lang.String r5 = "Editor"
            r0.add(r5)
            android.app.Dialog r0 = new android.app.Dialog
            r0.<init>(r9)
            r9.f24450T = r0
            r0.requestWindowFeature(r4)
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131492929(0x7f0c0041, float:1.8609324E38)
            r0.setContentView(r5)
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131296464(0x7f0900d0, float:1.8210845E38)
            android.view.View r0 = r0.findViewById(r5)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f24451U = r0
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131296511(0x7f0900ff, float:1.821094E38)
            android.view.View r0 = r0.findViewById(r5)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r9.f24454X = r0
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131296354(0x7f090062, float:1.8210622E38)
            android.view.View r0 = r0.findViewById(r5)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.f24455Y = r0
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131296463(0x7f0900cf, float:1.8210843E38)
            android.view.View r0 = r0.findViewById(r5)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r0 = (com.spreadsheet.app.Utils.Fonts.CustomTextView) r0
            r9.f24452V = r0
            android.app.Dialog r0 = r9.f24450T
            r5 = 2131296907(0x7f09028b, float:1.8211744E38)
            android.view.View r0 = r0.findViewById(r5)
            com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold r0 = (com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold) r0
            r9.f24453W = r0
            com.spreadsheet.app.Utils.Fonts.CustomTextView r0 = r9.f24452V
            r0.setVisibility(r2)
            android.widget.Button r0 = r9.f24455Y
            r0.setOnClickListener(r9)
            android.widget.Button r0 = r9.f24455Y
            r0.setOnClickListener(r9)
            android.widget.ImageView r0 = r9.f24454X
            r0.setOnClickListener(r9)
            e.e.a.a.e r0 = new e.e.a.a.e
            r0.<init>()
            r9.f24458b0 = r0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r9.f24460d0 = r0
            java.lang.String r5 = "Status"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24460d0
            java.lang.String r5 = "Date"
            r0.add(r5)
            java.util.List<java.lang.String> r0 = r9.f24460d0
            java.lang.String r5 = "Task"
            r0.add(r5)
            androidx.recyclerview.widget.LinearLayoutManager r0 = new androidx.recyclerview.widget.LinearLayoutManager
            r0.<init>(r9, r4, r3)
            androidx.recyclerview.widget.RecyclerView r5 = r9.recyclerviewSheets
            r5.setLayoutManager(r0)
            e.e.a.a.n r0 = new e.e.a.a.n
            r0.<init>()
            e.e.a.d.b r0 = r9.f24466j0
            r0.mo25614o(r9, r9)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            r0.mo24225f(r9)
            e.e.a.d.f r0 = r9.f24486y0
            r0.mo25655k(r9, r9)
            e.e.a.d.c r0 = r9.f24479v
            r0.mo25619g(r9, r9)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r0 = r0.mo24221a(r5)
            if (r0 == 0) goto L_0x028a
            com.google.firebase.auth.FirebaseAuth r0 = r9.f24483x
            java.lang.String r0 = r0.mo17659d()
            r9.f24481w = r0
            r9.m33753m1()
            android.widget.RelativeLayout r0 = r9.layoutProgress
            r0.setVisibility(r3)
            r9.m33748Z0()
            com.google.firebase.messaging.FirebaseMessaging r0 = com.google.firebase.messaging.FirebaseMessaging.m32962a()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23985M
            r0.mo23833b(r2)
            com.google.firebase.messaging.FirebaseMessaging r0 = com.google.firebase.messaging.FirebaseMessaging.m32962a()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23984L
            r0.mo23833b(r2)
            r9.m33730B1()
            goto L_0x02af
        L_0x028a:
            android.widget.RelativeLayout r0 = r9.layoutProgress
            r0.setVisibility(r2)
            androidx.constraintlayout.widget.ConstraintLayout r0 = r9.layoutEmptyScreen
            r0.setVisibility(r3)
            com.mikhaellopez.circularimageview.CircularImageView r0 = r9.f24441L
            r5 = 2131230815(0x7f08005f, float:1.8077693E38)
            r0.setImageResource(r5)
            androidx.constraintlayout.widget.ConstraintLayout r0 = r9.f24445O
            r0.setVisibility(r2)
            com.mikhaellopez.circularimageview.CircularImageView r0 = r9.f24441L
            r0.setVisibility(r3)
            com.google.firebase.messaging.FirebaseMessaging r0 = com.google.firebase.messaging.FirebaseMessaging.m32962a()
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f23984L
            r0.mo23833b(r2)
        L_0x02af:
            r9.mo24403X0()
            r9.mo24397K0()
            r9.mo24394F0()
            r9.mo24396H0()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r5 = 5
            int r5 = r0.get(r5)
            r2.append(r5)
            java.lang.String r5 = "-"
            r2.append(r5)
            r6 = 2
            int r6 = r0.get(r6)
            r2.append(r6)
            r2.append(r5)
            int r0 = r0.get(r4)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.spreadsheet.app.Utils.h r2 = r9.f24477u
            java.lang.String r5 = "lastDate"
            java.lang.String r2 = r2.mo24222b(r5)
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0316
            com.spreadsheet.app.Utils.h r2 = r9.f24477u
            java.lang.String r6 = "dayCount"
            int r2 = r2.mo24223d(r6)
            int r2 = r2 + r4
            com.spreadsheet.app.Utils.h r7 = r9.f24477u
            r7.mo24228i(r6, r2)
            com.spreadsheet.app.Utils.h r2 = r9.f24477u
            r2.mo24227h(r5, r0)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = "isRate"
            r0.mo24226g(r2, r3)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = "isFeedback"
            r0.mo24226g(r2, r3)
        L_0x0316:
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r0 = r0.mo24221a(r2)
            if (r0 == 0) goto L_0x0353
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24031v
            java.lang.String r0 = r0.mo24222b(r2)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0353
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24035z
            long r5 = r0.mo24224e(r2)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24032w
            long r7 = r0.mo24224e(r2)
            boolean r0 = com.spreadsheet.app.Utils.C6873b.m33480r(r5, r7)
            if (r0 == 0) goto L_0x0353
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            com.spreadsheet.app.activities.MainActivity$o0 r2 = new com.spreadsheet.app.activities.MainActivity$o0
            r2.<init>()
            r5 = 3000(0xbb8, double:1.482E-320)
            r0.postDelayed(r2, r5)
        L_0x0353:
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r0 = r0.mo24221a(r2)
            if (r0 == 0) goto L_0x0360
            r9.m33737I0()
        L_0x0360:
            r9.mo24399M0()
            r9.mo24406e1()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.util.Date r0 = r0.getTime()
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r5 = "dd-MM-yyyy"
            r2.<init>(r5)
            java.lang.String r0 = r2.format(r0)
            com.spreadsheet.app.Utils.h r2 = r9.f24477u
            java.lang.String r5 = "adsDate"
            java.lang.String r2 = r2.mo24222b(r5)
            boolean r1 = r2.equalsIgnoreCase(r1)
            java.lang.String r2 = "removeAdsShown"
            if (r1 == 0) goto L_0x0394
        L_0x0389:
            com.spreadsheet.app.Utils.h r1 = r9.f24477u
            r1.mo24227h(r5, r0)
            com.spreadsheet.app.Utils.h r0 = r9.f24477u
            r0.mo24226g(r2, r3)
            goto L_0x03a0
        L_0x0394:
            com.spreadsheet.app.Utils.h r1 = r9.f24477u
            java.lang.String r1 = r1.mo24222b(r5)
            boolean r1 = r1.equalsIgnoreCase(r0)
            if (r1 == 0) goto L_0x0389
        L_0x03a0:
            android.app.Dialog r0 = new android.app.Dialog
            r0.<init>(r9)
            r9.f24430F0 = r0
            r0.requestWindowFeature(r4)
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131492943(0x7f0c004f, float:1.8609352E38)
            r0.setContentView(r1)
            android.app.Dialog r0 = r9.f24430F0
            android.view.Window r0 = r0.getWindow()
            r1 = -1
            r2 = -2
            r0.setLayout(r1, r2)
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296364(0x7f09006c, float:1.8210643E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.f24434H0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296365(0x7f09006d, float:1.8210645E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.f24436I0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296355(0x7f090063, float:1.8210624E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.Button r0 = (android.widget.Button) r0
            r9.f24438J0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296892(0x7f09027c, float:1.8211714E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r9.f24432G0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296518(0x7f090106, float:1.8210955E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r9.f24440K0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296890(0x7f09027a, float:1.821171E38)
            android.view.View r0 = r0.findViewById(r1)
            android.widget.EditText r0 = (android.widget.EditText) r0
            r9.f24428E0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296784(0x7f090210, float:1.8211494E38)
            android.view.View r0 = r0.findViewById(r1)
            androidx.appcompat.widget.AppCompatSpinner r0 = (androidx.appcompat.widget.AppCompatSpinner) r0
            r9.f24424C0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296785(0x7f090211, float:1.8211496E38)
            android.view.View r0 = r0.findViewById(r1)
            androidx.appcompat.widget.AppCompatSpinner r0 = (androidx.appcompat.widget.AppCompatSpinner) r0
            r9.f24426D0 = r0
            android.app.Dialog r0 = r9.f24430F0
            r1 = 2131296727(0x7f0901d7, float:1.8211379E38)
            android.view.View r0 = r0.findViewById(r1)
            androidx.recyclerview.widget.RecyclerView r0 = (androidx.recyclerview.widget.RecyclerView) r0
            r9.f24488z0 = r0
            androidx.appcompat.widget.SearchView r0 = r9.searchView
            com.spreadsheet.app.activities.MainActivity$u0 r1 = new com.spreadsheet.app.activities.MainActivity$u0
            r1.<init>()
            r0.setOnCloseListener(r1)
            androidx.appcompat.widget.SearchView r0 = r9.searchView
            com.spreadsheet.app.activities.MainActivity$v0 r1 = new com.spreadsheet.app.activities.MainActivity$v0
            r1.<init>()
            r0.setOnQueryTextListener(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.MainActivity.m33750d1():void");
    }

    /* renamed from: g1 */
    private void m33751g1() {
        if (!this.f24427E.mo24205b(this.f24465i0)) {
            this.f24427E.mo24208e(this.f24465i0);
        } else if (!this.f24427E.mo24207d()) {
            Toast.makeText(this, R.string.internet_check, 0).show();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: h1 */
    public void m33752h1() {
        this.f24429F.mo24211a();
        HashMap hashMap = new HashMap();
        hashMap.put("name", this.f24477u.mo24222b(C6873b.f24027r) + " " + this.f24477u.mo24222b(C6873b.f24028s));
        hashMap.put("email", this.f24477u.mo24222b(C6873b.f24025p));
        hashMap.put("photoUrl", this.f24477u.mo24222b(C6873b.f24029t));
        mo24408g(hashMap);
    }

    /* renamed from: m1 */
    private void m33753m1() {
        this.f24445O.setVisibility(0);
        this.f24443M.setText(this.f24477u.mo24222b(C6873b.f24027r) + " " + this.f24477u.mo24222b(C6873b.f24028s));
        this.f24444N.setText(this.f24477u.mo24222b(C6873b.f24025p));
        if (!this.f24477u.mo24222b(C6873b.f24029t).equals("")) {
            try {
                C7838x j = C7822t.m36816o(this).mo26286j(this.f24477u.mo24222b(C6873b.f24029t));
                j.mo26320f(R.drawable.app_logo_white);
                j.mo26317b(R.drawable.app_logo_white);
                j.mo26318d(this.f24441L);
            } catch (Exception unused) {
            }
        } else {
            Paint paint = new Paint();
            paint.setColor(getResources().getColor(R.color.colorPrimary));
            paint.setStyle(Paint.Style.FILL);
            paint.setFlags(1);
            Paint paint2 = new Paint();
            paint2.setColor(-1);
            paint2.setStyle(Paint.Style.FILL);
            String substring = this.f24477u.mo24222b(C6873b.f24027r).substring(0, 1);
            int dimension = (int) getResources().getDimension(R.dimen.dp_60);
            int dimension2 = (int) getResources().getDimension(R.dimen.dp_40);
            paint.setTextSize((float) dimension2);
            Bitmap createBitmap = Bitmap.createBitmap(dimension, (int) getResources().getDimension(R.dimen.dp_60), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            canvas.drawPaint(paint2);
            canvas.drawText(substring, (float) C6873b.m33467e(substring, dimension2, dimension), (float) ((int) (((float) (canvas.getHeight() / 2)) - ((paint.descent() + paint.ascent()) / 2.0f))), paint);
            this.f24441L.setImageBitmap(createBitmap);
        }
        this.f24444N.setVisibility(0);
        this.f24443M.setVisibility(0);
        this.f24441L.setVisibility(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: n1 */
    public void m33754n1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(C6873b.f24009f);
        C0681i.C0682a c = C0681i.m3985c();
        c.mo4241b(arrayList);
        c.mo4242c("subs");
        this.f24484x0.mo4205f(c.mo4240a(), new C7038p());
    }

    /* renamed from: o1 */
    private void m33755o1() {
        int i;
        RecyclerView recyclerView;
        if (this.f24473q0.size() > 0) {
            this.f24449S.setAdapter(new C7077a(this, this.f24473q0, this));
            recyclerView = this.f24449S;
            i = 0;
        } else {
            recyclerView = this.f24449S;
            i = 8;
        }
        recyclerView.setVisibility(i);
    }

    /* renamed from: q1 */
    private void m33756q1() {
        int i;
        ImageView imageView;
        m33738J0();
        m33734D1();
        m33732C1();
        C7099f fVar = new C7099f(this, this.f24425D, this);
        this.f24463g0 = fVar;
        this.recyclerviewSheets.setAdapter(fVar);
        if (this.f24425D.size() > 0) {
            imageView = this.imageSearch;
            i = 0;
        } else {
            imageView = this.imageSearch;
            i = 8;
        }
        imageView.setVisibility(i);
    }

    /* renamed from: u1 */
    private void m33758u1() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_premium_success);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((Button) dialog.findViewById(R.id.button_premium_success_continue)).setOnClickListener(new C7029k0(this, dialog));
    }

    /* access modifiers changed from: private */
    /* renamed from: v1 */
    public void m33760v1(String str, String str2) {
        C0065b.C0066a aVar = new C0065b.C0066a(this);
        aVar.mo236g(str);
        aVar.mo240k("Update", new C7058x0(str2));
        aVar.mo233d(false);
        aVar.mo243n();
    }

    /* renamed from: w1 */
    private void m33762w1() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_send_feedback);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_feedback_ratings)).setOnClickListener(new C7008a(dialog));
        ((TextView) dialog.findViewById(R.id.text_feedback)).setOnClickListener(new C7010b(dialog));
        ((TextView) dialog.findViewById(R.id.text_feedback_cancel)).setOnClickListener(new C7012c(this, dialog));
    }

    /* renamed from: x1 */
    private void m33764x1() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_rate_us);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_rate_us)).setOnClickListener(new C7014d(dialog));
        ((TextView) dialog.findViewById(R.id.text_rate_us_feedback)).setOnClickListener(new C7016e(dialog));
        ((TextView) dialog.findViewById(R.id.text_rate_us_cancel)).setOnClickListener(new C7018f(this, dialog));
    }

    /* access modifiers changed from: private */
    /* renamed from: y1 */
    public void m33765y1(boolean z) {
        int i;
        MenuItem menuItem;
        for (int i2 = 0; i2 < this.navigationView.getMenu().size(); i2++) {
            MenuItem item = this.navigationView.getMenu().getItem(i2);
            if (z) {
                if (item.getItemId() == R.id.menu_login) {
                    if (!this.f24477u.mo24221a(C6873b.f24023n)) {
                        this.navigationView.getMenu().getItem(i2).setTitle("Login");
                        menuItem = this.navigationView.getMenu().getItem(i2);
                        i = R.drawable.login;
                    } else {
                        this.navigationView.getMenu().getItem(i2).setTitle("Logout");
                        menuItem = this.navigationView.getMenu().getItem(i2);
                        i = R.drawable.logout;
                    }
                    menuItem.setIcon(i);
                    mo24394F0();
                }
                this.navigationView.getMenu().getItem(i2).setVisible(true);
            } else {
                item.setVisible(false);
            }
        }
    }

    /* renamed from: B */
    public void mo4235B(C0676g gVar, List<Purchase> list) {
        if (gVar.mo4230a() != 0 || list == null) {
            Toast.makeText(this, gVar.mo4230a() == 1 ? "Purchase Cancelled" : "Error processing subscription.", 0).show();
            return;
        }
        for (Purchase c1 : list) {
            m33749c1(c1);
        }
    }

    /* renamed from: C */
    public void mo24391C(String str, String str2) {
        this.f24486y0.mo25655k(this, this);
        this.f24429F.mo24213d("");
        C7595f fVar = this.f24486y0;
        fVar.mo25659o(this.f24480v0, str2, fVar.mo25654j(str));
    }

    /* renamed from: E0 */
    public void mo24392E0(C7560e eVar, String str) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_add_access);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((ImageView) dialog.findViewById(R.id.image_close_access_diaLog)).setOnClickListener(new C7011b0(this, dialog));
        ((Button) dialog.findViewById(R.id.button_share_add_access)).setOnClickListener(new C7013c0(dialog, (RadioGroup) dialog.findViewById(R.id.radiogroup_access), eVar, str));
    }

    /* renamed from: F */
    public void mo24393F(C7560e eVar) {
        int i;
        Intent intent;
        if (eVar.getSheetType() == null) {
            mo24415p1(eVar);
            return;
        }
        if (eVar.getSheetType().equals("CustomPro") || eVar.getSheetType().equals("Custom") || eVar.getSheetType().equals("customized spreadsheet")) {
            intent = new Intent(this, ActivitySheetDetails.class);
            intent.putExtra("spreadsheet", eVar);
            i = 4;
        } else if (eVar.getSheetType().equals("ToDo") || eVar.getSheetType().equals("to do list")) {
            intent = new Intent(this, ActivityTodoDetails.class);
            intent.putExtra("spreadsheet", eVar);
            i = 5;
        } else if (eVar.getSheetType().equals("Contact") || eVar.getSheetType().equals("contact spreadsheet")) {
            intent = new Intent(this, ActivityContacts.class);
            intent.putExtra("spreadsheet", eVar);
            i = 6;
        } else if (eVar.getSheetType().equals("Barcode")) {
            intent = new Intent(this, ActivityBarcodeSheet.class);
            intent.putExtra("spreadsheet", eVar);
            i = 7;
        } else {
            return;
        }
        startActivityForResult(intent, i);
    }

    /* renamed from: F0 */
    public void mo24394F0() {
        Menu menu = this.navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            C6873b.m33463a(this, menu.getItem(i));
        }
    }

    /* renamed from: G0 */
    public void mo24395G0() {
        C0065b.C0066a aVar = new C0065b.C0066a(this);
        aVar.mo236g("You will be logged out of all connected accounts. Do you want to continue?");
        aVar.mo240k("Yes", new C7049t());
        aVar.mo237h("No", new C7051u(this));
        aVar.mo233d(true);
        aVar.mo243n();
    }

    /* renamed from: H */
    public void mo24237H(String str, String str2) {
        str2.hashCode();
        char c = 65535;
        switch (str2.hashCode()) {
            case -2022976223:
                if (str2.equals("deleteSpreadsheet")) {
                    c = 0;
                    break;
                }
                break;
            case -1809886158:
                if (str2.equals("getAllSpreadsheets")) {
                    c = 1;
                    break;
                }
                break;
            case -387756467:
                if (str2.equals("userAccessList")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.f24433H.mo25638c("DELETE_SPREADSHEET", "DELETE_SPREADSHEET");
                this.f24475s0.mo25639d(this.f24437J);
                return;
            case 1:
                this.f24429F.mo24211a();
                this.layoutProgress.setVisibility(8);
                this.f24425D = this.f24439K.getSheetsList();
                m33756q1();
                return;
            case 2:
                this.f24429F.mo24211a();
                return;
            default:
                return;
        }
    }

    /* renamed from: H0 */
    public void mo24396H0() {
        new Handler().postDelayed(new C7045r(), 50);
    }

    /* renamed from: I */
    public void mo24357I() {
        FirebaseMessaging.m32962a().mo23833b(C6873b.f23985M);
        FirebaseMessaging.m32962a().mo23833b(C6873b.f23984L);
        this.f24479v.mo25623l();
        this.f24481w = this.f24483x.mo17659d();
        m33753m1();
        mo24403X0();
    }

    /* renamed from: K0 */
    public void mo24397K0() {
        this.f24474r0.mo20962C();
    }

    /* renamed from: L0 */
    public boolean mo24398L0(String str) {
        for (int i = 0; i < this.f24439K.getSheetAccessUsersList().size(); i++) {
            if (this.f24439K.getSheetAccessUsersList().get(i).getEmailAddress().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: M0 */
    public void mo24399M0() {
        this.f24421B = C4534g.m24100b().mo18599d(C6873b.f23982J);
        this.f24423C = C4534g.m24100b().mo18599d(C6873b.f23983K);
        this.f24421B.mo18590g(this.f24477u.mo24222b(C6873b.f24024o)).mo18606b(new C7039p0());
    }

    /* renamed from: N */
    public void mo24400N(C7560e eVar, int i) {
        this.f24478u0 = eVar;
        this.f24429F.mo24212c(this);
        this.f24429F.mo24213d("");
        this.f24486y0.mo25655k(this, this);
        this.f24486y0.mo25653i(eVar.getSheetId());
    }

    /* renamed from: S0 */
    public void mo24401S0() {
        this.f24477u.mo24226g("feedback", true);
        this.f24479v.mo25625n();
    }

    /* renamed from: W0 */
    public void mo24402W0() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_free_limits);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_free_limits_dialog_msg)).setText("Create unlimited spreadsheets with UpSheet Premium");
        ((Button) dialog.findViewById(R.id.button_free_limits_dialog_close)).setOnClickListener(new C7031l0(this, dialog));
        ((Button) dialog.findViewById(R.id.button_free_limits_go_premium)).setOnClickListener(new C7033m0(dialog));
        ((ImageView) dialog.findViewById(R.id.image_close_free_limits_dialog)).setOnClickListener(new C7035n0(this, dialog));
    }

    /* renamed from: X0 */
    public void mo24403X0() {
        try {
            this.f24473q0.clear();
            String b = this.f24477u.mo24222b(C6873b.f24026q);
            if (!b.equals("")) {
                JSONArray jSONArray = new JSONArray(b);
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    HashMap hashMap = new HashMap();
                    if (!this.f24477u.mo24222b(C6873b.f24025p).equals(jSONObject.optString("email"))) {
                        hashMap.put("name", jSONObject.optString("name"));
                        hashMap.put("email", jSONObject.optString("email"));
                        hashMap.put("photoUrl", jSONObject.optString("photoUrl"));
                        hashMap.put("refreshToken", jSONObject.optString("refreshToken"));
                        this.f24473q0.add(hashMap);
                    }
                }
            }
            m33755o1();
        } catch (Exception unused) {
        }
    }

    /* renamed from: a1 */
    public void mo24404a1() {
        new Handler().postDelayed(new C7043q0(), 1000);
    }

    /* renamed from: b1 */
    public void mo24405b1() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_go_premium);
        dialog.getWindow().setLayout(-1, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        TabLayout tabLayout = (TabLayout) dialog.findViewById(R.id.tabs_go_premium);
        CardView cardView = (CardView) dialog.findViewById(R.id.card_go_premium);
        tabLayout.mo14461u(1).mo14493k();
        ((TextView) dialog.findViewById(R.id.text_go_premium_price)).setText(this.f24482w0);
        tabLayout.setOnTabSelectedListener((TabLayout.C3824d) new C7021g0(this, (CardView) dialog.findViewById(R.id.card_go_premium_free_features), (CardView) dialog.findViewById(R.id.card_go_premium_features), cardView));
        ((ImageView) dialog.findViewById(R.id.image_close_free_limits_dialog)).setOnClickListener(new C7023h0(this, dialog));
        cardView.setOnClickListener(new C7025i0(dialog));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b2, code lost:
        r3.f24479v.mo25624m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return false;
     */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo14328e(MenuItem r4) {
        /*
            r3 = this;
            int r0 = r4.getItemId()
            r1 = 2131296636(0x7f09017c, float:1.8211194E38)
            if (r0 == r1) goto L_0x00c7
            r1 = 2131296640(0x7f090180, float:1.8211202E38)
            if (r0 == r1) goto L_0x00b8
            switch(r0) {
                case 2131296627: goto L_0x009b;
                case 2131296628: goto L_0x0089;
                case 2131296629: goto L_0x0065;
                case 2131296630: goto L_0x0013;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x00ef
        L_0x0013:
            androidx.drawerlayout.widget.DrawerLayout r4 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r0 = r3.navigationView
            r4.mo2364d(r0)
            e.e.a.d.d r4 = r3.f24433H
            java.lang.String r0 = "NAV_RATE_YES"
            r4.mo25638c(r0, r0)
            android.content.Intent r4 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x004b }
            java.lang.String r0 = "android.intent.action.VIEW"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x004b }
            r1.<init>()     // Catch:{ ActivityNotFoundException -> 0x004b }
            java.lang.String r2 = "market://details?id="
            r1.append(r2)     // Catch:{ ActivityNotFoundException -> 0x004b }
            java.lang.String r2 = r3.getPackageName()     // Catch:{ ActivityNotFoundException -> 0x004b }
            r1.append(r2)     // Catch:{ ActivityNotFoundException -> 0x004b }
            java.lang.String r1 = r1.toString()     // Catch:{ ActivityNotFoundException -> 0x004b }
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ ActivityNotFoundException -> 0x004b }
            r4.<init>(r0, r1)     // Catch:{ ActivityNotFoundException -> 0x004b }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r4.addFlags(r0)     // Catch:{ ActivityNotFoundException -> 0x004b }
            r3.startActivity(r4)     // Catch:{ ActivityNotFoundException -> 0x004b }
            goto L_0x00ef
        L_0x004b:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "https://play.google.com/store/apps/details?id="
            r4.append(r0)
            java.lang.String r0 = r3.getPackageName()
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            m33736F1(r3, r4)
            goto L_0x00ef
        L_0x0065:
            androidx.drawerlayout.widget.DrawerLayout r0 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r1 = r3.navigationView
            r0.mo2364d(r1)
            java.lang.CharSequence r4 = r4.getTitle()
            java.lang.String r4 = r4.toString()
            android.content.res.Resources r0 = r3.getResources()
            r1 = 2131820750(0x7f1100ce, float:1.9274224E38)
            java.lang.String r0 = r0.getString(r1)
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 == 0) goto L_0x00b2
            r3.mo24395G0()
            goto L_0x00ef
        L_0x0089:
            androidx.drawerlayout.widget.DrawerLayout r4 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r0 = r3.navigationView
            r4.mo2364d(r0)
            android.content.Intent r4 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityVideo> r0 = com.spreadsheet.app.activities.ActivityVideo.class
            r4.<init>(r3, r0)
        L_0x0097:
            r3.startActivity(r4)
            goto L_0x00ef
        L_0x009b:
            androidx.drawerlayout.widget.DrawerLayout r4 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r0 = r3.navigationView
            r4.mo2364d(r0)
            com.spreadsheet.app.Utils.h r4 = r3.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r4 = r4.mo24221a(r0)
            if (r4 == 0) goto L_0x00b2
            e.e.a.d.c r4 = r3.f24479v
            r4.mo25625n()
            goto L_0x00ef
        L_0x00b2:
            e.e.a.d.c r4 = r3.f24479v
            r4.mo25624m()
            goto L_0x00ef
        L_0x00b8:
            androidx.drawerlayout.widget.DrawerLayout r4 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r0 = r3.navigationView
            r4.mo2364d(r0)
            android.content.Intent r4 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityTermsOfUse> r0 = com.spreadsheet.app.activities.ActivityTermsOfUse.class
            r4.<init>(r3, r0)
            goto L_0x0097
        L_0x00c7:
            androidx.drawerlayout.widget.DrawerLayout r4 = r3.drawerLayout
            com.google.android.material.navigation.NavigationView r0 = r3.navigationView
            r4.mo2364d(r0)
            e.e.a.d.d r4 = r3.f24433H
            java.lang.String r0 = "SHARE_APP"
            r4.mo25638c(r0, r0)
            android.content.Intent r4 = new android.content.Intent
            java.lang.String r0 = "android.intent.action.SEND"
            r4.<init>(r0)
            java.lang.String r0 = "text/plain"
            r4.setType(r0)
            java.lang.String r0 = "android.intent.extra.TEXT"
            java.lang.String r1 = "Hey, checkout this great app 'UpSheet' for managing google sheets with simplified interface!\nhttps://play.google.com/store/apps/details?id=com.spreadsheet.app"
            r4.putExtra(r0, r1)
            java.lang.String r0 = "Share Using...."
            android.content.Intent r4 = android.content.Intent.createChooser(r4, r0)
            goto L_0x0097
        L_0x00ef:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.MainActivity.mo14328e(android.view.MenuItem):boolean");
    }

    /* renamed from: e1 */
    public void mo24406e1() {
        C0665c.C0666a d = C0665c.m3907d(this);
        d.mo4208b();
        d.mo4209c(this);
        C0665c a = d.mo4207a();
        this.f24484x0 = a;
        a.mo4206g(new C7036o());
    }

    /* renamed from: f1 */
    public void mo24407f1() {
        FirebaseAuth.getInstance().mo17661f();
        this.f24445O.setVisibility(8);
        this.f24473q0.clear();
        this.f24477u.mo24226g(C6873b.f24023n, false);
        this.f24477u.mo24227h(C6873b.f24025p, "");
        this.f24477u.mo24227h(C6873b.f24027r, "");
        this.f24477u.mo24227h(C6873b.f24028s, "");
        this.f24477u.mo24227h(C6873b.f24029t, "");
        this.f24477u.mo24227h(C6873b.f24024o, "");
        this.f24441L.setImageResource(R.drawable.app_logo_white);
        this.f24425D.clear();
        m33756q1();
        this.layoutEmptyScreen.setVisibility(0);
        this.toolbarHomeGoPremium.setVisibility(8);
        m33765y1(true);
    }

    /* renamed from: g */
    public void mo24408g(HashMap<String, String> hashMap) {
        this.drawerLayout.mo2364d(this.navigationView);
        this.f24479v.mo25616c(hashMap);
    }

    /* renamed from: i1 */
    public void mo24409i1() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_renew_subscription);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_renew_sub_dialog_msg)).setText("Renew your subscription to continue using UpSheet Premium");
        ((Button) dialog.findViewById(R.id.button_renew_sub_dialog_close)).setOnClickListener(new C7046r0(this, dialog));
        ((Button) dialog.findViewById(R.id.button_renew_sub_renew)).setOnClickListener(new C7048s0(dialog));
        ((ImageView) dialog.findViewById(R.id.image_close_renew_sub_dialog)).setOnClickListener(new C7050t0(this, dialog));
    }

    /* renamed from: j1 */
    public void mo24410j1(String str) {
        this.f24484x0.mo4206g(new C7041q(str));
    }

    /* renamed from: k */
    public void mo24358k() {
        this.f24479v.mo25623l();
        this.f24479v.mo25617d();
        this.f24429F.mo24211a();
    }

    /* renamed from: k1 */
    public void mo24411k1() {
        this.f24424C0.setOnItemSelectedListener(new C7017e0());
    }

    /* renamed from: l1 */
    public void mo24412l1() {
        this.f24426D0.setOnItemSelectedListener(new C7019f0());
    }

    /* renamed from: n */
    public void mo24359n() {
        this.f24429F.mo24211a();
        mo24403X0();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        this.f24429F.mo24212c(this);
        this.f24431G.mo25662C(this, this);
        this.f24475s0.mo25642h(this, this);
        this.f24466j0.mo25614o(this, this);
        this.f24464h0.mo25704r(this, this);
        this.f24477u.mo24225f(this);
        this.f24433H.mo25637b(this);
        this.f24427E.mo24206c(this);
        if (i == 3 && intent != null) {
            this.f24429F.mo24213d("");
            this.f24479v.mo25622k(intent);
        }
        if (i == 20 && intent != null) {
            this.f24479v.mo25621j(intent);
        }
        C7562g gVar = this.f24439K;
        if (gVar.isUpdated) {
            gVar.isUpdated = false;
            this.f24475s0.mo25642h(this, this);
            this.f24475s0.mo25641g();
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.button_add_todo) {
            if (this.f24453W.getText().equals("Contacts Backup")) {
                if (!this.f24451U.getText().toString().equals("")) {
                    this.f24450T.dismiss();
                    String obj = this.f24451U.getText().toString();
                    this.f24469m0 = obj;
                    m33741P0(obj);
                    return;
                }
            } else if (this.f24453W.getText().equals("To Do")) {
                if (!this.f24451U.getText().toString().equals("")) {
                    this.f24450T.dismiss();
                    String obj2 = this.f24451U.getText().toString();
                    this.f24462f0 = obj2;
                    m33742Q0(obj2);
                    return;
                }
            } else if (!this.f24453W.getText().equals("Barcode Sheet")) {
                return;
            } else {
                if (!this.f24451U.getText().toString().equals("")) {
                    this.f24450T.dismiss();
                    this.f24429F.mo24213d("");
                    String obj3 = this.f24451U.getText().toString();
                    this.f24439K.spreadSheetTitle = obj3;
                    C7596g gVar = this.f24431G;
                    gVar.mo25669g(gVar.mo25680r(obj3));
                    return;
                }
            }
            Toast.makeText(this, "Please enter title ", 0).show();
        } else if (id == R.id.image_close) {
            this.f24450T.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        ButterKnife.bind((Activity) this);
        this.f24427E.mo24206c(this);
        this.f24429F.mo24212c(this);
        this.f24475s0.mo25642h(this, this);
        this.f24431G.mo25662C(this, this);
        this.f24464h0.mo25704r(this, this);
        m33750d1();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        C7562g gVar = this.f24439K;
        if (gVar.isUpdated) {
            gVar.isUpdated = false;
            this.f24429F.mo24212c(this);
            this.f24431G.mo25662C(this, this);
            this.f24475s0.mo25642h(this, this);
            this.f24466j0.mo25614o(this, this);
            this.f24464h0.mo25704r(this, this);
            this.f24477u.mo24225f(this);
            this.f24433H.mo25637b(this);
            this.f24427E.mo24206c(this);
            this.f24475s0.mo25641g();
        }
        C7562g gVar2 = this.f24439K;
        if (gVar2.shouldRequestAuth) {
            gVar2.shouldRequestAuth = false;
            this.f24429F.mo24212c(this);
            this.f24429F.mo24213d("");
            this.f24479v.mo25619g(this, this);
            new Handler().postDelayed(new C7032m(), 3000);
        }
        this.f24439K.getColumnList().clear();
        if (C6873b.f24021l) {
            C6873b.f24021l = false;
            mo24405b1();
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 1) {
            m33751g1();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0077, code lost:
        if (r6.f24439K.getSheetsList().size() < 5) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0108, code lost:
        r6.f24451U.setText("");
        r6.f24453W.setText(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0112, code lost:
        r6.f24450T.show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0122, code lost:
        if (r6.f24439K.getSheetsList().size() < 5) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0125, code lost:
        mo24402W0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0129, code lost:
        r6.f24479v.mo25624m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x012f, code lost:
        android.widget.Toast.makeText(r6, getResources().getString(com.spreadsheet.app.R.string.internet_check), 0).show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        startActivityForResult(r7, 2);
     */
    @butterknife.OnClick({2131296835, 2131296344, 2131296377, 2131296387, 2131296538, 2131296376, 2131296374, 2131296931})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewClicked(View r7) {
        /*
            r6 = this;
            int r7 = r7.getId()
            r0 = 8
            r1 = 2
            r2 = 5
            r3 = 2131820726(0x7f1100b6, float:1.9274175E38)
            r4 = 0
            java.lang.String r5 = ""
            switch(r7) {
                case 2131296344: goto L_0x013f;
                case 2131296374: goto L_0x00ea;
                case 2131296376: goto L_0x00b4;
                case 2131296377: goto L_0x007b;
                case 2131296387: goto L_0x004d;
                case 2131296538: goto L_0x0036;
                case 2131296835: goto L_0x0018;
                case 2131296931: goto L_0x0013;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x014c
        L_0x0013:
            r6.mo24405b1()
            goto L_0x014c
        L_0x0018:
            com.spreadsheet.app.Utils.a r7 = r6.f24427E
            boolean r7 = r7.mo24207d()
            if (r7 == 0) goto L_0x012f
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x0129
            android.content.Intent r7 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityCreateSheet> r0 = com.spreadsheet.app.activities.ActivityCreateSheet.class
            r7.<init>(r6, r0)
        L_0x0031:
            r6.startActivityForResult(r7, r1)
            goto L_0x014c
        L_0x0036:
            android.widget.ImageView r7 = r6.imageSearch
            r7.setVisibility(r0)
            androidx.appcompat.widget.SearchView r7 = r6.searchView
            r7.setVisibility(r4)
            androidx.appcompat.widget.SearchView r7 = r6.searchView
            r7.setIconified(r4)
            com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold r7 = r6.textSpreadsheet
            r0 = 4
            r7.setVisibility(r0)
            goto L_0x014c
        L_0x004d:
            com.spreadsheet.app.Utils.a r7 = r6.f24427E
            boolean r7 = r7.mo24207d()
            if (r7 == 0) goto L_0x012f
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x0129
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r7 = r7.mo24221a(r0)
            java.lang.String r0 = "To Do"
            if (r7 == 0) goto L_0x006d
            goto L_0x0108
        L_0x006d:
            e.e.a.a.g r7 = r6.f24439K
            java.util.List r7 = r7.getSheetsList()
            int r7 = r7.size()
            if (r7 >= r2) goto L_0x0125
            goto L_0x0108
        L_0x007b:
            com.spreadsheet.app.Utils.a r7 = r6.f24427E
            boolean r7 = r7.mo24207d()
            if (r7 == 0) goto L_0x012f
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x0129
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x009f
            android.content.Intent r7 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityCreateSheet> r0 = com.spreadsheet.app.activities.ActivityCreateSheet.class
            r7.<init>(r6, r0)
            goto L_0x0031
        L_0x009f:
            e.e.a.a.g r7 = r6.f24439K
            java.util.List r7 = r7.getSheetsList()
            int r7 = r7.size()
            if (r7 >= r2) goto L_0x0125
            android.content.Intent r7 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityCreateSheet> r0 = com.spreadsheet.app.activities.ActivityCreateSheet.class
            r7.<init>(r6, r0)
            goto L_0x0031
        L_0x00b4:
            com.spreadsheet.app.Utils.a r7 = r6.f24427E
            boolean r7 = r7.mo24207d()
            if (r7 == 0) goto L_0x012f
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x0129
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r7 = r7.mo24221a(r0)
            java.lang.String r0 = "Contacts Backup"
            if (r7 == 0) goto L_0x00dd
        L_0x00d2:
            com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold r7 = r6.f24453W
            r7.setText(r0)
            android.widget.EditText r7 = r6.f24451U
            r7.setText(r5)
            goto L_0x0112
        L_0x00dd:
            e.e.a.a.g r7 = r6.f24439K
            java.util.List r7 = r7.getSheetsList()
            int r7 = r7.size()
            if (r7 >= r2) goto L_0x0125
            goto L_0x00d2
        L_0x00ea:
            com.spreadsheet.app.Utils.a r7 = r6.f24427E
            boolean r7 = r7.mo24207d()
            if (r7 == 0) goto L_0x012f
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24023n
            boolean r7 = r7.mo24221a(r0)
            if (r7 == 0) goto L_0x0129
            com.spreadsheet.app.Utils.h r7 = r6.f24477u
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r7 = r7.mo24221a(r0)
            java.lang.String r0 = "Barcode Sheet"
            if (r7 == 0) goto L_0x0118
        L_0x0108:
            android.widget.EditText r7 = r6.f24451U
            r7.setText(r5)
            com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold r7 = r6.f24453W
            r7.setText(r0)
        L_0x0112:
            android.app.Dialog r7 = r6.f24450T
            r7.show()
            goto L_0x014c
        L_0x0118:
            e.e.a.a.g r7 = r6.f24439K
            java.util.List r7 = r7.getSheetsList()
            int r7 = r7.size()
            if (r7 >= r2) goto L_0x0125
            goto L_0x0108
        L_0x0125:
            r6.mo24402W0()
            goto L_0x014c
        L_0x0129:
            e.e.a.d.c r7 = r6.f24479v
            r7.mo25624m()
            goto L_0x014c
        L_0x012f:
            android.content.res.Resources r7 = r6.getResources()
            java.lang.String r7 = r7.getString(r3)
            android.widget.Toast r7 = android.widget.Toast.makeText(r6, r7, r4)
            r7.show()
            goto L_0x014c
        L_0x013f:
            android.widget.RelativeLayout r7 = r6.layoutTryAgain
            r7.setVisibility(r0)
            com.spreadsheet.app.Utils.d r7 = r6.f24429F
            r7.mo24213d(r5)
            r6.m33748Z0()
        L_0x014c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.MainActivity.onViewClicked(android.view.View):void");
    }

    /* renamed from: p */
    public void mo24360p() {
        mo24396H0();
        mo24399M0();
        m33748Z0();
    }

    /* renamed from: p1 */
    public void mo24415p1(C7560e eVar) {
        this.f24487z.mo18590g(this.f24481w).mo18590g(eVar.getSheetId()).mo18590g("sheetType").mo18595k("Custom").mo23027b(new C7020g());
    }

    /* renamed from: r1 */
    public void mo24416r1() {
        if (this.f24477u.mo24221a(C6873b.f24017j)) {
            this.toolbarHomeTitlePremium.setVisibility(0);
            this.toolbarHomeGoPremium.setVisibility(8);
            return;
        }
        this.toolbarHomeTitlePremium.setVisibility(8);
        this.toolbarHomeGoPremium.setVisibility(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x014e, code lost:
        r1.putExtra("spreadsheet", r6);
        startActivityForResult(r1, 5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0156, code lost:
        m33746V0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x02b0, code lost:
        runOnUiThread(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return;
     */
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24244s(String r6, String r7) {
        /*
            r5 = this;
            r7.hashCode()
            int r6 = r7.hashCode()
            r0 = 5
            r1 = 8
            r2 = 1
            r3 = 0
            r4 = -1
            switch(r6) {
                case -2022976223: goto L_0x0107;
                case -1872159696: goto L_0x00fc;
                case -1809886158: goto L_0x00f1;
                case -1422526087: goto L_0x00e6;
                case -1096923260: goto L_0x00db;
                case -1033874466: goto L_0x00d0;
                case -790104630: goto L_0x00c5;
                case -573803217: goto L_0x00ba;
                case -387756467: goto L_0x00ac;
                case -366255800: goto L_0x009e;
                case 227556787: goto L_0x0090;
                case 323754608: goto L_0x0082;
                case 342629838: goto L_0x0074;
                case 562612100: goto L_0x0066;
                case 829310011: goto L_0x0058;
                case 851988258: goto L_0x004a;
                case 1369217922: goto L_0x003c;
                case 1592658672: goto L_0x002e;
                case 1594339684: goto L_0x0020;
                case 1924427394: goto L_0x0012;
                default: goto L_0x0010;
            }
        L_0x0010:
            goto L_0x0111
        L_0x0012:
            java.lang.String r6 = "formatTodoSheet"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x001c
            goto L_0x0111
        L_0x001c:
            r4 = 19
            goto L_0x0111
        L_0x0020:
            java.lang.String r6 = "formatHeader"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x002a
            goto L_0x0111
        L_0x002a:
            r4 = 18
            goto L_0x0111
        L_0x002e:
            java.lang.String r6 = "updateDrivePermission"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0038
            goto L_0x0111
        L_0x0038:
            r4 = 17
            goto L_0x0111
        L_0x003c:
            java.lang.String r6 = "createToDo"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0046
            goto L_0x0111
        L_0x0046:
            r4 = 16
            goto L_0x0111
        L_0x004a:
            java.lang.String r6 = "allowRestrictedAccessLink"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0054
            goto L_0x0111
        L_0x0054:
            r4 = 15
            goto L_0x0111
        L_0x0058:
            java.lang.String r6 = "hideColumns"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0062
            goto L_0x0111
        L_0x0062:
            r4 = 14
            goto L_0x0111
        L_0x0066:
            java.lang.String r6 = "addCheckbox"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0070
            goto L_0x0111
        L_0x0070:
            r4 = 13
            goto L_0x0111
        L_0x0074:
            java.lang.String r6 = "deleteDrivePermission"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x007e
            goto L_0x0111
        L_0x007e:
            r4 = 12
            goto L_0x0111
        L_0x0082:
            java.lang.String r6 = "formatRows"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x008c
            goto L_0x0111
        L_0x008c:
            r4 = 11
            goto L_0x0111
        L_0x0090:
            java.lang.String r6 = "addAllContacts"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x009a
            goto L_0x0111
        L_0x009a:
            r4 = 10
            goto L_0x0111
        L_0x009e:
            java.lang.String r6 = "createBarcodeSpreadsheet"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00a8
            goto L_0x0111
        L_0x00a8:
            r4 = 9
            goto L_0x0111
        L_0x00ac:
            java.lang.String r6 = "userAccessList"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00b6
            goto L_0x0111
        L_0x00b6:
            r4 = 8
            goto L_0x0111
        L_0x00ba:
            java.lang.String r6 = "shareSpreadsheetEmail"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00c3
            goto L_0x0111
        L_0x00c3:
            r4 = 7
            goto L_0x0111
        L_0x00c5:
            java.lang.String r6 = "formatTodoHeader"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00ce
            goto L_0x0111
        L_0x00ce:
            r4 = 6
            goto L_0x0111
        L_0x00d0:
            java.lang.String r6 = "getAllContacts"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00d9
            goto L_0x0111
        L_0x00d9:
            r4 = 5
            goto L_0x0111
        L_0x00db:
            java.lang.String r6 = "createContact"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00e4
            goto L_0x0111
        L_0x00e4:
            r4 = 4
            goto L_0x0111
        L_0x00e6:
            java.lang.String r6 = "addRow"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00ef
            goto L_0x0111
        L_0x00ef:
            r4 = 3
            goto L_0x0111
        L_0x00f1:
            java.lang.String r6 = "getAllSpreadsheets"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x00fa
            goto L_0x0111
        L_0x00fa:
            r4 = 2
            goto L_0x0111
        L_0x00fc:
            java.lang.String r6 = "allowPublicAccessLink"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0105
            goto L_0x0111
        L_0x0105:
            r4 = 1
            goto L_0x0111
        L_0x0107:
            java.lang.String r6 = "deleteSpreadsheet"
            boolean r6 = r7.equals(r6)
            if (r6 != 0) goto L_0x0110
            goto L_0x0111
        L_0x0110:
            r4 = 0
        L_0x0111:
            java.lang.String r6 = "To Do List created succefully!"
            java.lang.String r7 = "spreadsheet"
            switch(r4) {
                case 0: goto L_0x02b4;
                case 1: goto L_0x02ab;
                case 2: goto L_0x025f;
                case 3: goto L_0x0255;
                case 4: goto L_0x0247;
                case 5: goto L_0x0228;
                case 6: goto L_0x0219;
                case 7: goto L_0x020b;
                case 8: goto L_0x01f4;
                case 9: goto L_0x01ef;
                case 10: goto L_0x01e8;
                case 11: goto L_0x01b5;
                case 12: goto L_0x020b;
                case 13: goto L_0x0180;
                case 14: goto L_0x0171;
                case 15: goto L_0x016a;
                case 16: goto L_0x015b;
                case 17: goto L_0x020b;
                case 18: goto L_0x0156;
                case 19: goto L_0x011a;
                default: goto L_0x0118;
            }
        L_0x0118:
            goto L_0x02c2
        L_0x011a:
            e.e.a.a.g r1 = r5.f24439K
            java.util.List r1 = r1.getSheetsList()
            e.e.a.a.e r2 = r5.f24476t0
            r1.add(r2)
            e.e.a.d.e r1 = r5.f24475s0
            r1.mo25641g()
            com.spreadsheet.app.Utils.d r1 = r5.f24429F
            r1.mo24211a()
            android.widget.Toast r6 = android.widget.Toast.makeText(r5, r6, r3)
            r6.show()
            e.e.a.a.e r6 = new e.e.a.a.e
            r6.<init>()
            e.e.a.a.g r1 = r5.f24439K
            java.lang.String r1 = r1.spreadsheetId
            r6.setSheetId(r1)
            java.lang.String r1 = r5.f24462f0
            r6.setSheetName(r1)
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityTodoDetails> r2 = com.spreadsheet.app.activities.ActivityTodoDetails.class
            r1.<init>(r5, r2)
        L_0x014e:
            r1.putExtra(r7, r6)
            r5.startActivityForResult(r1, r0)
            goto L_0x02c2
        L_0x0156:
            r5.m33746V0()
            goto L_0x02c2
        L_0x015b:
            e.e.a.a.g r6 = r5.f24439K
            java.lang.String r7 = r6.spreadsheetId
            r5.f24456Z = r7
            int r6 = r6.sheetId
            r5.f24457a0 = r6
            r5.m33735E1()
            goto L_0x02c2
        L_0x016a:
            com.spreadsheet.app.activities.MainActivity$j r6 = new com.spreadsheet.app.activities.MainActivity$j
            r6.<init>()
            goto L_0x02b0
        L_0x0171:
            e.e.a.d.h r6 = r5.f24464h0
            java.lang.String r7 = r5.f24456Z
            int r0 = r5.f24457a0
            org.json.JSONObject r0 = r6.mo25700m(r0)
            r6.mo25693f(r7, r0)
            goto L_0x02c2
        L_0x0180:
            e.e.a.a.g r1 = r5.f24439K
            java.util.List r1 = r1.getSheetsList()
            e.e.a.a.e r2 = r5.f24476t0
            r1.add(r2)
            e.e.a.d.e r1 = r5.f24475s0
            r1.mo25641g()
            com.spreadsheet.app.Utils.d r1 = r5.f24429F
            r1.mo24211a()
            android.widget.Toast r6 = android.widget.Toast.makeText(r5, r6, r3)
            r6.show()
            e.e.a.a.e r6 = new e.e.a.a.e
            r6.<init>()
            e.e.a.a.g r1 = r5.f24439K
            java.lang.String r1 = r1.spreadsheetId
            r6.setSheetId(r1)
            java.lang.String r1 = r5.f24462f0
            r6.setSheetName(r1)
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityTodoDetails> r2 = com.spreadsheet.app.activities.ActivityTodoDetails.class
            r1.<init>(r5, r2)
            goto L_0x014e
        L_0x01b5:
            com.spreadsheet.app.Utils.d r6 = r5.f24429F
            r6.mo24211a()
            e.e.a.a.g r6 = r5.f24439K
            java.util.List r6 = r6.getSheetsList()
            e.e.a.a.e r0 = r5.f24476t0
            r6.add(r0)
            e.e.a.d.e r6 = r5.f24475s0
            r6.mo25641g()
            e.e.a.a.e r6 = new e.e.a.a.e
            r6.<init>()
            java.lang.String r0 = r5.f24467k0
            r6.setSheetId(r0)
            java.lang.String r0 = r5.f24469m0
            r6.setSheetName(r0)
            android.content.Intent r0 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivityContacts> r1 = com.spreadsheet.app.activities.ActivityContacts.class
            r0.<init>(r5, r1)
            r0.putExtra(r7, r6)
            r5.startActivity(r0)
            goto L_0x02c2
        L_0x01e8:
            r5.f24472p0 = r2
            r5.m33744T0()
            goto L_0x02c2
        L_0x01ef:
            r5.m33739N0()
            goto L_0x02c2
        L_0x01f4:
            com.spreadsheet.app.Utils.d r6 = r5.f24429F
            r6.mo24211a()
            androidx.appcompat.widget.AppCompatSpinner r6 = r5.f24426D0
            r7 = 0
            r6.setOnItemSelectedListener(r7)
            androidx.appcompat.widget.AppCompatSpinner r6 = r5.f24424C0
            r6.setOnItemSelectedListener(r7)
            e.e.a.a.e r6 = r5.f24478u0
            r5.mo24418t1(r6)
            goto L_0x02c2
        L_0x020b:
            e.e.a.d.f r6 = r5.f24486y0
            r6.mo25655k(r5, r5)
            e.e.a.d.f r6 = r5.f24486y0
            java.lang.String r7 = r5.f24480v0
            r6.mo25653i(r7)
            goto L_0x02c2
        L_0x0219:
            e.e.a.d.h r6 = r5.f24464h0
            java.lang.String r7 = r5.f24456Z
            int r0 = r5.f24457a0
            org.json.JSONObject r0 = r6.mo25695h(r0)
            r6.mo25690c(r7, r0)
            goto L_0x02c2
        L_0x0228:
            boolean r6 = r5.f24472p0
            if (r6 == 0) goto L_0x0231
            r5.f24472p0 = r3
            r5.m33748Z0()
        L_0x0231:
            com.spreadsheet.app.Utils.d r6 = r5.f24429F
            r6.mo24211a()
            e.e.a.a.g r6 = r5.f24439K
            java.util.List r6 = r6.getAllRowsList()
            r5.f24471o0 = r6
            com.spreadsheet.app.Utils.h r7 = r5.f24477u
            java.lang.String r0 = "allContacts"
            r7.mo24229j(r0, r6)
            goto L_0x02c2
        L_0x0247:
            e.e.a.a.g r6 = r5.f24439K
            java.lang.String r7 = r6.spreadsheetId
            r5.f24467k0 = r7
            int r6 = r6.sheetId
            r5.f24468l0 = r6
            r5.m33728A1()
            goto L_0x02c2
        L_0x0255:
            boolean r6 = r5.f24470n0
            if (r6 == 0) goto L_0x0156
            r5.f24470n0 = r3
            r5.m33745U0()
            goto L_0x02c2
        L_0x025f:
            e.e.a.a.g r6 = r5.f24439K
            java.util.List r6 = r6.getSheetsList()
            r5.f24425D = r6
            android.widget.RelativeLayout r6 = r5.layoutProgress
            r6.setVisibility(r1)
            com.spreadsheet.app.Utils.d r6 = r5.f24429F
            r6.mo24211a()
            java.util.List<e.e.a.a.e> r6 = r5.f24425D
            boolean r6 = r6.isEmpty()
            java.lang.String r7 = "howItWork"
            if (r6 == 0) goto L_0x0298
            androidx.constraintlayout.widget.ConstraintLayout r6 = r5.layoutEmptyScreen
            r6.setVisibility(r3)
            androidx.recyclerview.widget.RecyclerView r6 = r5.recyclerviewSheets
            r6.setVisibility(r1)
            com.spreadsheet.app.Utils.h r6 = r5.f24477u
            boolean r6 = r6.mo24221a(r7)
            if (r6 != 0) goto L_0x02a7
            e.e.a.d.c r6 = r5.f24479v
            r6.mo25626o()
            com.spreadsheet.app.Utils.h r6 = r5.f24477u
            r6.mo24226g(r7, r2)
            goto L_0x02a7
        L_0x0298:
            com.spreadsheet.app.Utils.h r6 = r5.f24477u
            r6.mo24226g(r7, r2)
            androidx.constraintlayout.widget.ConstraintLayout r6 = r5.layoutEmptyScreen
            r6.setVisibility(r1)
            androidx.recyclerview.widget.RecyclerView r6 = r5.recyclerviewSheets
            r6.setVisibility(r3)
        L_0x02a7:
            r5.m33756q1()
            goto L_0x02c2
        L_0x02ab:
            com.spreadsheet.app.activities.MainActivity$h r6 = new com.spreadsheet.app.activities.MainActivity$h
            r6.<init>()
        L_0x02b0:
            r5.runOnUiThread(r6)
            goto L_0x02c2
        L_0x02b4:
            e.e.a.d.d r6 = r5.f24433H
            java.lang.String r7 = "DELETE_SPREADSHEET"
            r6.mo25638c(r7, r7)
            e.e.a.d.e r6 = r5.f24475s0
            java.lang.String r7 = r5.f24437J
            r6.mo25639d(r7)
        L_0x02c2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.MainActivity.mo24244s(java.lang.String, java.lang.String):void");
    }

    /* renamed from: s1 */
    public void mo24417s1() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", "https://docs.google.com/spreadsheets/d/" + this.f24480v0 + "/edit?usp=sharing");
        startActivity(Intent.createChooser(intent, "Share Using...."));
    }

    /* renamed from: t1 */
    public void mo24418t1(C7560e eVar) {
        AppCompatSpinner appCompatSpinner;
        if (!isFinishing() && !this.f24430F0.isShowing()) {
            this.f24430F0.show();
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.spinner_text, this.f24420A0);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_text_white);
        this.f24424C0.setAdapter((SpinnerAdapter) arrayAdapter);
        new Handler().postDelayed(new C7053v(), 1000);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, R.layout.spinner_text, this.f24422B0);
        arrayAdapter2.setDropDownViewResource(R.layout.spinner_text_white);
        this.f24426D0.setAdapter((SpinnerAdapter) arrayAdapter2);
        new Handler().postDelayed(new C7055w(), 1000);
        int i = 0;
        this.f24488z0.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.f24432G0.setText(eVar.getSheetName());
        this.f24480v0 = eVar.getSheetId();
        this.f24488z0.setAdapter(new SheetAccessAdapter(this, this.f24439K.getSheetAccessUsersList()));
        if (this.f24439K.isPublicAccess()) {
            this.f24424C0.setSelection(1);
            String str = this.f24439K.getPublicAccessMap().get("role");
            str.hashCode();
            char c = 65535;
            switch (str.hashCode()) {
                case -1732764110:
                    if (str.equals("Viewer")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1495015604:
                    if (str.equals("commenter")) {
                        c = 1;
                        break;
                    }
                    break;
                case -779574157:
                    if (str.equals("writer")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    this.f24426D0.setSelection(0);
                    break;
                case 1:
                    this.f24426D0.setSelection(2);
                    break;
                case 2:
                    this.f24426D0.setSelection(1);
                    break;
            }
            appCompatSpinner = this.f24426D0;
        } else {
            this.f24424C0.setSelection(0);
            appCompatSpinner = this.f24426D0;
            i = 8;
        }
        appCompatSpinner.setVisibility(i);
        this.f24434H0.setOnClickListener(new C7057x(eVar));
        this.f24436I0.setOnClickListener(new C7059y());
        this.f24438J0.setOnClickListener(new C7060z());
        this.f24440K0.setOnClickListener(new C7009a0());
    }

    /* renamed from: u */
    public void mo24305u() {
    }

    /* renamed from: y */
    public void mo24419y(String str) {
        this.f24486y0.mo25655k(this, this);
        this.f24429F.mo24213d("");
        this.f24486y0.mo25650e(this.f24480v0, str);
    }

    /* renamed from: z1 */
    public void mo24420z1(SkuDetails skuDetails) {
        C0673f.C0674a e = C0673f.m3956e();
        e.mo4228b(skuDetails);
        this.f24484x0.mo4203c(this, e.mo4227a());
    }
}
