package com.spreadsheet.app.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.List;
import p153e.p300e.p301a.p302a.C7563h;
import p153e.p300e.p301a.p304c.C7577g;

public class SheetAccessAdapter extends RecyclerView.C0503g<SheetsAccessHolder> {

    /* renamed from: e */
    Context f24580e;

    /* renamed from: f */
    List<C7563h> f24581f = new ArrayList();

    /* renamed from: g */
    C7577g f24582g;

    public class SheetsAccessHolder extends RecyclerView.C0500d0 {
        @BindView(2131296507)
        ImageView imageAccessrowDelete;
        @BindView(2131296508)
        ImageView imageAccessrowEdit;
        @BindView(2131296555)
        ConstraintLayout layoutAccessrowAccess;
        @BindView(2131296821)
        CustomTextView textAccessrowAccess;
        @BindView(2131296822)
        CustomTextView textAccessrowEmail;
        @BindView(2131296823)
        CustomTextView textAccessrowUsername;

        public SheetsAccessHolder(SheetAccessAdapter sheetAccessAdapter, View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }
    }

    public class SheetsAccessHolder_ViewBinding implements Unbinder {

        /* renamed from: a */
        private SheetsAccessHolder f24583a;

        public SheetsAccessHolder_ViewBinding(SheetsAccessHolder sheetsAccessHolder, View view) {
            this.f24583a = sheetsAccessHolder;
            sheetsAccessHolder.textAccessrowUsername = (CustomTextView) C0642c.m3874c(view, R.id.text_accessrow_username, "field 'textAccessrowUsername'", CustomTextView.class);
            sheetsAccessHolder.textAccessrowEmail = (CustomTextView) C0642c.m3874c(view, R.id.text_accessrow_email, "field 'textAccessrowEmail'", CustomTextView.class);
            sheetsAccessHolder.textAccessrowAccess = (CustomTextView) C0642c.m3874c(view, R.id.text_accessrow_access, "field 'textAccessrowAccess'", CustomTextView.class);
            sheetsAccessHolder.imageAccessrowEdit = (ImageView) C0642c.m3874c(view, R.id.image_accessrow_edit, "field 'imageAccessrowEdit'", ImageView.class);
            sheetsAccessHolder.imageAccessrowDelete = (ImageView) C0642c.m3874c(view, R.id.image_accessrow_delete, "field 'imageAccessrowDelete'", ImageView.class);
            sheetsAccessHolder.layoutAccessrowAccess = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_accessrow_access, "field 'layoutAccessrowAccess'", ConstraintLayout.class);
        }

        public void unbind() {
            SheetsAccessHolder sheetsAccessHolder = this.f24583a;
            if (sheetsAccessHolder != null) {
                this.f24583a = null;
                sheetsAccessHolder.textAccessrowUsername = null;
                sheetsAccessHolder.textAccessrowEmail = null;
                sheetsAccessHolder.textAccessrowAccess = null;
                sheetsAccessHolder.imageAccessrowEdit = null;
                sheetsAccessHolder.imageAccessrowDelete = null;
                sheetsAccessHolder.layoutAccessrowAccess = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SheetAccessAdapter$a */
    class C7069a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24584c;

        C7069a(int i) {
            this.f24584c = i;
        }

        public void onClick(View view) {
            if (!SheetAccessAdapter.this.f24581f.get(this.f24584c).getRole().equalsIgnoreCase("Owner")) {
                SheetAccessAdapter sheetAccessAdapter = SheetAccessAdapter.this;
                sheetAccessAdapter.mo24460u(sheetAccessAdapter.f24581f.get(this.f24584c).getRole(), SheetAccessAdapter.this.f24581f.get(this.f24584c).getId());
            }
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SheetAccessAdapter$b */
    class C7070b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24586c;

        C7070b(int i) {
            this.f24586c = i;
        }

        public void onClick(View view) {
            SheetAccessAdapter sheetAccessAdapter = SheetAccessAdapter.this;
            sheetAccessAdapter.f24582g.mo24419y(sheetAccessAdapter.f24581f.get(this.f24586c).getId());
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SheetAccessAdapter$c */
    class C7071c implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24588c;

        C7071c(SheetAccessAdapter sheetAccessAdapter, Dialog dialog) {
            this.f24588c = dialog;
        }

        public void onClick(View view) {
            this.f24588c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SheetAccessAdapter$d */
    class C7072d implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24589c;

        /* renamed from: d */
        final /* synthetic */ String f24590d;

        C7072d(Dialog dialog, String str) {
            this.f24589c = dialog;
            this.f24590d = str;
        }

        public void onClick(View view) {
            this.f24589c.dismiss();
            SheetAccessAdapter.this.f24582g.mo24419y(this.f24590d);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SheetAccessAdapter$e */
    class C7073e implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ RadioGroup f24592c;

        /* renamed from: d */
        final /* synthetic */ Dialog f24593d;

        /* renamed from: e */
        final /* synthetic */ String f24594e;

        C7073e(RadioGroup radioGroup, Dialog dialog, String str) {
            this.f24592c = radioGroup;
            this.f24593d = dialog;
            this.f24594e = str;
        }

        public void onClick(View view) {
            RadioGroup radioGroup = this.f24592c;
            RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
            String str = "reader";
            if (!radioButton.getText().toString().equalsIgnoreCase("Viewer")) {
                if (radioButton.getText().toString().equalsIgnoreCase("Editor")) {
                    str = "writer";
                } else if (!radioButton.getText().toString().equalsIgnoreCase("Commenter")) {
                    str = "";
                }
            }
            this.f24593d.dismiss();
            SheetAccessAdapter.this.f24582g.mo24391C(str, this.f24594e);
        }
    }

    public SheetAccessAdapter(Context context, List<C7563h> list) {
        this.f24580e = context;
        this.f24581f = list;
        this.f24582g = (C7577g) context;
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24581f.size();
    }

    /* renamed from: u */
    public void mo24460u(String str, String str2) {
        Dialog dialog = new Dialog(this.f24580e);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_add_access);
        dialog.getWindow().setLayout(-2, -2);
        if (!((Activity) this.f24580e).isFinishing()) {
            dialog.show();
        }
        ImageView imageView = (ImageView) dialog.findViewById(R.id.image_close_access_diaLog);
        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radiogroup_access);
        Button button = (Button) dialog.findViewById(R.id.button_share_add_access);
        Button button2 = (Button) dialog.findViewById(R.id.button_remove_access);
        button2.setVisibility(0);
        RadioButton radioButton = (RadioButton) radioGroup.findViewById(R.id.radioButton_view);
        RadioButton radioButton2 = (RadioButton) radioGroup.findViewById(R.id.radioButton_edit);
        if (str.equalsIgnoreCase("reader")) {
            radioButton.setChecked(true);
        } else if (str.equalsIgnoreCase("writer")) {
            radioButton2.setChecked(true);
        }
        button.setText("Save");
        imageView.setOnClickListener(new C7071c(this, dialog));
        button2.setOnClickListener(new C7072d(dialog, str2));
        button.setOnClickListener(new C7073e(radioGroup, dialog, str2));
    }

    /* renamed from: v */
    public void mo3300j(SheetsAccessHolder sheetsAccessHolder, int i) {
        String displayName = this.f24581f.get(i).getDisplayName();
        if (!displayName.equalsIgnoreCase("")) {
            sheetsAccessHolder.textAccessrowUsername.setText(displayName);
            sheetsAccessHolder.imageAccessrowDelete.setVisibility(8);
            sheetsAccessHolder.layoutAccessrowAccess.setVisibility(0);
        } else {
            sheetsAccessHolder.textAccessrowUsername.setText("Invalid user");
            sheetsAccessHolder.imageAccessrowDelete.setVisibility(0);
            sheetsAccessHolder.layoutAccessrowAccess.setVisibility(8);
            sheetsAccessHolder.textAccessrowUsername.setTextColor(this.f24580e.getResources().getColor(R.color.colorPrimary));
        }
        sheetsAccessHolder.textAccessrowEmail.setText(this.f24581f.get(i).getEmailAddress());
        String role = this.f24581f.get(i).getRole();
        if (role.equalsIgnoreCase("writer")) {
            sheetsAccessHolder.textAccessrowAccess.setText("Editor");
        } else if (role.equalsIgnoreCase("reader") || role.equalsIgnoreCase("commenter")) {
            sheetsAccessHolder.textAccessrowAccess.setText("Viewer");
        } else if (role.equalsIgnoreCase("Owner")) {
            sheetsAccessHolder.textAccessrowAccess.setText("Owner");
            sheetsAccessHolder.imageAccessrowEdit.setVisibility(8);
        }
        sheetsAccessHolder.layoutAccessrowAccess.setOnClickListener(new C7069a(i));
        sheetsAccessHolder.imageAccessrowDelete.setOnClickListener(new C7070b(i));
    }

    /* renamed from: w */
    public SheetsAccessHolder mo3302l(ViewGroup viewGroup, int i) {
        return new SheetsAccessHolder(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_sheet_access, viewGroup, false));
    }
}
