package com.spreadsheet.app.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.spreadsheet.app.adapters.h */
public class C7105h extends BaseAdapter {

    /* renamed from: c */
    Context f24692c;

    /* renamed from: d */
    List<HashMap<String, String>> f24693d = new ArrayList();

    public C7105h(Context context, List<HashMap<String, String>> list) {
        this.f24692c = context;
        this.f24693d = list;
    }

    public int getCount() {
        return this.f24693d.size();
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        Resources resources;
        int i2;
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_text_white, viewGroup, false);
        CustomTextView customTextView = (CustomTextView) inflate.findViewById(R.id.text_spinner_black);
        customTextView.setText((CharSequence) this.f24693d.get(i).get("Value"));
        customTextView.setGravity(17);
        if (i == 0) {
            resources = this.f24692c.getResources();
            i2 = R.color.text_color_grey;
        } else {
            resources = this.f24692c.getResources();
            i2 = R.color.text_color_dark;
        }
        customTextView.setTextColor(resources.getColor(i2));
        return inflate;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Resources resources;
        int i2;
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_text, viewGroup, false);
        CustomTextView customTextView = (CustomTextView) inflate.findViewById(R.id.text_spinner_black);
        customTextView.setText((CharSequence) this.f24693d.get(i).get("Value"));
        if (i == 0) {
            resources = this.f24692c.getResources();
            i2 = R.color.text_color_grey;
        } else {
            resources = this.f24692c.getResources();
            i2 = R.color.text_color_dark;
        }
        customTextView.setTextColor(resources.getColor(i2));
        return inflate;
    }
}
