package com.spreadsheet.app.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import com.spreadsheet.app.R;

/* renamed from: com.spreadsheet.app.Utils.d */
public class C6875d {

    /* renamed from: c */
    private static C6875d f24036c = new C6875d();

    /* renamed from: a */
    Dialog f24037a;

    /* renamed from: b */
    Context f24038b;

    /* renamed from: b */
    public static C6875d m33482b() {
        return f24036c;
    }

    /* renamed from: a */
    public void mo24211a() {
        if (!((Activity) this.f24038b).isFinishing()) {
            this.f24037a.dismiss();
        }
    }

    /* renamed from: c */
    public void mo24212c(Context context) {
        this.f24038b = context;
        Dialog dialog = new Dialog(this.f24038b);
        this.f24037a = dialog;
        dialog.requestWindowFeature(1);
        this.f24037a.setContentView(R.layout.dialog_progress);
        this.f24037a.setCancelable(false);
        this.f24037a.setCanceledOnTouchOutside(false);
    }

    /* renamed from: d */
    public void mo24213d(String str) {
        if (!((Activity) this.f24038b).isFinishing()) {
            this.f24037a.show();
        }
    }
}
