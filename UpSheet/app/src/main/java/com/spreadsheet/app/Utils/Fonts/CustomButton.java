package com.spreadsheet.app.Utils.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import com.spreadsheet.app.Utils.C6873b;

public class CustomButton extends Button {
    public CustomButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo24196a();
    }

    /* renamed from: a */
    public void mo24196a() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), C6873b.f23973A));
    }
}
