package com.spreadsheet.app.Utils;

import android.content.Context;
import android.content.Intent;
import com.spreadsheet.app.Service.TokenRefreshService;
import com.spreadsheet.app.Utils.C4777e;
import p099d.p147o.C5091a;
import p099d.p147o.C5099b;

public class MyApplication extends C5099b {

    /* renamed from: com.spreadsheet.app.Utils.MyApplication$a */
    class C4776a implements C4777e.C4778b {
        C4776a() {
        }

        /* renamed from: a */
        public void mo19177a() {
            MyApplication.this.stopService(new Intent(MyApplication.this, TokenRefreshService.class));
        }

        /* renamed from: b */
        public void mo19178b() {
            MyApplication.this.startService(new Intent(MyApplication.this, TokenRefreshService.class));
        }
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        C5091a.m26602l(this);
    }

    public void onCreate() {
        super.onCreate();
        C4777e.m25153e(this);
        C4777e.m25152d(new C4776a());
    }
}
