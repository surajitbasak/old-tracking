package com.spreadsheet.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p153e.p300e.p301a.p304c.C7574d;

/* renamed from: com.spreadsheet.app.adapters.c */
public class C7085c extends RecyclerView.C0503g<C7088c> {

    /* renamed from: e */
    List<HashMap<String, String>> f24628e = new ArrayList();

    /* renamed from: f */
    C7574d f24629f;

    /* renamed from: com.spreadsheet.app.adapters.c$a */
    class C7086a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ HashMap f24630c;

        /* renamed from: d */
        final /* synthetic */ int f24631d;

        C7086a(HashMap hashMap, int i) {
            this.f24630c = hashMap;
            this.f24631d = i;
        }

        public void onClick(View view) {
            C7085c.this.f24629f.mo24300d(this.f24630c, this.f24631d);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.c$b */
    class C7087b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ HashMap f24633c;

        /* renamed from: d */
        final /* synthetic */ int f24634d;

        C7087b(HashMap hashMap, int i) {
            this.f24633c = hashMap;
            this.f24634d = i;
        }

        public void onClick(View view) {
            C7085c.this.f24629f.mo24304t(this.f24633c, this.f24634d);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.c$c */
    public class C7088c extends RecyclerView.C0500d0 {

        /* renamed from: A */
        ImageView f24636A;

        /* renamed from: t */
        CustomTextView f24637t;

        /* renamed from: u */
        CustomTextView f24638u;

        /* renamed from: v */
        CustomTextView f24639v;

        /* renamed from: w */
        CustomTextView f24640w;

        /* renamed from: x */
        CustomTextView f24641x;

        /* renamed from: y */
        CustomTextView f24642y;

        /* renamed from: z */
        ImageView f24643z;

        public C7088c(C7085c cVar, View view) {
            super(view);
            this.f24642y = (CustomTextView) view.findViewById(R.id.text_letter);
            this.f24637t = (CustomTextView) view.findViewById(R.id.text_name);
            this.f24638u = (CustomTextView) view.findViewById(R.id.text_contact1);
            this.f24639v = (CustomTextView) view.findViewById(R.id.text_contact2);
            this.f24640w = (CustomTextView) view.findViewById(R.id.text_contact3);
            this.f24641x = (CustomTextView) view.findViewById(R.id.text_email);
            this.f24636A = (ImageView) view.findViewById(R.id.img_call);
            this.f24643z = (ImageView) view.findViewById(R.id.img_mail);
        }
    }

    public C7085c(Context context, List<HashMap<String, String>> list, C7574d dVar) {
        this.f24628e = list;
        this.f24629f = dVar;
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24628e.size();
    }

    /* renamed from: u */
    public void mo3300j(C7088c cVar, int i) {
        HashMap hashMap = this.f24628e.get(i);
        new ArrayList(hashMap.keySet());
        String str = (String) hashMap.get("Contact Name");
        String valueOf = String.valueOf(str.charAt(0));
        cVar.f24637t.setText(str);
        cVar.f24642y.setText(valueOf);
        if (!hashMap.containsKey("Contact No1") || ((String) hashMap.get("Contact No1")).equals("")) {
            cVar.f24638u.setVisibility(8);
        } else {
            cVar.f24638u.setText((CharSequence) hashMap.get("Contact No1"));
            cVar.f24638u.setVisibility(0);
        }
        if (!hashMap.containsKey("Contact No2") || ((String) hashMap.get("Contact No2")).equals("")) {
            cVar.f24639v.setVisibility(8);
        } else {
            cVar.f24639v.setText((CharSequence) hashMap.get("Contact No2"));
            cVar.f24639v.setVisibility(0);
        }
        if (!hashMap.containsKey("Contact No3") || ((String) hashMap.get("Contact No3")).equals("")) {
            cVar.f24640w.setVisibility(8);
        } else {
            cVar.f24640w.setText((CharSequence) hashMap.get("Contact No3"));
            cVar.f24640w.setVisibility(0);
        }
        if (!hashMap.containsKey("Email") || ((String) hashMap.get("Email")).equals("")) {
            cVar.f24643z.setVisibility(8);
            cVar.f24641x.setVisibility(8);
        } else {
            cVar.f24643z.setVisibility(0);
            cVar.f24641x.setText((CharSequence) hashMap.get("Email"));
            cVar.f24641x.setVisibility(0);
        }
        cVar.f24643z.setOnClickListener(new C7086a(hashMap, i));
        cVar.f24636A.setOnClickListener(new C7087b(hashMap, i));
    }

    /* renamed from: v */
    public C7088c mo3302l(ViewGroup viewGroup, int i) {
        return new C7088c(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_contacts, viewGroup, false));
    }
}
