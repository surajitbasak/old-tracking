package com.spreadsheet.app.activities;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.C0067c;
import com.spreadsheet.app.R;
import p153e.p300e.p301a.p303b.C7570a;

public class ActivityTermsOfUse extends C0067c {

    /* renamed from: u */
    C7570a f24350u;

    /* renamed from: com.spreadsheet.app.activities.ActivityTermsOfUse$a */
    class C6982a implements View.OnClickListener {
        C6982a() {
        }

        public void onClick(View view) {
            ActivityTermsOfUse.this.finish();
        }
    }

    /* renamed from: u0 */
    private void m33681u0() {
        this.f24350u.f25944b.setTitle((CharSequence) "Terms & Conditions");
        this.f24350u.f25944b.setTitleTextColor(getResources().getColor(R.color.black));
        this.f24350u.f25944b.setNavigationIcon((int) R.drawable.ic_back);
        this.f24350u.f25944b.setNavigationOnClickListener(new C6982a());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C7570a c = C7570a.m35476c(getLayoutInflater());
        this.f24350u = c;
        setContentView((View) c.mo25598b());
        m33681u0();
    }
}
