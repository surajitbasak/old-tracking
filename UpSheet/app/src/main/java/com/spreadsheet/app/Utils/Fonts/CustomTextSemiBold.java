package com.spreadsheet.app.Utils.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.spreadsheet.app.Utils.C6873b;

public class CustomTextSemiBold extends TextView {
    public CustomTextSemiBold(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo24201a();
    }

    /* renamed from: a */
    public void mo24201a() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), C6873b.f23975C));
    }
}
