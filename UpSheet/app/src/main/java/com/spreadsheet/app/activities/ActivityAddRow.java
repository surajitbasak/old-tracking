package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.location.C3275b;
import com.google.android.gms.location.C3281e;
import com.google.firebase.database.C4524a;
import com.google.firebase.database.C4527b;
import com.google.firebase.database.C4529d;
import com.google.firebase.database.C4534g;
import com.google.firebase.database.C4549p;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6876f;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomEditText;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.C7117b;
import com.wdullaer.materialdatetimepicker.time.C7158f;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import p153e.p174d.p176b.p190c.p217h.C6389d;
import p153e.p174d.p176b.p190c.p217h.C6391e;
import p153e.p174d.p267e.p268a0.p269a.C7318a;
import p153e.p174d.p267e.p268a0.p269a.C7319b;
import p153e.p300e.p301a.p302a.C7557b;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p302a.C7564i;
import p153e.p300e.p301a.p304c.C7573c;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivityAddRow extends C0067c implements C7598a, C7117b.C7121d, C7158f.C7167i, C7573c {

    /* renamed from: A */
    String f24056A = "";

    /* renamed from: B */
    String f24057B = "";

    /* renamed from: C */
    int f24058C = 1;

    /* renamed from: D */
    C4529d f24059D;

    /* renamed from: E */
    C4529d f24060E;

    /* renamed from: F */
    C7590d f24061F = C7590d.m35558a();

    /* renamed from: G */
    C7560e f24062G;

    /* renamed from: H */
    int f24063H;

    /* renamed from: I */
    C7596g f24064I = C7596g.m35606z();

    /* renamed from: J */
    List<C7557b> f24065J = new ArrayList();

    /* renamed from: K */
    List<View> f24066K = new ArrayList();

    /* renamed from: L */
    List<String> f24067L = new ArrayList();

    /* renamed from: M */
    List<String> f24068M = new ArrayList();

    /* renamed from: N */
    C7564i f24069N;

    /* renamed from: O */
    boolean f24070O = false;

    /* renamed from: P */
    C7562g f24071P = C7562g.getInstance();

    /* renamed from: Q */
    String f24072Q = "";

    /* renamed from: R */
    C6875d f24073R = C6875d.m33482b();

    /* renamed from: S */
    C6872a f24074S = C6872a.m33458a();

    /* renamed from: T */
    C7591e f24075T = C7591e.m35564f();

    /* renamed from: U */
    String f24076U = "";

    /* renamed from: V */
    C6876f f24077V;

    /* renamed from: W */
    C3275b f24078W;

    /* renamed from: X */
    HashMap<String, String> f24079X = new HashMap<>();

    /* renamed from: Y */
    C7564i f24080Y;
    @BindView(2131296344)
    CustomTextView btnTryAgain;
    @BindView(2131296366)
    Button buttonSubmit;
    @BindView(2131296578)
    LinearLayout layoutInputs;
    @BindView(2131296608)
    ConstraintLayout layoutTryAgain;
    @BindView(2131296926)
    Toolbar toolbarAddRow;

    /* renamed from: u */
    C6879h f24081u = C6879h.m33499c();

    /* renamed from: v */
    String f24082v = "";

    /* renamed from: w */
    String f24083w = "";

    /* renamed from: x */
    int f24084x = 0;

    /* renamed from: y */
    String f24085y = "";

    /* renamed from: z */
    String f24086z = "";

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$a */
    class C6881a implements C6391e<Location> {
        C6881a() {
        }

        /* renamed from: b */
        public void mo18618a(Location location) {
            if (location != null) {
                ActivityAddRow.this.f24073R.mo24211a();
                for (int i = 0; i < ActivityAddRow.this.f24066K.size(); i++) {
                    View view = ActivityAddRow.this.f24066K.get(i);
                    if (view.getTag().toString().equals("Location")) {
                        CustomEditText customEditText = (CustomEditText) view.findViewById(R.id.edit_latitude);
                        CustomEditText customEditText2 = (CustomEditText) view.findViewById(R.id.edit_longitude);
                        if (customEditText.getTag().toString().equals(ActivityAddRow.this.f24076U)) {
                            customEditText.setText(location.getLatitude() + "");
                            customEditText2.setText(location.getLongitude() + "");
                            return;
                        }
                    }
                }
                return;
            }
            ActivityAddRow.this.m33512D0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$b */
    class C6882b implements Runnable {
        C6882b() {
        }

        public void run() {
            ActivityAddRow.this.m33512D0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$c */
    class C6883c implements DialogInterface.OnClickListener {
        C6883c() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivityAddRow.this.f24077V.mo24216e(false);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$d */
    class C6884d implements View.OnClickListener {
        C6884d() {
        }

        public void onClick(View view) {
            ActivityAddRow.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$e */
    class C6885e implements C4549p {

        /* renamed from: a */
        final /* synthetic */ List f24091a;

        /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$e$a */
        class C6886a implements C4549p {

            /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$e$a$a */
            class C6887a implements Comparator<C7557b> {
                C6887a(C6886a aVar) {
                }

                /* renamed from: a */
                public int compare(C7557b bVar, C7557b bVar2) {
                    return String.valueOf(bVar.getColNum()).compareTo(String.valueOf(bVar2.getColNum()));
                }
            }

            C6886a() {
            }

            /* renamed from: a */
            public void mo18610a(C4527b bVar) {
            }

            /* renamed from: b */
            public void mo18611b(C4524a aVar) {
                C6885e.this.f24091a.clear();
                for (C4524a h : aVar.mo18573d()) {
                    C6885e.this.f24091a.add((C7557b) h.mo18577h(C7557b.class));
                }
                Collections.sort(C6885e.this.f24091a, new C6887a(this));
                ActivityAddRow.this.f24073R.mo24211a();
                C6885e eVar = C6885e.this;
                ActivityAddRow.this.f24071P.setColumnList(eVar.f24091a);
                ActivityAddRow.this.m33514F0();
            }
        }

        C6885e(List list) {
            this.f24091a = list;
        }

        /* renamed from: a */
        public void mo18610a(C4527b bVar) {
            ActivityAddRow.this.f24073R.mo24211a();
        }

        /* renamed from: b */
        public void mo18611b(C4524a aVar) {
            this.f24091a.clear();
            for (C4524a next : aVar.mo18573d()) {
                if (next.mo18574e().equalsIgnoreCase("columnInfo")) {
                    try {
                        JSONArray jSONArray = new JSONArray((String) next.mo18576g());
                        for (int i = 0; i < jSONArray.length(); i++) {
                            C7557b bVar = new C7557b();
                            bVar.setColNum(0);
                            bVar.setColumnId("");
                            bVar.setColumnName((String) jSONArray.getJSONObject(i).get(C6873b.f23994V));
                            bVar.setColumnType((String) jSONArray.getJSONObject(i).get(C6873b.f23995W));
                            bVar.setColumnData((String) jSONArray.getJSONObject(i).get(C6873b.f23996X));
                            this.f24091a.add(bVar);
                        }
                    } catch (Exception unused) {
                    }
                }
            }
            if (this.f24091a.size() > 0) {
                ActivityAddRow.this.f24071P.setColumnList(this.f24091a);
                ActivityAddRow.this.m33514F0();
                return;
            }
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            activityAddRow.f24059D.mo18590g(activityAddRow.f24062G.getSheetId()).mo18606b(new C6886a());
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$f */
    class C6888f implements View.OnClickListener {
        C6888f() {
        }

        public void onClick(View view) {
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            activityAddRow.f24076U = (String) view.getTag();
            activityAddRow.mo24236G0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$g */
    class C6889g implements View.OnClickListener {
        C6889g() {
        }

        public void onClick(View view) {
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            activityAddRow.f24076U = (String) view.getTag();
            activityAddRow.m33513E0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$h */
    class C6890h implements AdapterView.OnItemSelectedListener {

        /* renamed from: c */
        final /* synthetic */ AppCompatSpinner f24096c;

        /* renamed from: d */
        final /* synthetic */ List f24097d;

        C6890h(AppCompatSpinner appCompatSpinner, List list) {
            this.f24096c = appCompatSpinner;
            this.f24097d = list;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            ActivityAddRow.this.f24079X.put(this.f24096c.getTag().toString(), this.f24097d.get(i));
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$i */
    class C6891i implements View.OnClickListener {
        C6891i() {
        }

        public void onClick(View view) {
            C7117b bVar;
            Calendar instance = Calendar.getInstance();
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            if (activityAddRow.f24070O) {
                int parseInt = Integer.parseInt(activityAddRow.f24057B);
                ActivityAddRow activityAddRow2 = ActivityAddRow.this;
                bVar = C7117b.m33928y(activityAddRow, parseInt, activityAddRow2.f24058C, Integer.parseInt(activityAddRow2.f24056A));
            } else {
                bVar = C7117b.m33928y(activityAddRow, instance.get(1), instance.get(2), instance.get(5));
            }
            bVar.show(ActivityAddRow.this.getFragmentManager(), view.getTag().toString());
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$j */
    class C6892j implements View.OnClickListener {
        C6892j() {
        }

        public void onClick(View view) {
            Calendar instance = Calendar.getInstance();
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            (activityAddRow.f24070O ? C7158f.m34067D(activityAddRow, Integer.parseInt(activityAddRow.f24085y), Integer.parseInt(ActivityAddRow.this.f24086z), false) : C7158f.m34067D(activityAddRow, instance.get(11), instance.get(12), false)).show(ActivityAddRow.this.getFragmentManager(), view.getTag().toString());
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$k */
    class C6893k implements Runnable {
        C6893k() {
        }

        public void run() {
            ActivityAddRow activityAddRow = ActivityAddRow.this;
            activityAddRow.f24078W = C3281e.m19545a(activityAddRow);
            ActivityAddRow.this.mo24233A0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow$l */
    class C6894l implements C6389d {
        C6894l() {
        }

        /* renamed from: d */
        public void mo17810d(Exception exc) {
            ActivityAddRow.this.m33512D0();
        }
    }

    /* renamed from: B0 */
    private String m33511B0() {
        String str;
        StringBuilder sb;
        int i = this.f24063H;
        if (!this.f24070O) {
            i += 2;
        }
        this.f24062G.getColCount();
        switch (this.f24067L.size()) {
            case 2:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":B";
                break;
            case 3:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":C";
                break;
            case 4:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":D";
                break;
            case 5:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":E";
                break;
            case 6:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":F";
                break;
            case 7:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":G";
                break;
            case 8:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":H";
                break;
            case 9:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":I";
                break;
            case 10:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":J";
                break;
            case 11:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":K";
                break;
            case 12:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":L";
                break;
            case 13:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":M";
                break;
            case 14:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":N";
                break;
            case 15:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":O";
                break;
            case 16:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":P";
                break;
            case 17:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":Q";
                break;
            case 18:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":R";
                break;
            case 19:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":S";
                break;
            case 20:
                sb = new StringBuilder();
                sb.append(this.f24083w);
                sb.append("!A");
                sb.append(i);
                str = ":T";
                break;
            default:
                return "";
        }
        sb.append(str);
        sb.append(i);
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: D0 */
    public void m33512D0() {
        new Handler().postDelayed(new C6893k(), 1000);
    }

    /* access modifiers changed from: private */
    /* renamed from: E0 */
    public void m33513E0() {
        C7318a aVar = new C7318a(this);
        aVar.mo24944m(C7318a.f25291j);
        aVar.mo24946o("Scan Barcode/QrCode");
        aVar.mo24945n(false);
        aVar.mo24943l(0);
        aVar.mo24942k(false);
        aVar.mo24941j(true);
        aVar.mo24940f();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v53, resolved type: androidx.appcompat.widget.AppCompatSpinner} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v55, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v77, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v78, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v79, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v80, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v81, resolved type: com.spreadsheet.app.Utils.Fonts.CustomEditText} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0416, code lost:
        if (r9 < r0.f24069N.getCellsDataList().size()) goto L_0x0418;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x042c, code lost:
        r3.setText(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0507, code lost:
        if (r9 < r0.f24069N.getCellsDataList().size()) goto L_0x0418;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0649  */
    /* renamed from: F0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m33514F0() {
        /*
            r19 = this;
            r0 = r19
            java.lang.String r1 = ":"
            java.lang.String r2 = " "
            android.content.res.Resources r3 = r19.getResources()
            r4 = 2131165386(0x7f0700ca, float:1.7944988E38)
            float r3 = r3.getDimension(r4)
            int r3 = (int) r3
            android.content.res.Resources r4 = r19.getResources()
            r5 = 2131165346(0x7f0700a2, float:1.7944907E38)
            float r4 = r4.getDimension(r5)
            int r4 = (int) r4
            android.content.res.Resources r5 = r19.getResources()
            r6 = 2131165359(0x7f0700af, float:1.7944933E38)
            r5.getDimension(r6)
            android.content.res.Resources r5 = r19.getResources()
            r6 = 2131165356(0x7f0700ac, float:1.7944927E38)
            r5.getDimension(r6)
            android.content.res.Resources r5 = r19.getResources()
            r7 = 2131165384(0x7f0700c8, float:1.7944984E38)
            float r5 = r5.getDimension(r7)
            int r5 = (int) r5
            android.content.res.Resources r7 = r19.getResources()
            r8 = 2131165397(0x7f0700d5, float:1.794501E38)
            float r7 = r7.getDimension(r8)
            int r7 = (int) r7
            android.widget.LinearLayout r8 = r0.layoutInputs
            r8.removeAllViews()
            java.util.List<android.view.View> r8 = r0.f24066K
            r8.clear()
            r8 = 0
            r9 = 0
        L_0x0056:
            java.util.List<e.e.a.a.b> r10 = r0.f24065J
            int r10 = r10.size()
            if (r9 >= r10) goto L_0x0682
            java.util.List<e.e.a.a.b> r10 = r0.f24065J
            java.lang.Object r10 = r10.get(r9)
            e.e.a.a.b r10 = (p153e.p300e.p301a.p302a.C7557b) r10
            android.widget.LinearLayout$LayoutParams r11 = new android.widget.LinearLayout$LayoutParams
            r12 = -1
            r11.<init>(r12, r5)
            r11.setMargins(r3, r3, r3, r3)
            java.lang.String r13 = r10.getColumnType()
            java.lang.String r14 = "Location"
            boolean r13 = r13.equals(r14)
            r15 = 0
            java.lang.String r12 = ","
            java.lang.String r6 = ""
            if (r13 == 0) goto L_0x0144
            android.view.LayoutInflater r11 = r19.getLayoutInflater()
            r13 = 2131493019(0x7f0c009b, float:1.8609506E38)
            android.view.View r11 = r11.inflate(r13, r15, r8)
            r13 = 2131296865(0x7f090261, float:1.8211659E38)
            android.view.View r13 = r11.findViewById(r13)
            android.widget.TextView r13 = (android.widget.TextView) r13
            r15 = 2131296456(0x7f0900c8, float:1.821083E38)
            android.view.View r15 = r11.findViewById(r15)
            android.widget.EditText r15 = (android.widget.EditText) r15
            r8 = 2131296457(0x7f0900c9, float:1.8210831E38)
            android.view.View r8 = r11.findViewById(r8)
            android.widget.EditText r8 = (android.widget.EditText) r8
            r16 = r5
            r5 = 2131296520(0x7f090108, float:1.821096E38)
            android.view.View r5 = r11.findViewById(r5)
            android.widget.ImageView r5 = (android.widget.ImageView) r5
            r17 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r9)
            r18 = r7
            java.lang.String r7 = r10.getColumnName()
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r15.setTag(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r9)
            java.lang.String r7 = r10.getColumnName()
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r5.setTag(r3)
            java.lang.String r3 = r10.getColumnName()
            r13.setText(r3)
            com.spreadsheet.app.activities.ActivityAddRow$f r3 = new com.spreadsheet.app.activities.ActivityAddRow$f
            r3.<init>()
            r5.setOnClickListener(r3)
            r11.setTag(r14)
            boolean r3 = r0.f24070O
            if (r3 == 0) goto L_0x0133
            e.e.a.a.i r3 = r0.f24069N
            java.util.List r3 = r3.getCellsDataList()
            int r3 = r3.size()
            if (r9 >= r3) goto L_0x012d
            e.e.a.a.i r3 = r0.f24069N
            java.util.List r3 = r3.getCellsDataList()
            java.lang.Object r3 = r3.get(r9)
            e.e.a.a.f r3 = (p153e.p300e.p301a.p302a.C7561f) r3
            java.lang.String r3 = r3.getColData()
            int r5 = r3.indexOf(r12)
            r6 = 0
            java.lang.String r5 = r3.substring(r6, r5)
            int r6 = r3.indexOf(r12)
            r7 = 1
            int r6 = r6 + r7
            java.lang.String r3 = r3.substring(r6)
            r15.setText(r5)
            r8.setText(r3)
            goto L_0x0133
        L_0x012d:
            r15.setText(r6)
            r8.setText(r6)
        L_0x0133:
            android.widget.LinearLayout r3 = r0.layoutInputs
            r3.addView(r11)
            java.util.List<android.view.View> r3 = r0.f24066K
            r3.add(r11)
        L_0x013d:
            r14 = r17
            r13 = r18
        L_0x0141:
            r15 = 0
            goto L_0x0676
        L_0x0144:
            r17 = r3
            r16 = r5
            r18 = r7
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f23992T
            boolean r3 = r3.equals(r5)
            if (r3 == 0) goto L_0x01e3
            android.view.LayoutInflater r3 = r19.getLayoutInflater()
            r5 = 2131493020(0x7f0c009c, float:1.8609508E38)
            r7 = 0
            android.view.View r3 = r3.inflate(r5, r15, r7)
            r5 = 2131296452(0x7f0900c4, float:1.8210821E38)
            android.view.View r5 = r3.findViewById(r5)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r5 = (com.spreadsheet.app.Utils.Fonts.CustomEditText) r5
            java.lang.String r7 = r10.getColumnName()
            r5.setHint(r7)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r9)
            java.lang.String r8 = r10.getColumnName()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5.setTag(r7)
            r7 = 2131296537(0x7f090119, float:1.8210993E38)
            android.view.View r7 = r3.findViewById(r7)
            android.widget.ImageView r7 = (android.widget.ImageView) r7
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r9)
            java.lang.String r10 = r10.getColumnName()
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r7.setTag(r8)
            com.spreadsheet.app.activities.ActivityAddRow$g r8 = new com.spreadsheet.app.activities.ActivityAddRow$g
            r8.<init>()
            r7.setOnClickListener(r8)
            boolean r7 = r0.f24070O
            if (r7 == 0) goto L_0x01d2
            e.e.a.a.i r7 = r0.f24069N
            java.util.List r7 = r7.getCellsDataList()
            int r7 = r7.size()
            if (r9 >= r7) goto L_0x01cf
            e.e.a.a.i r6 = r0.f24069N
            java.util.List r6 = r6.getCellsDataList()
            java.lang.Object r6 = r6.get(r9)
            e.e.a.a.f r6 = (p153e.p300e.p301a.p302a.C7561f) r6
            java.lang.String r6 = r6.getColData()
        L_0x01cf:
            r5.setText(r6)
        L_0x01d2:
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f23992T
            r3.setTag(r5)
        L_0x01d7:
            android.widget.LinearLayout r5 = r0.layoutInputs
            r5.addView(r3)
        L_0x01dc:
            java.util.List<android.view.View> r5 = r0.f24066K
            r5.add(r3)
            goto L_0x013d
        L_0x01e3:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.f23993U
            boolean r3 = r3.equals(r5)
            r5 = 2131230947(0x7f0800e3, float:1.8077961E38)
            if (r3 == 0) goto L_0x027b
            androidx.appcompat.widget.AppCompatSpinner r3 = new androidx.appcompat.widget.AppCompatSpinner
            r3.<init>(r0)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.String r8 = r10.getColumnName()
            r7.add(r8)
            java.lang.String r8 = r10.getColumnData()
            r8.contains(r12)
            java.lang.String[] r8 = r8.split(r12)
            r12 = 0
        L_0x020f:
            int r13 = r8.length
            if (r12 >= r13) goto L_0x021a
            r13 = r8[r12]
            r7.add(r13)
            int r12 = r12 + 1
            goto L_0x020f
        L_0x021a:
            r3.setLayoutParams(r11)
            r3.setBackgroundResource(r5)
            com.spreadsheet.app.adapters.g r5 = new com.spreadsheet.app.adapters.g
            r5.<init>(r0, r7)
            r3.setAdapter((android.widget.SpinnerAdapter) r5)
            com.spreadsheet.app.activities.ActivityAddRow$h r5 = new com.spreadsheet.app.activities.ActivityAddRow$h
            r5.<init>(r3, r7)
            r3.setOnItemSelectedListener(r5)
            boolean r5 = r0.f24070O
            if (r5 == 0) goto L_0x0269
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            int r5 = r5.size()
            if (r9 >= r5) goto L_0x0264
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            java.lang.Object r5 = r5.get(r9)
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5
            java.lang.String r5 = r5.getColData()
            java.util.HashMap<java.lang.String, java.lang.String> r6 = r0.f24079X
            java.lang.String r8 = r10.getColumnName()
            r6.put(r8, r5)
            boolean r6 = r7.contains(r5)
            if (r6 == 0) goto L_0x0272
            int r5 = r7.indexOf(r5)
            goto L_0x0265
        L_0x0264:
            r5 = 0
        L_0x0265:
            r3.setSelection(r5)
            goto L_0x0272
        L_0x0269:
            java.util.HashMap<java.lang.String, java.lang.String> r5 = r0.f24079X
            java.lang.String r7 = r10.getColumnName()
            r5.put(r7, r6)
        L_0x0272:
            java.lang.String r5 = r10.getColumnName()
            r3.setTag(r5)
            goto L_0x01d7
        L_0x027b:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f23987O
            boolean r3 = r3.equals(r7)
            java.lang.String r7 = "Website"
            java.lang.String r8 = "Mobile"
            java.lang.String r12 = "Email"
            if (r3 != 0) goto L_0x05e2
            java.lang.String r3 = r10.getColumnType()
            boolean r3 = r3.equals(r12)
            if (r3 != 0) goto L_0x05e2
            java.lang.String r3 = r10.getColumnType()
            boolean r3 = r3.equals(r8)
            if (r3 != 0) goto L_0x05e2
            java.lang.String r3 = r10.getColumnType()
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x02ad
            goto L_0x05e2
        L_0x02ad:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f23989Q
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x030f
            com.spreadsheet.app.Utils.Fonts.CustomEditText r3 = new com.spreadsheet.app.Utils.Fonts.CustomEditText
            r3.<init>(r0)
            java.lang.String r7 = r10.getColumnName()
            r3.setHint(r7)
            r3.setLayoutParams(r11)
            r7 = 8194(0x2002, float:1.1482E-41)
            r3.setInputType(r7)
            r3.setBackgroundResource(r5)
            r3.setTag(r6)
            android.content.res.Resources r5 = r19.getResources()
            r7 = 2131165356(0x7f0700ac, float:1.7944927E38)
            float r5 = r5.getDimension(r7)
            r7 = 0
            r3.setTextSize(r7, r5)
            r3.setPadding(r4, r7, r7, r7)
            boolean r5 = r0.f24070O
            if (r5 == 0) goto L_0x01d7
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            int r5 = r5.size()
            if (r9 >= r5) goto L_0x030a
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            java.lang.Object r5 = r5.get(r9)
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5
            java.lang.String r5 = r5.getColData()
            r3.setText(r5)
            goto L_0x01d7
        L_0x030a:
            r3.setText(r6)
            goto L_0x01d7
        L_0x030f:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f23988P
            boolean r3 = r3.equals(r7)
            if (r3 != 0) goto L_0x0575
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r7 = "Large Text"
            boolean r3 = r3.equals(r7)
            if (r3 == 0) goto L_0x0329
            goto L_0x0575
        L_0x0329:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r7 = com.spreadsheet.app.Utils.C6873b.f23990R
            boolean r3 = r3.equals(r7)
            r7 = 16
            r8 = 2131099968(0x7f060140, float:1.7812304E38)
            if (r3 == 0) goto L_0x043b
            com.spreadsheet.app.Utils.Fonts.CustomTextView r3 = new com.spreadsheet.app.Utils.Fonts.CustomTextView
            r3.<init>(r0)
            r3.setLayoutParams(r11)
            java.lang.String r10 = r10.getColumnName()
            r3.setHint(r10)
            android.content.res.Resources r10 = r19.getResources()
            r11 = 2131165356(0x7f0700ac, float:1.7944927E38)
            float r10 = r10.getDimension(r11)
            r11 = 0
            r3.setTextSize(r11, r10)
            android.content.res.Resources r10 = r19.getResources()
            int r8 = r10.getColor(r8)
            r3.setTextColor(r8)
            r3.setPadding(r4, r11, r4, r11)
            r3.setBackgroundResource(r5)
            r5 = 2131230908(0x7f0800bc, float:1.8077882E38)
            r3.setCompoundDrawablesWithIntrinsicBounds(r11, r11, r5, r11)
            r3.setGravity(r7)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "date"
            r5.append(r7)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r3.setTag(r5)
            e.e.a.a.i r5 = r0.f24069N     // Catch:{ Exception -> 0x03d7 }
            java.util.List r5 = r5.getCellsDataList()     // Catch:{ Exception -> 0x03d7 }
            java.lang.Object r5 = r5.get(r9)     // Catch:{ Exception -> 0x03d7 }
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5     // Catch:{ Exception -> 0x03d7 }
            java.lang.String r5 = r5.getColData()     // Catch:{ Exception -> 0x03d7 }
            int r7 = r5.indexOf(r2)     // Catch:{ Exception -> 0x03d7 }
            r8 = 0
            java.lang.String r7 = r5.substring(r8, r7)     // Catch:{ Exception -> 0x03d7 }
            r0.f24056A = r7     // Catch:{ Exception -> 0x03d7 }
            int r7 = r5.indexOf(r2)     // Catch:{ Exception -> 0x03d7 }
            r8 = 1
            int r7 = r7 + r8
            int r10 = r5.lastIndexOf(r2)     // Catch:{ Exception -> 0x03d7 }
            java.lang.String r7 = r5.substring(r7, r10)     // Catch:{ Exception -> 0x03d7 }
            int r10 = r5.lastIndexOf(r2)     // Catch:{ Exception -> 0x03d7 }
            int r10 = r10 + r8
            java.lang.String r5 = r5.substring(r10)     // Catch:{ Exception -> 0x03d7 }
            r0.f24057B = r5     // Catch:{ Exception -> 0x03d7 }
            int r5 = com.spreadsheet.app.Utils.C6873b.m33476n(r7)     // Catch:{ Exception -> 0x03d7 }
            r0.f24058C = r5     // Catch:{ Exception -> 0x03d7 }
            java.lang.String r5 = "DATE"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03d7 }
            r7.<init>()     // Catch:{ Exception -> 0x03d7 }
            r7.append(r6)     // Catch:{ Exception -> 0x03d7 }
            int r8 = r0.f24058C     // Catch:{ Exception -> 0x03d7 }
            r7.append(r8)     // Catch:{ Exception -> 0x03d7 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x03d7 }
            android.util.Log.e(r5, r7)     // Catch:{ Exception -> 0x03d7 }
            goto L_0x0400
        L_0x03d7:
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r7 = 5
            int r5 = r5.get(r7)
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r0.f24056A = r5
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r7 = 1
            int r5 = r5.get(r7)
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r0.f24057B = r5
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r7 = 2
            int r5 = r5.get(r7)
            r0.f24058C = r5
        L_0x0400:
            com.spreadsheet.app.activities.ActivityAddRow$i r5 = new com.spreadsheet.app.activities.ActivityAddRow$i
            r5.<init>()
            r3.setOnClickListener(r5)
            boolean r5 = r0.f24070O
            if (r5 == 0) goto L_0x042f
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            int r5 = r5.size()
            if (r9 >= r5) goto L_0x042c
        L_0x0418:
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            java.lang.Object r5 = r5.get(r9)
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5
            java.lang.String r5 = r5.getColData()
            r3.setText(r5)
            goto L_0x042f
        L_0x042c:
            r3.setText(r6)
        L_0x042f:
            java.util.List<android.view.View> r5 = r0.f24066K
            r5.add(r3)
            android.widget.LinearLayout r5 = r0.layoutInputs
            r5.addView(r3)
            goto L_0x013d
        L_0x043b:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r12 = com.spreadsheet.app.Utils.C6873b.f23991S
            boolean r3 = r3.equals(r12)
            if (r3 == 0) goto L_0x050b
            com.spreadsheet.app.Utils.Fonts.CustomTextView r3 = new com.spreadsheet.app.Utils.Fonts.CustomTextView
            r3.<init>(r0)
            r3.setLayoutParams(r11)
            java.lang.String r10 = r10.getColumnName()
            r3.setHint(r10)
            android.content.res.Resources r10 = r19.getResources()
            r11 = 2131165356(0x7f0700ac, float:1.7944927E38)
            float r10 = r10.getDimension(r11)
            r11 = 0
            r3.setTextSize(r11, r10)
            android.content.res.Resources r10 = r19.getResources()
            int r8 = r10.getColor(r8)
            r3.setTextColor(r8)
            r3.setPadding(r4, r11, r4, r11)
            r3.setBackgroundResource(r5)
            r5 = 2131230944(0x7f0800e0, float:1.8077955E38)
            r3.setCompoundDrawablesWithIntrinsicBounds(r11, r11, r5, r11)
            r3.setGravity(r7)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "time"
            r5.append(r7)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r3.setTag(r5)
            e.e.a.a.i r5 = r0.f24069N     // Catch:{ Exception -> 0x04d1 }
            java.util.List r5 = r5.getCellsDataList()     // Catch:{ Exception -> 0x04d1 }
            java.lang.Object r5 = r5.get(r9)     // Catch:{ Exception -> 0x04d1 }
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5     // Catch:{ Exception -> 0x04d1 }
            java.lang.String r5 = r5.getColData()     // Catch:{ Exception -> 0x04d1 }
            java.text.SimpleDateFormat r7 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x04d1 }
            java.lang.String r8 = "HH:mm"
            r7.<init>(r8)     // Catch:{ Exception -> 0x04d1 }
            java.text.SimpleDateFormat r8 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x04d1 }
            java.lang.String r10 = "hh:mm a"
            r8.<init>(r10)     // Catch:{ Exception -> 0x04d1 }
            java.util.Date r5 = r8.parse(r5)     // Catch:{ Exception -> 0x04d1 }
            java.lang.String r5 = r7.format(r5)     // Catch:{ Exception -> 0x04d1 }
            int r7 = r5.indexOf(r1)     // Catch:{ Exception -> 0x04d1 }
            r8 = 0
            java.lang.String r7 = r5.substring(r8, r7)     // Catch:{ Exception -> 0x04d1 }
            r0.f24085y = r7     // Catch:{ Exception -> 0x04d1 }
            int r7 = r5.indexOf(r1)     // Catch:{ Exception -> 0x04d1 }
            r8 = 1
            int r7 = r7 + r8
            java.lang.String r5 = r5.substring(r7)     // Catch:{ Exception -> 0x04d1 }
            r0.f24086z = r5     // Catch:{ Exception -> 0x04d1 }
            goto L_0x04f1
        L_0x04d1:
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r7 = 11
            int r5 = r5.get(r7)
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r0.f24085y = r5
            java.util.Calendar r5 = java.util.Calendar.getInstance()
            r7 = 12
            int r5 = r5.get(r7)
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r0.f24086z = r5
        L_0x04f1:
            com.spreadsheet.app.activities.ActivityAddRow$j r5 = new com.spreadsheet.app.activities.ActivityAddRow$j
            r5.<init>()
            r3.setOnClickListener(r5)
            boolean r5 = r0.f24070O
            if (r5 == 0) goto L_0x042f
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            int r5 = r5.size()
            if (r9 >= r5) goto L_0x042c
            goto L_0x0418
        L_0x050b:
            java.lang.String r3 = r10.getColumnType()
            java.lang.String r5 = "AutoTimeStamp"
            boolean r3 = r3.equals(r5)
            if (r3 == 0) goto L_0x013d
            android.view.LayoutInflater r3 = r19.getLayoutInflater()
            r7 = 2131493016(0x7f0c0098, float:1.86095E38)
            r8 = 0
            android.view.View r3 = r3.inflate(r7, r15, r8)
            r7 = 2131296394(0x7f09008a, float:1.8210703E38)
            android.view.View r7 = r3.findViewById(r7)
            android.widget.CheckBox r7 = (android.widget.CheckBox) r7
            boolean r8 = r0.f24070O
            if (r8 == 0) goto L_0x0570
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r11 = "Update "
            r8.append(r11)
            java.lang.String r10 = r10.getColumnName()
            r8.append(r10)
            java.lang.String r8 = r8.toString()
            r7.setText(r8)
            e.e.a.a.i r8 = r0.f24069N
            java.util.List r8 = r8.getCellsDataList()
            int r8 = r8.size()
            if (r9 >= r8) goto L_0x056d
            e.e.a.a.i r6 = r0.f24069N
            java.util.List r6 = r6.getCellsDataList()
            java.lang.Object r6 = r6.get(r9)
            e.e.a.a.f r6 = (p153e.p300e.p301a.p302a.C7561f) r6
            java.lang.String r6 = r6.getColData()
            r7.setTag(r6)
            android.widget.LinearLayout r6 = r0.layoutInputs
            r6.addView(r3)
            goto L_0x0570
        L_0x056d:
            r7.setTag(r6)
        L_0x0570:
            r3.setTag(r5)
            goto L_0x01dc
        L_0x0575:
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r13 = r18
            r7 = -1
            r3.<init>(r7, r13)
            r14 = r17
            r3.setMargins(r14, r14, r14, r14)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = new com.spreadsheet.app.Utils.Fonts.CustomEditText
            r7.<init>(r0)
            java.lang.String r8 = r10.getColumnName()
            r7.setHint(r8)
            r7.setLayoutParams(r3)
            r3 = 147457(0x24001, float:2.06631E-40)
            r7.setInputType(r3)
            r7.setBackgroundResource(r5)
            r7.setTag(r6)
            android.content.res.Resources r3 = r19.getResources()
            r5 = 2131165356(0x7f0700ac, float:1.7944927E38)
            float r3 = r3.getDimension(r5)
            r5 = 0
            r7.setTextSize(r5, r3)
            r7.setPadding(r4, r5, r5, r5)
            boolean r3 = r0.f24070O
            if (r3 == 0) goto L_0x05d6
            e.e.a.a.i r3 = r0.f24069N
            java.util.List r3 = r3.getCellsDataList()
            int r3 = r3.size()
            if (r9 >= r3) goto L_0x05d3
            e.e.a.a.i r3 = r0.f24069N
            java.util.List r3 = r3.getCellsDataList()
            java.lang.Object r3 = r3.get(r9)
            e.e.a.a.f r3 = (p153e.p300e.p301a.p302a.C7561f) r3
            java.lang.String r3 = r3.getColData()
            r7.setText(r3)
            goto L_0x05d6
        L_0x05d3:
            r7.setText(r6)
        L_0x05d6:
            android.widget.LinearLayout r3 = r0.layoutInputs
            r3.addView(r7)
            java.util.List<android.view.View> r3 = r0.f24066K
            r3.add(r7)
            goto L_0x0141
        L_0x05e2:
            r14 = r17
            r13 = r18
            com.spreadsheet.app.Utils.Fonts.CustomEditText r3 = new com.spreadsheet.app.Utils.Fonts.CustomEditText
            r3.<init>(r0)
            java.lang.String r15 = r10.getColumnName()
            r3.setHint(r15)
            r3.setLayoutParams(r11)
            r3.setBackgroundResource(r5)
            r3.setTag(r6)
            android.content.res.Resources r5 = r19.getResources()
            r11 = 2131165356(0x7f0700ac, float:1.7944927E38)
            float r5 = r5.getDimension(r11)
            r15 = 0
            r3.setTextSize(r15, r5)
            r3.setPadding(r4, r15, r15, r15)
            java.lang.String r5 = r10.getColumnType()
            java.lang.String r11 = com.spreadsheet.app.Utils.C6873b.f23987O
            boolean r5 = r5.equals(r11)
            if (r5 == 0) goto L_0x061f
            r5 = 16385(0x4001, float:2.296E-41)
        L_0x061b:
            r3.setInputType(r5)
            goto L_0x0645
        L_0x061f:
            java.lang.String r5 = r10.getColumnType()
            boolean r5 = r5.equals(r12)
            if (r5 == 0) goto L_0x062c
            r5 = 33
            goto L_0x061b
        L_0x062c:
            java.lang.String r5 = r10.getColumnType()
            boolean r5 = r5.equals(r8)
            if (r5 == 0) goto L_0x0638
            r5 = 3
            goto L_0x061b
        L_0x0638:
            java.lang.String r5 = r10.getColumnType()
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0645
            r5 = 17
            goto L_0x061b
        L_0x0645:
            boolean r5 = r0.f24070O
            if (r5 == 0) goto L_0x066c
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            int r5 = r5.size()
            if (r9 >= r5) goto L_0x0669
            e.e.a.a.i r5 = r0.f24069N
            java.util.List r5 = r5.getCellsDataList()
            java.lang.Object r5 = r5.get(r9)
            e.e.a.a.f r5 = (p153e.p300e.p301a.p302a.C7561f) r5
            java.lang.String r5 = r5.getColData()
            r3.setText(r5)
            goto L_0x066c
        L_0x0669:
            r3.setText(r6)
        L_0x066c:
            android.widget.LinearLayout r5 = r0.layoutInputs
            r5.addView(r3)
            java.util.List<android.view.View> r5 = r0.f24066K
            r5.add(r3)
        L_0x0676:
            int r9 = r9 + 1
            r7 = r13
            r3 = r14
            r5 = r16
            r6 = 2131165356(0x7f0700ac, float:1.7944927E38)
            r8 = 0
            goto L_0x0056
        L_0x0682:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityAddRow.m33514F0():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x007b, code lost:
        if (r4.getText().toString().equals("") != false) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0080, code lost:
        r10.f24067L.add(r4.getText().toString());
        r4 = r4.getText();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0152, code lost:
        if (r4.getText().toString().equals("") != false) goto L_0x007d;
     */
    /* renamed from: x0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m33518x0() {
        /*
            r10 = this;
            java.util.List<java.lang.String> r0 = r10.f24067L
            r0.clear()
            java.util.List<java.lang.String> r0 = r10.f24068M
            r0.clear()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            e.e.a.a.i r1 = new e.e.a.a.i
            r1.<init>()
            r10.f24080Y = r1
            boolean r2 = r10.f24070O
            if (r2 == 0) goto L_0x0021
            e.e.a.a.i r2 = r10.f24069N
            int r2 = r2.getRowPos()
            goto L_0x0023
        L_0x0021:
            int r2 = r10.f24063H
        L_0x0023:
            r1.setRowPos(r2)
            r1 = 1
            r2 = 0
            r3 = 0
        L_0x0029:
            java.util.List<android.view.View> r4 = r10.f24066K
            int r4 = r4.size()
            if (r3 >= r4) goto L_0x01d7
            java.util.List<android.view.View> r4 = r10.f24066K
            java.lang.Object r4 = r4.get(r3)
            android.view.View r4 = (android.view.View) r4
            java.lang.Object r5 = r4.getTag()
            java.lang.String r5 = r5.toString()
            e.e.a.a.f r6 = new e.e.a.a.f
            r6.<init>()
            java.util.List<e.e.a.a.b> r7 = r10.f24065J
            java.lang.Object r7 = r7.get(r3)
            e.e.a.a.b r7 = (p153e.p300e.p301a.p302a.C7557b) r7
            java.lang.String r8 = r7.getColumnName()
            r6.setColName(r8)
            java.lang.String r8 = r7.getColumnType()
            r6.setColType(r8)
            java.lang.String r8 = com.spreadsheet.app.Utils.C6873b.f23992T
            boolean r8 = r5.equals(r8)
            java.lang.String r9 = ""
            if (r8 == 0) goto L_0x009a
            r5 = 2131296452(0x7f0900c4, float:1.8210821E38)
            android.view.View r4 = r4.findViewById(r5)
            android.widget.EditText r4 = (android.widget.EditText) r4
            android.text.Editable r5 = r4.getText()
            java.lang.String r5 = r5.toString()
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x0080
        L_0x007d:
            r1 = 0
            goto L_0x01c7
        L_0x0080:
            java.util.List<java.lang.String> r5 = r10.f24067L
            android.text.Editable r8 = r4.getText()
            java.lang.String r8 = r8.toString()
            r5.add(r8)
            android.text.Editable r4 = r4.getText()
        L_0x0091:
            java.lang.String r4 = r4.toString()
        L_0x0095:
            r6.setColData(r4)
            goto L_0x01c7
        L_0x009a:
            boolean r8 = r4 instanceof androidx.appcompat.widget.AppCompatSpinner
            if (r8 == 0) goto L_0x00d1
            java.util.List<android.view.View> r4 = r10.f24066K
            java.lang.Object r4 = r4.get(r3)
            androidx.appcompat.widget.AppCompatSpinner r4 = (androidx.appcompat.widget.AppCompatSpinner) r4
            int r5 = r4.getSelectedItemPosition()
            if (r5 != 0) goto L_0x00ad
            goto L_0x007d
        L_0x00ad:
            java.util.List<java.lang.String> r5 = r10.f24067L
            java.util.HashMap<java.lang.String, java.lang.String> r8 = r10.f24079X
            java.lang.Object r9 = r4.getTag()
            java.lang.String r9 = r9.toString()
            java.lang.Object r8 = r8.get(r9)
            r5.add(r8)
            java.util.HashMap<java.lang.String, java.lang.String> r5 = r10.f24079X
            java.lang.Object r4 = r4.getTag()
            java.lang.String r4 = r4.toString()
            java.lang.Object r4 = r5.get(r4)
            java.lang.String r4 = (java.lang.String) r4
            goto L_0x0095
        L_0x00d1:
            java.lang.String r8 = "Location"
            boolean r8 = r5.equals(r8)
            if (r8 == 0) goto L_0x013a
            r5 = 2131296456(0x7f0900c8, float:1.821083E38)
            android.view.View r5 = r4.findViewById(r5)
            android.widget.EditText r5 = (android.widget.EditText) r5
            r8 = 2131296457(0x7f0900c9, float:1.8210831E38)
            android.view.View r4 = r4.findViewById(r8)
            android.widget.EditText r4 = (android.widget.EditText) r4
            android.text.Editable r8 = r5.getText()
            java.lang.String r8 = r8.toString()
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x00fc
            r8 = r9
            r1 = 0
            goto L_0x0104
        L_0x00fc:
            android.text.Editable r8 = r5.getText()
            java.lang.String r8 = r8.toString()
        L_0x0104:
            android.text.Editable r5 = r5.getText()
            java.lang.String r5 = r5.toString()
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x0114
            r1 = 0
            goto L_0x0130
        L_0x0114:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r8)
            java.lang.String r8 = ","
            r5.append(r8)
            android.text.Editable r4 = r4.getText()
            java.lang.String r4 = r4.toString()
            r5.append(r4)
            java.lang.String r8 = r5.toString()
        L_0x0130:
            java.util.List<java.lang.String> r4 = r10.f24067L
            r4.add(r8)
            r6.setColData(r8)
            goto L_0x01c7
        L_0x013a:
            boolean r8 = r4 instanceof android.widget.EditText
            if (r8 == 0) goto L_0x0156
            java.util.List<android.view.View> r4 = r10.f24066K
            java.lang.Object r4 = r4.get(r3)
            android.widget.EditText r4 = (android.widget.EditText) r4
            android.text.Editable r5 = r4.getText()
            java.lang.String r5 = r5.toString()
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x0080
            goto L_0x007d
        L_0x0156:
            boolean r8 = r4 instanceof com.spreadsheet.app.Utils.Fonts.CustomTextView
            if (r8 == 0) goto L_0x0189
            java.util.List<android.view.View> r4 = r10.f24066K
            java.lang.Object r4 = r4.get(r3)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r4 = (com.spreadsheet.app.Utils.Fonts.CustomTextView) r4
            java.lang.CharSequence r5 = r4.getText()
            java.lang.String r5 = r5.toString()
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x0172
            goto L_0x007d
        L_0x0172:
            java.util.List<java.lang.String> r5 = r10.f24067L
            java.lang.CharSequence r8 = r4.getText()
            java.lang.String r8 = r8.toString()
            r5.add(r8)
            java.lang.CharSequence r4 = r4.getText()
            java.lang.String r4 = r4.toString()
            goto L_0x0095
        L_0x0189:
            java.lang.String r8 = "AutoTimeStamp"
            boolean r5 = r5.equals(r8)
            if (r5 == 0) goto L_0x01c7
            r5 = 2131296394(0x7f09008a, float:1.8210703E38)
            android.view.View r4 = r4.findViewById(r5)
            android.widget.CheckBox r4 = (android.widget.CheckBox) r4
            boolean r5 = r10.f24070O
            if (r5 == 0) goto L_0x01b8
            boolean r5 = r4.isChecked()
            if (r5 == 0) goto L_0x01a5
            goto L_0x01b8
        L_0x01a5:
            java.util.List<java.lang.String> r5 = r10.f24067L
            java.lang.Object r8 = r4.getTag()
            java.lang.String r8 = r8.toString()
            r5.add(r8)
            java.lang.Object r4 = r4.getTag()
            goto L_0x0091
        L_0x01b8:
            java.util.List<java.lang.String> r4 = r10.f24067L
            java.lang.String r5 = com.spreadsheet.app.Utils.C6873b.m33470h()
            r4.add(r5)
            java.lang.String r4 = com.spreadsheet.app.Utils.C6873b.m33470h()
            goto L_0x0095
        L_0x01c7:
            java.util.List<java.lang.String> r4 = r10.f24068M
            java.lang.String r5 = r7.getColumnType()
            r4.add(r5)
            r0.add(r6)
            int r3 = r3 + 1
            goto L_0x0029
        L_0x01d7:
            e.e.a.a.i r3 = r10.f24080Y
            r3.setCellsDataList(r0)
            e.e.a.a.i r3 = r10.f24080Y
            java.lang.Object r0 = r0.get(r2)
            e.e.a.a.f r0 = (p153e.p300e.p301a.p302a.C7561f) r0
            java.lang.String r0 = r0.getColData()
            r3.setRowTitle(r0)
            if (r1 != 0) goto L_0x01f7
            r0 = 2131820711(0x7f1100a7, float:1.9274145E38)
            android.widget.Toast r0 = android.widget.Toast.makeText(r10, r0, r2)
            r0.show()
        L_0x01f7:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityAddRow.m33518x0():boolean");
    }

    /* renamed from: z0 */
    private void m33519z0() {
        ArrayList arrayList = new ArrayList();
        if (this.f24074S.mo24207d()) {
            this.f24060E.mo18590g(this.f24072Q).mo18590g(this.f24062G.getSheetId()).mo18606b(new C6885e(arrayList));
            return;
        }
        this.f24073R.mo24211a();
        this.layoutTryAgain.setVisibility(0);
        Toast.makeText(this, "check internet connection", 0).show();
    }

    /* renamed from: A0 */
    public void mo24233A0() {
        this.f24078W.mo12400p().mo23031f(this, new C6881a()).mo23029d(new C6894l());
    }

    /* renamed from: C0 */
    public void mo24234C0() {
        this.f24074S.mo24206c(this);
        this.f24073R.mo24212c(this);
        this.f24064I.mo25662C(this, this);
        this.f24059D = C4534g.m24100b().mo18599d(C6873b.f23979G);
        this.f24060E = C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.f24061F.mo25637b(this);
        this.toolbarAddRow.setTitle((CharSequence) this.f24083w);
        this.toolbarAddRow.setTitleTextColor(getResources().getColor(R.color.black));
        this.toolbarAddRow.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarAddRow.setNavigationOnClickListener(new C6884d());
        this.f24072Q = this.f24081u.mo24222b(C6873b.f24024o);
        if (this.f24071P.getColumnList().size() > 0) {
            this.f24065J = this.f24071P.getColumnList();
            m33514F0();
            return;
        }
        this.f24073R.mo24213d("");
        m33519z0();
    }

    /* renamed from: G */
    public void mo24235G() {
        this.f24073R.mo24213d("");
        m33512D0();
    }

    /* renamed from: G0 */
    public void mo24236G0() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(getResources().getString(R.string.choose_current_loc), new C6883c());
        builder.show();
    }

    /* renamed from: H */
    public void mo24237H(String str, String str2) {
        str2.hashCode();
        if (str2.equals("addRow")) {
            if (str.split(":")[0].equals("com.android.volley.NoConnectionError")) {
                Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
            }
            this.f24073R.mo24211a();
        }
    }

    /* renamed from: J */
    public void mo24238J(Location location, String str, String str2) {
    }

    /* renamed from: L */
    public void mo24239L(String str) {
    }

    /* renamed from: Q */
    public void mo24240Q() {
        this.f24077V.mo24216e(true);
    }

    /* renamed from: b0 */
    public void mo24241b0(Location location) {
    }

    /* renamed from: f */
    public void mo24242f(C7158f fVar, int i, int i2, int i3) {
        StringBuilder sb;
        String str;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
            String format = simpleDateFormat.format(simpleDateFormat.parse(i + ":" + i2));
            if (i >= 12) {
                sb = new StringBuilder();
                sb.append(format);
                str = " PM";
            } else {
                sb = new StringBuilder();
                sb.append(format);
                str = " AM";
            }
            sb.append(str);
            String sb2 = sb.toString();
            for (int i4 = 0; i4 < this.f24066K.size(); i4++) {
                View view = this.f24066K.get(i4);
                if (view.getTag().toString().equals(fVar.getTag().toString())) {
                    ((CustomTextView) view).setText(sb2);
                    return;
                }
            }
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 79) {
            C7319b h = C7318a.m34443h(i, i2, intent);
            if (h != null && h.mo24948a() != null) {
                for (int i3 = 0; i3 < this.f24066K.size(); i3++) {
                    View view = this.f24066K.get(i3);
                    if (view.getTag().toString().equals(C6873b.f23992T)) {
                        CustomEditText customEditText = (CustomEditText) view.findViewById(R.id.edit_barcode);
                        if (customEditText.getTag().toString().equals(this.f24076U)) {
                            customEditText.setText(h.mo24948a());
                            customEditText.setSelection(h.mo24948a().length());
                            return;
                        }
                    }
                }
            }
        } else if (i2 == -1) {
            if (this.f24074S.mo24205b(new String[]{"android.permission.ACCESS_FINE_LOCATION"})) {
                this.f24077V.mo24215c();
                this.f24077V.mo24217f();
                return;
            }
            this.f24074S.mo24208e(new String[]{"android.permission.ACCESS_FINE_LOCATION"});
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_add_row);
        ButterKnife.bind((Activity) this);
        this.f24081u.mo24225f(this);
        this.f24077V = new C6876f(this, this);
        this.f24075T.mo25642h(this, this);
        C7560e eVar = (C7560e) getIntent().getSerializableExtra("spreadsheet");
        this.f24062G = eVar;
        this.f24082v = eVar.getSheetId();
        this.f24063H = getIntent().getExtras().getInt("listsize");
        this.f24083w = getIntent().getExtras().getString("sheetTitle");
        this.f24084x = getIntent().getExtras().getInt("sheetId");
        if (getIntent().hasExtra("rowmap")) {
            this.f24069N = (C7564i) getIntent().getSerializableExtra("rowmap");
            this.f24070O = true;
            this.buttonSubmit.setText(getResources().getString(R.string.update));
        }
        mo24234C0();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 1) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                this.f24074S.mo24208e(new String[]{"android.permission.ACCESS_FINE_LOCATION"});
                return;
            }
            this.f24073R.mo24213d("");
            new Handler().postDelayed(new C6882b(), 3000);
        }
    }

    @OnClick({2131296366, 2131296344})
    public void onViewClicked(View view) {
        int id = view.getId();
        if (id == R.id.btn_try_again) {
            this.layoutTryAgain.setVisibility(8);
            if (!isFinishing()) {
                this.f24073R.mo24213d("");
            }
            m33519z0();
        } else if (id != R.id.button_submit || !m33518x0()) {
        } else {
            if (this.f24074S.mo24207d()) {
                if (!isFinishing()) {
                    this.f24073R.mo24213d("");
                }
                m33511B0();
                this.f24064I.mo25666d(this.f24062G.getSheetId(), this.f24064I.mo25687y(this.f24084x, this.f24070O ? this.f24063H : this.f24063H + 1, this.f24067L, this.f24068M));
                return;
            }
            Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: s */
    public void mo24244s(String str, String str2) {
        int i;
        str2.hashCode();
        if (str2.equals("addFormattedRow")) {
            this.f24071P.isUpdated = true;
            this.f24075T.mo25647m(this.f24082v);
            if (this.f24070O) {
                this.f24061F.mo25638c("UPDATE_ROW", "UPDATE_ROW");
                i = R.string.row_updated_successfully;
            } else {
                this.f24061F.mo25638c("ADD_ROW", "ADD_ROW");
                i = R.string.row_added_successfully;
            }
            Toast.makeText(this, i, 0).show();
            Intent intent = new Intent();
            intent.putExtra("isUpdated", true);
            intent.putExtra("isEdit", this.f24070O);
            intent.putExtra("sheetrow", this.f24080Y);
            intent.putExtra("sheetname", this.f24062G.getSheetName());
            setResult(-1, intent);
            finish();
        }
    }

    /* renamed from: x */
    public void mo24245x(C7117b bVar, int i, int i2, int i3) {
        String str = i3 + " " + getResources().getStringArray(R.array.months)[i2] + " " + i;
        for (int i4 = 0; i4 < this.f24066K.size(); i4++) {
            View view = this.f24066K.get(i4);
            if (view.getTag().toString().equals(bVar.getTag().toString())) {
                ((CustomTextView) view).setText(str);
                return;
            }
        }
    }
}
