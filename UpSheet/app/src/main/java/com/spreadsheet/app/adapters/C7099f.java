package com.spreadsheet.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.List;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p304c.C7577g;

/* renamed from: com.spreadsheet.app.adapters.f */
public class C7099f extends RecyclerView.C0503g<C7103d> implements Filterable {

    /* renamed from: e */
    List<C7560e> f24676e = new ArrayList();

    /* renamed from: f */
    List<C7560e> f24677f = new ArrayList();

    /* renamed from: g */
    C7577g f24678g;

    /* renamed from: h */
    C7562g f24679h = C7562g.getInstance();

    /* renamed from: i */
    C7102c f24680i;

    /* renamed from: com.spreadsheet.app.adapters.f$a */
    class C7100a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24681c;

        C7100a(int i) {
            this.f24681c = i;
        }

        public void onClick(View view) {
            C7099f fVar = C7099f.this;
            C7562g gVar = fVar.f24679h;
            int i = this.f24681c;
            gVar.positionClicked = i;
            fVar.f24678g.mo24393F(fVar.f24677f.get(i));
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.f$b */
    class C7101b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24683c;

        C7101b(int i) {
            this.f24683c = i;
        }

        public void onClick(View view) {
            C7099f fVar = C7099f.this;
            fVar.f24678g.mo24400N(fVar.f24677f.get(this.f24683c), this.f24683c);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.f$c */
    public class C7102c extends Filter {
        public C7102c() {
        }

        /* access modifiers changed from: protected */
        public FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            List<C7560e> list = C7099f.this.f24676e;
            ArrayList arrayList = new ArrayList();
            String lowerCase = charSequence.toString().toLowerCase();
            for (int i = 0; i < list.size(); i++) {
                C7560e eVar = list.get(i);
                if (eVar.getSheetName().toLowerCase().contains(lowerCase)) {
                    arrayList.add(eVar);
                }
            }
            filterResults.count = arrayList.size();
            filterResults.values = arrayList;
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, FilterResults filterResults) {
            C7099f fVar = C7099f.this;
            fVar.f24677f = (List) filterResults.values;
            fVar.mo3298h();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.f$d */
    public class C7103d extends RecyclerView.C0500d0 {

        /* renamed from: t */
        CardView f24686t;

        /* renamed from: u */
        CustomTextView f24687u;

        /* renamed from: v */
        CustomTextView f24688v;

        /* renamed from: w */
        ImageView f24689w;

        public C7103d(C7099f fVar, View view) {
            super(view);
            this.f24686t = (CardView) view.findViewById(R.id.card_sheet);
            this.f24687u = (CustomTextView) view.findViewById(R.id.text_sheet_name);
            this.f24688v = (CustomTextView) view.findViewById(R.id.text_sheet_date);
            this.f24689w = (ImageView) view.findViewById(R.id.image_add_user);
        }
    }

    public C7099f(Context context, List<C7560e> list, C7577g gVar) {
        this.f24676e = list;
        this.f24677f = list;
        this.f24678g = gVar;
        this.f24680i = new C7102c();
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24677f.size();
    }

    public Filter getFilter() {
        return this.f24680i;
    }

    /* renamed from: u */
    public void mo3300j(C7103d dVar, int i) {
        C7560e eVar = this.f24677f.get(i);
        dVar.f24687u.setText(eVar.getSheetName());
        dVar.f24688v.setText(C6873b.m33471i(eVar.getDate()));
        dVar.f24686t.setOnClickListener(new C7100a(i));
        dVar.f24689w.setOnClickListener(new C7101b(i));
    }

    /* renamed from: v */
    public C7103d mo3302l(ViewGroup viewGroup, int i) {
        return new C7103d(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_sheet, viewGroup, false));
    }
}
