package com.spreadsheet.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.core.app.C0329a;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.List;
import p153e.p300e.p301a.p302a.C7561f;
import p153e.p300e.p301a.p302a.C7564i;
import p153e.p300e.p301a.p304c.C7576f;

/* renamed from: com.spreadsheet.app.adapters.e */
public class C7091e extends RecyclerView.C0503g<C7098e> implements Filterable, C0329a.C0331b {

    /* renamed from: e */
    Context f24649e;

    /* renamed from: f */
    String f24650f = "";

    /* renamed from: g */
    C6872a f24651g = C6872a.m33458a();

    /* renamed from: h */
    List<C7564i> f24652h = new ArrayList();

    /* renamed from: i */
    List<C7564i> f24653i = new ArrayList();

    /* renamed from: j */
    C7576f f24654j;

    /* renamed from: k */
    List<String> f24655k;

    /* renamed from: l */
    List<String> f24656l;

    /* renamed from: m */
    C7097d f24657m;

    /* renamed from: com.spreadsheet.app.adapters.e$a */
    class C7092a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ C7561f f24658c;

        C7092a(C7561f fVar) {
            this.f24658c = fVar;
        }

        public void onClick(View view) {
            String obj = view.getTag().toString();
            obj.hashCode();
            char c = 65535;
            switch (obj.hashCode()) {
                case -1984987966:
                    if (obj.equals("Mobile")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1405978501:
                    if (obj.equals("Website")) {
                        c = 1;
                        break;
                    }
                    break;
                case 67066748:
                    if (obj.equals("Email")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    C7091e.this.f24650f = this.f24658c.getColData();
                    if (C7091e.this.f24651g.mo24205b(new String[]{"android.permission.CALL_PHONE"})) {
                        C7091e.this.mo24488u(this.f24658c.getColData());
                        return;
                    }
                    C7091e.this.f24651g.mo24208e(new String[]{"android.permission.CALL_PHONE"});
                    return;
                case 1:
                    C7091e.this.mo24491x(this.f24658c.getColData());
                    return;
                case 2:
                    C7091e.this.mo24492y(this.f24658c.getColData());
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.e$b */
    class C7093b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24660c;

        /* renamed from: com.spreadsheet.app.adapters.e$b$a */
        class C7094a implements PopupMenu.OnMenuItemClickListener {
            C7094a() {
            }

            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_row_color:
                        C7093b bVar = C7093b.this;
                        int rowPos = C7091e.this.f24653i.get(bVar.f24660c).getRowPos();
                        C7093b bVar2 = C7093b.this;
                        C7091e eVar = C7091e.this;
                        eVar.f24654j.mo24339j(eVar.f24653i.get(bVar2.f24660c), rowPos);
                        return true;
                    case R.id.menu_row_delete:
                        C7093b bVar3 = C7093b.this;
                        int rowPos2 = C7091e.this.f24653i.get(bVar3.f24660c).getRowPos();
                        C7093b bVar4 = C7093b.this;
                        C7091e eVar2 = C7091e.this;
                        eVar2.f24654j.mo24338M(eVar2.f24653i.get(bVar4.f24660c), rowPos2);
                        return true;
                    case R.id.menu_row_edit:
                        C7093b bVar5 = C7093b.this;
                        int rowPos3 = C7091e.this.f24653i.get(bVar5.f24660c).getRowPos();
                        C7093b bVar6 = C7093b.this;
                        C7091e eVar3 = C7091e.this;
                        eVar3.f24654j.mo24336K(eVar3.f24653i.get(bVar6.f24660c), rowPos3);
                        return true;
                    default:
                        return true;
                }
            }
        }

        C7093b(int i) {
            this.f24660c = i;
        }

        public void onClick(View view) {
            PopupMenu popupMenu = new PopupMenu(C7091e.this.f24649e, view);
            popupMenu.getMenuInflater().inflate(R.menu.menu_row, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new C7094a());
            popupMenu.show();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.e$c */
    class C7095c implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24663c;

        /* renamed from: d */
        final /* synthetic */ C7098e f24664d;

        /* renamed from: e */
        final /* synthetic */ List f24665e;

        /* renamed from: com.spreadsheet.app.adapters.e$c$a */
        class C7096a implements View.OnClickListener {

            /* renamed from: c */
            final /* synthetic */ C7561f f24667c;

            C7096a(C7561f fVar) {
                this.f24667c = fVar;
            }

            public void onClick(View view) {
                String obj = view.getTag().toString();
                obj.hashCode();
                char c = 65535;
                switch (obj.hashCode()) {
                    case -1984987966:
                        if (obj.equals("Mobile")) {
                            c = 0;
                            break;
                        }
                        break;
                    case -1405978501:
                        if (obj.equals("Website")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 67066748:
                        if (obj.equals("Email")) {
                            c = 2;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        C7091e.this.f24650f = this.f24667c.getColData();
                        if (C7091e.this.f24651g.mo24205b(new String[]{"android.permission.CALL_PHONE"})) {
                            C7091e.this.mo24488u(this.f24667c.getColData());
                            return;
                        }
                        C7091e.this.f24651g.mo24208e(new String[]{"android.permission.CALL_PHONE"});
                        return;
                    case 1:
                        C7091e.this.mo24491x(this.f24667c.getColData());
                        return;
                    case 2:
                        C7091e.this.mo24492y(this.f24667c.getColData());
                        return;
                    default:
                        return;
                }
            }
        }

        C7095c(int i, C7098e eVar, List list) {
            this.f24663c = i;
            this.f24664d = eVar;
            this.f24665e = list;
        }

        public void onClick(View view) {
            ImageView imageView;
            int i;
            int i2;
            List<String> list = C7091e.this.f24656l;
            if (list.contains(this.f24663c + "")) {
                List<String> list2 = C7091e.this.f24656l;
                list2.remove(this.f24663c + "");
                this.f24664d.f24671u.setVisibility(8);
                imageView = this.f24664d.f24672v;
                i = R.drawable.ic_expand;
            } else {
                this.f24664d.f24671u.removeAllViews();
                List<String> list3 = C7091e.this.f24655k;
                if (!list3.contains(this.f24663c + "")) {
                    for (int i3 = 0; i3 < this.f24665e.size(); i3++) {
                        View inflate = LayoutInflater.from(C7091e.this.f24649e).inflate(R.layout.row_data, (ViewGroup) null, false);
                        CustomTextView customTextView = (CustomTextView) inflate.findViewById(R.id.text_column_title);
                        CustomTextView customTextView2 = (CustomTextView) inflate.findViewById(R.id.text_column_info);
                        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.image_column_action);
                        customTextView2.setTextSize(0, C7091e.this.f24649e.getResources().getDimension(R.dimen.dp_14));
                        customTextView.setTextSize(0, C7091e.this.f24649e.getResources().getDimension(R.dimen.dp_14));
                        C7561f fVar = (C7561f) this.f24665e.get(i3);
                        customTextView.setText(((C7561f) this.f24665e.get(i3)).getColName());
                        customTextView2.setText(C7091e.this.f24653i.get(this.f24663c).getCellsDataList().get(i3).getColData());
                        String str = "Website";
                        if (fVar.getColType().equalsIgnoreCase(str)) {
                            i2 = R.drawable.ic_web;
                        } else {
                            str = "Mobile";
                            if (fVar.getColType().equalsIgnoreCase(str)) {
                                i2 = R.drawable.ic_call;
                            } else {
                                str = "Email";
                                if (fVar.getColType().equalsIgnoreCase(str)) {
                                    i2 = R.drawable.ic_email;
                                } else {
                                    imageView2.setVisibility(8);
                                    imageView2.setOnClickListener(new C7096a(fVar));
                                    this.f24664d.f24671u.addView(inflate);
                                }
                            }
                        }
                        imageView2.setImageResource(i2);
                        imageView2.setTag(str);
                        imageView2.setVisibility(0);
                        imageView2.setOnClickListener(new C7096a(fVar));
                        this.f24664d.f24671u.addView(inflate);
                    }
                }
                List<String> list4 = C7091e.this.f24656l;
                list4.add(this.f24663c + "");
                this.f24664d.f24671u.setVisibility(0);
                imageView = this.f24664d.f24672v;
                i = R.drawable.ic_collapse;
            }
            imageView.setImageResource(i);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.e$d */
    public class C7097d extends Filter {
        public C7097d() {
        }

        /* access modifiers changed from: protected */
        public FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            ArrayList arrayList = new ArrayList();
            List<C7564i> list = C7091e.this.f24652h;
            String lowerCase = charSequence.toString().toLowerCase();
            for (int i = 0; i < list.size(); i++) {
                C7564i iVar = list.get(i);
                if (iVar.getCellsDataList().get(0).getColData().toLowerCase().contains(lowerCase)) {
                    arrayList.add(iVar);
                }
            }
            filterResults.count = arrayList.size();
            filterResults.values = arrayList;
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, FilterResults filterResults) {
            C7091e eVar = C7091e.this;
            eVar.f24653i = (List) filterResults.values;
            eVar.mo3298h();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.e$e */
    public class C7098e extends RecyclerView.C0500d0 {

        /* renamed from: t */
        CustomTextView f24670t;

        /* renamed from: u */
        LinearLayout f24671u;

        /* renamed from: v */
        ImageView f24672v;

        /* renamed from: w */
        CardView f24673w;

        /* renamed from: x */
        ImageView f24674x;

        /* renamed from: y */
        LinearLayout f24675y;

        public C7098e(C7091e eVar, View view) {
            super(view);
            this.f24670t = (CustomTextView) view.findViewById(R.id.text_first_column);
            this.f24671u = (LinearLayout) view.findViewById(R.id.layout_row_data);
            this.f24672v = (ImageView) view.findViewById(R.id.image_expand);
            this.f24673w = (CardView) view.findViewById(R.id.card_sheet_row);
            this.f24674x = (ImageView) view.findViewById(R.id.image_menu);
            this.f24675y = (LinearLayout) view.findViewById(R.id.layout_space);
        }
    }

    public C7091e(Context context, List<C7564i> list, C7576f fVar) {
        this.f24649e = context;
        this.f24652h = list;
        this.f24653i = list;
        this.f24654j = fVar;
        this.f24655k = new ArrayList();
        this.f24656l = new ArrayList();
        this.f24657m = new C7097d();
        this.f24651g.mo24206c(this.f24649e);
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24653i.size();
    }

    public Filter getFilter() {
        return this.f24657m;
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 1 && iArr.length > 0 && iArr[0] == 0) {
            mo24488u(this.f24650f);
        }
    }

    /* renamed from: u */
    public void mo24488u(String str) {
        Intent intent = new Intent("android.intent.action.CALL");
        intent.setData(Uri.parse("tel:" + str));
        this.f24649e.startActivity(intent);
    }

    /* renamed from: v */
    public void mo3300j(C7098e eVar, int i) {
        ImageView imageView;
        int i2;
        int i3;
        List<C7561f> cellsDataList = this.f24653i.get(i).getCellsDataList();
        eVar.f24670t.setText(cellsDataList.get(0).getColData());
        if (i == 0) {
            eVar.f24675y.setVisibility(0);
        }
        List<String> list = this.f24656l;
        if (!list.contains(i + "")) {
            eVar.f24671u.setVisibility(8);
            imageView = eVar.f24672v;
            i2 = R.drawable.ic_expand;
        } else {
            eVar.f24671u.removeAllViews();
            List<String> list2 = this.f24655k;
            if (!list2.contains(i + "")) {
                for (int i4 = 0; i4 < cellsDataList.size(); i4++) {
                    View inflate = LayoutInflater.from(this.f24649e).inflate(R.layout.row_data, (ViewGroup) null, false);
                    CustomTextView customTextView = (CustomTextView) inflate.findViewById(R.id.text_column_title);
                    CustomTextView customTextView2 = (CustomTextView) inflate.findViewById(R.id.text_column_info);
                    ImageView imageView2 = (ImageView) inflate.findViewById(R.id.image_column_action);
                    customTextView2.setTextSize(0, this.f24649e.getResources().getDimension(R.dimen.dp_14));
                    customTextView.setTextSize(0, this.f24649e.getResources().getDimension(R.dimen.dp_14));
                    C7561f fVar = cellsDataList.get(i4);
                    customTextView.setText(fVar.getColName());
                    customTextView2.setText(this.f24653i.get(i).getCellsDataList().get(i4).getColData());
                    String str = "Website";
                    if (fVar.getColType().equalsIgnoreCase(str)) {
                        i3 = R.drawable.ic_web;
                    } else {
                        str = "Mobile";
                        if (fVar.getColType().equalsIgnoreCase(str)) {
                            i3 = R.drawable.ic_call;
                        } else {
                            str = "Email";
                            if (fVar.getColType().equalsIgnoreCase(str)) {
                                i3 = R.drawable.ic_email;
                            } else {
                                imageView2.setVisibility(8);
                                imageView2.setOnClickListener(new C7092a(fVar));
                                eVar.f24671u.addView(inflate);
                            }
                        }
                    }
                    imageView2.setImageResource(i3);
                    imageView2.setTag(str);
                    imageView2.setVisibility(0);
                    imageView2.setOnClickListener(new C7092a(fVar));
                    eVar.f24671u.addView(inflate);
                }
            }
            eVar.f24671u.setVisibility(0);
            imageView = eVar.f24672v;
            i2 = R.drawable.ic_collapse;
        }
        imageView.setImageResource(i2);
        eVar.f24674x.setOnClickListener(new C7093b(i));
        eVar.f24673w.setOnClickListener(new C7095c(i, eVar, cellsDataList));
    }

    /* renamed from: w */
    public C7098e mo3302l(ViewGroup viewGroup, int i) {
        return new C7098e(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_sheet_row, viewGroup, false));
    }

    /* renamed from: x */
    public void mo24491x(String str) {
        try {
            if (!str.startsWith("http://") && !str.startsWith("https://")) {
                str = "http://" + str;
            }
            this.f24649e.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception unused) {
        }
    }

    /* renamed from: y */
    public void mo24492y(String str) {
        Intent intent = new Intent("android.intent.action.SENDTO");
        intent.setData(Uri.parse("mailto:"));
        Intent intent2 = new Intent("android.intent.action.SEND");
        intent2.putExtra("android.intent.extra.SUBJECT", "");
        intent2.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent2.setSelector(intent);
        if (intent2.resolveActivity(this.f24649e.getPackageManager()) != null) {
            this.f24649e.startActivity(intent2);
        }
    }
}
