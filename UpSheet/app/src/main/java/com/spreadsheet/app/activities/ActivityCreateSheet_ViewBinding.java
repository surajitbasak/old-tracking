package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;

public class ActivityCreateSheet_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityCreateSheet f24276a;

    /* renamed from: b */
    private View f24277b;

    /* renamed from: c */
    private View f24278c;

    /* renamed from: d */
    private View f24279d;

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet_ViewBinding$a */
    class C6954a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityCreateSheet f24280e;

        C6954a(ActivityCreateSheet_ViewBinding activityCreateSheet_ViewBinding, ActivityCreateSheet activityCreateSheet) {
            this.f24280e = activityCreateSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24280e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet_ViewBinding$b */
    class C6955b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityCreateSheet f24281e;

        C6955b(ActivityCreateSheet_ViewBinding activityCreateSheet_ViewBinding, ActivityCreateSheet activityCreateSheet) {
            this.f24281e = activityCreateSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24281e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet_ViewBinding$c */
    class C6956c extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityCreateSheet f24282e;

        C6956c(ActivityCreateSheet_ViewBinding activityCreateSheet_ViewBinding, ActivityCreateSheet activityCreateSheet) {
            this.f24282e = activityCreateSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24282e.onViewClicked(view);
        }
    }

    public ActivityCreateSheet_ViewBinding(ActivityCreateSheet activityCreateSheet, View view) {
        this.f24276a = activityCreateSheet;
        activityCreateSheet.toolbarCreateSheet = (Toolbar) C0642c.m3874c(view, R.id.toolbar_create_sheet, "field 'toolbarCreateSheet'", Toolbar.class);
        activityCreateSheet.editSheetName = (EditText) C0642c.m3874c(view, R.id.edit_sheet_name, "field 'editSheetName'", EditText.class);
        activityCreateSheet.spinnerColumns = (Spinner) C0642c.m3874c(view, R.id.spinner_columns, "field 'spinnerColumns'", Spinner.class);
        activityCreateSheet.layoutColumns = (LinearLayout) C0642c.m3874c(view, R.id.layout_columns, "field 'layoutColumns'", LinearLayout.class);
        View b = C0642c.m3873b(view, R.id.button_create_sheet, "field 'buttonCreateSheet' and method 'onViewClicked'");
        activityCreateSheet.buttonCreateSheet = (Button) C0642c.m3872a(b, R.id.button_create_sheet, "field 'buttonCreateSheet'", Button.class);
        this.f24277b = b;
        b.setOnClickListener(new C6954a(this, activityCreateSheet));
        View b2 = C0642c.m3873b(view, R.id.button_add_column, "field 'buttonAddColumn' and method 'onViewClicked'");
        activityCreateSheet.buttonAddColumn = (Button) C0642c.m3872a(b2, R.id.button_add_column, "field 'buttonAddColumn'", Button.class);
        this.f24278c = b2;
        b2.setOnClickListener(new C6955b(this, activityCreateSheet));
        View b3 = C0642c.m3873b(view, R.id.button_remove_column, "field 'buttonRemoveColumn' and method 'onViewClicked'");
        activityCreateSheet.buttonRemoveColumn = (Button) C0642c.m3872a(b3, R.id.button_remove_column, "field 'buttonRemoveColumn'", Button.class);
        this.f24279d = b3;
        b3.setOnClickListener(new C6956c(this, activityCreateSheet));
    }

    public void unbind() {
        ActivityCreateSheet activityCreateSheet = this.f24276a;
        if (activityCreateSheet != null) {
            this.f24276a = null;
            activityCreateSheet.toolbarCreateSheet = null;
            activityCreateSheet.editSheetName = null;
            activityCreateSheet.spinnerColumns = null;
            activityCreateSheet.layoutColumns = null;
            activityCreateSheet.buttonCreateSheet = null;
            activityCreateSheet.buttonAddColumn = null;
            activityCreateSheet.buttonRemoveColumn = null;
            this.f24277b.setOnClickListener((View.OnClickListener) null);
            this.f24277b = null;
            this.f24278c.setOnClickListener((View.OnClickListener) null);
            this.f24278c = null;
            this.f24279d.setOnClickListener((View.OnClickListener) null);
            this.f24279d = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
