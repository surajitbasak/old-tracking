package com.spreadsheet.app.Utils.Fonts;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/* renamed from: com.spreadsheet.app.Utils.Fonts.a */
public class C6871a extends TypefaceSpan {

    /* renamed from: c */
    private final Typeface f23970c;

    public C6871a(String str, Typeface typeface) {
        super(str);
        this.f23970c = typeface;
    }

    /* renamed from: a */
    private static void m33457a(Paint paint, Typeface typeface) {
        Typeface typeface2 = paint.getTypeface();
        int style = (typeface2 == null ? 0 : typeface2.getStyle()) & (typeface.getStyle() ^ -1);
        if ((style & 1) != 0) {
            paint.setFakeBoldText(true);
        }
        if ((style & 2) != 0) {
            paint.setTextSkewX(-0.25f);
        }
        paint.setTypeface(typeface);
    }

    public void updateDrawState(TextPaint textPaint) {
        m33457a(textPaint, this.f23970c);
    }

    public void updateMeasureState(TextPaint textPaint) {
        m33457a(textPaint, this.f23970c);
    }
}
