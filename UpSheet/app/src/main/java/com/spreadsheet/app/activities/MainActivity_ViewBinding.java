package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextBold;
import com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class MainActivity_ViewBinding implements Unbinder {

    /* renamed from: a */
    private MainActivity f24563a;

    /* renamed from: b */
    private View f24564b;

    /* renamed from: c */
    private View f24565c;

    /* renamed from: d */
    private View f24566d;

    /* renamed from: e */
    private View f24567e;

    /* renamed from: f */
    private View f24568f;

    /* renamed from: g */
    private View f24569g;

    /* renamed from: h */
    private View f24570h;

    /* renamed from: i */
    private View f24571i;

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$a */
    class C7061a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24572e;

        C7061a(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24572e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24572e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$b */
    class C7062b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24573e;

        C7062b(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24573e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24573e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$c */
    class C7063c extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24574e;

        C7063c(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24574e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24574e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$d */
    class C7064d extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24575e;

        C7064d(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24575e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24575e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$e */
    class C7065e extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24576e;

        C7065e(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24576e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24576e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$f */
    class C7066f extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24577e;

        C7066f(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24577e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24577e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$g */
    class C7067g extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24578e;

        C7067g(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24578e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24578e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.MainActivity_ViewBinding$h */
    class C7068h extends C0641b {

        /* renamed from: e */
        final /* synthetic */ MainActivity f24579e;

        C7068h(MainActivity_ViewBinding mainActivity_ViewBinding, MainActivity mainActivity) {
            this.f24579e = mainActivity;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24579e.onViewClicked(view);
        }
    }

    public MainActivity_ViewBinding(MainActivity mainActivity, View view) {
        this.f24563a = mainActivity;
        mainActivity.toolbarHome = (Toolbar) C0642c.m3874c(view, R.id.toolbar_home, "field 'toolbarHome'", Toolbar.class);
        mainActivity.recyclerviewSheets = (RecyclerView) C0642c.m3874c(view, R.id.recyclerview_sheets, "field 'recyclerviewSheets'", RecyclerView.class);
        mainActivity.layoutProgress = (RelativeLayout) C0642c.m3874c(view, R.id.layout_progress, "field 'layoutProgress'", RelativeLayout.class);
        View b = C0642c.m3873b(view, R.id.text_create_spreadsheet, "field 'textCreateSpreadsheet' and method 'onViewClicked'");
        mainActivity.textCreateSpreadsheet = (CustomTextView) C0642c.m3872a(b, R.id.text_create_spreadsheet, "field 'textCreateSpreadsheet'", CustomTextView.class);
        this.f24564b = b;
        b.setOnClickListener(new C7061a(this, mainActivity));
        mainActivity.layoutEmptyScreen = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_empty_screen, "field 'layoutEmptyScreen'", ConstraintLayout.class);
        mainActivity.textTemplate = (CustomTextSemiBold) C0642c.m3874c(view, R.id.text_template, "field 'textTemplate'", CustomTextSemiBold.class);
        mainActivity.layoutSheetTypes = (HorizontalScrollView) C0642c.m3874c(view, R.id.layout_sheet_types, "field 'layoutSheetTypes'", HorizontalScrollView.class);
        mainActivity.textSpreadsheet = (CustomTextSemiBold) C0642c.m3874c(view, R.id.text_spreadsheet, "field 'textSpreadsheet'", CustomTextSemiBold.class);
        mainActivity.adView = (AdView) C0642c.m3874c(view, R.id.ad_view, "field 'adView'", AdView.class);
        mainActivity.navigationView = (NavigationView) C0642c.m3874c(view, R.id.nav_drawer, "field 'navigationView'", NavigationView.class);
        mainActivity.drawerLayout = (DrawerLayout) C0642c.m3874c(view, R.id.layout_drawer, "field 'drawerLayout'", DrawerLayout.class);
        View b2 = C0642c.m3873b(view, R.id.card_barcode_sheet, "field 'cardBarcodeSheet' and method 'onViewClicked'");
        mainActivity.cardBarcodeSheet = (CardView) C0642c.m3872a(b2, R.id.card_barcode_sheet, "field 'cardBarcodeSheet'", CardView.class);
        this.f24565c = b2;
        b2.setOnClickListener(new C7062b(this, mainActivity));
        View b3 = C0642c.m3873b(view, R.id.image_search, "field 'imageSearch' and method 'onViewClicked'");
        mainActivity.imageSearch = (ImageView) C0642c.m3872a(b3, R.id.image_search, "field 'imageSearch'", ImageView.class);
        this.f24566d = b3;
        b3.setOnClickListener(new C7063c(this, mainActivity));
        mainActivity.searchView = (SearchView) C0642c.m3874c(view, R.id.searchview_spreadsheets, "field 'searchView'", SearchView.class);
        mainActivity.nestedScrollView = (NestedScrollView) C0642c.m3874c(view, R.id.nested_scroll, "field 'nestedScrollView'", NestedScrollView.class);
        View b4 = C0642c.m3873b(view, R.id.btn_try_again, "field 'btnTryAgain' and method 'onViewClicked'");
        mainActivity.btnTryAgain = (CustomTextView) C0642c.m3872a(b4, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        this.f24567e = b4;
        b4.setOnClickListener(new C7064d(this, mainActivity));
        mainActivity.layoutTryAgain = (RelativeLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", RelativeLayout.class);
        View b5 = C0642c.m3873b(view, R.id.card_customised, "field 'cardCustomised' and method 'onViewClicked'");
        mainActivity.cardCustomised = (CardView) C0642c.m3872a(b5, R.id.card_customised, "field 'cardCustomised'", CardView.class);
        this.f24568f = b5;
        b5.setOnClickListener(new C7065e(this, mainActivity));
        View b6 = C0642c.m3873b(view, R.id.card_todo, "field 'cardTodo' and method 'onViewClicked'");
        mainActivity.cardTodo = (CardView) C0642c.m3872a(b6, R.id.card_todo, "field 'cardTodo'", CardView.class);
        this.f24569g = b6;
        b6.setOnClickListener(new C7066f(this, mainActivity));
        View b7 = C0642c.m3873b(view, R.id.card_contact, "field 'cardContact' and method 'onViewClicked'");
        mainActivity.cardContact = (CardView) C0642c.m3872a(b7, R.id.card_contact, "field 'cardContact'", CardView.class);
        this.f24570h = b7;
        b7.setOnClickListener(new C7067g(this, mainActivity));
        mainActivity.toolbarHomeTitlePremium = (CustomTextBold) C0642c.m3874c(view, R.id.toolbar_home_title_premium, "field 'toolbarHomeTitlePremium'", CustomTextBold.class);
        View b8 = C0642c.m3873b(view, R.id.toolbar_home_go_premium, "field 'toolbarHomeGoPremium' and method 'onViewClicked'");
        mainActivity.toolbarHomeGoPremium = (CustomTextBold) C0642c.m3872a(b8, R.id.toolbar_home_go_premium, "field 'toolbarHomeGoPremium'", CustomTextBold.class);
        this.f24571i = b8;
        b8.setOnClickListener(new C7068h(this, mainActivity));
    }

    public void unbind() {
        MainActivity mainActivity = this.f24563a;
        if (mainActivity != null) {
            this.f24563a = null;
            mainActivity.toolbarHome = null;
            mainActivity.recyclerviewSheets = null;
            mainActivity.layoutProgress = null;
            mainActivity.textCreateSpreadsheet = null;
            mainActivity.layoutEmptyScreen = null;
            mainActivity.textTemplate = null;
            mainActivity.layoutSheetTypes = null;
            mainActivity.textSpreadsheet = null;
            mainActivity.adView = null;
            mainActivity.navigationView = null;
            mainActivity.drawerLayout = null;
            mainActivity.cardBarcodeSheet = null;
            mainActivity.imageSearch = null;
            mainActivity.searchView = null;
            mainActivity.nestedScrollView = null;
            mainActivity.btnTryAgain = null;
            mainActivity.layoutTryAgain = null;
            mainActivity.cardCustomised = null;
            mainActivity.cardTodo = null;
            mainActivity.cardContact = null;
            mainActivity.toolbarHomeTitlePremium = null;
            mainActivity.toolbarHomeGoPremium = null;
            this.f24564b.setOnClickListener((View.OnClickListener) null);
            this.f24564b = null;
            this.f24565c.setOnClickListener((View.OnClickListener) null);
            this.f24565c = null;
            this.f24566d.setOnClickListener((View.OnClickListener) null);
            this.f24566d = null;
            this.f24567e.setOnClickListener((View.OnClickListener) null);
            this.f24567e = null;
            this.f24568f.setOnClickListener((View.OnClickListener) null);
            this.f24568f = null;
            this.f24569g.setOnClickListener((View.OnClickListener) null);
            this.f24569g = null;
            this.f24570h.setOnClickListener((View.OnClickListener) null);
            this.f24570h = null;
            this.f24571i.setOnClickListener((View.OnClickListener) null);
            this.f24571i = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
