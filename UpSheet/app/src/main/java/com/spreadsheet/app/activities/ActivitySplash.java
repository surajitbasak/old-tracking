package com.spreadsheet.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.C0067c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6879h;
import p153e.p300e.p301a.p304c.C7575e;
import p153e.p300e.p301a.p305d.C7583c;
import p328i.p335b.p336a.C8262f;

public class ActivitySplash extends C0067c implements C7575e {

    /* renamed from: u */
    C6872a f24346u = C6872a.m33458a();

    /* renamed from: v */
    C6879h f24347v = C6879h.m33499c();

    /* renamed from: w */
    C7583c f24348w = C7583c.m35532f();

    /* renamed from: com.spreadsheet.app.activities.ActivitySplash$a */
    class C6981a implements Runnable {
        C6981a() {
        }

        public void run() {
            ActivitySplash.this.startActivity(ActivitySplash.this.getIntent().hasExtra(C6873b.f24012g0) ? new Intent(ActivitySplash.this, ActivityCreateSheet.class) : new Intent(ActivitySplash.this, MainActivity.class));
            ActivitySplash.this.finish();
        }
    }

    /* renamed from: u0 */
    private void m33676u0() {
        new Handler().postDelayed(new C6981a(), 2000);
    }

    /* renamed from: I */
    public void mo24357I() {
    }

    /* renamed from: k */
    public void mo24358k() {
    }

    /* renamed from: n */
    public void mo24359n() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_splash);
        this.f24347v.mo24225f(this);
        this.f24346u.mo24206c(this);
        this.f24348w.mo25619g(this, this);
        C8262f.m38843j0(1899, 12, 30, 0, 0).mo27541s0(43946).toString();
        boolean a = this.f24347v.mo24221a(C6873b.f24023n);
        m33676u0();
    }

    /* renamed from: p */
    public void mo24360p() {
        m33676u0();
    }
}
