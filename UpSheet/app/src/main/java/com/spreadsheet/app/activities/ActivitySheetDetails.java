package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.C0065b;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.material.bottomsheet.C3670a;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.p067v4.SheetsScopes;
import com.google.firebase.database.C4524a;
import com.google.firebase.database.C4527b;
import com.google.firebase.database.C4529d;
import com.google.firebase.database.C4534g;
import com.google.firebase.database.C4549p;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7091e;
import com.spreadsheet.app.adapters.SubsheetsAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import p007cn.pedant.SweetAlert.SweetAlertDialog;
import p153e.p174d.p176b.p190c.p217h.C6386c;
import p153e.p174d.p176b.p190c.p217h.C6394h;
import p153e.p300e.p301a.p302a.C7557b;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p302a.C7564i;
import p153e.p300e.p301a.p302a.C7565j;
import p153e.p300e.p301a.p304c.C7576f;
import p153e.p300e.p301a.p304c.C7578h;
import p153e.p300e.p301a.p305d.C7583c;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivitySheetDetails extends C0067c implements C7598a, C7576f, View.OnClickListener, C7578h {

    /* renamed from: c0 */
    private static final List<String> f24283c0 = Collections.singletonList(SheetsScopes.SPREADSHEETS);

    /* renamed from: A */
    int f24284A;

    /* renamed from: B */
    int f24285B = 0;

    /* renamed from: C */
    C7560e f24286C;

    /* renamed from: D */
    C7596g f24287D = C7596g.m35606z();

    /* renamed from: E */
    C7562g f24288E = C7562g.getInstance();

    /* renamed from: F */
    List<C7564i> f24289F = new ArrayList();

    /* renamed from: G */
    List<String> f24290G = new ArrayList();

    /* renamed from: H */
    List<Integer> f24291H = new ArrayList();

    /* renamed from: I */
    List<C7565j> f24292I = new ArrayList();

    /* renamed from: J */
    Dialog f24293J;

    /* renamed from: K */
    CustomTextView f24294K;

    /* renamed from: L */
    CustomTextView f24295L;

    /* renamed from: M */
    EditText f24296M;

    /* renamed from: N */
    Button f24297N;

    /* renamed from: O */
    ImageView f24298O;

    /* renamed from: P */
    C4529d f24299P;

    /* renamed from: Q */
    C4529d f24300Q;

    /* renamed from: R */
    Animation f24301R;

    /* renamed from: S */
    Animation f24302S;

    /* renamed from: T */
    C6875d f24303T = C6875d.m33482b();

    /* renamed from: U */
    C6872a f24304U;

    /* renamed from: V */
    C7590d f24305V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public String f24306W;

    /* renamed from: X */
    private C7564i f24307X;

    /* renamed from: Y */
    C7091e f24308Y;

    /* renamed from: Z */
    C7591e f24309Z;

    /* renamed from: a0 */
    SubsheetsAdapter f24310a0;

    /* renamed from: b0 */
    C3670a f24311b0;
    @BindView(2131296344)
    CustomTextView btnTryAgain;
    @BindView(2131296350)
    ImageView buttonAddRow;
    @BindView(2131296352)
    ImageView buttonAddSubsheet;
    @BindView(2131296570)
    LinearLayout layoutEmptyScreen;
    @BindView(2131296591)
    RelativeLayout layoutProgress;
    @BindView(2131296592)
    SwipeRefreshLayout layoutPullToRefresh;
    @BindView(2131296604)
    ConstraintLayout layoutSubsheetsList;
    @BindView(2131296608)
    ConstraintLayout layoutTryAgain;
    @BindView(2131296728)
    RecyclerView recyclerSheetRows;
    @BindView(2131296729)
    RecyclerView recyclerSubsheets;
    @BindView(2131296825)
    CustomTextView textAddRow;
    @BindView(2131296912)
    CustomTextView textTryAgain;
    @BindView(2131296934)
    Toolbar toolbarSheetDetail;

    /* renamed from: u */
    Boolean f24312u = Boolean.TRUE;

    /* renamed from: v */
    C6879h f24313v = C6879h.m33499c();

    /* renamed from: w */
    TextView f24314w;

    /* renamed from: x */
    String f24315x = "";

    /* renamed from: y */
    String f24316y = "";

    /* renamed from: z */
    int f24317z = 0;

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$a */
    class C6957a implements DialogInterface.OnClickListener {
        C6957a(ActivitySheetDetails activitySheetDetails) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$b */
    class C6958b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24318c;

        C6958b(ActivitySheetDetails activitySheetDetails, Dialog dialog) {
            this.f24318c = dialog;
        }

        public void onClick(View view) {
            this.f24318c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$c */
    class C6959c implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24319c;

        C6959c(Dialog dialog) {
            this.f24319c = dialog;
        }

        public void onClick(View view) {
            this.f24319c.dismiss();
            C6873b.f24021l = true;
            ActivitySheetDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$d */
    class C6960d implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24321c;

        C6960d(ActivitySheetDetails activitySheetDetails, Dialog dialog) {
            this.f24321c = dialog;
        }

        public void onClick(View view) {
            this.f24321c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$e */
    class C6961e implements SweetAlertDialog.OnSweetClickListener {
        C6961e() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivitySheetDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$f */
    class C6962f implements SweetAlertDialog.OnSweetClickListener {
        C6962f() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivitySheetDetails.this.f24303T.mo24213d("");
            sweetAlertDialog.dismiss();
            ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
            activitySheetDetails.f24309Z.mo25639d(activitySheetDetails.f24315x);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$g */
    class C6963g implements SweetAlertDialog.OnSweetClickListener {
        C6963g() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivitySheetDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$h */
    class C6964h implements SweetAlertDialog.OnSweetClickListener {
        C6964h() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
            ActivitySheetDetails.this.f24303T.mo24213d("");
            ActivitySheetDetails.this.m33638C0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$i */
    class C6965i implements C4549p {

        /* renamed from: a */
        final /* synthetic */ List f24326a;

        /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$i$a */
        class C6966a implements Comparator<C7557b> {
            C6966a(C6965i iVar) {
            }

            /* renamed from: a */
            public int compare(C7557b bVar, C7557b bVar2) {
                return String.valueOf(bVar.getColNum()).compareTo(String.valueOf(bVar2.getColNum()));
            }
        }

        /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$i$b */
        class C6967b implements C6386c<Void> {
            C6967b(C6965i iVar) {
            }

            /* renamed from: b */
            public void mo7656b(C6394h<Void> hVar) {
            }
        }

        C6965i(List list) {
            this.f24326a = list;
        }

        /* renamed from: a */
        public void mo18610a(C4527b bVar) {
        }

        /* renamed from: b */
        public void mo18611b(C4524a aVar) {
            this.f24326a.clear();
            for (C4524a h : aVar.mo18573d()) {
                this.f24326a.add((C7557b) h.mo18577h(C7557b.class));
            }
            Collections.sort(this.f24326a, new C6966a(this));
            ActivitySheetDetails.this.f24286C.setColumnsList(this.f24326a);
            ActivitySheetDetails.this.f24288E.setColumnList(this.f24326a);
            ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
            activitySheetDetails.f24299P.mo18590g(activitySheetDetails.f24306W).mo18590g(ActivitySheetDetails.this.f24315x).mo18595k(ActivitySheetDetails.this.f24286C).mo23027b(new C6967b(this));
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$j */
    class C6968j implements View.OnClickListener {
        C6968j() {
        }

        public void onClick(View view) {
            ActivitySheetDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$k */
    class C6969k extends RecyclerView.C0523t {
        C6969k() {
        }

        /* renamed from: a */
        public void mo3461a(RecyclerView recyclerView, int i) {
            super.mo3461a(recyclerView, i);
        }

        /* renamed from: b */
        public void mo3462b(RecyclerView recyclerView, int i, int i2) {
            ImageView imageView;
            int i3;
            super.mo3462b(recyclerView, i, i2);
            if (i2 > 0 && ActivitySheetDetails.this.buttonAddRow.getVisibility() == 0) {
                ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
                activitySheetDetails.buttonAddRow.startAnimation(activitySheetDetails.f24302S);
                imageView = ActivitySheetDetails.this.buttonAddRow;
                i3 = 8;
            } else if (i2 < 0 && ActivitySheetDetails.this.buttonAddRow.getVisibility() != 0) {
                ActivitySheetDetails activitySheetDetails2 = ActivitySheetDetails.this;
                activitySheetDetails2.buttonAddRow.startAnimation(activitySheetDetails2.f24301R);
                imageView = ActivitySheetDetails.this.buttonAddRow;
                i3 = 0;
            } else {
                return;
            }
            imageView.setVisibility(i3);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$l */
    class C6970l implements SwipeRefreshLayout.C0604j {
        C6970l() {
        }

        /* renamed from: a */
        public void mo3852a() {
            ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
            activitySheetDetails.f24287D.mo25662C(activitySheetDetails, activitySheetDetails);
            ActivitySheetDetails.this.m33637B0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$m */
    class C6971m implements SearchView.C0183m {
        C6971m() {
        }

        /* renamed from: a */
        public boolean mo1181a(String str) {
            if (ActivitySheetDetails.this.f24289F.size() <= 0) {
                return true;
            }
            ActivitySheetDetails.this.f24308Y.getFilter().filter(str);
            return true;
        }

        /* renamed from: b */
        public boolean mo1182b(String str) {
            return false;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$n */
    class C6972n implements DialogInterface.OnClickListener {
        C6972n() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ActivitySheetDetails.this.f24304U.mo24207d()) {
                ActivitySheetDetails.this.f24303T.mo24213d("");
                ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
                activitySheetDetails.f24287D.mo25673k(activitySheetDetails.f24286C.getSheetId());
                return;
            }
            ActivitySheetDetails activitySheetDetails2 = ActivitySheetDetails.this;
            Toast.makeText(activitySheetDetails2, activitySheetDetails2.getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$o */
    class C6973o implements DialogInterface.OnClickListener {
        C6973o(ActivitySheetDetails activitySheetDetails) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$p */
    class C6974p implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24333c;

        C6974p(int i) {
            this.f24333c = i;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ActivitySheetDetails.this.f24304U.mo24207d()) {
                ActivitySheetDetails.this.f24303T.mo24213d("");
                ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
                C7596g gVar = activitySheetDetails.f24287D;
                gVar.mo25672j(activitySheetDetails.f24315x, gVar.mo25681s(activitySheetDetails.f24317z, this.f24333c + 1));
                return;
            }
            ActivitySheetDetails activitySheetDetails2 = ActivitySheetDetails.this;
            Toast.makeText(activitySheetDetails2, activitySheetDetails2.getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$q */
    class C6975q implements DialogInterface.OnClickListener {
        C6975q(ActivitySheetDetails activitySheetDetails) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails$r */
    class C6976r implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24335c;

        C6976r(int i) {
            this.f24335c = i;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivitySheetDetails.this.f24303T.mo24213d("");
            ActivitySheetDetails activitySheetDetails = ActivitySheetDetails.this;
            C7596g gVar = activitySheetDetails.f24287D;
            gVar.mo25671i(activitySheetDetails.f24315x, gVar.mo25682t(String.valueOf(this.f24335c)));
        }
    }

    static {
        JacksonFactory.getDefaultInstance();
    }

    public ActivitySheetDetails() {
        C7583c.m35532f();
        this.f24304U = C6872a.m33458a();
        this.f24305V = C7590d.m35558a();
        this.f24306W = "";
        this.f24309Z = C7591e.m35564f();
    }

    /* access modifiers changed from: private */
    /* renamed from: B0 */
    public void m33637B0() {
        if (this.f24304U.mo24207d()) {
            this.f24287D.mo25677o(this.f24315x, m33640E0());
            return;
        }
        this.layoutProgress.setVisibility(8);
        this.f24303T.mo24211a();
        this.layoutTryAgain.setVisibility(0);
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* access modifiers changed from: private */
    /* renamed from: C0 */
    public void m33638C0() {
        if (this.f24304U.mo24207d()) {
            this.f24287D.mo25679q(this.f24315x);
            return;
        }
        this.layoutProgress.setVisibility(8);
        this.f24303T.mo24211a();
        this.layoutTryAgain.setVisibility(0);
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* renamed from: D0 */
    private void m33639D0() {
        try {
            ArrayList arrayList = new ArrayList();
            if (this.f24304U.mo24207d()) {
                this.f24300Q.mo18590g(this.f24286C.getSheetId()).mo18606b(new C6965i(arrayList));
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: E0 */
    private String m33640E0() {
        return this.f24316y + "!A1:" + C6873b.f23999a[this.f24286C.getColCount() - 1];
    }

    /* renamed from: G0 */
    private void m33641G0() {
        this.f24291H.clear();
        this.f24290G.clear();
        this.f24292I.clear();
        for (int i = 0; i < this.f24288E.sheetIdList.size(); i++) {
            this.f24290G.add(this.f24288E.sheetTitleList.get(i));
            this.f24291H.add(this.f24288E.sheetIdList.get(i));
            C7565j jVar = new C7565j();
            jVar.setSubSheetId(String.valueOf(this.f24288E.sheetIdList.get(i)));
            jVar.setSubSheetName(this.f24288E.sheetTitleList.get(i));
            this.f24292I.add(jVar);
        }
        if (this.f24312u.booleanValue()) {
            this.f24312u = Boolean.FALSE;
            if (this.f24286C.getLastSheetId() == "" || this.f24286C.getLastSheetId() == null || !this.f24291H.contains(Integer.valueOf(Integer.parseInt(this.f24286C.getLastSheetId())))) {
                this.f24284A = 0;
            } else {
                this.f24284A = this.f24291H.indexOf(Integer.valueOf(Integer.parseInt(this.f24286C.getLastSheetId())));
            }
        }
        this.f24316y = this.f24290G.get(this.f24284A);
        this.f24317z = this.f24291H.get(this.f24284A).intValue();
        this.textAddRow.setVisibility(8);
        this.layoutProgress.setVisibility(0);
        this.recyclerSubsheets.setLayoutManager(new LinearLayoutManager(this, 0, false));
        SubsheetsAdapter subsheetsAdapter = new SubsheetsAdapter(this, this.f24292I, String.valueOf(this.f24317z));
        this.f24310a0 = subsheetsAdapter;
        this.recyclerSubsheets.setAdapter(subsheetsAdapter);
        this.layoutSubsheetsList.setVisibility(0);
        m33637B0();
    }

    /* renamed from: I0 */
    private void m33642I0() {
        C7091e eVar = new C7091e(this, this.f24289F, this);
        this.f24308Y = eVar;
        this.recyclerSheetRows.setAdapter(eVar);
        if (!this.f24313v.mo24221a(C6873b.f24007e)) {
            mo24335H0();
        }
        if (this.f24289F.size() > 0) {
            this.buttonAddRow.setVisibility(0);
        }
    }

    /* renamed from: J0 */
    private void m33643J0() {
        this.f24303T.mo24211a();
        SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("Something Went Wrong!").setConfirmButton("Try Again", (SweetAlertDialog.OnSweetClickListener) new C6964h()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C6963g());
        cancelButton.setCancelable(false);
        cancelButton.setCanceledOnTouchOutside(false);
        cancelButton.show();
    }

    /* renamed from: K0 */
    private void m33644K0(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.do_you_wan_to_delet_this_row));
        builder.setPositiveButton(getResources().getString(R.string.yes), new C6974p(i));
        builder.setNegativeButton(getResources().getString(R.string.no), new C6975q(this));
        builder.show();
    }

    /* renamed from: x0 */
    private void m33648x0() {
        Intent intent = new Intent(this, ActivityAddRow.class);
        intent.putExtra("spreadsheet", this.f24286C);
        intent.putExtra("listsize", this.f24289F.size());
        intent.putExtra("sheetId", this.f24317z);
        intent.putExtra("sheetTitle", this.f24316y);
        startActivityForResult(intent, 2);
    }

    /* renamed from: A0 */
    public void mo24333A0(String str) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_free_limits);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_free_limits_dialog_msg)).setText(str);
        ((Button) dialog.findViewById(R.id.button_free_limits_dialog_close)).setOnClickListener(new C6958b(this, dialog));
        ((Button) dialog.findViewById(R.id.button_free_limits_go_premium)).setOnClickListener(new C6959c(dialog));
        ((ImageView) dialog.findViewById(R.id.image_close_free_limits_dialog)).setOnClickListener(new C6960d(this, dialog));
    }

    /* renamed from: E */
    public void mo24261E(String str) {
        mo24333A0(str);
    }

    /* renamed from: F0 */
    public void mo24334F0() {
        Calendar.getInstance().getTimeInMillis();
        this.f24309Z.mo25642h(this, this);
        this.f24304U.mo24206c(this);
        this.f24313v.mo24225f(this);
        this.f24303T.mo24212c(this);
        this.f24299P = C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.f24300Q = C4534g.m24100b().mo18599d(C6873b.f23979G);
        this.f24286C = (C7560e) getIntent().getSerializableExtra("spreadsheet");
        this.f24287D.mo25662C(this, this);
        this.f24305V.mo25637b(this);
        new C7564i();
        this.f24301R = AnimationUtils.loadAnimation(this, R.anim.scale_in);
        this.f24302S = AnimationUtils.loadAnimation(this, R.anim.scale_out);
        this.f24306W = this.f24313v.mo24222b(C6873b.f24024o);
        C3670a aVar = new C3670a(this);
        this.f24311b0 = aVar;
        aVar.requestWindowFeature(1);
        this.f24311b0.setContentView((int) R.layout.dialog_add_row);
        Dialog dialog = new Dialog(this);
        this.f24293J = dialog;
        dialog.requestWindowFeature(1);
        this.f24293J.setContentView(R.layout.dialog_add_new_sheet);
        this.f24294K = (CustomTextView) this.f24293J.findViewById(R.id.text_spreadsheet_title);
        this.f24295L = (CustomTextView) this.f24293J.findViewById(R.id.text_sheet_title);
        this.f24296M = (EditText) this.f24293J.findViewById(R.id.edit_sheet_name);
        this.f24297N = (Button) this.f24293J.findViewById(R.id.button_add_sheet);
        this.f24298O = (ImageView) this.f24293J.findViewById(R.id.image_close);
        this.f24297N.setOnClickListener(this);
        this.f24298O.setOnClickListener(this);
        this.f24315x = this.f24286C.getSheetId();
        this.toolbarSheetDetail.setTitle((CharSequence) "");
        this.toolbarSheetDetail.setTitleTextColor(getResources().getColor(R.color.black));
        mo276r0(this.toolbarSheetDetail);
        this.toolbarSheetDetail.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarSheetDetail.setNavigationOnClickListener(new C6968j());
        TextView textView = (TextView) this.toolbarSheetDetail.findViewById(R.id.text_toolbar_sheet_details);
        this.f24314w = textView;
        textView.setText(this.f24286C.getSheetName());
        this.recyclerSheetRows.setLayoutManager(new LinearLayoutManager(this, 1, false));
        this.layoutProgress.setVisibility(0);
        new ArrayAdapter(this, R.layout.spinner_text, this.f24290G);
        this.f24303T.mo24213d("");
        m33638C0();
        this.f24288E.getColumnList().clear();
        if (this.f24286C.getColumnsList().size() > 0) {
            this.f24288E.setColumnList(this.f24286C.getColumnsList());
        } else {
            m33639D0();
        }
        this.recyclerSheetRows.addOnScrollListener(new C6969k());
        this.layoutPullToRefresh.setOnRefreshListener(new C6970l());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x010a, code lost:
        r7.f24303T.mo24211a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x010f, code lost:
        m33643J0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x014f, code lost:
        r7.layoutTryAgain.setVisibility(0);
        r7.f24288E.isRowRequested = false;
        r7.f24303T.mo24211a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0188, code lost:
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01b5, code lost:
        if (r0[0].equals("com.android.volley.NoConnectionError") == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01b7, code lost:
        android.widget.Toast.makeText(r7, getResources().getString(com.spreadsheet.app.R.string.internet_check), 0).show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return;
     */
    /* renamed from: H */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24237H(String r8, String r9) {
        /*
            r7 = this;
            java.lang.String r0 = ":"
            java.lang.String[] r0 = r8.split(r0)
            r9.hashCode()
            int r1 = r9.hashCode()
            r2 = 3
            r3 = 1
            r4 = 8
            r5 = 0
            r6 = -1
            switch(r1) {
                case -2022976223: goto L_0x00c7;
                case -1809886158: goto L_0x00bc;
                case -1479983679: goto L_0x00b1;
                case -1422526087: goto L_0x00a6;
                case -1133266316: goto L_0x009b;
                case -561789226: goto L_0x0090;
                case -452548289: goto L_0x0085;
                case -358721489: goto L_0x007a;
                case -351206292: goto L_0x006c;
                case 124646116: goto L_0x005e;
                case 281689075: goto L_0x0050;
                case 323754608: goto L_0x0042;
                case 391607136: goto L_0x0034;
                case 1001959285: goto L_0x0026;
                case 1594339684: goto L_0x0018;
                default: goto L_0x0016;
            }
        L_0x0016:
            goto L_0x00d1
        L_0x0018:
            java.lang.String r1 = "formatHeader"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0022
            goto L_0x00d1
        L_0x0022:
            r6 = 14
            goto L_0x00d1
        L_0x0026:
            java.lang.String r1 = "getAllRowsCustom"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0030
            goto L_0x00d1
        L_0x0030:
            r6 = 13
            goto L_0x00d1
        L_0x0034:
            java.lang.String r1 = "addNewSheet"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x003e
            goto L_0x00d1
        L_0x003e:
            r6 = 12
            goto L_0x00d1
        L_0x0042:
            java.lang.String r1 = "formatRows"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x004c
            goto L_0x00d1
        L_0x004c:
            r6 = 11
            goto L_0x00d1
        L_0x0050:
            java.lang.String r1 = "File_Not_Found"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x005a
            goto L_0x00d1
        L_0x005a:
            r6 = 10
            goto L_0x00d1
        L_0x005e:
            java.lang.String r1 = "getAllRows"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0068
            goto L_0x00d1
        L_0x0068:
            r6 = 9
            goto L_0x00d1
        L_0x006c:
            java.lang.String r1 = "addHeaderRow"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0076
            goto L_0x00d1
        L_0x0076:
            r6 = 8
            goto L_0x00d1
        L_0x007a:
            java.lang.String r1 = "deleteRow"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0083
            goto L_0x00d1
        L_0x0083:
            r6 = 7
            goto L_0x00d1
        L_0x0085:
            java.lang.String r1 = "getAllSheets"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x008e
            goto L_0x00d1
        L_0x008e:
            r6 = 6
            goto L_0x00d1
        L_0x0090:
            java.lang.String r1 = "Access_Denied"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x0099
            goto L_0x00d1
        L_0x0099:
            r6 = 5
            goto L_0x00d1
        L_0x009b:
            java.lang.String r1 = "deleteSheet"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x00a4
            goto L_0x00d1
        L_0x00a4:
            r6 = 4
            goto L_0x00d1
        L_0x00a6:
            java.lang.String r1 = "addRow"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x00af
            goto L_0x00d1
        L_0x00af:
            r6 = 3
            goto L_0x00d1
        L_0x00b1:
            java.lang.String r1 = "updateSheetName"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x00ba
            goto L_0x00d1
        L_0x00ba:
            r6 = 2
            goto L_0x00d1
        L_0x00bc:
            java.lang.String r1 = "getAllSpreadsheets"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x00c5
            goto L_0x00d1
        L_0x00c5:
            r6 = 1
            goto L_0x00d1
        L_0x00c7:
            java.lang.String r1 = "deleteSpreadsheet"
            boolean r9 = r9.equals(r1)
            if (r9 != 0) goto L_0x00d0
            goto L_0x00d1
        L_0x00d0:
            r6 = 0
        L_0x00d1:
            r9 = 2131820726(0x7f1100b6, float:1.9274175E38)
            switch(r6) {
                case 0: goto L_0x0196;
                case 1: goto L_0x018c;
                case 2: goto L_0x010a;
                case 3: goto L_0x010a;
                case 4: goto L_0x010a;
                case 5: goto L_0x017f;
                case 6: goto L_0x016f;
                case 7: goto L_0x0167;
                case 8: goto L_0x015e;
                case 9: goto L_0x014a;
                case 10: goto L_0x0114;
                case 11: goto L_0x010a;
                case 12: goto L_0x010a;
                case 13: goto L_0x00d8;
                case 14: goto L_0x010a;
                default: goto L_0x00d7;
            }
        L_0x00d7:
            goto L_0x010f
        L_0x00d8:
            android.widget.RelativeLayout r1 = r7.layoutProgress
            r1.setVisibility(r4)
            androidx.recyclerview.widget.RecyclerView r1 = r7.recyclerSheetRows
            r1.setVisibility(r4)
            java.lang.String r1 = "org.json.JSONException: No value for rowData"
            boolean r8 = r8.equalsIgnoreCase(r1)
            if (r8 == 0) goto L_0x00f7
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.textTryAgain
            java.lang.String r1 = "No data found"
            r8.setText(r1)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.btnTryAgain
            r8.setVisibility(r4)
            goto L_0x014f
        L_0x00f7:
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.textTryAgain
            android.content.res.Resources r1 = r7.getResources()
            java.lang.String r1 = r1.getString(r9)
            r8.setText(r1)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.btnTryAgain
            r8.setVisibility(r5)
            goto L_0x014f
        L_0x010a:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
        L_0x010f:
            r7.m33643J0()
            goto L_0x01ad
        L_0x0114:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            cn.pedant.SweetAlert.SweetAlertDialog r8 = new cn.pedant.SweetAlert.SweetAlertDialog
            r8.<init>(r7, r2)
            java.lang.String r1 = "File not found!"
            cn.pedant.SweetAlert.SweetAlertDialog r8 = r8.setTitleText(r1)
            java.lang.String r1 = "You have deleted this sheet\nfrom Drive!"
            cn.pedant.SweetAlert.SweetAlertDialog r8 = r8.setContentText(r1)
            com.spreadsheet.app.activities.ActivitySheetDetails$f r1 = new com.spreadsheet.app.activities.ActivitySheetDetails$f
            r1.<init>()
            java.lang.String r2 = "Remove from list"
            cn.pedant.SweetAlert.SweetAlertDialog r8 = r8.setConfirmButton((java.lang.String) r2, (p007cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener) r1)
            com.spreadsheet.app.activities.ActivitySheetDetails$e r1 = new com.spreadsheet.app.activities.ActivitySheetDetails$e
            r1.<init>()
            java.lang.String r2 = "Exit"
            cn.pedant.SweetAlert.SweetAlertDialog r8 = r8.setCancelButton((java.lang.String) r2, (p007cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener) r1)
            r8.setCancelable(r5)
            r8.setCanceledOnTouchOutside(r5)
            r8.show()
            goto L_0x01ad
        L_0x014a:
            android.widget.RelativeLayout r8 = r7.layoutProgress
            r8.setVisibility(r4)
        L_0x014f:
            androidx.constraintlayout.widget.ConstraintLayout r8 = r7.layoutTryAgain
            r8.setVisibility(r5)
            e.e.a.a.g r8 = r7.f24288E
            r8.isRowRequested = r5
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            goto L_0x01ad
        L_0x015e:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            r7.m33638C0()
            goto L_0x01ad
        L_0x0167:
            e.e.a.d.d r8 = r7.f24305V
            java.lang.String r1 = "DELETE_ROW"
            r8.mo25638c(r1, r1)
            goto L_0x010a
        L_0x016f:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            android.widget.RelativeLayout r8 = r7.layoutProgress
            r8.setVisibility(r4)
            androidx.constraintlayout.widget.ConstraintLayout r8 = r7.layoutTryAgain
            r8.setVisibility(r5)
            goto L_0x01ad
        L_0x017f:
            e.e.a.a.g r8 = r7.f24288E
            r8.shouldRequestAuth = r3
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
        L_0x0188:
            r7.finish()
            goto L_0x01ad
        L_0x018c:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            e.e.a.a.g r8 = r7.f24288E
            r8.isUpdated = r3
            goto L_0x0188
        L_0x0196:
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24211a()
            e.e.a.d.d r8 = r7.f24305V
            java.lang.String r1 = "DELETE_SPREADSHEET"
            r8.mo25638c(r1, r1)
            e.e.a.d.e r8 = r7.f24309Z
            e.e.a.a.e r1 = r7.f24286C
            java.lang.String r1 = r1.getSheetId()
            r8.mo25639d(r1)
        L_0x01ad:
            r8 = r0[r5]
            java.lang.String r0 = "com.android.volley.NoConnectionError"
            boolean r8 = r8.equals(r0)
            if (r8 == 0) goto L_0x01c6
            android.content.res.Resources r8 = r7.getResources()
            java.lang.String r8 = r8.getString(r9)
            android.widget.Toast r8 = android.widget.Toast.makeText(r7, r8, r5)
            r8.show()
        L_0x01c6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivitySheetDetails.mo24237H(java.lang.String, java.lang.String):void");
    }

    /* renamed from: H0 */
    public void mo24335H0() {
    }

    /* renamed from: K */
    public void mo24336K(C7564i iVar, int i) {
        Intent intent = new Intent(this, ActivityAddRow.class);
        intent.putExtra("spreadsheet", this.f24286C);
        intent.putExtra("listsize", i + 1);
        intent.putExtra("sheetTitle", this.f24316y);
        intent.putExtra("sheetId", this.f24317z);
        intent.putExtra("rowmap", iVar);
        startActivityForResult(intent, 2);
    }

    /* renamed from: L0 */
    public void mo24337L0(C7564i iVar, boolean z) {
        List<C7564i> allCustomRowsList = this.f24288E.getAllCustomRowsList();
        this.f24289F = allCustomRowsList;
        if (z) {
            allCustomRowsList.remove(iVar.getRowPos());
        }
        C7562g gVar = this.f24288E;
        if (gVar.isDeleted) {
            gVar.isDeleted = false;
        } else {
            this.f24289F.add(iVar.getRowPos(), iVar);
        }
        m33642I0();
    }

    /* renamed from: M */
    public void mo24338M(C7564i iVar, int i) {
        this.f24307X = iVar;
        m33644K0(i);
    }

    /* renamed from: O */
    public void mo24265O(int i, String str) {
        this.f24317z = i;
        this.f24294K.setText("Rename Sheet");
        this.f24296M.setHint("Sheet name");
        this.f24296M.setText(str);
        EditText editText = this.f24296M;
        editText.setSelection(editText.length());
        this.f24296M.setVisibility(0);
        this.f24295L.setVisibility(8);
        this.f24297N.setText(R.string.update);
        this.f24293J.show();
    }

    /* renamed from: R */
    public void mo24266R(int i, String str) {
        this.f24317z = i;
        mo24344z0(i, str);
    }

    /* renamed from: h */
    public void mo24267h(int i, String str, int i2) {
        this.f24316y = str;
        this.f24317z = i;
        this.f24286C.setLastSheetId(String.valueOf(i));
        this.f24284A = i2;
        this.textAddRow.setVisibility(8);
        this.layoutProgress.setVisibility(0);
        this.f24288E.isUpdated = true;
        C7591e eVar = this.f24309Z;
        String str2 = this.f24315x;
        eVar.mo25643i(str2, this.f24317z + "");
        m33637B0();
    }

    /* renamed from: j */
    public void mo24339j(C7564i iVar, int i) {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != -1) {
            return;
        }
        if (i != 2 || intent == null) {
            this.f24287D.mo25662C(this, this);
            this.f24300Q = C4534g.m24100b().mo18599d(C6873b.f23979G);
            this.f24286C = (C7560e) intent.getSerializableExtra("sheet");
            this.f24288E.isUpdated = true;
            this.f24314w.setText(intent.getStringExtra("sheetname"));
            m33637B0();
            this.f24288E.getColumnList().clear();
            if (this.f24286C.getColumnsList().size() > 0) {
                this.f24288E.setColumnList(this.f24286C.getColumnsList());
            } else {
                m33639D0();
            }
        } else if (intent.getExtras().getBoolean("isUpdated")) {
            this.f24287D.mo25662C(this, this);
            this.f24303T.mo24212c(this);
            this.f24288E.isUpdated = true;
            this.textAddRow.setVisibility(8);
            this.f24314w.setText(intent.getStringExtra("sheetname"));
            mo24337L0((C7564i) intent.getExtras().getSerializable("sheetrow"), intent.getExtras().getBoolean("isEdit"));
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onClick(View view) {
        Toast toast;
        int id = view.getId();
        if (id == R.id.button_add_sheet) {
            this.f24303T.mo24212c(this);
            if (this.f24304U.mo24207d()) {
                if (this.f24297N.getText().toString().equals(getResources().getString(R.string.update))) {
                    String obj = this.f24296M.getText().toString();
                    if (!obj.equals("")) {
                        if (!obj.equals(this.f24316y)) {
                            if (!this.f24290G.contains(obj)) {
                                this.f24293J.dismiss();
                                C7596g gVar = this.f24287D;
                                String str = this.f24315x;
                                gVar.mo25663Q(str, gVar.mo25661B(this.f24317z + "", obj));
                                this.f24303T.mo24213d("");
                                return;
                            }
                        }
                    }
                    toast = Toast.makeText(this, R.string.enter_sheet_name, 0);
                } else if (this.f24297N.getText().toString().equals(getResources().getString(R.string.add))) {
                    String obj2 = this.f24296M.getText().toString();
                    if (!obj2.equals("")) {
                        if (!this.f24290G.contains(obj2)) {
                            this.f24293J.dismiss();
                            this.f24303T.mo24213d("");
                            C7596g gVar2 = this.f24287D;
                            gVar2.mo25667e(this.f24315x, gVar2.mo25676n(obj2));
                            return;
                        }
                    }
                    toast = Toast.makeText(this, R.string.enter_sheet_name, 0);
                } else if (this.f24297N.getText().toString().equals(getResources().getString(R.string.delete))) {
                    this.f24293J.dismiss();
                    this.f24303T.mo24213d("");
                    C7596g gVar3 = this.f24287D;
                    gVar3.mo25671i(this.f24315x, gVar3.mo25682t(String.valueOf(this.f24317z)));
                    return;
                } else {
                    return;
                }
                toast = Toast.makeText(this, R.string.sheet_name_already_exists, 0);
            } else {
                this.f24293J.dismiss();
                toast = Toast.makeText(this, getResources().getString(R.string.internet_check), 0);
            }
            toast.show();
            return;
        } else if (id != R.id.image_close) {
            return;
        }
        this.f24293J.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_sheet_details);
        ButterKnife.bind((Activity) this);
        mo24334F0();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        MenuItem findItem = menu.findItem(R.id.action_search_user);
        SearchManager searchManager = (SearchManager) getSystemService("search");
        SearchView searchView = findItem != null ? (SearchView) findItem.getActionView() : null;
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setQueryHint("Search Here..");
        searchView.setOnQueryTextListener(new C6971m());
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_delete_spreadsheet) {
            C0065b.C0066a aVar = new C0065b.C0066a(this);
            aVar.mo236g(getResources().getString(R.string.do_you_want_to_delete) + " '" + this.f24286C.getSheetName() + "' " + getResources().getString(R.string.file_permanently));
            aVar.mo240k(getResources().getString(R.string.yes), new C6972n());
            aVar.mo237h(getResources().getString(R.string.no), new C6973o(this));
            aVar.mo243n();
        } else if (itemId == R.id.menu_update_spreadsheet) {
            Intent intent = new Intent(this, ActivityCreateSheet.class);
            intent.putExtra("spreadsheet", this.f24286C);
            if (this.f24286C.getSheetType().equals("CustomPro")) {
                intent.putExtra("Pro", "true");
            }
            startActivityForResult(intent, 0);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.f24309Z.mo25642h(this, this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f24309Z.mo25642h(this, this);
        this.f24313v.mo24225f(this);
        this.f24287D.mo25662C(this, this);
        this.f24304U.mo24206c(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005f, code lost:
        mo24333A0(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0088, code lost:
        android.widget.Toast.makeText(r7, getResources().getString(com.spreadsheet.app.R.string.internet_check), 0).show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (r7.f24304U.mo24207d() != false) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0019, code lost:
        m33648x0();
     */
    @butterknife.OnClick({2131296350, 2131296825, 2131296344, 2131296352})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewClicked(View r8) {
        /*
            r7 = this;
            int r8 = r8.getId()
            r0 = 2131820726(0x7f1100b6, float:1.9274175E38)
            java.lang.String r1 = ""
            r2 = 8
            r3 = 0
            switch(r8) {
                case 2131296344: goto L_0x0098;
                case 2131296350: goto L_0x0063;
                case 2131296352: goto L_0x001e;
                case 2131296825: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            goto L_0x00a5
        L_0x0011:
            com.spreadsheet.app.Utils.a r8 = r7.f24304U
            boolean r8 = r8.mo24207d()
            if (r8 == 0) goto L_0x0088
        L_0x0019:
            r7.m33648x0()
            goto L_0x00a5
        L_0x001e:
            com.spreadsheet.app.Utils.h r8 = r7.f24313v
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r8 = r8.mo24221a(r0)
            r0 = 2131820585(0x7f110029, float:1.927389E38)
            java.lang.String r4 = "Sheet name"
            java.lang.String r5 = "Add Sheet"
            if (r8 == 0) goto L_0x0053
        L_0x002f:
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.f24294K
            r8.setText(r5)
            android.widget.EditText r8 = r7.f24296M
            r8.setText(r1)
            android.widget.EditText r8 = r7.f24296M
            r8.setHint(r4)
            android.widget.EditText r8 = r7.f24296M
            r8.setVisibility(r3)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r7.f24295L
            r8.setVisibility(r2)
            android.widget.Button r8 = r7.f24297N
            r8.setText(r0)
            android.app.Dialog r8 = r7.f24293J
            r8.show()
            goto L_0x00a5
        L_0x0053:
            java.util.List<e.e.a.a.j> r8 = r7.f24292I
            int r8 = r8.size()
            r6 = 2
            if (r8 >= r6) goto L_0x005d
            goto L_0x002f
        L_0x005d:
            java.lang.String r8 = "Add more than 2 subsheets with UpSheet Premium"
        L_0x005f:
            r7.mo24333A0(r8)
            goto L_0x00a5
        L_0x0063:
            com.spreadsheet.app.Utils.a r8 = r7.f24304U
            boolean r8 = r8.mo24207d()
            if (r8 == 0) goto L_0x0088
            com.spreadsheet.app.Utils.h r8 = r7.f24313v
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r8 = r8.mo24221a(r0)
            if (r8 == 0) goto L_0x0076
        L_0x0075:
            goto L_0x0019
        L_0x0076:
            e.e.a.a.g r8 = r7.f24288E
            java.util.List r8 = r8.getAllCustomRowsList()
            int r8 = r8.size()
            r0 = 100
            if (r8 >= r0) goto L_0x0085
            goto L_0x0075
        L_0x0085:
            java.lang.String r8 = "Add unlimited rows with UpSheet Premium"
            goto L_0x005f
        L_0x0088:
            android.content.res.Resources r8 = r7.getResources()
            java.lang.String r8 = r8.getString(r0)
            android.widget.Toast r8 = android.widget.Toast.makeText(r7, r8, r3)
            r8.show()
            goto L_0x00a5
        L_0x0098:
            androidx.constraintlayout.widget.ConstraintLayout r8 = r7.layoutTryAgain
            r8.setVisibility(r2)
            com.spreadsheet.app.Utils.d r8 = r7.f24303T
            r8.mo24213d(r1)
            r7.m33638C0()
        L_0x00a5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivitySheetDetails.onViewClicked(android.view.View):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019c, code lost:
        m33642I0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x021f, code lost:
        m33638C0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        return;
     */
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24244s(String r5, String r6) {
        /*
            r4 = this;
            r6.hashCode()
            int r5 = r6.hashCode()
            r0 = 8
            r1 = 0
            r2 = 1
            r3 = -1
            switch(r5) {
                case -2022976223: goto L_0x00a4;
                case -1978479736: goto L_0x0099;
                case -1809886158: goto L_0x008e;
                case -1479983679: goto L_0x0083;
                case -1422526087: goto L_0x0078;
                case -1133266316: goto L_0x006d;
                case -452548289: goto L_0x0062;
                case -358721489: goto L_0x0057;
                case 63418517: goto L_0x0049;
                case 323754608: goto L_0x003b;
                case 391607136: goto L_0x002d;
                case 1001959285: goto L_0x001f;
                case 1594339684: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            goto L_0x00ae
        L_0x0011:
            java.lang.String r5 = "formatHeader"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x001b
            goto L_0x00ae
        L_0x001b:
            r3 = 12
            goto L_0x00ae
        L_0x001f:
            java.lang.String r5 = "getAllRowsCustom"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0029
            goto L_0x00ae
        L_0x0029:
            r3 = 11
            goto L_0x00ae
        L_0x002d:
            java.lang.String r5 = "addNewSheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0037
            goto L_0x00ae
        L_0x0037:
            r3 = 10
            goto L_0x00ae
        L_0x003b:
            java.lang.String r5 = "formatRows"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0045
            goto L_0x00ae
        L_0x0045:
            r3 = 9
            goto L_0x00ae
        L_0x0049:
            java.lang.String r5 = "applyColor"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0053
            goto L_0x00ae
        L_0x0053:
            r3 = 8
            goto L_0x00ae
        L_0x0057:
            java.lang.String r5 = "deleteRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0060
            goto L_0x00ae
        L_0x0060:
            r3 = 7
            goto L_0x00ae
        L_0x0062:
            java.lang.String r5 = "getAllSheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x006b
            goto L_0x00ae
        L_0x006b:
            r3 = 6
            goto L_0x00ae
        L_0x006d:
            java.lang.String r5 = "deleteSheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0076
            goto L_0x00ae
        L_0x0076:
            r3 = 5
            goto L_0x00ae
        L_0x0078:
            java.lang.String r5 = "addRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0081
            goto L_0x00ae
        L_0x0081:
            r3 = 4
            goto L_0x00ae
        L_0x0083:
            java.lang.String r5 = "updateSheetName"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x008c
            goto L_0x00ae
        L_0x008c:
            r3 = 3
            goto L_0x00ae
        L_0x008e:
            java.lang.String r5 = "getAllSpreadsheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0097
            goto L_0x00ae
        L_0x0097:
            r3 = 2
            goto L_0x00ae
        L_0x0099:
            java.lang.String r5 = "addFormattedHeader"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x00a2
            goto L_0x00ae
        L_0x00a2:
            r3 = 1
            goto L_0x00ae
        L_0x00a4:
            java.lang.String r5 = "deleteSpreadsheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x00ad
            goto L_0x00ae
        L_0x00ad:
            r3 = 0
        L_0x00ae:
            java.lang.String r5 = ""
            switch(r3) {
                case 0: goto L_0x0223;
                case 1: goto L_0x021f;
                case 2: goto L_0x0212;
                case 3: goto L_0x01ff;
                case 4: goto L_0x01e9;
                case 5: goto L_0x01c3;
                case 6: goto L_0x01ba;
                case 7: goto L_0x01a1;
                case 8: goto L_0x0185;
                case 9: goto L_0x021f;
                case 10: goto L_0x0129;
                case 11: goto L_0x00c4;
                case 12: goto L_0x00b5;
                default: goto L_0x00b3;
            }
        L_0x00b3:
            goto L_0x0235
        L_0x00b5:
            e.e.a.d.g r5 = r4.f24287D
            java.lang.String r6 = r4.f24315x
            int r0 = r4.f24317z
            org.json.JSONObject r0 = r5.mo25683u(r0)
            r5.mo25674l(r6, r0)
            goto L_0x0235
        L_0x00c4:
            androidx.swiperefreshlayout.widget.SwipeRefreshLayout r5 = r4.layoutPullToRefresh
            r5.setRefreshing(r1)
            com.spreadsheet.app.Utils.d r5 = r4.f24303T
            r5.mo24211a()
            android.widget.RelativeLayout r5 = r4.layoutProgress
            r5.setVisibility(r0)
            androidx.constraintlayout.widget.ConstraintLayout r5 = r4.layoutTryAgain
            r5.setVisibility(r0)
            e.e.a.a.g r5 = r4.f24288E
            java.util.List r5 = r5.getAllCustomRowsList()
            r4.f24289F = r5
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x00fb
            android.widget.RelativeLayout r5 = r4.layoutProgress
            r5.setVisibility(r0)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r5 = r4.textAddRow
            r5.setVisibility(r1)
            android.widget.ImageView r5 = r4.buttonAddRow
            r5.setVisibility(r0)
            android.widget.LinearLayout r5 = r4.layoutEmptyScreen
            r5.setVisibility(r1)
            goto L_0x0123
        L_0x00fb:
            int r5 = r4.f24285B
            java.util.List<e.e.a.a.i> r6 = r4.f24289F
            int r6 = r6.size()
            if (r5 >= r6) goto L_0x0114
            java.util.List<e.e.a.a.i> r5 = r4.f24289F
            int r5 = r5.size()
            r4.f24285B = r5
            com.spreadsheet.app.Utils.h r6 = r4.f24313v
            java.lang.String r2 = "allRowCount"
            r6.mo24228i(r2, r5)
        L_0x0114:
            com.spreadsheet.app.Utils.Fonts.CustomTextView r5 = r4.textAddRow
            r5.setVisibility(r0)
            android.widget.LinearLayout r5 = r4.layoutEmptyScreen
            r5.setVisibility(r0)
            android.widget.ImageView r5 = r4.buttonAddRow
            r5.setVisibility(r1)
        L_0x0123:
            androidx.recyclerview.widget.RecyclerView r5 = r4.recyclerSheetRows
            r5.setVisibility(r1)
            goto L_0x019c
        L_0x0129:
            e.e.a.d.d r6 = r4.f24305V
            java.lang.String r0 = "ADD_NEW_SHEET"
            r6.mo25638c(r0, r0)
            e.e.a.a.g r6 = r4.f24288E
            int r6 = r6.sheetId
            r4.f24317z = r6
            e.e.a.a.e r6 = r4.f24286C
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.util.List<java.lang.String> r2 = r4.f24290G
            int r2 = r2.size()
            r0.append(r2)
            r0.append(r5)
            java.lang.String r5 = r0.toString()
            r6.setLastSheetId(r5)
            java.util.List<java.lang.String> r5 = r4.f24290G
            int r5 = r5.size()
            r4.f24284A = r5
            java.util.List<java.lang.String> r5 = r4.f24290G
            r5.size()
            android.widget.EditText r5 = r4.f24296M
            android.text.Editable r5 = r5.getText()
            java.lang.String r5 = r5.toString()
            e.e.a.a.e r6 = r4.f24286C
            int r6 = r6.getColCount()
            com.spreadsheet.app.Utils.C6873b.m33473k(r5, r6)
            e.e.a.d.g r5 = r4.f24287D
            java.lang.String r6 = r4.f24315x
            int r0 = r4.f24317z
            e.e.a.a.g r2 = r4.f24288E
            java.util.List r2 = r2.getHeaderColumns()
            org.json.JSONObject r0 = r5.mo25686x(r0, r1, r2)
            r5.mo25665c(r6, r0)
            goto L_0x0235
        L_0x0185:
            e.e.a.a.g r5 = r4.f24288E
            r5.isUpdated = r2
            e.e.a.d.e r5 = r4.f24309Z
            java.lang.String r6 = r4.f24315x
            r5.mo25647m(r6)
            e.e.a.d.d r5 = r4.f24305V
            java.lang.String r6 = "APPLY_COLOR_TO_ROW"
            r5.mo25638c(r6, r6)
            com.spreadsheet.app.Utils.d r5 = r4.f24303T
            r5.mo24211a()
        L_0x019c:
            r4.m33642I0()
            goto L_0x0235
        L_0x01a1:
            com.spreadsheet.app.Utils.d r5 = r4.f24303T
            r5.mo24211a()
            e.e.a.d.e r5 = r4.f24309Z
            java.lang.String r6 = r4.f24315x
            r5.mo25647m(r6)
            e.e.a.a.g r5 = r4.f24288E
            r5.isUpdated = r2
            r5.isDeleted = r2
            e.e.a.a.i r5 = r4.f24307X
            r4.mo24337L0(r5, r2)
            goto L_0x0235
        L_0x01ba:
            com.spreadsheet.app.Utils.d r5 = r4.f24303T
            r5.mo24211a()
            r4.m33641G0()
            goto L_0x0235
        L_0x01c3:
            e.e.a.d.d r6 = r4.f24305V
            java.lang.String r0 = "DELETE_SHEET"
            r6.mo25638c(r0, r0)
            r4.f24284A = r1
            e.e.a.a.g r6 = r4.f24288E
            r6.isUpdated = r2
            e.e.a.d.e r6 = r4.f24309Z
            java.lang.String r0 = r4.f24315x
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            int r2 = r4.f24284A
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            r6.mo25643i(r0, r5)
            goto L_0x021f
        L_0x01e9:
            e.e.a.d.e r5 = r4.f24309Z
            java.lang.String r6 = r4.f24315x
            r5.mo25647m(r6)
            e.e.a.a.g r5 = r4.f24288E
            r5.isUpdated = r2
            e.e.a.d.d r5 = r4.f24305V
            java.lang.String r6 = "ADD_ROW"
            r5.mo25638c(r6, r6)
            r4.m33637B0()
            goto L_0x0235
        L_0x01ff:
            e.e.a.a.g r5 = r4.f24288E
            r5.isUpdated = r2
            e.e.a.d.e r5 = r4.f24309Z
            java.lang.String r6 = r4.f24315x
            r5.mo25647m(r6)
            e.e.a.d.d r5 = r4.f24305V
            java.lang.String r6 = "UPDATE_SHEET_NAME"
            r5.mo25638c(r6, r6)
            goto L_0x021f
        L_0x0212:
            com.spreadsheet.app.Utils.d r5 = r4.f24303T
            r5.mo24211a()
            e.e.a.a.g r5 = r4.f24288E
            r5.isUpdated = r2
            r4.finish()
            goto L_0x0235
        L_0x021f:
            r4.m33638C0()
            goto L_0x0235
        L_0x0223:
            e.e.a.d.d r5 = r4.f24305V
            java.lang.String r6 = "DELETE_SPREADSHEET"
            r5.mo25638c(r6, r6)
            e.e.a.d.e r5 = r4.f24309Z
            e.e.a.a.e r6 = r4.f24286C
            java.lang.String r6 = r6.getSheetId()
            r5.mo25639d(r6)
        L_0x0235:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivitySheetDetails.mo24244s(java.lang.String, java.lang.String):void");
    }

    /* renamed from: z0 */
    public void mo24344z0(int i, String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete " + str + "?");
        builder.setPositiveButton(getResources().getString(R.string.yes), new C6976r(i));
        builder.setNegativeButton(getResources().getString(R.string.no), new C6957a(this));
        builder.show();
    }
}
