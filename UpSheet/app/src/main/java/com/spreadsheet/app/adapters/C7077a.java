package com.spreadsheet.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6873b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p153e.p300e.p301a.p304c.C7571a;
import p153e.p307f.p314b.C7822t;
import p153e.p307f.p314b.C7838x;

/* renamed from: com.spreadsheet.app.adapters.a */
public class C7077a extends RecyclerView.C0503g<C7079b> {

    /* renamed from: e */
    Context f24607e;

    /* renamed from: f */
    List<HashMap<String, String>> f24608f = new ArrayList();

    /* renamed from: g */
    C7571a f24609g;

    /* renamed from: com.spreadsheet.app.adapters.a$a */
    class C7078a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24610c;

        C7078a(int i) {
            this.f24610c = i;
        }

        public void onClick(View view) {
            C7077a aVar = C7077a.this;
            aVar.f24609g.mo24408g(aVar.f24608f.get(this.f24610c));
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.a$b */
    public class C7079b extends RecyclerView.C0500d0 {

        /* renamed from: t */
        TextView f24612t;

        /* renamed from: u */
        CircularImageView f24613u;

        /* renamed from: v */
        ConstraintLayout f24614v;

        public C7079b(C7077a aVar, View view) {
            super(view);
            this.f24612t = (TextView) view.findViewById(R.id.text_account_email);
            this.f24613u = (CircularImageView) view.findViewById(R.id.image_account);
            this.f24614v = (ConstraintLayout) view.findViewById(R.id.layout_account_detail);
        }
    }

    public C7077a(Context context, List<HashMap<String, String>> list, C7571a aVar) {
        this.f24607e = context;
        this.f24608f = list;
        this.f24609g = aVar;
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24608f.size();
    }

    /* renamed from: u */
    public void mo3300j(C7079b bVar, int i) {
        HashMap hashMap = this.f24608f.get(i);
        bVar.f24612t.setText((CharSequence) hashMap.get("email"));
        if (!((String) hashMap.get("photoUrl")).equals("")) {
            try {
                C7838x j = C7822t.m36816o(this.f24607e).mo26286j((String) hashMap.get("photoUrl"));
                j.mo26320f(R.drawable.applogo);
                j.mo26317b(R.drawable.applogo);
                j.mo26318d(bVar.f24613u);
            } catch (Exception unused) {
            }
        } else {
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setStyle(Paint.Style.FILL);
            paint.setFlags(1);
            Paint paint2 = new Paint();
            paint2.setColor(this.f24607e.getResources().getColor(R.color.colorPrimary));
            paint2.setStyle(Paint.Style.FILL);
            String substring = ((String) hashMap.get("name")).substring(0, 1);
            int dimension = (int) this.f24607e.getResources().getDimension(R.dimen.dp_35);
            int dimension2 = (int) this.f24607e.getResources().getDimension(R.dimen.dp_20);
            paint.setTextSize((float) dimension2);
            Bitmap createBitmap = Bitmap.createBitmap(dimension, (int) this.f24607e.getResources().getDimension(R.dimen.dp_35), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            canvas.drawPaint(paint2);
            canvas.drawText(substring, (float) C6873b.m33467e(substring, dimension2, dimension), (float) ((int) (((float) (canvas.getHeight() / 2)) - ((paint.descent() + paint.ascent()) / 2.0f))), paint);
            bVar.f24613u.setImageBitmap(createBitmap);
        }
        bVar.f24614v.setOnClickListener(new C7078a(i));
    }

    /* renamed from: v */
    public C7079b mo3302l(ViewGroup viewGroup, int i) {
        return new C7079b(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_account, viewGroup, false));
    }
}
