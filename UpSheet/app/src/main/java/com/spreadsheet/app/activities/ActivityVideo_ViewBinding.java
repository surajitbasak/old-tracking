package com.spreadsheet.app.activities;

import android.view.View;
import butterknife.Unbinder;
import butterknife.p002c.C0642c;
import com.google.android.youtube.player.YouTubePlayerView;
import com.spreadsheet.app.R;

public class ActivityVideo_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityVideo f24418a;

    public ActivityVideo_ViewBinding(ActivityVideo activityVideo, View view) {
        this.f24418a = activityVideo;
        activityVideo.playerVideo = (YouTubePlayerView) C0642c.m3874c(view, R.id.player_video, "field 'playerVideo'", YouTubePlayerView.class);
    }

    public void unbind() {
        ActivityVideo activityVideo = this.f24418a;
        if (activityVideo != null) {
            this.f24418a = null;
            activityVideo.playerVideo = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
