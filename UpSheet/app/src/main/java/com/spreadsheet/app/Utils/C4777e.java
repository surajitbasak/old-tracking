package com.spreadsheet.app.Utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: com.spreadsheet.app.Utils.e */
public class C4777e implements Application.ActivityLifecycleCallbacks {

    /* renamed from: g */
    public static final String f18751g = C4777e.class.getName();

    /* renamed from: h */
    private static C4777e f18752h;

    /* renamed from: i */
    public static List<C4778b> f18753i = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public boolean f18754c = false;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f18755d = true;

    /* renamed from: e */
    private Handler f18756e = new Handler();

    /* renamed from: f */
    private Runnable f18757f;

    /* renamed from: com.spreadsheet.app.Utils.e$b */
    public interface C4778b {
        /* renamed from: a */
        void mo19177a();

        /* renamed from: b */
        void mo19178b();
    }

    /* renamed from: com.spreadsheet.app.Utils.e$a */
    class C4779a implements Runnable {
        C4779a() {
        }

        public void run() {
            if (!C4777e.this.f18754c || !C4777e.this.f18755d) {
                Log.i(C4777e.f18751g, "still foreground");
                return;
            }
            boolean unused = C4777e.this.f18754c = false;
            for (C4778b a : C4777e.f18753i) {
                try {
                    a.mo19177a();
                } catch (Exception e) {
                    Log.e(C4777e.f18751g, "Listener threw exception!", e);
                }
            }
        }
    }

    /* renamed from: d */
    public static void m25152d(C4778b bVar) {
        f18753i.add(bVar);
    }

    /* renamed from: e */
    public static C4777e m25153e(Application application) {
        if (f18752h == null) {
            C4777e eVar = new C4777e();
            f18752h = eVar;
            application.registerActivityLifecycleCallbacks(eVar);
        }
        return f18752h;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        this.f18755d = true;
        Runnable runnable = this.f18757f;
        if (runnable != null) {
            this.f18756e.removeCallbacks(runnable);
        }
        Handler handler = this.f18756e;
        C4779a aVar = new C4779a();
        this.f18757f = aVar;
        handler.postDelayed(aVar, 500);
    }

    public void onActivityResumed(Activity activity) {
        this.f18755d = false;
        boolean z = !this.f18754c;
        this.f18754c = true;
        Runnable runnable = this.f18757f;
        if (runnable != null) {
            this.f18756e.removeCallbacks(runnable);
        }
        if (z) {
            Log.i(f18751g, "went foreground");
            for (C4778b b : f18753i) {
                try {
                    b.mo19178b();
                } catch (Exception e) {
                    Log.e(f18751g, "Listener threw exception!", e);
                }
            }
            return;
        }
        Log.i(f18751g, "still foreground");
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }
}
