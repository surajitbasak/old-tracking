package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class ActivitySheetDetails_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivitySheetDetails f24337a;

    /* renamed from: b */
    private View f24338b;

    /* renamed from: c */
    private View f24339c;

    /* renamed from: d */
    private View f24340d;

    /* renamed from: e */
    private View f24341e;

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails_ViewBinding$a */
    class C6977a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivitySheetDetails f24342e;

        C6977a(ActivitySheetDetails_ViewBinding activitySheetDetails_ViewBinding, ActivitySheetDetails activitySheetDetails) {
            this.f24342e = activitySheetDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24342e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails_ViewBinding$b */
    class C6978b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivitySheetDetails f24343e;

        C6978b(ActivitySheetDetails_ViewBinding activitySheetDetails_ViewBinding, ActivitySheetDetails activitySheetDetails) {
            this.f24343e = activitySheetDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24343e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails_ViewBinding$c */
    class C6979c extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivitySheetDetails f24344e;

        C6979c(ActivitySheetDetails_ViewBinding activitySheetDetails_ViewBinding, ActivitySheetDetails activitySheetDetails) {
            this.f24344e = activitySheetDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24344e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivitySheetDetails_ViewBinding$d */
    class C6980d extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivitySheetDetails f24345e;

        C6980d(ActivitySheetDetails_ViewBinding activitySheetDetails_ViewBinding, ActivitySheetDetails activitySheetDetails) {
            this.f24345e = activitySheetDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24345e.onViewClicked(view);
        }
    }

    public ActivitySheetDetails_ViewBinding(ActivitySheetDetails activitySheetDetails, View view) {
        this.f24337a = activitySheetDetails;
        activitySheetDetails.toolbarSheetDetail = (Toolbar) C0642c.m3874c(view, R.id.toolbar_sheet_detail, "field 'toolbarSheetDetail'", Toolbar.class);
        activitySheetDetails.recyclerSheetRows = (RecyclerView) C0642c.m3874c(view, R.id.recycler_sheet_rows, "field 'recyclerSheetRows'", RecyclerView.class);
        activitySheetDetails.layoutProgress = (RelativeLayout) C0642c.m3874c(view, R.id.layout_progress, "field 'layoutProgress'", RelativeLayout.class);
        View b = C0642c.m3873b(view, R.id.text_add_row, "field 'textAddRow' and method 'onViewClicked'");
        activitySheetDetails.textAddRow = (CustomTextView) C0642c.m3872a(b, R.id.text_add_row, "field 'textAddRow'", CustomTextView.class);
        this.f24338b = b;
        b.setOnClickListener(new C6977a(this, activitySheetDetails));
        activitySheetDetails.textTryAgain = (CustomTextView) C0642c.m3874c(view, R.id.text_try_again, "field 'textTryAgain'", CustomTextView.class);
        activitySheetDetails.layoutEmptyScreen = (LinearLayout) C0642c.m3874c(view, R.id.layout_empty_screen, "field 'layoutEmptyScreen'", LinearLayout.class);
        View b2 = C0642c.m3873b(view, R.id.btn_try_again, "field 'btnTryAgain' and method 'onViewClicked'");
        activitySheetDetails.btnTryAgain = (CustomTextView) C0642c.m3872a(b2, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        this.f24339c = b2;
        b2.setOnClickListener(new C6978b(this, activitySheetDetails));
        activitySheetDetails.layoutTryAgain = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", ConstraintLayout.class);
        View b3 = C0642c.m3873b(view, R.id.button_add_row, "field 'buttonAddRow' and method 'onViewClicked'");
        activitySheetDetails.buttonAddRow = (ImageView) C0642c.m3872a(b3, R.id.button_add_row, "field 'buttonAddRow'", ImageView.class);
        this.f24340d = b3;
        b3.setOnClickListener(new C6979c(this, activitySheetDetails));
        activitySheetDetails.layoutPullToRefresh = (SwipeRefreshLayout) C0642c.m3874c(view, R.id.layout_pull_to_refresh, "field 'layoutPullToRefresh'", SwipeRefreshLayout.class);
        View b4 = C0642c.m3873b(view, R.id.button_add_subsheet, "field 'buttonAddSubsheet' and method 'onViewClicked'");
        activitySheetDetails.buttonAddSubsheet = (ImageView) C0642c.m3872a(b4, R.id.button_add_subsheet, "field 'buttonAddSubsheet'", ImageView.class);
        this.f24341e = b4;
        b4.setOnClickListener(new C6980d(this, activitySheetDetails));
        activitySheetDetails.recyclerSubsheets = (RecyclerView) C0642c.m3874c(view, R.id.recycler_subsheets, "field 'recyclerSubsheets'", RecyclerView.class);
        activitySheetDetails.layoutSubsheetsList = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_subsheets_list, "field 'layoutSubsheetsList'", ConstraintLayout.class);
    }

    public void unbind() {
        ActivitySheetDetails activitySheetDetails = this.f24337a;
        if (activitySheetDetails != null) {
            this.f24337a = null;
            activitySheetDetails.toolbarSheetDetail = null;
            activitySheetDetails.recyclerSheetRows = null;
            activitySheetDetails.layoutProgress = null;
            activitySheetDetails.textAddRow = null;
            activitySheetDetails.textTryAgain = null;
            activitySheetDetails.layoutEmptyScreen = null;
            activitySheetDetails.btnTryAgain = null;
            activitySheetDetails.layoutTryAgain = null;
            activitySheetDetails.buttonAddRow = null;
            activitySheetDetails.layoutPullToRefresh = null;
            activitySheetDetails.buttonAddSubsheet = null;
            activitySheetDetails.recyclerSubsheets = null;
            activitySheetDetails.layoutSubsheetsList = null;
            this.f24338b.setOnClickListener((View.OnClickListener) null);
            this.f24338b = null;
            this.f24339c.setOnClickListener((View.OnClickListener) null);
            this.f24339c = null;
            this.f24340d.setOnClickListener((View.OnClickListener) null);
            this.f24340d = null;
            this.f24341e.setOnClickListener((View.OnClickListener) null);
            this.f24341e = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
