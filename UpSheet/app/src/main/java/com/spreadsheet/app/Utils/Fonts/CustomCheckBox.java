package com.spreadsheet.app.Utils.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import androidx.appcompat.widget.C0226g;
import com.spreadsheet.app.Utils.C6873b;

public class CustomCheckBox extends C0226g {
    public CustomCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo24197b();
    }

    /* renamed from: b */
    public void mo24197b() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), C6873b.f23973A));
    }
}
