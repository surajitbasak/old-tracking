package com.spreadsheet.app.Utils.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.spreadsheet.app.Utils.C6873b;

public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
        mo24202a();
    }

    public CustomTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo24202a();
    }

    /* renamed from: a */
    public void mo24202a() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), C6873b.f23973A));
    }
}
