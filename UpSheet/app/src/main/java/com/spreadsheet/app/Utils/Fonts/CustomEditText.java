package com.spreadsheet.app.Utils.Fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import com.spreadsheet.app.Utils.C6873b;

public class CustomEditText extends EditText {
    public CustomEditText(Context context) {
        super(context);
        mo24198a();
    }

    public CustomEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mo24198a();
    }

    /* renamed from: a */
    public void mo24198a() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), C6873b.f23973A));
    }

    public void setMaxLines(int i) {
        super.setMaxLines(i);
    }
}
