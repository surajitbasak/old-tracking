package com.spreadsheet.app.activities;

import android.app.Activity;
import android.os.Bundle;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.youtube.player.C3888b;
import com.google.android.youtube.player.C3890c;
import com.google.android.youtube.player.C3891d;
import com.google.android.youtube.player.YouTubePlayerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6873b;

public class ActivityVideo extends C3888b implements C3891d.C3892a {

    /* renamed from: g */
    C3891d f24417g;
    @BindView(2131296714)
    YouTubePlayerView playerVideo;

    /* renamed from: a */
    public void mo14759a(C3891d.C3893b bVar, C3891d dVar, boolean z) {
        this.f24417g = dVar;
        if (!z) {
            try {
                dVar.mo14758a(C6873b.f24005d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: b */
    public void mo14760b(C3891d.C3893b bVar, C3890c cVar) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video);
        ButterKnife.bind((Activity) this);
        try {
            this.playerVideo.mo14743v(C6873b.f24003c, this);
        } catch (IllegalStateException unused) {
        }
    }
}
