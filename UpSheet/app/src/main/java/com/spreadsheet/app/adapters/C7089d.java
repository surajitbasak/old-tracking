package com.spreadsheet.app.adapters;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

/* renamed from: com.spreadsheet.app.adapters.d */
public class C7089d extends RecyclerView.C0513n {

    /* renamed from: a */
    private final int f24644a;

    /* renamed from: b */
    private final boolean f24645b;

    /* renamed from: c */
    private final C7090a f24646c;

    /* renamed from: d */
    private View f24647d;

    /* renamed from: e */
    private CustomTextView f24648e;

    /* renamed from: com.spreadsheet.app.adapters.d$a */
    public interface C7090a {
        /* renamed from: a */
        boolean mo24384a(int i);

        /* renamed from: b */
        CharSequence mo24385b(int i);
    }

    public C7089d(Context context, int i, boolean z, C7090a aVar) {
        this.f24644a = i;
        this.f24645b = z;
        this.f24646c = aVar;
    }

    /* renamed from: j */
    private void m33863j(Canvas canvas, View view, View view2) {
        canvas.save();
        canvas.translate(0.0f, (float) (this.f24645b ? Math.max(0, view.getTop() - view2.getHeight()) : view.getTop() - view2.getHeight()));
        view2.draw(canvas);
        canvas.restore();
    }

    /* renamed from: k */
    private void m33864k(View view, ViewGroup viewGroup) {
        view.measure(ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(viewGroup.getWidth(), 1073741824), viewGroup.getPaddingLeft() + viewGroup.getPaddingRight(), view.getLayoutParams().width), ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(viewGroup.getHeight(), 0), viewGroup.getPaddingTop() + viewGroup.getPaddingBottom(), view.getLayoutParams().height));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    /* renamed from: l */
    private View m33865l(RecyclerView recyclerView) {
        return LayoutInflater.from(recyclerView.getContext()).inflate(R.layout.row_task_header, recyclerView, false);
    }

    /* renamed from: e */
    public void mo3342e(Rect rect, View view, RecyclerView recyclerView, RecyclerView.C0494a0 a0Var) {
        super.mo3342e(rect, view, recyclerView, a0Var);
        if (this.f24646c.mo24384a(recyclerView.getChildAdapterPosition(view))) {
            rect.top = this.f24644a;
        }
    }

    /* renamed from: i */
    public void mo3346i(Canvas canvas, RecyclerView recyclerView, RecyclerView.C0494a0 a0Var) {
        super.mo3346i(canvas, recyclerView, a0Var);
        if (this.f24647d == null) {
            View l = m33865l(recyclerView);
            this.f24647d = l;
            this.f24648e = (CustomTextView) l.findViewById(R.id.text_header_task);
            ConstraintLayout constraintLayout = (ConstraintLayout) this.f24647d.findViewById(R.id.layout_header);
            m33864k(this.f24647d, recyclerView);
        }
        String str = "";
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            View childAt = recyclerView.getChildAt(i);
            int childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
            String valueOf = String.valueOf(this.f24646c.mo24385b(childAdapterPosition));
            this.f24648e.setText(valueOf);
            if (!str.equals(valueOf) || this.f24646c.mo24384a(childAdapterPosition)) {
                m33863j(canvas, childAt, this.f24647d);
                this.f24647d.setVisibility(0);
                str = valueOf;
            } else {
                this.f24647d.setVisibility(8);
            }
        }
    }
}
