package com.spreadsheet.app.Utils;

import java.util.Comparator;
import java.util.Map;

/* renamed from: com.spreadsheet.app.Utils.g */
public class C6878g implements Comparator<Map<String, Object>> {

    /* renamed from: c */
    private final String f24051c;

    public C6878g(String str) {
        this.f24051c = str;
    }

    /* renamed from: a */
    public int compare(Map<String, Object> map, Map<String, Object> map2) {
        return String.valueOf(map.get(this.f24051c)).compareTo(String.valueOf(map2.get(this.f24051c)));
    }
}
