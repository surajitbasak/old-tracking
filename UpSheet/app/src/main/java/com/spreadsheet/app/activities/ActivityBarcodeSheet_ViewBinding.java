package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomButton;
import com.spreadsheet.app.Utils.Fonts.CustomCheckBox;
import com.spreadsheet.app.Utils.Fonts.CustomEditText;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class ActivityBarcodeSheet_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityBarcodeSheet f24176a;

    /* renamed from: b */
    private View f24177b;

    /* renamed from: c */
    private View f24178c;

    /* renamed from: d */
    private View f24179d;

    /* renamed from: e */
    private View f24180e;

    /* renamed from: f */
    private View f24181f;

    /* renamed from: g */
    private View f24182g;

    /* renamed from: h */
    private View f24183h;

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$a */
    class C6925a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24184e;

        C6925a(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24184e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24184e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$b */
    class C6926b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24185e;

        C6926b(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24185e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24185e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$c */
    class C6927c extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24186e;

        C6927c(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24186e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24186e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$d */
    class C6928d extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24187e;

        C6928d(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24187e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24187e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$e */
    class C6929e extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24188e;

        C6929e(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24188e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24188e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$f */
    class C6930f extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24189e;

        C6930f(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24189e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24189e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet_ViewBinding$g */
    class C6931g extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityBarcodeSheet f24190e;

        C6931g(ActivityBarcodeSheet_ViewBinding activityBarcodeSheet_ViewBinding, ActivityBarcodeSheet activityBarcodeSheet) {
            this.f24190e = activityBarcodeSheet;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24190e.onViewClicked(view);
        }
    }

    public ActivityBarcodeSheet_ViewBinding(ActivityBarcodeSheet activityBarcodeSheet, View view) {
        this.f24176a = activityBarcodeSheet;
        activityBarcodeSheet.toolbarBarcodeSheet = (Toolbar) C0642c.m3874c(view, R.id.toolbar_barcode_sheet, "field 'toolbarBarcodeSheet'", Toolbar.class);
        activityBarcodeSheet.recyclerBarcodes = (RecyclerView) C0642c.m3874c(view, R.id.recycler_barcodes, "field 'recyclerBarcodes'", RecyclerView.class);
        activityBarcodeSheet.layoutProgress = (RelativeLayout) C0642c.m3874c(view, R.id.layout_progress, "field 'layoutProgress'", RelativeLayout.class);
        View b = C0642c.m3873b(view, R.id.btn_try_again, "field 'btnTryAgain' and method 'onViewClicked'");
        activityBarcodeSheet.btnTryAgain = (CustomTextView) C0642c.m3872a(b, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        this.f24177b = b;
        b.setOnClickListener(new C6925a(this, activityBarcodeSheet));
        activityBarcodeSheet.layoutTryAgain = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", ConstraintLayout.class);
        activityBarcodeSheet.textAddRow = (CustomTextView) C0642c.m3874c(view, R.id.text_add_row, "field 'textAddRow'", CustomTextView.class);
        activityBarcodeSheet.textTryAgain = (CustomTextView) C0642c.m3874c(view, R.id.text_try_again, "field 'textTryAgain'", CustomTextView.class);
        activityBarcodeSheet.layoutEmptyScreen = (LinearLayout) C0642c.m3874c(view, R.id.layout_empty_screen, "field 'layoutEmptyScreen'", LinearLayout.class);
        View b2 = C0642c.m3873b(view, R.id.button_add_row, "field 'buttonAddRow' and method 'onViewClicked'");
        activityBarcodeSheet.buttonAddRow = (ImageView) C0642c.m3872a(b2, R.id.button_add_row, "field 'buttonAddRow'", ImageView.class);
        this.f24178c = b2;
        b2.setOnClickListener(new C6926b(this, activityBarcodeSheet));
        activityBarcodeSheet.editQuantity = (CustomEditText) C0642c.m3874c(view, R.id.edit_quantity, "field 'editQuantity'", CustomEditText.class);
        activityBarcodeSheet.editBarcode = (CustomEditText) C0642c.m3874c(view, R.id.edit_barcode, "field 'editBarcode'", CustomEditText.class);
        View b3 = C0642c.m3873b(view, R.id.button_add_barcode, "field 'buttonAddBarcode' and method 'onViewClicked'");
        activityBarcodeSheet.buttonAddBarcode = (CustomButton) C0642c.m3872a(b3, R.id.button_add_barcode, "field 'buttonAddBarcode'", CustomButton.class);
        this.f24179d = b3;
        b3.setOnClickListener(new C6927c(this, activityBarcodeSheet));
        activityBarcodeSheet.cardAddBarcode = (CardView) C0642c.m3874c(view, R.id.card_add_barcode, "field 'cardAddBarcode'", CardView.class);
        View b4 = C0642c.m3873b(view, R.id.image_close_card, "field 'imageCloseCard' and method 'onViewClicked'");
        activityBarcodeSheet.imageCloseCard = (ImageView) C0642c.m3872a(b4, R.id.image_close_card, "field 'imageCloseCard'", ImageView.class);
        this.f24180e = b4;
        b4.setOnClickListener(new C6928d(this, activityBarcodeSheet));
        View b5 = C0642c.m3873b(view, R.id.layout_add_manually, "field 'layoutAddManually' and method 'onViewClicked'");
        activityBarcodeSheet.layoutAddManually = (LinearLayout) C0642c.m3872a(b5, R.id.layout_add_manually, "field 'layoutAddManually'", LinearLayout.class);
        this.f24181f = b5;
        b5.setOnClickListener(new C6929e(this, activityBarcodeSheet));
        View b6 = C0642c.m3873b(view, R.id.layout_scan, "field 'layoutScan' and method 'onViewClicked'");
        activityBarcodeSheet.layoutScan = (LinearLayout) C0642c.m3872a(b6, R.id.layout_scan, "field 'layoutScan'", LinearLayout.class);
        this.f24182g = b6;
        b6.setOnClickListener(new C6930f(this, activityBarcodeSheet));
        activityBarcodeSheet.cardButtons = (CardView) C0642c.m3874c(view, R.id.card_buttons, "field 'cardButtons'", CardView.class);
        activityBarcodeSheet.layoutAddProgress = (RelativeLayout) C0642c.m3874c(view, R.id.layout_add_progress, "field 'layoutAddProgress'", RelativeLayout.class);
        activityBarcodeSheet.checkboxUpdateDate = (CustomCheckBox) C0642c.m3874c(view, R.id.checkbox_update_date, "field 'checkboxUpdateDate'", CustomCheckBox.class);
        activityBarcodeSheet.textRepeatAlert = (CustomTextView) C0642c.m3874c(view, R.id.text_repeat_alert, "field 'textRepeatAlert'", CustomTextView.class);
        activityBarcodeSheet.layoutPullToRefresh = (SwipeRefreshLayout) C0642c.m3874c(view, R.id.layout_pull_to_refresh, "field 'layoutPullToRefresh'", SwipeRefreshLayout.class);
        View b7 = C0642c.m3873b(view, R.id.button_add_subsheet, "field 'buttonAddSubsheet' and method 'onViewClicked'");
        activityBarcodeSheet.buttonAddSubsheet = (ImageView) C0642c.m3872a(b7, R.id.button_add_subsheet, "field 'buttonAddSubsheet'", ImageView.class);
        this.f24183h = b7;
        b7.setOnClickListener(new C6931g(this, activityBarcodeSheet));
        activityBarcodeSheet.recyclerSubsheets = (RecyclerView) C0642c.m3874c(view, R.id.recycler_subsheets, "field 'recyclerSubsheets'", RecyclerView.class);
        activityBarcodeSheet.layoutSubsheetsList = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_subsheets_list, "field 'layoutSubsheetsList'", ConstraintLayout.class);
    }

    public void unbind() {
        ActivityBarcodeSheet activityBarcodeSheet = this.f24176a;
        if (activityBarcodeSheet != null) {
            this.f24176a = null;
            activityBarcodeSheet.toolbarBarcodeSheet = null;
            activityBarcodeSheet.recyclerBarcodes = null;
            activityBarcodeSheet.layoutProgress = null;
            activityBarcodeSheet.btnTryAgain = null;
            activityBarcodeSheet.layoutTryAgain = null;
            activityBarcodeSheet.textAddRow = null;
            activityBarcodeSheet.textTryAgain = null;
            activityBarcodeSheet.layoutEmptyScreen = null;
            activityBarcodeSheet.buttonAddRow = null;
            activityBarcodeSheet.editQuantity = null;
            activityBarcodeSheet.editBarcode = null;
            activityBarcodeSheet.buttonAddBarcode = null;
            activityBarcodeSheet.cardAddBarcode = null;
            activityBarcodeSheet.imageCloseCard = null;
            activityBarcodeSheet.layoutAddManually = null;
            activityBarcodeSheet.layoutScan = null;
            activityBarcodeSheet.cardButtons = null;
            activityBarcodeSheet.layoutAddProgress = null;
            activityBarcodeSheet.checkboxUpdateDate = null;
            activityBarcodeSheet.textRepeatAlert = null;
            activityBarcodeSheet.layoutPullToRefresh = null;
            activityBarcodeSheet.buttonAddSubsheet = null;
            activityBarcodeSheet.recyclerSubsheets = null;
            activityBarcodeSheet.layoutSubsheetsList = null;
            this.f24177b.setOnClickListener((View.OnClickListener) null);
            this.f24177b = null;
            this.f24178c.setOnClickListener((View.OnClickListener) null);
            this.f24178c = null;
            this.f24179d.setOnClickListener((View.OnClickListener) null);
            this.f24179d = null;
            this.f24180e.setOnClickListener((View.OnClickListener) null);
            this.f24180e = null;
            this.f24181f.setOnClickListener((View.OnClickListener) null);
            this.f24181f = null;
            this.f24182g.setOnClickListener((View.OnClickListener) null);
            this.f24182g = null;
            this.f24183h.setOnClickListener((View.OnClickListener) null);
            this.f24183h = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
