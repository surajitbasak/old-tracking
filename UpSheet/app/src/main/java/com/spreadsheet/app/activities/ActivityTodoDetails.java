package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.appcompat.app.C0065b;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.material.bottomsheet.C3670a;
import com.google.firebase.database.C4534g;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomEditText;
import com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7089d;
import com.spreadsheet.app.adapters.C7106i;
import com.wdullaer.materialdatetimepicker.date.C7117b;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import p007cn.pedant.SweetAlert.SweetAlertDialog;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p302a.C7566k;
import p153e.p300e.p301a.p302a.C7567l;
import p153e.p300e.p301a.p304c.C7579i;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p305d.C7597h;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivityTodoDetails extends C0067c implements View.OnClickListener, C7598a, C7579i, C7117b.C7121d {

    /* renamed from: A */
    CustomTextSemiBold f24352A;

    /* renamed from: B */
    CustomTextSemiBold f24353B;

    /* renamed from: C */
    CustomTextSemiBold f24354C;

    /* renamed from: D */
    Button f24355D;

    /* renamed from: E */
    C6875d f24356E = C6875d.m33482b();

    /* renamed from: F */
    C7597h f24357F = C7597h.m35641o();
    /* access modifiers changed from: private */

    /* renamed from: G */
    public String f24358G;

    /* renamed from: H */
    private String f24359H = "";

    /* renamed from: I */
    int f24360I = 0;

    /* renamed from: J */
    C7562g f24361J = C7562g.getInstance();
    /* access modifiers changed from: private */

    /* renamed from: K */
    public List<C7566k> f24362K;
    /* access modifiers changed from: private */

    /* renamed from: L */
    public List<C7566k> f24363L;

    /* renamed from: M */
    private int f24364M;

    /* renamed from: N */
    C6879h f24365N;

    /* renamed from: O */
    private ImageView f24366O;

    /* renamed from: P */
    C7089d f24367P;

    /* renamed from: Q */
    private int f24368Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public int f24369R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public boolean f24370S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public boolean f24371T;

    /* renamed from: U */
    C7591e f24372U;

    /* renamed from: V */
    C6872a f24373V;

    /* renamed from: W */
    C7567l f24374W;

    /* renamed from: X */
    String f24375X;

    /* renamed from: Y */
    C7596g f24376Y;

    /* renamed from: Z */
    C7590d f24377Z;
    @BindView(2131296344)
    CustomTextView btnTryAgain;
    @BindView(2131296353)
    ImageView buttonAddTask;
    @BindView(2131296372)
    CardView cardAddTask;
    @BindView(2131296513)
    ImageView imageCloseCard;
    @BindView(2131296542)
    ImageView imageTodoMenu;
    @BindView(2131296562)
    RelativeLayout layoutAddProgress;
    @BindView(2131296608)
    ConstraintLayout layoutTryAgain;
    @BindView(2131296731)
    RecyclerView recylerTask;
    @BindView(2131296732)
    RelativeLayout relativeEmptyScreen;
    @BindView(2131296826)
    CustomTextView textAddTask;
    @BindView(2131296936)
    Toolbar toolbarTodoDetail;

    /* renamed from: u */
    C7106i f24378u;

    /* renamed from: v */
    C7560e f24379v;

    /* renamed from: w */
    List<String> f24380w = new ArrayList();

    /* renamed from: x */
    C3670a f24381x;

    /* renamed from: y */
    CustomEditText f24382y;

    /* renamed from: z */
    CustomTextView f24383z;

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$a */
    class C6983a implements SweetAlertDialog.OnSweetClickListener {
        C6983a() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityTodoDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$b */
    class C6984b implements SweetAlertDialog.OnSweetClickListener {
        C6984b() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
            ActivityTodoDetails.this.f24356E.mo24213d("");
            ActivityTodoDetails.this.m33690I0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$c */
    class C6985c implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ C7566k f24386c;

        C6985c(C7566k kVar) {
            this.f24386c = kVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivityTodoDetails.this.f24362K.remove(this.f24386c);
            boolean unused = ActivityTodoDetails.this.f24371T = true;
            List unused2 = ActivityTodoDetails.this.f24363L = new ArrayList(C6873b.m33469g(ActivityTodoDetails.this.f24362K));
            ActivityTodoDetails.this.m33698S0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$d */
    class C6986d implements DialogInterface.OnClickListener {
        C6986d(ActivityTodoDetails activityTodoDetails) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$e */
    class C6987e implements DialogInterface.OnClickListener {
        C6987e() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivityTodoDetails.this.f24356E.mo24213d("");
            if (ActivityTodoDetails.this.f24363L.size() < ActivityTodoDetails.this.f24369R) {
                for (int size = ActivityTodoDetails.this.f24363L.size(); size < ActivityTodoDetails.this.f24369R; size++) {
                    C7566k kVar = new C7566k();
                    kVar.setTaskId("");
                    kVar.setTaskDate(0);
                    kVar.setDone(false);
                    kVar.setTaskName("");
                    ActivityTodoDetails.this.f24363L.add(kVar);
                }
            }
            ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
            C7597h hVar = activityTodoDetails.f24357F;
            String x0 = activityTodoDetails.f24358G;
            String F0 = ActivityTodoDetails.this.m33691J0();
            ActivityTodoDetails activityTodoDetails2 = ActivityTodoDetails.this;
            hVar.mo25688E(x0, F0, activityTodoDetails2.f24357F.mo25689F(activityTodoDetails2.f24360I, activityTodoDetails2.f24363L.size(), ActivityTodoDetails.this.f24363L));
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$f */
    class C6988f implements DialogInterface.OnClickListener {
        C6988f() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivityTodoDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$g */
    class C6989g implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ EditText f24390c;

        /* renamed from: d */
        final /* synthetic */ Dialog f24391d;

        C6989g(EditText editText, Dialog dialog) {
            this.f24390c = editText;
            this.f24391d = dialog;
        }

        public void onClick(View view) {
            String obj = this.f24390c.getText().toString();
            if (!obj.equals("")) {
                this.f24391d.dismiss();
                ActivityTodoDetails.this.f24356E.mo24213d("");
                ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
                activityTodoDetails.f24361J.updatedSpreadsheetTitle = obj;
                activityTodoDetails.f24376Y.mo25664R(activityTodoDetails.f24358G, ActivityTodoDetails.this.f24376Y.mo25660A(obj));
                return;
            }
            Toast.makeText(ActivityTodoDetails.this, "Spreadsheet name cannot be empty!", 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$h */
    class C6990h implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24393c;

        C6990h(ActivityTodoDetails activityTodoDetails, Dialog dialog) {
            this.f24393c = dialog;
        }

        public void onClick(View view) {
            this.f24393c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$i */
    class C6991i implements View.OnClickListener {
        C6991i() {
        }

        public void onClick(View view) {
            ActivityTodoDetails.this.f24382y.setText("");
            ActivityTodoDetails.this.f24381x.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$j */
    class C6992j implements View.OnClickListener {
        C6992j() {
        }

        public void onClick(View view) {
            boolean unused = ActivityTodoDetails.this.f24370S = true;
            if (ActivityTodoDetails.this.f24371T) {
                ActivityTodoDetails.this.mo24367V0();
            } else {
                ActivityTodoDetails.this.finish();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$k */
    class C6993k implements TextWatcher {
        C6993k() {
        }

        public void afterTextChanged(Editable editable) {
            if (ActivityTodoDetails.this.f24382y.getLineCount() > 2) {
                ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
                activityTodoDetails.f24382y.setText(activityTodoDetails.f24375X);
                CustomEditText customEditText = ActivityTodoDetails.this.f24382y;
                customEditText.setSelection(customEditText.getText().length());
                return;
            }
            ActivityTodoDetails activityTodoDetails2 = ActivityTodoDetails.this;
            activityTodoDetails2.f24375X = activityTodoDetails2.f24382y.getText().toString();
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$l */
    class C6994l implements C7089d.C7090a {

        /* renamed from: a */
        final /* synthetic */ List f24397a;

        C6994l(ActivityTodoDetails activityTodoDetails, List list) {
            this.f24397a = list;
        }

        /* renamed from: a */
        public boolean mo24384a(int i) {
            return i == 0 || !C6873b.m33472j(((C7566k) this.f24397a.get(i)).getTaskDate()).equalsIgnoreCase(C6873b.m33472j(((C7566k) this.f24397a.get(i - 1)).getTaskDate()));
        }

        /* renamed from: b */
        public CharSequence mo24385b(int i) {
            return C6873b.m33472j(((C7566k) this.f24397a.get(i)).getTaskDate());
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$m */
    class C6995m implements PopupMenu.OnMenuItemClickListener {

        /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$m$a */
        class C6996a implements DialogInterface.OnClickListener {
            C6996a() {
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                if (ActivityTodoDetails.this.f24373V.mo24207d()) {
                    ActivityTodoDetails.this.f24356E.mo24213d("");
                    ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
                    activityTodoDetails.f24376Y.mo25673k(activityTodoDetails.f24379v.getSheetId());
                    return;
                }
                ActivityTodoDetails activityTodoDetails2 = ActivityTodoDetails.this;
                Toast.makeText(activityTodoDetails2, activityTodoDetails2.getResources().getString(R.string.internet_check), 0).show();
            }
        }

        /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$m$b */
        class C6997b implements DialogInterface.OnClickListener {
            C6997b(C6995m mVar) {
            }

            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }

        C6995m() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            int itemId = menuItem.getItemId();
            if (itemId == R.id.menu_delete_spreadsheet) {
                C0065b.C0066a aVar = new C0065b.C0066a(ActivityTodoDetails.this);
                aVar.mo236g(ActivityTodoDetails.this.getResources().getString(R.string.do_you_want_to_delete) + " '" + ActivityTodoDetails.this.f24379v.getSheetName() + "' " + ActivityTodoDetails.this.getResources().getString(R.string.file_permanently));
                aVar.mo240k(ActivityTodoDetails.this.getResources().getString(R.string.yes), new C6996a());
                aVar.mo237h(ActivityTodoDetails.this.getResources().getString(R.string.no), new C6997b(this));
                aVar.mo243n();
                return true;
            } else if (itemId != R.id.menu_rename_spreadsheet) {
                return true;
            } else {
                ActivityTodoDetails.this.mo24366R0();
                return true;
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$n */
    class C6998n implements Runnable {
        C6998n() {
        }

        public void run() {
            ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
            activityTodoDetails.cardAddTask.startAnimation(AnimationUtils.loadAnimation(activityTodoDetails, R.anim.translate_in));
            ActivityTodoDetails.this.cardAddTask.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$o */
    class C6999o implements Runnable {
        C6999o() {
        }

        public void run() {
            ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
            activityTodoDetails.buttonAddTask.startAnimation(AnimationUtils.loadAnimation(activityTodoDetails, R.anim.scale_in));
            ActivityTodoDetails.this.buttonAddTask.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$p */
    class C7000p implements SweetAlertDialog.OnSweetClickListener {
        C7000p() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityTodoDetails.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails$q */
    class C7001q implements SweetAlertDialog.OnSweetClickListener {
        C7001q() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityTodoDetails.this.f24356E.mo24213d("");
            sweetAlertDialog.dismiss();
            ActivityTodoDetails activityTodoDetails = ActivityTodoDetails.this;
            activityTodoDetails.f24372U.mo25639d(activityTodoDetails.f24358G);
        }
    }

    public ActivityTodoDetails() {
        new ArrayList();
        this.f24362K = new ArrayList();
        this.f24363L = new ArrayList();
        this.f24364M = 0;
        this.f24365N = C6879h.m33499c();
        this.f24368Q = 0;
        this.f24369R = 0;
        this.f24370S = false;
        this.f24371T = false;
        this.f24372U = C7591e.m35564f();
        this.f24373V = C6872a.m33458a();
        this.f24374W = C7567l.getInstance();
        this.f24375X = "";
        this.f24376Y = C7596g.m35606z();
        this.f24377Z = C7590d.m35558a();
    }

    /* renamed from: G0 */
    private void m33688G0(String str) {
        this.f24380w.clear();
        long longValue = ((Long) this.f24383z.getTag()).longValue();
        C7566k kVar = new C7566k();
        kVar.setTaskName(str);
        kVar.setTaskDate(longValue);
        kVar.setTaskId(UUID.randomUUID().toString());
        this.f24362K.add(kVar);
        this.f24371T = true;
        ArrayList arrayList = new ArrayList(C6873b.m33469g(this.f24362K));
        this.f24363L = arrayList;
        if (arrayList.size() > 0) {
            m33698S0();
            this.textAddTask.setVisibility(8);
            this.buttonAddTask.setVisibility(0);
            return;
        }
        this.textAddTask.setVisibility(0);
        this.buttonAddTask.setVisibility(8);
    }

    /* renamed from: H0 */
    private void m33689H0() {
        C7597h hVar = this.f24357F;
        hVar.mo25692e(this.f24358G, hVar.mo25699l(0));
    }

    /* access modifiers changed from: private */
    /* renamed from: I0 */
    public void m33690I0() {
        ConstraintLayout constraintLayout;
        int i = 8;
        if (this.f24373V.mo24207d()) {
            this.f24356E.mo24213d("");
            this.f24357F.mo25696i(this.f24358G);
            constraintLayout = this.layoutTryAgain;
        } else {
            this.relativeEmptyScreen.setVisibility(8);
            this.buttonAddTask.setVisibility(8);
            constraintLayout = this.layoutTryAgain;
            i = 0;
        }
        constraintLayout.setVisibility(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: J0 */
    public String m33691J0() {
        return this.f24359H + "!A" + 2 + ":C" + (this.f24363L.size() + 2);
    }

    /* renamed from: K0 */
    private String m33692K0() {
        return this.f24359H + "!A1:C";
    }

    /* renamed from: L0 */
    private String m33693L0(int i) {
        int i2 = i + 2;
        this.f24379v.getColCount();
        return this.f24359H + "!B" + i2 + ":C" + i2;
    }

    /* renamed from: M0 */
    private C7089d.C7090a m33694M0(List<C7566k> list) {
        return new C6994l(this, list);
    }

    /* renamed from: N0 */
    private void m33695N0() {
        this.f24357F.mo25697j(this.f24358G, m33692K0());
    }

    /* renamed from: O0 */
    private int m33696O0(C7566k kVar) {
        for (int i = 0; i < this.f24362K.size(); i++) {
            if (this.f24362K.get(i).getTaskId().equals(kVar.getTaskId())) {
                return i;
            }
        }
        return 0;
    }

    /* renamed from: P0 */
    private void m33697P0() {
        Calendar.getInstance().getTimeInMillis();
        this.cardAddTask.setVisibility(8);
        C7560e eVar = (C7560e) getIntent().getSerializableExtra("spreadsheet");
        this.f24379v = eVar;
        this.f24358G = eVar.getSheetId();
        this.toolbarTodoDetail.setTitle((CharSequence) this.f24379v.sheetName);
        this.toolbarTodoDetail.setTitleTextColor(getResources().getColor(R.color.black));
        this.f24354C = (CustomTextSemiBold) this.toolbarTodoDetail.findViewById(R.id.text_save_list);
        CustomTextSemiBold customTextSemiBold = (CustomTextSemiBold) this.toolbarTodoDetail.findViewById(R.id.text_toolbar_title);
        this.f24353B = customTextSemiBold;
        customTextSemiBold.setText(this.f24379v.sheetName);
        mo276r0(this.toolbarTodoDetail);
        C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.f24373V.mo24206c(this);
        this.f24372U.mo25642h(this, this);
        this.f24376Y.mo25662C(this, this);
        this.f24356E.mo24212c(this);
        this.f24377Z.mo25637b(this);
        this.f24357F.mo25704r(this, this);
        this.f24365N.mo24225f(this);
        this.f24365N.mo24222b(C6873b.f24024o);
        this.recylerTask.setLayoutManager(new LinearLayoutManager(this, 1, false));
        C3670a aVar = new C3670a(this);
        this.f24381x = aVar;
        aVar.requestWindowFeature(1);
        this.f24381x.setContentView((int) R.layout.dialog_add_task);
        this.f24366O = (ImageView) this.f24381x.findViewById(R.id.image_close);
        this.f24382y = (CustomEditText) this.f24381x.findViewById(R.id.edit_task);
        this.f24383z = (CustomTextView) this.f24381x.findViewById(R.id.text_task_date);
        this.f24352A = (CustomTextSemiBold) this.f24381x.findViewById(R.id.text_save_task);
        this.f24366O.setOnClickListener(new C6991i());
        CustomTextSemiBold customTextSemiBold2 = (CustomTextSemiBold) this.f24381x.findViewById(R.id.text_todo);
        Calendar instance = Calendar.getInstance();
        this.f24383z.setTag(Long.valueOf(instance.getTimeInMillis()));
        this.f24383z.setText(instance.get(5) + " " + getResources().getStringArray(R.array.months)[instance.get(2)] + " " + instance.get(1));
        this.f24383z.setOnClickListener(this);
        this.f24352A.setOnClickListener(this);
        this.f24354C.setOnClickListener(this);
        this.toolbarTodoDetail.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarTodoDetail.setNavigationOnClickListener(new C6992j());
        this.f24382y.setHorizontallyScrolling(false);
        this.f24382y.setImeOptions(6);
        this.f24382y.setRawInputType(131072);
        this.f24382y.addTextChangedListener(new C6993k());
        m33690I0();
    }

    /* access modifiers changed from: private */
    /* renamed from: S0 */
    public void m33698S0() {
        this.f24378u = new C7106i(this, this.f24363L, this);
        this.recylerTask.removeItemDecoration(this.f24367P);
        C7089d dVar = new C7089d(this, getResources().getDimensionPixelSize(R.dimen.dp_40), false, m33694M0(this.f24363L));
        this.f24367P = dVar;
        this.recylerTask.addItemDecoration(dVar);
        this.recylerTask.setAdapter(this.f24378u);
        if (!this.f24365N.mo24221a(C6873b.f24007e)) {
            mo24365Q0();
        }
    }

    /* renamed from: T0 */
    private void m33699T0(boolean z) {
        Handler handler;
        Runnable runnable;
        if (z) {
            this.buttonAddTask.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_out));
            this.buttonAddTask.setVisibility(8);
            handler = new Handler();
            runnable = new C6998n();
        } else {
            this.cardAddTask.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_out));
            this.cardAddTask.setVisibility(8);
            handler = new Handler();
            runnable = new C6999o();
        }
        handler.postDelayed(runnable, 250);
    }

    /* renamed from: U0 */
    private void m33700U0() {
        this.f24356E.mo24211a();
        SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("Something Went Wrong!").setConfirmButton("Try Again", (SweetAlertDialog.OnSweetClickListener) new C6984b()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C6983a());
        cancelButton.setCancelable(false);
        cancelButton.setCanceledOnTouchOutside(false);
        cancelButton.show();
    }

    /* renamed from: X0 */
    private void m33701X0(String str) {
        long longValue = ((Long) this.f24383z.getTag()).longValue();
        this.f24362K.get(this.f24368Q).setTaskName(str);
        this.f24362K.get(this.f24368Q).setTaskDate(longValue);
        this.f24371T = true;
        this.f24363L = new ArrayList(C6873b.m33469g(this.f24362K));
        m33698S0();
    }

    /* renamed from: A */
    public void mo24363A(C7566k kVar) {
        m33696O0(kVar);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete this task?");
        builder.setPositiveButton("Yes", new C6985c(kVar));
        builder.setNegativeButton("No", new C6986d(this));
        builder.show();
    }

    /* renamed from: H */
    public void mo24237H(String str, String str2) {
        str2.hashCode();
        char c = 65535;
        switch (str2.hashCode()) {
            case -2022976223:
                if (str2.equals("deleteSpreadsheet")) {
                    c = 0;
                    break;
                }
                break;
            case -1809886158:
                if (str2.equals("getAllSpreadsheets")) {
                    c = 1;
                    break;
                }
                break;
            case -561789226:
                if (str2.equals("Access_Denied")) {
                    c = 2;
                    break;
                }
                break;
            case 281689075:
                if (str2.equals("File_Not_Found")) {
                    c = 3;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.f24377Z.mo25638c("DELETE_SPREADSHEET", "DELETE_SPREADSHEET");
                this.f24372U.mo25642h(this, this);
                this.f24372U.mo25639d(this.f24379v.getSheetId());
                return;
            case 1:
                this.f24356E.mo24211a();
                this.f24361J.isUpdated = true;
                break;
            case 2:
                this.f24361J.shouldRequestAuth = true;
                this.f24356E.mo24211a();
                break;
            case 3:
                this.f24356E.mo24211a();
                SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("File not found!").setContentText("You have deleted this sheet\nfrom Drive!").setConfirmButton("Remove from list", (SweetAlertDialog.OnSweetClickListener) new C7001q()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C7000p());
                cancelButton.setCancelable(false);
                cancelButton.setCanceledOnTouchOutside(false);
                cancelButton.show();
                return;
            default:
                m33700U0();
                return;
        }
        finish();
    }

    /* renamed from: P */
    public void mo24364P(int i, C7566k kVar) {
        Calendar instance = Calendar.getInstance();
        this.f24368Q = m33696O0(kVar);
        instance.setTimeInMillis(kVar.getTaskDate());
        this.f24382y.setText(kVar.getTaskName());
        this.f24383z.setTag(Long.valueOf(instance.getTimeInMillis()));
        this.f24383z.setText(instance.get(5) + " " + getResources().getStringArray(R.array.months)[instance.get(2)] + " " + instance.get(1));
        this.f24352A.setText("Update");
        this.f24381x.show();
    }

    /* renamed from: Q0 */
    public void mo24365Q0() {
    }

    /* renamed from: R0 */
    public void mo24366R0() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_add_new_sheet);
        EditText editText = (EditText) dialog.findViewById(R.id.edit_sheet_name);
        Button button = (Button) dialog.findViewById(R.id.button_add_sheet);
        editText.setText(this.f24379v.getSheetName());
        ((CustomTextView) dialog.findViewById(R.id.text_spreadsheet_title)).setText("Rename Spreadsheet");
        button.setText("Update");
        button.setOnClickListener(new C6989g(editText, dialog));
        ((ImageView) dialog.findViewById(R.id.image_close)).setOnClickListener(new C6990h(this, dialog));
        dialog.show();
    }

    /* renamed from: V0 */
    public void mo24367V0() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to save this list?");
        builder.setPositiveButton("Yes", new C6987e());
        builder.setNegativeButton("No", new C6988f());
        if (!isFinishing()) {
            builder.show();
        }
    }

    /* renamed from: W0 */
    public void mo24368W0() {
        this.f24356E.mo24211a();
        Toast.makeText(this, "Spreadsheet Renamed!", 0).show();
        this.f24372U.mo25644j(this.f24358G, C7562g.KEY_SPREADSHEET_NAME, this.f24361J.updatedSpreadsheetTitle);
        this.f24372U.mo25647m(this.f24358G);
        this.f24353B.setText(this.f24361J.updatedSpreadsheetTitle);
        this.f24361J.isUpdated = true;
    }

    /* renamed from: l */
    public boolean mo24369l(Boolean bool, C7566k kVar) {
        this.f24362K.get(m33696O0(kVar)).setDone(bool.booleanValue());
        this.f24371T = true;
        return false;
    }

    public void onBackPressed() {
        this.f24370S = true;
        if (this.f24371T) {
            mo24367V0();
        } else {
            finish();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00d1, code lost:
        android.widget.Toast.makeText(r6, "Enter task", 0).show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f7, code lost:
        m33688G0(r6.f24382y.getText().toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0104, code lost:
        r6.f24382y.setText("");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(View r7) {
        /*
            r6 = this;
            int r7 = r7.getId()
            java.lang.String r0 = "Enter task"
            java.lang.String r1 = "Add Task"
            r2 = 1
            r3 = 0
            java.lang.String r4 = ""
            switch(r7) {
                case 2131296354: goto L_0x00c1;
                case 2131296887: goto L_0x006a;
                case 2131296888: goto L_0x0032;
                case 2131296903: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            goto L_0x0109
        L_0x0011:
            java.util.Calendar r7 = java.util.Calendar.getInstance()
            int r0 = r7.get(r2)
            r1 = 2
            int r1 = r7.get(r1)
            r2 = 5
            int r7 = r7.get(r2)
            com.wdullaer.materialdatetimepicker.date.b r7 = com.wdullaer.materialdatetimepicker.date.C7117b.m33928y(r6, r0, r1, r7)
            android.app.FragmentManager r0 = r6.getFragmentManager()
            java.lang.String r1 = "date"
            r7.show(r0, r1)
            goto L_0x0109
        L_0x0032:
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = r6.f24382y
            android.text.Editable r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x0044
            goto L_0x00d1
        L_0x0044:
            com.google.android.material.bottomsheet.a r7 = r6.f24381x
            r7.dismiss()
            com.spreadsheet.app.Utils.Fonts.CustomTextSemiBold r7 = r6.f24352A
            java.lang.CharSequence r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            boolean r7 = r7.equals(r1)
            if (r7 == 0) goto L_0x005b
            goto L_0x00f7
        L_0x005b:
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = r6.f24382y
            android.text.Editable r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            r6.m33701X0(r7)
            goto L_0x0104
        L_0x006a:
            r6.f24370S = r3
            com.spreadsheet.app.Utils.d r7 = r6.f24356E
            r7.mo24213d(r4)
            java.util.List<e.e.a.a.k> r7 = r6.f24363L
            int r7 = r7.size()
            int r0 = r6.f24369R
            if (r7 >= r0) goto L_0x00a0
            java.util.List<e.e.a.a.k> r7 = r6.f24363L
            int r7 = r7.size()
        L_0x0081:
            int r0 = r6.f24369R
            if (r7 >= r0) goto L_0x00a0
            e.e.a.a.k r0 = new e.e.a.a.k
            r0.<init>()
            r0.setTaskId(r4)
            r1 = 0
            r0.setTaskDate(r1)
            r0.setDone(r3)
            r0.setTaskName(r4)
            java.util.List<e.e.a.a.k> r1 = r6.f24363L
            r1.add(r0)
            int r7 = r7 + 1
            goto L_0x0081
        L_0x00a0:
            e.e.a.d.h r7 = r6.f24357F
            r7.mo25704r(r6, r6)
            e.e.a.d.h r7 = r6.f24357F
            java.lang.String r0 = r6.f24358G
            java.lang.String r1 = r6.m33691J0()
            e.e.a.d.h r2 = r6.f24357F
            int r3 = r6.f24360I
            java.util.List<e.e.a.a.k> r4 = r6.f24363L
            int r4 = r4.size()
            java.util.List<e.e.a.a.k> r5 = r6.f24363L
            org.json.JSONObject r2 = r2.mo25689F(r3, r4, r5)
            r7.mo25688E(r0, r1, r2)
            goto L_0x0109
        L_0x00c1:
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = r6.f24382y
            android.text.Editable r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00d9
        L_0x00d1:
            android.widget.Toast r7 = android.widget.Toast.makeText(r6, r0, r3)
            r7.show()
            goto L_0x0109
        L_0x00d9:
            com.google.android.material.bottomsheet.a r7 = r6.f24381x
            r7.dismiss()
            android.widget.Button r7 = r6.f24355D
            java.lang.CharSequence r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            boolean r7 = r7.equals(r1)
            if (r7 == 0) goto L_0x00f1
            int r7 = r6.f24364M
            goto L_0x00f4
        L_0x00f1:
            int r7 = r6.f24368Q
            int r7 = r7 - r2
        L_0x00f4:
            r6.m33693L0(r7)
        L_0x00f7:
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = r6.f24382y
            android.text.Editable r7 = r7.getText()
            java.lang.String r7 = r7.toString()
            r6.m33688G0(r7)
        L_0x0104:
            com.spreadsheet.app.Utils.Fonts.CustomEditText r7 = r6.f24382y
            r7.setText(r4)
        L_0x0109:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityTodoDetails.onClick(android.view.View):void");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_todo_details);
        ButterKnife.bind((Activity) this);
        m33697P0();
    }

    @OnClick({2131296826, 2131296353, 2131296344, 2131296513, 2131296367, 2131296542})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_try_again:
                m33690I0();
                return;
            case R.id.button_add_task:
                this.f24382y.setHint("Task");
                this.f24352A.setText("Add Task");
                Date time = Calendar.getInstance().getTime();
                String str = getResources().getStringArray(R.array.months)[time.getMonth()];
                this.f24383z.setText(time.getDate() + " " + str + " " + Calendar.getInstance().get(1));
                this.f24383z.setTag(Long.valueOf(Calendar.getInstance().getTimeInMillis()));
                break;
            case R.id.button_submit_task:
                if (this.f24382y.getText().toString().equals("")) {
                    Toast.makeText(this, "Enter task", 0).show();
                    return;
                }
                this.layoutAddProgress.setVisibility(0);
                m33693L0(this.f24355D.getText().toString().equals("Add Task") ? this.f24364M : this.f24368Q - 1);
                this.f24381x.dismiss();
                m33688G0(this.f24382y.getText().toString());
                this.f24382y.setText("");
                return;
            case R.id.image_close_card:
                this.f24382y.setText("");
                m33699T0(false);
                return;
            case R.id.image_todo_menu:
                showDeleteSpreadsheetMenu(this.imageTodoMenu);
                return;
            case R.id.text_add_task:
                Date time2 = Calendar.getInstance().getTime();
                String str2 = getResources().getStringArray(R.array.months)[time2.getMonth()];
                this.f24383z.setText(time2.getDate() + " " + str2 + " " + Calendar.getInstance().get(1));
                this.f24382y.setHint("Task");
                this.f24352A.setText("Add Task");
                break;
            default:
                return;
        }
        this.f24381x.show();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0135, code lost:
        m33698S0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0156, code lost:
        m33695N0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x018e, code lost:
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        return;
     */
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24244s(String r5, String r6) {
        /*
            r4 = this;
            r6.hashCode()
            int r5 = r6.hashCode()
            r0 = 8
            r1 = 1
            r2 = 0
            r3 = -1
            switch(r5) {
                case -2022976223: goto L_0x0088;
                case -1974023935: goto L_0x007d;
                case -1809886158: goto L_0x0072;
                case -1788871435: goto L_0x0067;
                case -1422526087: goto L_0x005c;
                case -940738021: goto L_0x0051;
                case -452548289: goto L_0x0046;
                case -358721489: goto L_0x003b;
                case 124692112: goto L_0x002d;
                case 323754608: goto L_0x001f;
                case 1269283904: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            goto L_0x0092
        L_0x0011:
            java.lang.String r5 = "getAllTaskWithData"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x001b
            goto L_0x0092
        L_0x001b:
            r3 = 10
            goto L_0x0092
        L_0x001f:
            java.lang.String r5 = "formatRows"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0029
            goto L_0x0092
        L_0x0029:
            r3 = 9
            goto L_0x0092
        L_0x002d:
            java.lang.String r5 = "getAllTask"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0037
            goto L_0x0092
        L_0x0037:
            r3 = 8
            goto L_0x0092
        L_0x003b:
            java.lang.String r5 = "deleteRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0044
            goto L_0x0092
        L_0x0044:
            r3 = 7
            goto L_0x0092
        L_0x0046:
            java.lang.String r5 = "getAllSheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x004f
            goto L_0x0092
        L_0x004f:
            r3 = 6
            goto L_0x0092
        L_0x0051:
            java.lang.String r5 = "updateStatus"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x005a
            goto L_0x0092
        L_0x005a:
            r3 = 5
            goto L_0x0092
        L_0x005c:
            java.lang.String r5 = "addRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0065
            goto L_0x0092
        L_0x0065:
            r3 = 4
            goto L_0x0092
        L_0x0067:
            java.lang.String r5 = "updateSpreadSheetTitle"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0070
            goto L_0x0092
        L_0x0070:
            r3 = 3
            goto L_0x0092
        L_0x0072:
            java.lang.String r5 = "getAllSpreadsheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x007b
            goto L_0x0092
        L_0x007b:
            r3 = 2
            goto L_0x0092
        L_0x007d:
            java.lang.String r5 = "update_todo_list"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0086
            goto L_0x0092
        L_0x0086:
            r3 = 1
            goto L_0x0092
        L_0x0088:
            java.lang.String r5 = "deleteSpreadsheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0091
            goto L_0x0092
        L_0x0091:
            r3 = 0
        L_0x0092:
            java.lang.String r5 = "allTaskCount"
            switch(r3) {
                case 0: goto L_0x01bc;
                case 1: goto L_0x0192;
                case 2: goto L_0x0183;
                case 3: goto L_0x0176;
                case 4: goto L_0x0167;
                case 5: goto L_0x015b;
                case 6: goto L_0x013a;
                case 7: goto L_0x015b;
                case 8: goto L_0x00f3;
                case 9: goto L_0x00e2;
                case 10: goto L_0x0099;
                default: goto L_0x0097;
            }
        L_0x0097:
            goto L_0x01d3
        L_0x0099:
            com.spreadsheet.app.Utils.d r6 = r4.f24356E
            r6.mo24211a()
            e.e.a.a.l r6 = r4.f24374W
            java.util.List r6 = r6.getAllTaskList()
            r4.f24362K = r6
            int r6 = r6.size()
            r4.f24364M = r6
            com.spreadsheet.app.Utils.h r1 = r4.f24365N
            r1.mo24228i(r5, r6)
            java.util.List<e.e.a.a.k> r5 = r4.f24362K
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x00c4
            android.widget.RelativeLayout r5 = r4.relativeEmptyScreen
            r5.setVisibility(r2)
            android.widget.ImageView r5 = r4.buttonAddTask
            r5.setVisibility(r0)
            goto L_0x00ce
        L_0x00c4:
            android.widget.RelativeLayout r5 = r4.relativeEmptyScreen
            r5.setVisibility(r0)
            android.widget.ImageView r5 = r4.buttonAddTask
            r5.setVisibility(r2)
        L_0x00ce:
            java.util.ArrayList r5 = new java.util.ArrayList
            java.util.List<e.e.a.a.k> r6 = r4.f24362K
            java.util.List r6 = com.spreadsheet.app.Utils.C6873b.m33469g(r6)
            r5.<init>(r6)
            r4.f24363L = r5
            int r5 = r5.size()
            r4.f24369R = r5
            goto L_0x0135
        L_0x00e2:
            androidx.cardview.widget.CardView r5 = r4.cardAddTask
            int r5 = r5.getVisibility()
            if (r5 != 0) goto L_0x0156
            r4.m33699T0(r2)
            android.widget.RelativeLayout r5 = r4.layoutAddProgress
            r5.setVisibility(r0)
            goto L_0x0156
        L_0x00f3:
            com.spreadsheet.app.Utils.d r6 = r4.f24356E
            r6.mo24211a()
            e.e.a.a.l r6 = r4.f24374W
            java.util.List r6 = r6.getAllTaskList()
            r4.f24362K = r6
            int r6 = r6.size()
            r4.f24364M = r6
            com.spreadsheet.app.Utils.h r1 = r4.f24365N
            r1.mo24228i(r5, r6)
            java.util.List<e.e.a.a.k> r5 = r4.f24362K
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x011e
            android.widget.RelativeLayout r5 = r4.relativeEmptyScreen
            r5.setVisibility(r2)
            android.widget.ImageView r5 = r4.buttonAddTask
            r5.setVisibility(r0)
            goto L_0x0128
        L_0x011e:
            android.widget.RelativeLayout r5 = r4.relativeEmptyScreen
            r5.setVisibility(r0)
            android.widget.ImageView r5 = r4.buttonAddTask
            r5.setVisibility(r2)
        L_0x0128:
            java.util.ArrayList r5 = new java.util.ArrayList
            java.util.List<e.e.a.a.k> r6 = r4.f24362K
            java.util.List r6 = com.spreadsheet.app.Utils.C6873b.m33469g(r6)
            r5.<init>(r6)
            r4.f24363L = r5
        L_0x0135:
            r4.m33698S0()
            goto L_0x01d3
        L_0x013a:
            e.e.a.a.g r5 = r4.f24361J
            java.util.List<java.lang.String> r5 = r5.sheetTitleList
            java.lang.Object r5 = r5.get(r2)
            java.lang.String r5 = (java.lang.String) r5
            r4.f24359H = r5
            e.e.a.a.g r5 = r4.f24361J
            java.util.List<java.lang.Integer> r5 = r5.sheetIdList
            java.lang.Object r5 = r5.get(r2)
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r5 = r5.intValue()
            r4.f24360I = r5
        L_0x0156:
            r4.m33695N0()
            goto L_0x01d3
        L_0x015b:
            e.e.a.a.g r5 = r4.f24361J
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24372U
            java.lang.String r6 = r4.f24358G
            r5.mo25647m(r6)
            goto L_0x0156
        L_0x0167:
            e.e.a.a.g r5 = r4.f24361J
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24372U
            java.lang.String r6 = r4.f24358G
            r5.mo25647m(r6)
            r4.m33689H0()
            goto L_0x01d3
        L_0x0176:
            e.e.a.a.e r5 = r4.f24379v
            e.e.a.a.g r6 = r4.f24361J
            java.lang.String r6 = r6.updatedSpreadsheetTitle
            r5.setSheetName(r6)
            r4.mo24368W0()
            goto L_0x01d3
        L_0x0183:
            com.spreadsheet.app.Utils.d r5 = r4.f24356E
            java.lang.String r6 = ""
            r5.mo24213d(r6)
            e.e.a.a.g r5 = r4.f24361J
            r5.isUpdated = r1
        L_0x018e:
            r4.finish()
            goto L_0x01d3
        L_0x0192:
            com.spreadsheet.app.Utils.d r5 = r4.f24356E
            r5.mo24211a()
            java.util.List<e.e.a.a.k> r5 = r4.f24363L
            int r5 = r5.size()
            r4.f24369R = r5
            e.e.a.a.g r5 = r4.f24361J
            r5.isUpdated = r1
            r4.f24371T = r2
            e.e.a.d.e r5 = r4.f24372U
            java.lang.String r6 = r4.f24358G
            r5.mo25647m(r6)
            java.lang.String r5 = "Todo list updated."
            android.widget.Toast r5 = android.widget.Toast.makeText(r4, r5, r2)
            r5.show()
            boolean r5 = r4.f24370S
            if (r5 == 0) goto L_0x01d3
            r4.f24370S = r2
            goto L_0x018e
        L_0x01bc:
            e.e.a.d.d r5 = r4.f24377Z
            java.lang.String r6 = "DELETE_SPREADSHEET"
            r5.mo25638c(r6, r6)
            e.e.a.d.e r5 = r4.f24372U
            r5.mo25642h(r4, r4)
            e.e.a.d.e r5 = r4.f24372U
            e.e.a.a.e r6 = r4.f24379v
            java.lang.String r6 = r6.getSheetId()
            r5.mo25639d(r6)
        L_0x01d3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityTodoDetails.mo24244s(java.lang.String, java.lang.String):void");
    }

    public void showDeleteSpreadsheetMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_delete, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new C6995m());
        popupMenu.show();
    }

    /* renamed from: x */
    public void mo24245x(C7117b bVar, int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(2, i2);
        instance.set(1, i);
        instance.set(5, i3);
        this.f24383z.setTag(Long.valueOf(instance.getTimeInMillis()));
        this.f24383z.setText(i3 + " " + getResources().getStringArray(R.array.months)[i2] + " " + i);
    }
}
