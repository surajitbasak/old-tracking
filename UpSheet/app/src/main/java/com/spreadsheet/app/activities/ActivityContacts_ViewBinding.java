package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class ActivityContacts_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityContacts f24226a;

    public ActivityContacts_ViewBinding(ActivityContacts activityContacts, View view) {
        this.f24226a = activityContacts;
        activityContacts.toolbarContacts = (Toolbar) C0642c.m3874c(view, R.id.toolbar_contacts, "field 'toolbarContacts'", Toolbar.class);
        activityContacts.recyclerSheetRows = (RecyclerView) C0642c.m3874c(view, R.id.recycler_sheet_rows, "field 'recyclerSheetRows'", RecyclerView.class);
        activityContacts.layoutSync = (LinearLayout) C0642c.m3874c(view, R.id.layout_sync, "field 'layoutSync'", LinearLayout.class);
        activityContacts.textSync = (CustomTextView) C0642c.m3874c(view, R.id.text_sync, "field 'textSync'", CustomTextView.class);
        activityContacts.btnTryAgain = (CustomTextView) C0642c.m3874c(view, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        activityContacts.layoutTryAgain = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", ConstraintLayout.class);
    }

    public void unbind() {
        ActivityContacts activityContacts = this.f24226a;
        if (activityContacts != null) {
            this.f24226a = null;
            activityContacts.toolbarContacts = null;
            activityContacts.recyclerSheetRows = null;
            activityContacts.layoutSync = null;
            activityContacts.textSync = null;
            activityContacts.btnTryAgain = null;
            activityContacts.layoutTryAgain = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
