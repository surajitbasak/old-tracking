package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.C0065b;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.firebase.database.C4529d;
import com.google.firebase.database.C4534g;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomButton;
import com.spreadsheet.app.Utils.Fonts.CustomCheckBox;
import com.spreadsheet.app.Utils.Fonts.CustomEditText;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7080b;
import com.spreadsheet.app.adapters.SubsheetsAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import p007cn.pedant.SweetAlert.SweetAlertDialog;
import p153e.p174d.p267e.p268a0.p269a.C7318a;
import p153e.p174d.p267e.p268a0.p269a.C7319b;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p302a.C7565j;
import p153e.p300e.p301a.p304c.C7572b;
import p153e.p300e.p301a.p304c.C7578h;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivityBarcodeSheet extends C0067c implements C7598a, C7572b, View.OnClickListener, C7578h {

    /* renamed from: A */
    List<C7565j> f24108A = new ArrayList();

    /* renamed from: B */
    List<String> f24109B = new ArrayList();

    /* renamed from: C */
    String f24110C = "";

    /* renamed from: D */
    String f24111D = "";

    /* renamed from: E */
    int f24112E = 0;

    /* renamed from: F */
    int f24113F;

    /* renamed from: G */
    int f24114G = 0;

    /* renamed from: H */
    Dialog f24115H;

    /* renamed from: I */
    CustomTextView f24116I;

    /* renamed from: J */
    CustomTextView f24117J;

    /* renamed from: K */
    EditText f24118K;

    /* renamed from: L */
    Button f24119L;

    /* renamed from: M */
    ImageView f24120M;

    /* renamed from: N */
    C6872a f24121N = C6872a.m33458a();

    /* renamed from: O */
    C6875d f24122O = C6875d.m33482b();

    /* renamed from: P */
    Boolean f24123P = Boolean.TRUE;

    /* renamed from: Q */
    Boolean f24124Q;

    /* renamed from: R */
    C4529d f24125R;

    /* renamed from: S */
    String f24126S;

    /* renamed from: T */
    Animation f24127T;

    /* renamed from: U */
    Animation f24128U;

    /* renamed from: V */
    Animation f24129V;

    /* renamed from: W */
    List<HashMap<String, String>> f24130W;

    /* renamed from: X */
    List<HashMap<String, String>> f24131X;

    /* renamed from: Y */
    HashMap<String, String> f24132Y;

    /* renamed from: Z */
    Boolean f24133Z;

    /* renamed from: a0 */
    int f24134a0;

    /* renamed from: b0 */
    int f24135b0;
    @BindView(2131296344)
    CustomTextView btnTryAgain;
    @BindView(2131296347)
    CustomButton buttonAddBarcode;
    @BindView(2131296350)
    ImageView buttonAddRow;
    @BindView(2131296352)
    ImageView buttonAddSubsheet;

    /* renamed from: c0 */
    String f24136c0;
    @BindView(2131296371)
    CardView cardAddBarcode;
    @BindView(2131296375)
    CardView cardButtons;
    @BindView(2131296397)
    CustomCheckBox checkboxUpdateDate;

    /* renamed from: d0 */
    C7080b f24137d0;

    /* renamed from: e0 */
    C7591e f24138e0;
    @BindView(2131296452)
    CustomEditText editBarcode;
    @BindView(2131296459)
    CustomEditText editQuantity;

    /* renamed from: f0 */
    SubsheetsAdapter f24139f0;

    /* renamed from: g0 */
    C7590d f24140g0;

    /* renamed from: h0 */
    TextView f24141h0;
    @BindView(2131296513)
    ImageView imageCloseCard;
    @BindView(2131296561)
    LinearLayout layoutAddManually;
    @BindView(2131296562)
    RelativeLayout layoutAddProgress;
    @BindView(2131296570)
    LinearLayout layoutEmptyScreen;
    @BindView(2131296591)
    RelativeLayout layoutProgress;
    @BindView(2131296592)
    SwipeRefreshLayout layoutPullToRefresh;
    @BindView(2131296597)
    LinearLayout layoutScan;
    @BindView(2131296604)
    ConstraintLayout layoutSubsheetsList;
    @BindView(2131296608)
    ConstraintLayout layoutTryAgain;
    @BindView(2131296726)
    RecyclerView recyclerBarcodes;
    @BindView(2131296729)
    RecyclerView recyclerSubsheets;
    @BindView(2131296825)
    CustomTextView textAddRow;
    @BindView(2131296885)
    CustomTextView textRepeatAlert;
    @BindView(2131296912)
    CustomTextView textTryAgain;
    @BindView(2131296927)
    Toolbar toolbarBarcodeSheet;

    /* renamed from: u */
    C7596g f24142u = C7596g.m35606z();

    /* renamed from: v */
    C6879h f24143v = C6879h.m33499c();

    /* renamed from: w */
    C7562g f24144w = C7562g.getInstance();

    /* renamed from: x */
    C7560e f24145x;

    /* renamed from: y */
    List<String> f24146y = new ArrayList();

    /* renamed from: z */
    List<Integer> f24147z = new ArrayList();

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$a */
    class C6897a implements Runnable {
        C6897a() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardButtons.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardButtons.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$a0 */
    class C6898a0 implements SweetAlertDialog.OnSweetClickListener {
        C6898a0() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
            ActivityBarcodeSheet.this.f24122O.mo24213d("");
            ActivityBarcodeSheet.this.m33543C0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$b */
    class C6899b implements Runnable {
        C6899b() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardAddBarcode.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardAddBarcode.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$b0 */
    class C6900b0 implements Runnable {
        C6900b0() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardAddBarcode.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardAddBarcode.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$c */
    class C6901c implements Runnable {
        C6901c() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardAddBarcode.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardAddBarcode.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$d */
    class C6902d implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24153c;

        C6902d(ActivityBarcodeSheet activityBarcodeSheet, Dialog dialog) {
            this.f24153c = dialog;
        }

        public void onClick(View view) {
            this.f24153c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$e */
    class C6903e implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24154c;

        C6903e(Dialog dialog) {
            this.f24154c = dialog;
        }

        public void onClick(View view) {
            this.f24154c.dismiss();
            C6873b.f24021l = true;
            ActivityBarcodeSheet.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$f */
    class C6904f implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24156c;

        C6904f(ActivityBarcodeSheet activityBarcodeSheet, Dialog dialog) {
            this.f24156c = dialog;
        }

        public void onClick(View view) {
            this.f24156c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$g */
    class C6905g implements Runnable {
        C6905g() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardAddBarcode.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardAddBarcode.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$h */
    class C6906h implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24158c;

        C6906h(int i) {
            this.f24158c = i;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ActivityBarcodeSheet.this.f24121N.mo24207d()) {
                ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
                activityBarcodeSheet.f24134a0 = this.f24158c;
                activityBarcodeSheet.f24122O.mo24213d("");
                ActivityBarcodeSheet activityBarcodeSheet2 = ActivityBarcodeSheet.this;
                C7596g gVar = activityBarcodeSheet2.f24142u;
                gVar.mo25672j(activityBarcodeSheet2.f24110C, gVar.mo25681s(activityBarcodeSheet2.f24112E, this.f24158c + 1));
                return;
            }
            ActivityBarcodeSheet activityBarcodeSheet3 = ActivityBarcodeSheet.this;
            Toast.makeText(activityBarcodeSheet3, activityBarcodeSheet3.getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$i */
    class C6907i implements DialogInterface.OnClickListener {
        C6907i(ActivityBarcodeSheet activityBarcodeSheet) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$j */
    class C6908j implements Runnable {
        C6908j() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardAddBarcode.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardAddBarcode.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$k */
    class C6909k implements View.OnClickListener {
        C6909k() {
        }

        public void onClick(View view) {
            ActivityBarcodeSheet.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$l */
    class C6910l implements SearchView.C0183m {
        C6910l() {
        }

        /* renamed from: a */
        public boolean mo1181a(String str) {
            if (ActivityBarcodeSheet.this.f24130W.size() <= 0) {
                return true;
            }
            ActivityBarcodeSheet.this.f24137d0.getFilter().filter(str);
            return true;
        }

        /* renamed from: b */
        public boolean mo1182b(String str) {
            return false;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$m */
    class C6911m implements SearchView.C0182l {
        C6911m(ActivityBarcodeSheet activityBarcodeSheet) {
        }

        /* renamed from: a */
        public boolean mo1180a() {
            return true;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$n */
    class C6912n implements View.OnClickListener {
        C6912n(ActivityBarcodeSheet activityBarcodeSheet) {
        }

        public void onClick(View view) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$o */
    class C6913o implements DialogInterface.OnClickListener {
        C6913o() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ActivityBarcodeSheet.this.f24121N.mo24207d()) {
                ActivityBarcodeSheet.this.f24122O.mo24213d("");
                ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
                activityBarcodeSheet.f24142u.mo25673k(activityBarcodeSheet.f24145x.getSheetId());
                return;
            }
            ActivityBarcodeSheet activityBarcodeSheet2 = ActivityBarcodeSheet.this;
            Toast.makeText(activityBarcodeSheet2, activityBarcodeSheet2.getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$p */
    class C6914p implements DialogInterface.OnClickListener {
        C6914p(ActivityBarcodeSheet activityBarcodeSheet) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$q */
    class C6915q implements DialogInterface.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24164c;

        C6915q(int i) {
            this.f24164c = i;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ActivityBarcodeSheet.this.f24122O.mo24213d("");
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            C7596g gVar = activityBarcodeSheet.f24142u;
            gVar.mo25671i(activityBarcodeSheet.f24110C, gVar.mo25682t(String.valueOf(this.f24164c)));
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$r */
    class C6916r implements DialogInterface.OnClickListener {
        C6916r(ActivityBarcodeSheet activityBarcodeSheet) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$s */
    class C6917s implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ EditText f24166c;

        /* renamed from: d */
        final /* synthetic */ Dialog f24167d;

        C6917s(EditText editText, Dialog dialog) {
            this.f24166c = editText;
            this.f24167d = dialog;
        }

        public void onClick(View view) {
            String obj = this.f24166c.getText().toString();
            if (!obj.equals("")) {
                this.f24167d.dismiss();
                ActivityBarcodeSheet.this.f24122O.mo24213d("");
                ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
                activityBarcodeSheet.f24144w.updatedSpreadsheetTitle = obj;
                C7596g gVar = activityBarcodeSheet.f24142u;
                gVar.mo25664R(activityBarcodeSheet.f24110C, gVar.mo25660A(obj));
                return;
            }
            Toast.makeText(ActivityBarcodeSheet.this, "Spreadsheet name cannot be empty!", 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$t */
    class C6918t implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24169c;

        C6918t(ActivityBarcodeSheet activityBarcodeSheet, Dialog dialog) {
            this.f24169c = dialog;
        }

        public void onClick(View view) {
            this.f24169c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$u */
    class C6919u implements TextWatcher {
        C6919u() {
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            int i4;
            CustomTextView customTextView;
            if (!ActivityBarcodeSheet.this.m33552x0(charSequence.toString()) || ActivityBarcodeSheet.this.f24133Z.booleanValue()) {
                customTextView = ActivityBarcodeSheet.this.textRepeatAlert;
                i4 = 8;
            } else {
                customTextView = ActivityBarcodeSheet.this.textRepeatAlert;
                i4 = 0;
            }
            customTextView.setVisibility(i4);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$v */
    class C6920v implements SwipeRefreshLayout.C0604j {
        C6920v() {
        }

        /* renamed from: a */
        public void mo3852a() {
            ActivityBarcodeSheet.this.m33542B0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$w */
    class C6921w implements Runnable {
        C6921w() {
        }

        public void run() {
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.cardButtons.startAnimation(activityBarcodeSheet.f24128U);
            ActivityBarcodeSheet.this.cardButtons.setVisibility(0);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$x */
    class C6922x implements SweetAlertDialog.OnSweetClickListener {
        C6922x() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityBarcodeSheet.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$y */
    class C6923y implements SweetAlertDialog.OnSweetClickListener {
        C6923y() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityBarcodeSheet.this.f24122O.mo24213d("");
            sweetAlertDialog.dismiss();
            ActivityBarcodeSheet activityBarcodeSheet = ActivityBarcodeSheet.this;
            activityBarcodeSheet.f24138e0.mo25639d(activityBarcodeSheet.f24110C);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityBarcodeSheet$z */
    class C6924z implements SweetAlertDialog.OnSweetClickListener {
        C6924z() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityBarcodeSheet.this.finish();
        }
    }

    public ActivityBarcodeSheet() {
        Boolean bool = Boolean.FALSE;
        this.f24124Q = bool;
        this.f24130W = new ArrayList();
        this.f24131X = new ArrayList();
        this.f24132Y = new HashMap<>();
        this.f24133Z = bool;
        this.f24135b0 = -1;
        this.f24136c0 = "";
        this.f24138e0 = C7591e.m35564f();
        this.f24140g0 = C7590d.m35558a();
    }

    /* access modifiers changed from: private */
    /* renamed from: B0 */
    public void m33542B0() {
        if (this.f24121N.mo24207d()) {
            this.f24142u.mo25678p(this.f24110C, C6873b.m33466d(this.f24111D, this.f24145x.colCount));
            return;
        }
        this.layoutProgress.setVisibility(8);
        this.f24122O.mo24211a();
        this.layoutTryAgain.setVisibility(0);
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* access modifiers changed from: private */
    /* renamed from: C0 */
    public void m33543C0() {
        if (this.f24121N.mo24207d()) {
            this.f24142u.mo25679q(this.f24145x.getSheetId());
            this.cardButtons.setVisibility(0);
            return;
        }
        this.layoutProgress.setVisibility(8);
        this.f24122O.mo24211a();
        this.layoutTryAgain.setVisibility(0);
        this.cardButtons.setVisibility(8);
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* renamed from: E0 */
    private void m33544E0() {
        if (getIntent().hasExtra("newSheet")) {
            this.f24124Q = Boolean.TRUE;
        }
        Calendar.getInstance().getTimeInMillis();
        this.cardAddBarcode.setVisibility(8);
        this.cardButtons.setVisibility(0);
        this.buttonAddRow.setVisibility(8);
        this.textAddRow.setVisibility(8);
        this.layoutEmptyScreen.setVisibility(8);
        AnimationUtils.loadAnimation(this, R.anim.scale_in);
        this.f24127T = AnimationUtils.loadAnimation(this, R.anim.scale_out);
        this.f24128U = AnimationUtils.loadAnimation(this, R.anim.translate_in);
        this.f24129V = AnimationUtils.loadAnimation(this, R.anim.translate_out);
        this.f24125R = C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.f24138e0.mo25642h(this, this);
        this.f24122O.mo24212c(this);
        this.f24121N.mo24206c(this);
        this.f24143v.mo24225f(this);
        this.f24140g0.mo25637b(this);
        this.f24142u.mo25662C(this, this);
        this.toolbarBarcodeSheet.setTitle((CharSequence) "");
        this.toolbarBarcodeSheet.setTitleTextColor(getResources().getColor(R.color.black));
        mo276r0(this.toolbarBarcodeSheet);
        this.toolbarBarcodeSheet.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarBarcodeSheet.setNavigationOnClickListener(new C6909k());
        this.f24145x = (C7560e) getIntent().getSerializableExtra("spreadsheet");
        this.f24109B.add("Barcode");
        this.f24109B.add("Quantity");
        this.f24109B.add("Date");
        this.f24126S = this.f24143v.mo24222b(C6873b.f24024o);
        this.f24112E = this.f24144w.sheetId;
        this.f24110C = this.f24145x.getSheetId();
        TextView textView = (TextView) this.toolbarBarcodeSheet.findViewById(R.id.text_toolbar_sheet_barcode);
        this.f24141h0 = textView;
        textView.setText(this.f24145x.getSheetName());
        this.recyclerBarcodes.setLayoutManager(new LinearLayoutManager(this, 1, false));
        Dialog dialog = new Dialog(this);
        this.f24115H = dialog;
        dialog.requestWindowFeature(1);
        this.f24115H.setContentView(R.layout.dialog_add_new_sheet);
        this.f24116I = (CustomTextView) this.f24115H.findViewById(R.id.text_spreadsheet_title);
        this.f24117J = (CustomTextView) this.f24115H.findViewById(R.id.text_sheet_title);
        this.f24118K = (EditText) this.f24115H.findViewById(R.id.edit_sheet_name);
        this.f24119L = (Button) this.f24115H.findViewById(R.id.button_add_sheet);
        this.f24120M = (ImageView) this.f24115H.findViewById(R.id.image_close);
        this.f24119L.setOnClickListener(this);
        this.f24120M.setOnClickListener(this);
        new ArrayAdapter(this, R.layout.spinner_text, this.f24146y);
        this.f24122O.mo24213d("");
        m33543C0();
        this.editBarcode.addTextChangedListener(new C6919u());
        this.layoutPullToRefresh.setOnRefreshListener(new C6920v());
        if (!this.f24143v.mo24221a(C6873b.f24007e)) {
            m33545F0();
        }
        Calendar.getInstance().getTimeInMillis();
    }

    /* renamed from: F0 */
    private void m33545F0() {
    }

    /* renamed from: G0 */
    private void m33546G0() {
        this.f24147z.clear();
        this.f24146y.clear();
        this.f24108A.clear();
        for (int i = 0; i < this.f24144w.sheetIdList.size(); i++) {
            this.f24146y.add(this.f24144w.sheetTitleList.get(i));
            this.f24147z.add(this.f24144w.sheetIdList.get(i));
            C7565j jVar = new C7565j();
            jVar.setSubSheetId(String.valueOf(this.f24144w.sheetIdList.get(i)));
            jVar.setSubSheetName(this.f24144w.sheetTitleList.get(i));
            this.f24108A.add(jVar);
        }
        if (this.f24123P.booleanValue()) {
            this.f24123P = Boolean.FALSE;
            if (this.f24145x.getLastSheetId() == "" || this.f24145x.getLastSheetId() == null || !this.f24147z.contains(Integer.valueOf(Integer.parseInt(this.f24145x.getLastSheetId())))) {
                this.f24113F = 0;
            } else {
                this.f24113F = this.f24147z.indexOf(Integer.valueOf(Integer.parseInt(this.f24145x.getLastSheetId())));
            }
        }
        this.f24111D = this.f24146y.get(this.f24113F);
        this.f24112E = this.f24147z.get(this.f24113F).intValue();
        this.textAddRow.setVisibility(8);
        this.layoutEmptyScreen.setVisibility(8);
        this.layoutProgress.setVisibility(0);
        if (this.f24124Q.booleanValue()) {
            C6873b.m33473k(this.f24144w.sheet1, this.f24109B.size());
            this.f24142u.mo25662C(this, this);
            C7596g gVar = this.f24142u;
            gVar.mo25665c(this.f24110C, gVar.mo25684v(this.f24112E, 0, this.f24109B));
        } else {
            m33542B0();
        }
        this.recyclerSubsheets.setLayoutManager(new LinearLayoutManager(this, 0, false));
        SubsheetsAdapter subsheetsAdapter = new SubsheetsAdapter(this, this.f24108A, String.valueOf(this.f24112E));
        this.f24139f0 = subsheetsAdapter;
        this.recyclerSubsheets.setAdapter(subsheetsAdapter);
        this.layoutSubsheetsList.setVisibility(0);
    }

    /* renamed from: J0 */
    private void m33547J0() {
        int i;
        CustomTextView customTextView;
        if (this.f24130W.size() > 0) {
            customTextView = this.textAddRow;
            i = 8;
        } else {
            customTextView = this.textAddRow;
            i = 0;
        }
        customTextView.setVisibility(i);
        this.layoutEmptyScreen.setVisibility(i);
        ArrayList arrayList = new ArrayList(this.f24130W);
        this.f24131X = arrayList;
        Collections.reverse(arrayList);
        C7080b bVar = new C7080b(this, this.f24131X, this);
        this.f24137d0 = bVar;
        this.recyclerBarcodes.setAdapter(bVar);
        if (!this.f24143v.mo24221a(C6873b.f24007e)) {
            mo24262H0();
        }
    }

    /* renamed from: K0 */
    private void m33548K0() {
        this.f24122O.mo24211a();
        SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("Something Went Wrong!").setConfirmButton("Try Again", (SweetAlertDialog.OnSweetClickListener) new C6898a0()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C6924z());
        cancelButton.setCancelable(false);
        cancelButton.setCanceledOnTouchOutside(false);
        cancelButton.show();
    }

    /* access modifiers changed from: private */
    /* renamed from: x0 */
    public boolean m33552x0(String str) {
        for (int i = 0; i < this.f24130W.size(); i++) {
            String str2 = (String) this.f24130W.get(i).get("Barcode");
            if (str2 != null && str2.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: A0 */
    public void mo24259A0(String str) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_free_limits);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_free_limits_dialog_msg)).setText(str);
        ((Button) dialog.findViewById(R.id.button_free_limits_dialog_close)).setOnClickListener(new C6902d(this, dialog));
        ((Button) dialog.findViewById(R.id.button_free_limits_go_premium)).setOnClickListener(new C6903e(dialog));
        ((ImageView) dialog.findViewById(R.id.image_close_free_limits_dialog)).setOnClickListener(new C6904f(this, dialog));
    }

    /* renamed from: D0 */
    public void mo24260D0() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* renamed from: E */
    public void mo24261E(String str) {
        mo24259A0(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0102, code lost:
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0106, code lost:
        m33548K0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* renamed from: H */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24237H(String r7, String r8) {
        /*
            r6 = this;
            r8.hashCode()
            int r0 = r8.hashCode()
            r1 = 3
            r2 = 1
            r3 = 0
            r4 = 8
            r5 = -1
            switch(r0) {
                case -2022976223: goto L_0x007b;
                case -1809886158: goto L_0x0070;
                case -1788871435: goto L_0x0065;
                case -561789226: goto L_0x005a;
                case -452548289: goto L_0x004f;
                case -358721489: goto L_0x0044;
                case -351206292: goto L_0x0039;
                case 124646116: goto L_0x002e;
                case 281689075: goto L_0x0020;
                case 1594339684: goto L_0x0012;
                default: goto L_0x0010;
            }
        L_0x0010:
            goto L_0x0085
        L_0x0012:
            java.lang.String r0 = "formatHeader"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x001c
            goto L_0x0085
        L_0x001c:
            r5 = 9
            goto L_0x0085
        L_0x0020:
            java.lang.String r0 = "File_Not_Found"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x002a
            goto L_0x0085
        L_0x002a:
            r5 = 8
            goto L_0x0085
        L_0x002e:
            java.lang.String r0 = "getAllRows"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0037
            goto L_0x0085
        L_0x0037:
            r5 = 7
            goto L_0x0085
        L_0x0039:
            java.lang.String r0 = "addHeaderRow"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0042
            goto L_0x0085
        L_0x0042:
            r5 = 6
            goto L_0x0085
        L_0x0044:
            java.lang.String r0 = "deleteRow"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x004d
            goto L_0x0085
        L_0x004d:
            r5 = 5
            goto L_0x0085
        L_0x004f:
            java.lang.String r0 = "getAllSheets"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0058
            goto L_0x0085
        L_0x0058:
            r5 = 4
            goto L_0x0085
        L_0x005a:
            java.lang.String r0 = "Access_Denied"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0063
            goto L_0x0085
        L_0x0063:
            r5 = 3
            goto L_0x0085
        L_0x0065:
            java.lang.String r0 = "updateSpreadSheetTitle"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x006e
            goto L_0x0085
        L_0x006e:
            r5 = 2
            goto L_0x0085
        L_0x0070:
            java.lang.String r0 = "getAllSpreadsheets"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0079
            goto L_0x0085
        L_0x0079:
            r5 = 1
            goto L_0x0085
        L_0x007b:
            java.lang.String r0 = "deleteSpreadsheet"
            boolean r8 = r8.equals(r0)
            if (r8 != 0) goto L_0x0084
            goto L_0x0085
        L_0x0084:
            r5 = 0
        L_0x0085:
            switch(r5) {
                case 0: goto L_0x0114;
                case 1: goto L_0x010a;
                case 2: goto L_0x0106;
                case 3: goto L_0x00f9;
                case 4: goto L_0x0106;
                case 5: goto L_0x0106;
                case 6: goto L_0x0106;
                case 7: goto L_0x00c0;
                case 8: goto L_0x008a;
                case 9: goto L_0x0106;
                default: goto L_0x0088;
            }
        L_0x0088:
            goto L_0x0106
        L_0x008a:
            com.spreadsheet.app.Utils.d r7 = r6.f24122O
            r7.mo24211a()
            cn.pedant.SweetAlert.SweetAlertDialog r7 = new cn.pedant.SweetAlert.SweetAlertDialog
            r7.<init>(r6, r1)
            java.lang.String r8 = "File not found!"
            cn.pedant.SweetAlert.SweetAlertDialog r7 = r7.setTitleText(r8)
            java.lang.String r8 = "You have deleted this sheet\nfrom Drive!"
            cn.pedant.SweetAlert.SweetAlertDialog r7 = r7.setContentText(r8)
            com.spreadsheet.app.activities.ActivityBarcodeSheet$y r8 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$y
            r8.<init>()
            java.lang.String r0 = "Remove from list"
            cn.pedant.SweetAlert.SweetAlertDialog r7 = r7.setConfirmButton((java.lang.String) r0, (p007cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener) r8)
            com.spreadsheet.app.activities.ActivityBarcodeSheet$x r8 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$x
            r8.<init>()
            java.lang.String r0 = "Exit"
            cn.pedant.SweetAlert.SweetAlertDialog r7 = r7.setCancelButton((java.lang.String) r0, (p007cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener) r8)
            r7.setCancelable(r3)
            r7.setCanceledOnTouchOutside(r3)
            r7.show()
            goto L_0x0126
        L_0x00c0:
            com.spreadsheet.app.Utils.d r8 = r6.f24122O
            r8.mo24211a()
            android.widget.RelativeLayout r8 = r6.layoutProgress
            r8.setVisibility(r4)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r8 = r6.textAddRow
            r8.setVisibility(r4)
            android.widget.LinearLayout r8 = r6.layoutEmptyScreen
            r8.setVisibility(r4)
            androidx.recyclerview.widget.RecyclerView r8 = r6.recyclerBarcodes
            r8.setVisibility(r4)
            java.lang.String r8 = "org.json.JSONException: No value for rowData"
            boolean r7 = r7.equalsIgnoreCase(r8)
            if (r7 == 0) goto L_0x00f3
            com.spreadsheet.app.Utils.Fonts.CustomTextView r7 = r6.textTryAgain
            java.lang.String r8 = "No data found"
            r7.setText(r8)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r7 = r6.btnTryAgain
            r7.setVisibility(r4)
            androidx.constraintlayout.widget.ConstraintLayout r7 = r6.layoutTryAgain
            r7.setVisibility(r3)
            goto L_0x0126
        L_0x00f3:
            androidx.constraintlayout.widget.ConstraintLayout r7 = r6.layoutTryAgain
            r7.setVisibility(r4)
            goto L_0x0106
        L_0x00f9:
            e.e.a.a.g r7 = r6.f24144w
            r7.shouldRequestAuth = r2
            com.spreadsheet.app.Utils.d r7 = r6.f24122O
            r7.mo24211a()
        L_0x0102:
            r6.finish()
            goto L_0x0126
        L_0x0106:
            r6.m33548K0()
            goto L_0x0126
        L_0x010a:
            com.spreadsheet.app.Utils.d r7 = r6.f24122O
            r7.mo24211a()
            e.e.a.a.g r7 = r6.f24144w
            r7.isUpdated = r2
            goto L_0x0102
        L_0x0114:
            e.e.a.d.d r7 = r6.f24140g0
            java.lang.String r8 = "DELETE_SPREADSHEET"
            r7.mo25638c(r8, r8)
            e.e.a.d.e r7 = r6.f24138e0
            e.e.a.a.e r8 = r6.f24145x
            java.lang.String r8 = r8.getSheetId()
            r7.mo25639d(r8)
        L_0x0126:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityBarcodeSheet.mo24237H(java.lang.String, java.lang.String):void");
    }

    /* renamed from: H0 */
    public void mo24262H0() {
    }

    /* renamed from: I0 */
    public void mo24263I0() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_add_new_sheet);
        EditText editText = (EditText) dialog.findViewById(R.id.edit_sheet_name);
        Button button = (Button) dialog.findViewById(R.id.button_add_sheet);
        editText.setText(this.f24145x.getSheetName());
        ((CustomTextView) dialog.findViewById(R.id.text_spreadsheet_title)).setText("Rename Spreadsheet");
        button.setText("Update");
        button.setOnClickListener(new C6917s(editText, dialog));
        ((ImageView) dialog.findViewById(R.id.image_close)).setOnClickListener(new C6918t(this, dialog));
        dialog.show();
    }

    /* renamed from: L0 */
    public void mo24264L0() {
        this.f24122O.mo24211a();
        Toast.makeText(this, "Spreadsheet Renamed!", 0).show();
        this.f24138e0.mo25644j(this.f24110C, C7562g.KEY_SPREADSHEET_NAME, this.f24144w.updatedSpreadsheetTitle);
        this.f24138e0.mo25647m(this.f24110C);
        this.f24141h0.setText(this.f24144w.updatedSpreadsheetTitle);
        this.f24144w.isUpdated = true;
    }

    /* renamed from: O */
    public void mo24265O(int i, String str) {
        this.f24112E = i;
        this.f24116I.setText("Rename Sheet");
        this.f24118K.setText(str);
        EditText editText = this.f24118K;
        editText.setSelection(editText.length());
        this.f24118K.setVisibility(0);
        this.f24117J.setVisibility(8);
        this.f24119L.setText(R.string.update);
        this.f24115H.show();
    }

    /* renamed from: R */
    public void mo24266R(int i, String str) {
        this.f24112E = i;
        mo24274z0(i, str);
    }

    /* renamed from: h */
    public void mo24267h(int i, String str, int i2) {
        this.f24111D = str;
        this.f24112E = i;
        this.f24145x.setLastSheetId(String.valueOf(i));
        this.f24113F = i2;
        this.textAddRow.setVisibility(8);
        this.layoutEmptyScreen.setVisibility(8);
        this.layoutProgress.setVisibility(0);
        C4529d g = this.f24125R.mo18590g(this.f24126S).mo18590g(this.f24110C).mo18590g("lastSheetId");
        g.mo18595k(this.f24112E + "");
        if (!this.f24124Q.booleanValue()) {
            C7591e eVar = this.f24138e0;
            String str2 = this.f24110C;
            eVar.mo25643i(str2, this.f24112E + "");
            m33542B0();
        }
    }

    /* renamed from: o */
    public void mo24268o(HashMap<String, String> hashMap) {
        if (hashMap.get("$POS$") != null && !hashMap.get("$POS$").equalsIgnoreCase("")) {
            this.checkboxUpdateDate.setVisibility(0);
            int parseInt = Integer.parseInt(hashMap.get("$POS$"));
            this.f24136c0 = hashMap.get("Date");
            if (this.cardAddBarcode.getVisibility() == 0) {
                this.editQuantity.setText(hashMap.get("Quantity"));
                this.editBarcode.setText(hashMap.get("Barcode"));
                this.f24133Z = Boolean.TRUE;
                this.f24135b0 = parseInt;
            } else {
                this.f24133Z = Boolean.TRUE;
                this.f24135b0 = parseInt;
                this.cardButtons.startAnimation(this.f24129V);
                this.cardButtons.setVisibility(8);
                this.editQuantity.setText(hashMap.get("Quantity"));
                this.editBarcode.setText(hashMap.get("Barcode"));
                new Handler().postDelayed(new C6908j(), 250);
            }
            this.buttonAddBarcode.setText("Update");
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 0) {
            this.f24142u.mo25662C(this, this);
            m33543C0();
            return;
        }
        C7319b h = C7318a.m34443h(i, i2, intent);
        if (h != null && h.mo24948a() != null) {
            this.cardButtons.startAnimation(this.f24129V);
            this.cardButtons.setVisibility(8);
            this.editQuantity.setText("1");
            this.editBarcode.setText(h.mo24948a());
            new Handler().postDelayed(new C6905g(), 250);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onClick(View view) {
        Toast toast;
        int id = view.getId();
        if (id == R.id.button_add_sheet) {
            if (this.f24121N.mo24207d()) {
                if (this.f24119L.getText().toString().equals(getResources().getString(R.string.update))) {
                    String obj = this.f24118K.getText().toString();
                    if (!obj.equals("")) {
                        if (!obj.equals(this.f24111D)) {
                            if (!this.f24146y.contains(obj)) {
                                this.f24115H.dismiss();
                                C7596g gVar = this.f24142u;
                                String str = this.f24110C;
                                gVar.mo25663Q(str, gVar.mo25661B(this.f24112E + "", obj));
                                this.f24122O.mo24213d("");
                                return;
                            }
                        }
                    }
                    toast = Toast.makeText(this, R.string.enter_sheet_name, 0);
                } else if (this.f24119L.getText().toString().equals(getResources().getString(R.string.add))) {
                    String obj2 = this.f24118K.getText().toString();
                    if (!obj2.equals("")) {
                        if (!this.f24146y.contains(obj2)) {
                            this.f24115H.dismiss();
                            this.f24122O.mo24213d("");
                            C7596g gVar2 = this.f24142u;
                            gVar2.mo25667e(this.f24110C, gVar2.mo25676n(obj2));
                            return;
                        }
                    }
                    toast = Toast.makeText(this, R.string.enter_sheet_name, 0);
                } else if (this.f24119L.getText().toString().equals(getResources().getString(R.string.delete))) {
                    this.f24115H.dismiss();
                    this.f24122O.mo24213d("");
                    C7596g gVar3 = this.f24142u;
                    gVar3.mo25671i(this.f24110C, gVar3.mo25682t(String.valueOf(this.f24112E)));
                    return;
                } else {
                    return;
                }
                toast = Toast.makeText(this, R.string.sheet_name_already_exists, 0);
            } else {
                this.f24115H.dismiss();
                toast = Toast.makeText(this, getResources().getString(R.string.internet_check), 0);
            }
            toast.show();
            return;
        } else if (id != R.id.image_close) {
            return;
        }
        this.f24115H.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_barcode_sheet);
        ButterKnife.bind((Activity) this);
        m33544E0();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        MenuItem findItem = menu.findItem(R.id.action_search_user);
        menu.findItem(R.id.menu_update_spreadsheet).setVisible(false);
        menu.findItem(R.id.menu_rename_spreadsheet).setVisible(true);
        SearchManager searchManager = (SearchManager) getSystemService("search");
        SearchView searchView = findItem != null ? (SearchView) findItem.getActionView() : null;
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setQueryHint("Search Barcode..");
        searchView.setOnQueryTextListener(new C6910l());
        searchView.setOnCloseListener(new C6911m(this));
        searchView.setOnSearchClickListener(new C6912n(this));
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            finish();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_delete_spreadsheet) {
            C0065b.C0066a aVar = new C0065b.C0066a(this);
            aVar.mo236g(getResources().getString(R.string.do_you_want_to_delete) + " '" + this.f24145x.getSheetName() + "' " + getResources().getString(R.string.file_permanently));
            aVar.mo240k(getResources().getString(R.string.yes), new C6913o());
            aVar.mo237h(getResources().getString(R.string.no), new C6914p(this));
            aVar.mo243n();
        } else if (itemId == R.id.menu_rename_spreadsheet) {
            mo24263I0();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x009b, code lost:
        r10.buttonAddBarcode.setText("Add To Sheet");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00d8, code lost:
        mo24259A0("Add unlimited rows with UpSheet Premium");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0150, code lost:
        r11.postDelayed(r0, 250);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    @butterknife.OnClick({2131296350, 2131296347, 2131296513, 2131296561, 2131296597, 2131296344, 2131296352})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewClicked(View r11) {
        /*
            r10 = this;
            int r11 = r11.getId()
            java.lang.String r0 = "Add unlimited rows with UpSheet Premium"
            r1 = 100
            r2 = 2
            r3 = 1
            java.lang.String r4 = "Add To Sheet"
            r5 = 250(0xfa, double:1.235E-321)
            r7 = 0
            java.lang.String r8 = ""
            r9 = 8
            switch(r11) {
                case 2131296344: goto L_0x0254;
                case 2131296347: goto L_0x0155;
                case 2131296350: goto L_0x013a;
                case 2131296352: goto L_0x00f4;
                case 2131296513: goto L_0x00dd;
                case 2131296561: goto L_0x0065;
                case 2131296597: goto L_0x0018;
                default: goto L_0x0016;
            }
        L_0x0016:
            goto L_0x0261
        L_0x0018:
            com.spreadsheet.app.Utils.h r11 = r10.f24143v
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r11 = r11.mo24221a(r2)
            java.lang.String r2 = "Scan Barcode/QrCode"
            if (r11 == 0) goto L_0x004a
            java.lang.Boolean r11 = java.lang.Boolean.FALSE
            r10.f24133Z = r11
            com.spreadsheet.app.Utils.Fonts.CustomCheckBox r11 = r10.checkboxUpdateDate
            r11.setVisibility(r9)
            e.d.e.a0.a.a r11 = new e.d.e.a0.a.a
            r11.<init>(r10)
        L_0x0032:
            java.util.Collection<java.lang.String> r0 = p153e.p174d.p267e.p268a0.p269a.C7318a.f25291j
            r11.mo24944m(r0)
            r11.mo24946o(r2)
            r11.mo24945n(r7)
            r11.mo24943l(r7)
            r11.mo24942k(r7)
            r11.mo24941j(r3)
            r11.mo24940f()
            goto L_0x009b
        L_0x004a:
            e.e.a.a.g r11 = r10.f24144w
            java.util.List r11 = r11.getAllRowsList()
            int r11 = r11.size()
            if (r11 >= r1) goto L_0x00d8
            java.lang.Boolean r11 = java.lang.Boolean.FALSE
            r10.f24133Z = r11
            com.spreadsheet.app.Utils.Fonts.CustomCheckBox r11 = r10.checkboxUpdateDate
            r11.setVisibility(r9)
            e.d.e.a0.a.a r11 = new e.d.e.a0.a.a
            r11.<init>(r10)
            goto L_0x0032
        L_0x0065:
            com.spreadsheet.app.Utils.h r11 = r10.f24143v
            java.lang.String r2 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r11 = r11.mo24221a(r2)
            if (r11 == 0) goto L_0x00a2
            java.lang.Boolean r11 = java.lang.Boolean.FALSE
            r10.f24133Z = r11
            com.spreadsheet.app.Utils.Fonts.CustomCheckBox r11 = r10.checkboxUpdateDate
            r11.setVisibility(r9)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r11 = r10.editQuantity
            r11.setText(r8)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r11 = r10.editBarcode
            r11.setText(r8)
            androidx.cardview.widget.CardView r11 = r10.cardButtons
            android.view.animation.Animation r0 = r10.f24129V
            r11.startAnimation(r0)
            androidx.cardview.widget.CardView r11 = r10.cardButtons
            r11.setVisibility(r9)
            android.os.Handler r11 = new android.os.Handler
            r11.<init>()
            com.spreadsheet.app.activities.ActivityBarcodeSheet$b r0 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$b
            r0.<init>()
        L_0x0098:
            r11.postDelayed(r0, r5)
        L_0x009b:
            com.spreadsheet.app.Utils.Fonts.CustomButton r11 = r10.buttonAddBarcode
            r11.setText(r4)
            goto L_0x0261
        L_0x00a2:
            e.e.a.a.g r11 = r10.f24144w
            java.util.List r11 = r11.getAllRowsList()
            int r11 = r11.size()
            if (r11 >= r1) goto L_0x00d8
            java.lang.Boolean r11 = java.lang.Boolean.FALSE
            r10.f24133Z = r11
            com.spreadsheet.app.Utils.Fonts.CustomCheckBox r11 = r10.checkboxUpdateDate
            r11.setVisibility(r9)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r11 = r10.editQuantity
            r11.setText(r8)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r11 = r10.editBarcode
            r11.setText(r8)
            androidx.cardview.widget.CardView r11 = r10.cardButtons
            android.view.animation.Animation r0 = r10.f24129V
            r11.startAnimation(r0)
            androidx.cardview.widget.CardView r11 = r10.cardButtons
            r11.setVisibility(r9)
            android.os.Handler r11 = new android.os.Handler
            r11.<init>()
            com.spreadsheet.app.activities.ActivityBarcodeSheet$c r0 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$c
            r0.<init>()
            goto L_0x0098
        L_0x00d8:
            r10.mo24259A0(r0)
            goto L_0x0261
        L_0x00dd:
            androidx.cardview.widget.CardView r11 = r10.cardAddBarcode
            android.view.animation.Animation r0 = r10.f24129V
            r11.startAnimation(r0)
            androidx.cardview.widget.CardView r11 = r10.cardAddBarcode
            r11.setVisibility(r9)
            android.os.Handler r11 = new android.os.Handler
            r11.<init>()
            com.spreadsheet.app.activities.ActivityBarcodeSheet$a r0 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$a
            r0.<init>()
            goto L_0x0150
        L_0x00f4:
            com.spreadsheet.app.Utils.h r11 = r10.f24143v
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.f24017j
            boolean r11 = r11.mo24221a(r0)
            r0 = 2131820585(0x7f110029, float:1.927389E38)
            java.lang.String r1 = "Sheet name"
            java.lang.String r3 = "Add Sheet"
            if (r11 == 0) goto L_0x012a
        L_0x0105:
            com.spreadsheet.app.Utils.Fonts.CustomTextView r11 = r10.f24116I
            r11.setText(r3)
            android.widget.EditText r11 = r10.f24118K
            r11.setText(r8)
            android.widget.EditText r11 = r10.f24118K
            r11.setHint(r1)
            android.widget.EditText r11 = r10.f24118K
            r11.setVisibility(r7)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r11 = r10.f24117J
            r11.setVisibility(r9)
            android.widget.Button r11 = r10.f24119L
            r11.setText(r0)
            android.app.Dialog r11 = r10.f24115H
            r11.show()
            goto L_0x0261
        L_0x012a:
            java.util.List<e.e.a.a.j> r11 = r10.f24108A
            int r11 = r11.size()
            if (r11 >= r2) goto L_0x0133
            goto L_0x0105
        L_0x0133:
            java.lang.String r11 = "Add more than 2 subsheets with UpSheet Premium"
            r10.mo24259A0(r11)
            goto L_0x0261
        L_0x013a:
            android.widget.ImageView r11 = r10.buttonAddRow
            android.view.animation.Animation r0 = r10.f24127T
            r11.startAnimation(r0)
            android.widget.ImageView r11 = r10.buttonAddRow
            r11.setVisibility(r9)
            android.os.Handler r11 = new android.os.Handler
            r11.<init>()
            com.spreadsheet.app.activities.ActivityBarcodeSheet$b0 r0 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$b0
            r0.<init>()
        L_0x0150:
            r11.postDelayed(r0, r5)
            goto L_0x0261
        L_0x0155:
            com.spreadsheet.app.Utils.a r11 = r10.f24121N
            boolean r11 = r11.mo24207d()
            if (r11 == 0) goto L_0x0241
            java.util.HashMap<java.lang.String, java.lang.String> r11 = r10.f24132Y
            r11.clear()
            java.lang.Boolean r11 = r10.f24133Z
            boolean r11 = r11.booleanValue()
            if (r11 == 0) goto L_0x017b
            int r11 = r10.f24135b0
            int r0 = r11 + 1
            r10.f24114G = r0
            java.lang.String r0 = r10.f24111D
            e.e.a.a.e r1 = r10.f24145x
            int r1 = r1.colCount
            int r11 = r11 + r2
            com.spreadsheet.app.Utils.C6873b.m33478p(r0, r1, r11)
            goto L_0x0194
        L_0x017b:
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r11 = r10.f24130W
            int r11 = r11.size()
            int r11 = r11 + r3
            r10.f24114G = r11
            java.lang.String r11 = r10.f24111D
            e.e.a.a.e r0 = r10.f24145x
            int r0 = r0.colCount
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r1 = r10.f24130W
            int r1 = r1.size()
            int r1 = r1 + r2
            com.spreadsheet.app.Utils.C6873b.m33478p(r11, r0, r1)
        L_0x0194:
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r0 = com.spreadsheet.app.Utils.C6873b.m33470h()
            com.spreadsheet.app.Utils.Fonts.CustomEditText r1 = r10.editQuantity
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            com.spreadsheet.app.Utils.Fonts.CustomEditText r2 = r10.editBarcode
            android.text.Editable r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            boolean r3 = r1.equals(r8)
            if (r3 != 0) goto L_0x0261
            boolean r3 = r2.equals(r8)
            if (r3 != 0) goto L_0x0261
            r10.mo24260D0()
            r11.add(r2)
            r11.add(r1)
            com.spreadsheet.app.Utils.Fonts.CustomCheckBox r3 = r10.checkboxUpdateDate
            boolean r3 = r3.isChecked()
            java.lang.String r4 = "Date"
            if (r3 != 0) goto L_0x01e2
            java.lang.Boolean r3 = r10.f24133Z
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x01e2
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r10.f24132Y
            java.lang.String r3 = r10.f24136c0
            r0.put(r4, r3)
            java.lang.String r0 = r10.f24136c0
            goto L_0x01e7
        L_0x01e2:
            java.util.HashMap<java.lang.String, java.lang.String> r3 = r10.f24132Y
            r3.put(r4, r0)
        L_0x01e7:
            r11.add(r0)
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r10.f24132Y
            java.lang.String r3 = "Barcode"
            r0.put(r3, r2)
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r10.f24132Y
            java.lang.String r2 = "Quantity"
            r0.put(r2, r1)
            java.lang.Boolean r0 = r10.f24133Z
            boolean r0 = r0.booleanValue()
            java.lang.String r1 = "$POS$"
            if (r0 == 0) goto L_0x020c
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r10.f24132Y
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            int r3 = r10.f24135b0
            goto L_0x0219
        L_0x020c:
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r10.f24132Y
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r3 = r10.f24130W
            int r3 = r3.size()
        L_0x0219:
            r2.append(r3)
            r2.append(r8)
            java.lang.String r2 = r2.toString()
            r0.put(r1, r2)
            android.widget.RelativeLayout r0 = r10.layoutAddProgress
            r0.setVisibility(r7)
            e.e.a.d.g r0 = r10.f24142u
            e.e.a.a.e r1 = r10.f24145x
            java.lang.String r1 = r1.getSheetId()
            e.e.a.d.g r2 = r10.f24142u
            int r3 = r10.f24112E
            int r4 = r10.f24114G
            org.json.JSONObject r11 = r2.mo25685w(r3, r4, r11)
            r0.mo25666d(r1, r11)
            goto L_0x0261
        L_0x0241:
            android.content.res.Resources r11 = r10.getResources()
            r0 = 2131820726(0x7f1100b6, float:1.9274175E38)
            java.lang.String r11 = r11.getString(r0)
            android.widget.Toast r11 = android.widget.Toast.makeText(r10, r11, r7)
            r11.show()
            goto L_0x0261
        L_0x0254:
            androidx.constraintlayout.widget.ConstraintLayout r11 = r10.layoutTryAgain
            r11.setVisibility(r9)
            com.spreadsheet.app.Utils.d r11 = r10.f24122O
            r11.mo24213d(r8)
            r10.m33543C0()
        L_0x0261:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityBarcodeSheet.onViewClicked(android.view.View):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0196, code lost:
        if (r4.f24124Q.booleanValue() != false) goto L_0x022f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01bb, code lost:
        m33547J0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0204, code lost:
        m33543C0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x022d, code lost:
        if (r4.f24124Q.booleanValue() != false) goto L_0x022f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x022f, code lost:
        r4.f24124Q = java.lang.Boolean.FALSE;
        r4.layoutProgress.setVisibility(8);
        r4.textAddRow.setVisibility(0);
        r4.layoutEmptyScreen.setVisibility(0);
        android.widget.Toast.makeText(r4, "Barcode Sheet Created Successfully", 0).show();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return;
     */
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24244s(String r5, String r6) {
        /*
            r4 = this;
            r6.hashCode()
            int r5 = r6.hashCode()
            r0 = 8
            r1 = 1
            r2 = 0
            r3 = -1
            switch(r5) {
                case -2022976223: goto L_0x00a4;
                case -1978479736: goto L_0x0099;
                case -1809886158: goto L_0x008e;
                case -1788871435: goto L_0x0083;
                case -1479983679: goto L_0x0078;
                case -1133266316: goto L_0x006d;
                case -452548289: goto L_0x0062;
                case -358721489: goto L_0x0057;
                case 124646116: goto L_0x0049;
                case 323754608: goto L_0x003b;
                case 391607136: goto L_0x002d;
                case 635300543: goto L_0x001f;
                case 1594339684: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            goto L_0x00ae
        L_0x0011:
            java.lang.String r5 = "formatHeader"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x001b
            goto L_0x00ae
        L_0x001b:
            r3 = 12
            goto L_0x00ae
        L_0x001f:
            java.lang.String r5 = "addFormattedRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0029
            goto L_0x00ae
        L_0x0029:
            r3 = 11
            goto L_0x00ae
        L_0x002d:
            java.lang.String r5 = "addNewSheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0037
            goto L_0x00ae
        L_0x0037:
            r3 = 10
            goto L_0x00ae
        L_0x003b:
            java.lang.String r5 = "formatRows"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0045
            goto L_0x00ae
        L_0x0045:
            r3 = 9
            goto L_0x00ae
        L_0x0049:
            java.lang.String r5 = "getAllRows"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0053
            goto L_0x00ae
        L_0x0053:
            r3 = 8
            goto L_0x00ae
        L_0x0057:
            java.lang.String r5 = "deleteRow"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0060
            goto L_0x00ae
        L_0x0060:
            r3 = 7
            goto L_0x00ae
        L_0x0062:
            java.lang.String r5 = "getAllSheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x006b
            goto L_0x00ae
        L_0x006b:
            r3 = 6
            goto L_0x00ae
        L_0x006d:
            java.lang.String r5 = "deleteSheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0076
            goto L_0x00ae
        L_0x0076:
            r3 = 5
            goto L_0x00ae
        L_0x0078:
            java.lang.String r5 = "updateSheetName"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0081
            goto L_0x00ae
        L_0x0081:
            r3 = 4
            goto L_0x00ae
        L_0x0083:
            java.lang.String r5 = "updateSpreadSheetTitle"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x008c
            goto L_0x00ae
        L_0x008c:
            r3 = 3
            goto L_0x00ae
        L_0x008e:
            java.lang.String r5 = "getAllSpreadsheets"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x0097
            goto L_0x00ae
        L_0x0097:
            r3 = 2
            goto L_0x00ae
        L_0x0099:
            java.lang.String r5 = "addFormattedHeader"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x00a2
            goto L_0x00ae
        L_0x00a2:
            r3 = 1
            goto L_0x00ae
        L_0x00a4:
            java.lang.String r5 = "deleteSpreadsheet"
            boolean r5 = r6.equals(r5)
            if (r5 != 0) goto L_0x00ad
            goto L_0x00ae
        L_0x00ad:
            r3 = 0
        L_0x00ae:
            java.lang.String r5 = "Barcode Sheet Created Successfully"
            java.lang.String r6 = ""
            switch(r3) {
                case 0: goto L_0x024a;
                case 1: goto L_0x0222;
                case 2: goto L_0x0215;
                case 3: goto L_0x0208;
                case 4: goto L_0x01f9;
                case 5: goto L_0x01da;
                case 6: goto L_0x01d0;
                case 7: goto L_0x01c0;
                case 8: goto L_0x019a;
                case 9: goto L_0x0190;
                case 10: goto L_0x0134;
                case 11: goto L_0x00c6;
                case 12: goto L_0x00b7;
                default: goto L_0x00b5;
            }
        L_0x00b5:
            goto L_0x025c
        L_0x00b7:
            e.e.a.d.g r5 = r4.f24142u
            java.lang.String r6 = r4.f24110C
            int r0 = r4.f24112E
            org.json.JSONObject r0 = r5.mo25683u(r0)
            r5.mo25674l(r6, r0)
            goto L_0x025c
        L_0x00c6:
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24138e0
            java.lang.String r2 = r4.f24110C
            r5.mo25647m(r2)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r5 = r4.editQuantity
            r5.setText(r6)
            com.spreadsheet.app.Utils.Fonts.CustomEditText r5 = r4.editBarcode
            r5.setText(r6)
            android.widget.RelativeLayout r5 = r4.layoutAddProgress
            r5.setVisibility(r0)
            androidx.cardview.widget.CardView r5 = r4.cardAddBarcode
            android.view.animation.Animation r6 = r4.f24129V
            r5.startAnimation(r6)
            androidx.cardview.widget.CardView r5 = r4.cardAddBarcode
            r5.setVisibility(r0)
            android.os.Handler r5 = new android.os.Handler
            r5.<init>()
            com.spreadsheet.app.activities.ActivityBarcodeSheet$w r6 = new com.spreadsheet.app.activities.ActivityBarcodeSheet$w
            r6.<init>()
            r2 = 250(0xfa, double:1.235E-321)
            r5.postDelayed(r6, r2)
            java.lang.Boolean r5 = r4.f24133Z
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x0118
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r5 = r4.f24130W
            int r6 = r4.f24135b0
            java.lang.Object r5 = r5.get(r6)
            java.util.HashMap r5 = (java.util.HashMap) r5
            java.util.HashMap r6 = new java.util.HashMap
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.f24132Y
            r6.<init>(r0)
            r5.putAll(r6)
            goto L_0x0124
        L_0x0118:
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r5 = r4.f24130W
            java.util.HashMap r6 = new java.util.HashMap
            java.util.HashMap<java.lang.String, java.lang.String> r0 = r4.f24132Y
            r6.<init>(r0)
            r5.add(r6)
        L_0x0124:
            com.spreadsheet.app.Utils.h r5 = r4.f24143v
            java.lang.String r6 = "entriesCount"
            int r5 = r5.mo24223d(r6)
            int r5 = r5 + r1
            com.spreadsheet.app.Utils.h r0 = r4.f24143v
            r0.mo24228i(r6, r5)
            goto L_0x01bb
        L_0x0134:
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24138e0
            java.lang.String r0 = r4.f24110C
            r5.mo25647m(r0)
            e.e.a.a.g r5 = r4.f24144w
            int r5 = r5.sheetId
            r4.f24112E = r5
            e.e.a.a.e r5 = r4.f24145x
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.util.List<java.lang.String> r1 = r4.f24146y
            int r1 = r1.size()
            r0.append(r1)
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            r5.setLastSheetId(r6)
            java.util.List<java.lang.String> r5 = r4.f24146y
            int r5 = r5.size()
            r4.f24113F = r5
            android.widget.EditText r5 = r4.f24118K
            android.text.Editable r5 = r5.getText()
            java.lang.String r5 = r5.toString()
            e.e.a.a.e r6 = r4.f24145x
            int r6 = r6.getColCount()
            com.spreadsheet.app.Utils.C6873b.m33473k(r5, r6)
            e.e.a.d.g r5 = r4.f24142u
            r5.mo25662C(r4, r4)
            e.e.a.d.g r5 = r4.f24142u
            java.lang.String r6 = r4.f24110C
            int r0 = r4.f24112E
            java.util.List<java.lang.String> r1 = r4.f24109B
            org.json.JSONObject r0 = r5.mo25684v(r0, r2, r1)
            r5.mo25665c(r6, r0)
            goto L_0x025c
        L_0x0190:
            java.lang.Boolean r6 = r4.f24124Q
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x0204
            goto L_0x022f
        L_0x019a:
            com.spreadsheet.app.Utils.d r5 = r4.f24122O
            r5.mo24211a()
            androidx.swiperefreshlayout.widget.SwipeRefreshLayout r5 = r4.layoutPullToRefresh
            r5.setRefreshing(r2)
            android.widget.RelativeLayout r5 = r4.layoutProgress
            r5.setVisibility(r0)
            androidx.constraintlayout.widget.ConstraintLayout r5 = r4.layoutTryAgain
            r5.setVisibility(r0)
            e.e.a.a.g r5 = r4.f24144w
            java.util.List r5 = r5.getAllRowsList()
            r4.f24130W = r5
            androidx.recyclerview.widget.RecyclerView r5 = r4.recyclerBarcodes
            r5.setVisibility(r2)
        L_0x01bb:
            r4.m33547J0()
            goto L_0x025c
        L_0x01c0:
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24138e0
            java.lang.String r6 = r4.f24110C
            r5.mo25647m(r6)
            r4.m33542B0()
            goto L_0x025c
        L_0x01d0:
            com.spreadsheet.app.Utils.d r5 = r4.f24122O
            r5.mo24211a()
            r4.m33546G0()
            goto L_0x025c
        L_0x01da:
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            r4.f24113F = r2
            e.e.a.d.e r5 = r4.f24138e0
            java.lang.String r0 = r4.f24110C
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            int r2 = r4.f24113F
            r1.append(r2)
            r1.append(r6)
            java.lang.String r6 = r1.toString()
            r5.mo25643i(r0, r6)
            goto L_0x0204
        L_0x01f9:
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            e.e.a.d.e r5 = r4.f24138e0
            java.lang.String r6 = r4.f24110C
            r5.mo25647m(r6)
        L_0x0204:
            r4.m33543C0()
            goto L_0x025c
        L_0x0208:
            e.e.a.a.e r5 = r4.f24145x
            e.e.a.a.g r6 = r4.f24144w
            java.lang.String r6 = r6.updatedSpreadsheetTitle
            r5.setSheetName(r6)
            r4.mo24264L0()
            goto L_0x025c
        L_0x0215:
            com.spreadsheet.app.Utils.d r5 = r4.f24122O
            r5.mo24211a()
            e.e.a.a.g r5 = r4.f24144w
            r5.isUpdated = r1
            r4.finish()
            goto L_0x025c
        L_0x0222:
            com.spreadsheet.app.Utils.d r6 = r4.f24122O
            r6.mo24211a()
            java.lang.Boolean r6 = r4.f24124Q
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x0204
        L_0x022f:
            java.lang.Boolean r6 = java.lang.Boolean.FALSE
            r4.f24124Q = r6
            android.widget.RelativeLayout r6 = r4.layoutProgress
            r6.setVisibility(r0)
            com.spreadsheet.app.Utils.Fonts.CustomTextView r6 = r4.textAddRow
            r6.setVisibility(r2)
            android.widget.LinearLayout r6 = r4.layoutEmptyScreen
            r6.setVisibility(r2)
            android.widget.Toast r5 = android.widget.Toast.makeText(r4, r5, r2)
            r5.show()
            goto L_0x025c
        L_0x024a:
            e.e.a.d.d r5 = r4.f24140g0
            java.lang.String r6 = "DELETE_SPREADSHEET"
            r5.mo25638c(r6, r6)
            e.e.a.d.e r5 = r4.f24138e0
            e.e.a.a.e r6 = r4.f24145x
            java.lang.String r6 = r6.getSheetId()
            r5.mo25639d(r6)
        L_0x025c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityBarcodeSheet.mo24244s(java.lang.String, java.lang.String):void");
    }

    /* renamed from: w */
    public void mo24273w(HashMap<String, String> hashMap) {
        if (hashMap.get("$POS$") != null && !hashMap.get("$POS$").equalsIgnoreCase("")) {
            this.f24133Z = Boolean.FALSE;
            int parseInt = Integer.parseInt(hashMap.get("$POS$"));
            C0065b.C0066a aVar = new C0065b.C0066a(this);
            aVar.mo236g("Do you want to delete '" + hashMap.get("Barcode") + "' from the sheet?");
            aVar.mo240k("Yes", new C6906h(parseInt));
            aVar.mo237h("No", new C6907i(this));
            aVar.mo243n();
        }
    }

    /* renamed from: z0 */
    public void mo24274z0(int i, String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete " + str + "?");
        builder.setPositiveButton(getResources().getString(R.string.yes), new C6915q(i));
        builder.setNegativeButton(getResources().getString(R.string.no), new C6916r(this));
        builder.show();
    }
}
