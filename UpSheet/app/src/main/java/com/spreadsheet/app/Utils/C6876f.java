package com.spreadsheet.app.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.common.api.C1890f;
import com.google.android.gms.common.api.C2022m;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.C3279d;
import com.google.android.gms.location.C3281e;
import com.google.android.gms.location.C3284f;
import com.google.android.gms.location.C3287g;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.p063j.C3291a;
import p099d.p119h.p120e.C4901a;
import p153e.p174d.p176b.p190c.p192b.C5480c;
import p153e.p300e.p301a.p304c.C7573c;

/* renamed from: com.spreadsheet.app.Utils.f */
public class C6876f implements C1890f.C1892b, C1890f.C1893c, C3279d {

    /* renamed from: c */
    public C1890f f24039c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Context f24040d;

    /* renamed from: e */
    private Location f24041e;

    /* renamed from: f */
    C7573c f24042f;

    /* renamed from: g */
    public boolean f24043g = false;

    /* renamed from: h */
    public boolean f24044h = true;

    /* renamed from: i */
    public Status f24045i;

    /* renamed from: j */
    int f24046j = 6000;

    /* renamed from: k */
    int f24047k = (6000 / 2);

    /* renamed from: l */
    LocationRequest f24048l;

    /* renamed from: m */
    C6872a f24049m;

    /* renamed from: com.spreadsheet.app.Utils.f$a */
    class C6877a implements C2022m<C3287g> {
        C6877a() {
        }

        /* renamed from: b */
        public void mo7658a(C3287g gVar) {
            C6876f fVar;
            Status T = gVar.mo7289T();
            C6876f.this.f24045i = T;
            gVar.mo12411r0();
            int r0 = T.mo7367r0();
            if (r0 == 0) {
                if (C6876f.this.f24049m.mo24205b(new String[]{"android.permission.ACCESS_FINE_LOCATION"})) {
                    C6876f.this.mo24215c();
                    C6876f.this.mo24217f();
                    C6876f.this.f24042f.mo24235G();
                    return;
                }
                fVar = C6876f.this;
                if (fVar.f24043g) {
                    fVar.f24049m.mo24208e(new String[]{"android.permission.ACCESS_FINE_LOCATION"});
                    return;
                }
            } else if (r0 == 6) {
                fVar = C6876f.this;
                if (fVar.f24043g) {
                    try {
                        T.mo7363A0((Activity) fVar.f24040d, 79);
                        return;
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            } else {
                return;
            }
            fVar.f24042f.mo24240Q();
        }
    }

    public C6876f(Context context, C7573c cVar) {
        C6872a a = C6872a.m33458a();
        this.f24049m = a;
        this.f24040d = context;
        a.mo24206c(context);
        if (this.f24039c == null) {
            C1890f.C1891a aVar = new C1890f.C1891a(this.f24040d);
            aVar.mo7438c(this);
            aVar.mo7439d(this);
            aVar.mo7436a(C3281e.f15247c);
            aVar.mo7436a(C3291a.f15267c);
            aVar.mo7436a(C3291a.f15268d);
            this.f24039c = aVar.mo7440e();
        }
        this.f24042f = cVar;
        mo24214b();
    }

    /* renamed from: d */
    private void m33487d() {
        if (this.f24044h) {
            C3284f.C3285a aVar = new C3284f.C3285a();
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.mo12391y0(3600000);
            locationRequest.mo12390x0(60000);
            locationRequest.mo12392z0(102);
            aVar.mo12409a(locationRequest);
            C3281e.f15249e.mo12420a(this.f24039c, aVar.mo12410b()).mo7448e(new C6877a());
        }
    }

    /* renamed from: S0 */
    public void mo7554S0(C5480c cVar) {
        this.f24039c.mo7421h();
    }

    /* renamed from: U */
    public void mo7530U(int i) {
    }

    /* renamed from: b */
    public void mo24214b() {
        LocationRequest locationRequest = new LocationRequest();
        this.f24048l = locationRequest;
        locationRequest.mo12391y0((long) this.f24046j);
        this.f24048l.mo12390x0((long) this.f24047k);
        this.f24048l.mo12392z0(100);
    }

    /* renamed from: b0 */
    public void mo12406b0(Location location) {
        this.f24042f.mo24241b0(location);
    }

    /* renamed from: c */
    public void mo24215c() {
        if (C4901a.m25754a(this.f24040d, "android.permission.ACCESS_FINE_LOCATION") == 0 || C4901a.m25754a(this.f24040d, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Location a = C3281e.f15248d.mo12398a(this.f24039c);
            this.f24041e = a;
            if (a != null) {
                a.getLatitude();
                this.f24041e.getLongitude();
                this.f24042f.mo24238J(this.f24041e, "", "");
                return;
            }
            this.f24042f.mo24239L("No location found");
        }
    }

    /* renamed from: d0 */
    public void mo7531d0(Bundle bundle) {
        m33487d();
    }

    /* renamed from: e */
    public void mo24216e(boolean z) {
        this.f24043g = z;
        if (this.f24039c.mo7428p()) {
            m33487d();
        } else {
            this.f24039c.mo7419f();
        }
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: f */
    public void mo24217f() {
        C3281e.f15248d.mo12399b(this.f24039c, this.f24048l, this);
    }
}
