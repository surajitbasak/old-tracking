package com.spreadsheet.app.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.ArrayList;
import java.util.List;
import p099d.p119h.p120e.C4901a;
import p153e.p300e.p301a.p302a.C7565j;
import p153e.p300e.p301a.p304c.C7578h;

public class SubsheetsAdapter extends RecyclerView.C0503g<SubsheetsHolder> {

    /* renamed from: e */
    Context f24596e;

    /* renamed from: f */
    List<C7565j> f24597f = new ArrayList();

    /* renamed from: g */
    C7578h f24598g;

    /* renamed from: h */
    String f24599h;

    /* renamed from: i */
    C6879h f24600i;

    public class SubsheetsHolder extends RecyclerView.C0500d0 {
        @BindView(2131296386)
        CardView cardSubsheet;
        @BindView(2131296540)
        ImageView imageSubsheetMenu;
        @BindView(2131296567)
        ConstraintLayout layoutContent;
        @BindView(2131296586)
        LinearLayout layoutMargin;
        @BindView(2131296603)
        ConstraintLayout layoutSubsheet;
        @BindView(2131296901)
        CustomTextView textSubsheetName;

        public SubsheetsHolder(SubsheetsAdapter subsheetsAdapter, View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }
    }

    public class SubsheetsHolder_ViewBinding implements Unbinder {

        /* renamed from: a */
        private SubsheetsHolder f24601a;

        public SubsheetsHolder_ViewBinding(SubsheetsHolder subsheetsHolder, View view) {
            this.f24601a = subsheetsHolder;
            subsheetsHolder.textSubsheetName = (CustomTextView) C0642c.m3874c(view, R.id.text_subsheet_name, "field 'textSubsheetName'", CustomTextView.class);
            subsheetsHolder.imageSubsheetMenu = (ImageView) C0642c.m3874c(view, R.id.image_subsheet_menu, "field 'imageSubsheetMenu'", ImageView.class);
            subsheetsHolder.cardSubsheet = (CardView) C0642c.m3874c(view, R.id.card_subsheet, "field 'cardSubsheet'", CardView.class);
            subsheetsHolder.layoutSubsheet = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_subsheet, "field 'layoutSubsheet'", ConstraintLayout.class);
            subsheetsHolder.layoutMargin = (LinearLayout) C0642c.m3874c(view, R.id.layout_margin, "field 'layoutMargin'", LinearLayout.class);
            subsheetsHolder.layoutContent = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_content, "field 'layoutContent'", ConstraintLayout.class);
        }

        public void unbind() {
            SubsheetsHolder subsheetsHolder = this.f24601a;
            if (subsheetsHolder != null) {
                this.f24601a = null;
                subsheetsHolder.textSubsheetName = null;
                subsheetsHolder.imageSubsheetMenu = null;
                subsheetsHolder.cardSubsheet = null;
                subsheetsHolder.layoutSubsheet = null;
                subsheetsHolder.layoutMargin = null;
                subsheetsHolder.layoutContent = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SubsheetsAdapter$a */
    class C7074a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24602c;

        C7074a(int i) {
            this.f24602c = i;
        }

        public void onClick(View view) {
            if (SubsheetsAdapter.this.f24600i.mo24221a(C6873b.f24017j)) {
                SubsheetsAdapter subsheetsAdapter = SubsheetsAdapter.this;
                if (subsheetsAdapter.f24599h.equalsIgnoreCase(subsheetsAdapter.f24597f.get(this.f24602c).getSubSheetId())) {
                    return;
                }
            } else {
                int i = this.f24602c;
                if (i <= 1) {
                    SubsheetsAdapter subsheetsAdapter2 = SubsheetsAdapter.this;
                    if (subsheetsAdapter2.f24599h.equalsIgnoreCase(subsheetsAdapter2.f24597f.get(i).getSubSheetId())) {
                        return;
                    }
                } else {
                    SubsheetsAdapter.this.f24598g.mo24261E("Add more than 2 subsheets with UpSheet Premium");
                    return;
                }
            }
            SubsheetsAdapter subsheetsAdapter3 = SubsheetsAdapter.this;
            subsheetsAdapter3.f24599h = subsheetsAdapter3.f24597f.get(this.f24602c).getSubSheetId();
            SubsheetsAdapter.this.mo3298h();
            SubsheetsAdapter subsheetsAdapter4 = SubsheetsAdapter.this;
            subsheetsAdapter4.f24598g.mo24267h(Integer.parseInt(subsheetsAdapter4.f24597f.get(this.f24602c).getSubSheetId()), SubsheetsAdapter.this.f24597f.get(this.f24602c).getSubSheetName(), this.f24602c);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.SubsheetsAdapter$b */
    class C7075b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24604c;

        /* renamed from: com.spreadsheet.app.adapters.SubsheetsAdapter$b$a */
        class C7076a implements PopupMenu.OnMenuItemClickListener {
            C7076a() {
            }

            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_row_delete:
                        C7075b bVar = C7075b.this;
                        SubsheetsAdapter subsheetsAdapter = SubsheetsAdapter.this;
                        C7578h hVar = subsheetsAdapter.f24598g;
                        int parseInt = Integer.parseInt(subsheetsAdapter.f24597f.get(bVar.f24604c).getSubSheetId());
                        C7075b bVar2 = C7075b.this;
                        hVar.mo24266R(parseInt, SubsheetsAdapter.this.f24597f.get(bVar2.f24604c).getSubSheetName());
                        return true;
                    case R.id.menu_row_edit:
                        C7075b bVar3 = C7075b.this;
                        SubsheetsAdapter subsheetsAdapter2 = SubsheetsAdapter.this;
                        C7578h hVar2 = subsheetsAdapter2.f24598g;
                        int parseInt2 = Integer.parseInt(subsheetsAdapter2.f24597f.get(bVar3.f24604c).getSubSheetId());
                        C7075b bVar4 = C7075b.this;
                        hVar2.mo24265O(parseInt2, SubsheetsAdapter.this.f24597f.get(bVar4.f24604c).getSubSheetName());
                        return true;
                    default:
                        return true;
                }
            }
        }

        C7075b(int i) {
            this.f24604c = i;
        }

        public void onClick(View view) {
            PopupMenu popupMenu = new PopupMenu(SubsheetsAdapter.this.f24596e, view);
            popupMenu.getMenuInflater().inflate(R.menu.menu_spreadsheet, popupMenu.getMenu());
            popupMenu.getMenu().findItem(R.id.menu_row_share).setVisible(false);
            popupMenu.getMenu().findItem(R.id.menu_row_edit).setTitle("Rename");
            popupMenu.setOnMenuItemClickListener(new C7076a());
            popupMenu.show();
        }
    }

    public SubsheetsAdapter(Context context, List<C7565j> list, String str) {
        C6879h c = C6879h.m33499c();
        this.f24600i = c;
        this.f24596e = context;
        this.f24597f = list;
        this.f24598g = (C7578h) context;
        this.f24599h = str;
        c.mo24225f(context);
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24597f.size();
    }

    /* renamed from: u */
    public void mo3300j(SubsheetsHolder subsheetsHolder, int i) {
        int i2;
        ImageView imageView;
        subsheetsHolder.textSubsheetName.setText(this.f24597f.get(i).getSubSheetName());
        if (this.f24597f.get(i).getSubSheetId().equalsIgnoreCase(this.f24599h)) {
            subsheetsHolder.layoutSubsheet.setBackgroundColor(this.f24596e.getResources().getColor(R.color.primary_lite));
            subsheetsHolder.textSubsheetName.setTextColor(-1);
            imageView = subsheetsHolder.imageSubsheetMenu;
            i2 = C4901a.m25757d(this.f24596e, R.color.white);
        } else {
            subsheetsHolder.layoutSubsheet.setBackgroundColor(this.f24596e.getResources().getColor(R.color.white));
            subsheetsHolder.textSubsheetName.setTextColor(-16777216);
            imageView = subsheetsHolder.imageSubsheetMenu;
            i2 = C4901a.m25757d(this.f24596e, R.color.black);
        }
        imageView.setColorFilter(i2, PorterDuff.Mode.SRC_IN);
        subsheetsHolder.layoutSubsheet.setOnClickListener(new C7074a(i));
        subsheetsHolder.imageSubsheetMenu.setOnClickListener(new C7075b(i));
        if (i == this.f24597f.size() - 1) {
            subsheetsHolder.layoutContent.setPadding(0, 0, 16, 0);
        }
    }

    /* renamed from: v */
    public SubsheetsHolder mo3302l(ViewGroup viewGroup, int i) {
        return new SubsheetsHolder(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_subsheet, viewGroup, false));
    }
}
