package com.spreadsheet.app.Utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import androidx.core.app.C0329a;
import p099d.p119h.p120e.C4901a;

/* renamed from: com.spreadsheet.app.Utils.a */
public class C6872a {

    /* renamed from: b */
    private static C6872a f23971b = new C6872a();

    /* renamed from: a */
    private Context f23972a;

    /* renamed from: a */
    public static C6872a m33458a() {
        return f23971b;
    }

    /* renamed from: b */
    public boolean mo24205b(String[] strArr) {
        if (Build.VERSION.SDK_INT < 23 || this.f23972a == null || strArr == null) {
            return true;
        }
        for (String a : strArr) {
            if (C4901a.m25754a(this.f23972a, a) != 0) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: c */
    public void mo24206c(Context context) {
        this.f23972a = context;
    }

    /* renamed from: d */
    public boolean mo24207d() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f23972a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    /* renamed from: e */
    public void mo24208e(String[] strArr) {
        C0329a.m1736m((Activity) this.f23972a, strArr, 1);
    }
}
