package com.spreadsheet.app.Utils;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.MenuItem;
import com.spreadsheet.app.Utils.Fonts.C6871a;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import p153e.p300e.p301a.p302a.C7557b;
import p153e.p300e.p301a.p302a.C7566k;

/* renamed from: com.spreadsheet.app.Utils.b */
public class C6873b {

    /* renamed from: A */
    public static String f23973A = "fonts/Muli-Regular.ttf";

    /* renamed from: B */
    public static String f23974B = "fonts/Muli-Bold.ttf";

    /* renamed from: C */
    public static String f23975C = "fonts/Muli-SemiBold.ttf";

    /* renamed from: D */
    public static String f23976D = "Users";

    /* renamed from: E */
    public static String f23977E = "Configuration";

    /* renamed from: F */
    public static String f23978F = "Sheets";

    /* renamed from: G */
    public static String f23979G = "Columns";

    /* renamed from: H */
    public static String f23980H = "Feedback";

    /* renamed from: I */
    public static String f23981I = "AdFreePurchaseRecords";

    /* renamed from: J */
    public static String f23982J = "UpSheetPremiumSubs";

    /* renamed from: K */
    public static String f23983K = "UpSheetPurchaseOrders";

    /* renamed from: L */
    public static String f23984L = "All";

    /* renamed from: M */
    public static String f23985M = "LoggedIn";

    /* renamed from: N */
    public static String f23986N = "InputType";

    /* renamed from: O */
    public static String f23987O = "Text";

    /* renamed from: P */
    public static String f23988P = "LargeText";

    /* renamed from: Q */
    public static String f23989Q = "Number";

    /* renamed from: R */
    public static String f23990R = "Date";

    /* renamed from: S */
    public static String f23991S = "Time";

    /* renamed from: T */
    public static String f23992T = "Barcode";

    /* renamed from: U */
    public static String f23993U = "Dropdown";

    /* renamed from: V */
    public static String f23994V = "col_name";

    /* renamed from: W */
    public static String f23995W = "col_type";

    /* renamed from: X */
    public static String f23996X = "col_data";

    /* renamed from: Y */
    public static String f23997Y = "type";

    /* renamed from: Z */
    public static String f23998Z = "title";

    /* renamed from: a */
    public static String[] f23999a = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN"};

    /* renamed from: a0 */
    public static String f24000a0 = "message";

    /* renamed from: b */
    public static String[] f24001b = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    /* renamed from: b0 */
    public static String f24002b0 = "image";

    /* renamed from: c */
    public static String f24003c = "AIzaSyD12Mh5uDTRY9CxJdYliVCZjYscCeXORj8";

    /* renamed from: c0 */
    public static String f24004c0 = "package";

    /* renamed from: d */
    public static String f24005d = "34F2MFvy354";

    /* renamed from: d0 */
    public static String f24006d0 = "advertise_url";

    /* renamed from: e */
    public static String f24007e = "isRemoveAdsActive";

    /* renamed from: e0 */
    public static String f24008e0 = "rate";

    /* renamed from: f */
    public static String f24009f = "upsheet_premium";

    /* renamed from: f0 */
    public static String f24010f0 = "image";

    /* renamed from: g */
    public static String f24011g = "upsheet_premium_order_id";

    /* renamed from: g0 */
    public static String f24012g0 = "custom_sheet";

    /* renamed from: h */
    public static String f24013h = "upsheet_premium_purchase_token";

    /* renamed from: h0 */
    public static String f24014h0 = "contact_sheet";

    /* renamed from: i */
    public static String f24015i = "upsheet_premium_purchase_date";

    /* renamed from: i0 */
    public static String f24016i0 = "todo_sheet";

    /* renamed from: j */
    public static String f24017j = "isUpsheetpremiumActive";

    /* renamed from: j0 */
    public static String f24018j0 = "advertise";

    /* renamed from: k */
    public static String f24019k = "upsheetPremiumEndDate";

    /* renamed from: k0 */
    public static String f24020k0 = "https://sheets.googleapis.com/v4/spreadsheets/";

    /* renamed from: l */
    public static boolean f24021l = false;

    /* renamed from: m */
    public static String f24022m = "SpreadsheetApp";

    /* renamed from: n */
    public static String f24023n = "isLoggedIn";

    /* renamed from: o */
    public static String f24024o = "userId";

    /* renamed from: p */
    public static String f24025p = "userEmail";

    /* renamed from: q */
    public static String f24026q = "accountList";

    /* renamed from: r */
    public static String f24027r = "userFirstName";

    /* renamed from: s */
    public static String f24028s = "userLastName";

    /* renamed from: t */
    public static String f24029t = "userPhotoUrl";

    /* renamed from: u */
    public static String f24030u = "accessToken";

    /* renamed from: v */
    public static String f24031v = "refreshToken";

    /* renamed from: w */
    public static String f24032w = "expires_in";

    /* renamed from: x */
    public static String f24033x = "deviceToken";

    /* renamed from: y */
    public static String f24034y = "sheetCount";

    /* renamed from: z */
    public static String f24035z = "last_token_refresh_time";

    /* renamed from: a */
    public static void m33463a(Context context, MenuItem menuItem) {
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), f23973A);
        SpannableString spannableString = new SpannableString(menuItem.getTitle());
        spannableString.setSpan(new C6871a("", createFromAsset), 0, spannableString.length(), 18);
        menuItem.setTitle(spannableString);
    }

    /* renamed from: b */
    public static boolean m33464b(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        int i = instance.get(5);
        int i2 = instance.get(2);
        int i3 = instance.get(1);
        Calendar instance2 = Calendar.getInstance();
        if (instance2.get(1) <= i3) {
            if (instance2.get(1) < i3) {
                return true;
            }
            if (instance2.get(2) <= i2 && (instance2.get(2) < i2 || instance2.get(2) != i2 || instance2.get(5) <= i)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    public static String m33465c(float f, float f2, float f3) {
        String hexString = Integer.toHexString((int) (f * 255.0f));
        String hexString2 = Integer.toHexString((int) (f3 * 255.0f));
        String hexString3 = Integer.toHexString((int) (f2 * 255.0f));
        if (hexString3.length() == 1) {
            hexString3 = "0" + hexString3;
        }
        return "#" + hexString + hexString2 + hexString3;
    }

    /* renamed from: d */
    public static String m33466d(String str, int i) {
        return str + "!A1:" + f23999a[i - 1];
    }

    /* renamed from: e */
    public static int m33467e(String str, int i, int i2) {
        Paint paint = new Paint();
        paint.setTextSize((float) i);
        return (i2 / 2) - ((int) (paint.measureText(str) / 2.0f));
    }

    /* renamed from: f */
    public static List<C7557b> m33468f(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                C7557b bVar = new C7557b();
                bVar.setColNum(0);
                bVar.setColumnId("");
                bVar.setColumnName((String) jSONArray.getJSONObject(i).get(f23994V));
                bVar.setColumnType((String) jSONArray.getJSONObject(i).get(f23995W));
                bVar.setColumnData((String) jSONArray.getJSONObject(i).get(f23996X));
                arrayList.add(bVar);
                i++;
            } catch (Exception unused) {
            }
        }
        return arrayList;
    }

    /* renamed from: g */
    public static List<C7566k> m33469g(List<C7566k> list) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(list);
        Collections.sort(arrayList2, new C6880i());
        for (int i = 0; i < arrayList2.size(); i++) {
            C7566k kVar = (C7566k) arrayList2.get(i);
            if (m33464b(kVar.getTaskDate())) {
                arrayList.add(kVar);
            }
        }
        Collections.sort(list, new C6880i());
        Collections.reverse(list);
        for (int i2 = 0; i2 < list.size(); i2++) {
            C7566k kVar2 = list.get(i2);
            if (!m33464b(kVar2.getTaskDate())) {
                arrayList.add(kVar2);
            }
        }
        return arrayList;
    }

    /* renamed from: h */
    public static String m33470h() {
        return new SimpleDateFormat("dd MMM yyyy hh:mm:ss a").format(Calendar.getInstance().getTime());
    }

    /* renamed from: i */
    public static String m33471i(String str) {
        Date date;
        SimpleDateFormat simpleDateFormat;
        if (str == null) {
            return "";
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(Long.parseLong(str));
        Calendar instance2 = Calendar.getInstance();
        long timeInMillis = (instance2.getTimeInMillis() - instance.getTimeInMillis()) / 3600000;
        int i = instance2.get(11);
        if (instance.get(5) == instance2.get(5) && instance.get(2) == instance2.get(2) && instance.get(1) == instance2.get(1)) {
            simpleDateFormat = new SimpleDateFormat("hh:mm aa");
            date = new Date(Long.parseLong(str));
        } else if (timeInMillis <= ((long) (i + 24))) {
            return "Yesterday";
        } else {
            simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
            date = new Date(Long.parseLong(str));
        }
        return simpleDateFormat.format(date);
    }

    /* renamed from: j */
    public static String m33472j(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        Calendar instance2 = Calendar.getInstance();
        long timeInMillis = (instance2.getTimeInMillis() - instance.getTimeInMillis()) / 3600000;
        return (instance.get(5) == instance2.get(5) && instance.get(2) == instance2.get(2) && instance.get(1) == instance2.get(1)) ? "Today" : (timeInMillis >= 0 || timeInMillis < -24) ? (timeInMillis <= 0 || timeInMillis > ((long) (instance2.get(11) + 24))) ? new SimpleDateFormat("dd MMM yyyy").format(new Date(j)) : "Yesterday" : "Tomorrow";
    }

    /* renamed from: k */
    public static String m33473k(String str, int i) {
        return str + "!A1:" + f23999a[i - 1] + "1";
    }

    /* renamed from: l */
    public static List<HashMap<String, String>> m33474l(Context context) {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("Type", f23986N);
        hashMap.put("Value", "Input Type");
        hashMap.put("Access", "Free");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("Type", f23987O);
        hashMap2.put("Value", "Text");
        hashMap2.put("Access", "Free");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("Type", f23988P);
        hashMap3.put("Value", "Large Text");
        hashMap3.put("Access", "Free");
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("Type", f23989Q);
        hashMap4.put("Value", "Number");
        hashMap4.put("Access", "Free");
        arrayList.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("Type", f23990R);
        hashMap5.put("Value", "Date");
        hashMap5.put("Access", "Free");
        arrayList.add(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put("Type", f23991S);
        hashMap6.put("Value", "Time");
        hashMap6.put("Access", "Free");
        arrayList.add(hashMap6);
        HashMap hashMap7 = new HashMap();
        hashMap7.put("Type", f23992T);
        hashMap7.put("Value", "Barcode");
        hashMap7.put("Access", "Free");
        arrayList.add(hashMap7);
        HashMap hashMap8 = new HashMap();
        hashMap8.put("Type", f23993U);
        hashMap8.put("Value", "Dropdown");
        hashMap8.put("Access", "Free");
        arrayList.add(hashMap8);
        HashMap hashMap9 = new HashMap();
        hashMap9.put("Type", "Location");
        hashMap9.put("Value", "Location");
        hashMap9.put("Access", "Free");
        arrayList.add(hashMap9);
        HashMap hashMap10 = new HashMap();
        hashMap10.put("Type", "Mobile");
        hashMap10.put("Value", "Mobile");
        hashMap10.put("Access", "Free");
        arrayList.add(hashMap10);
        HashMap hashMap11 = new HashMap();
        hashMap11.put("Type", "Email");
        hashMap11.put("Value", "Email");
        hashMap11.put("Access", "Free");
        arrayList.add(hashMap11);
        HashMap hashMap12 = new HashMap();
        hashMap12.put("Type", "Website");
        hashMap12.put("Value", "Website");
        hashMap12.put("Access", "Free");
        arrayList.add(hashMap12);
        HashMap hashMap13 = new HashMap();
        hashMap13.put("Type", "AutoTimeStamp");
        hashMap13.put("Value", "Auto Timestamp");
        hashMap13.put("Access", "Free");
        arrayList.add(hashMap13);
        return arrayList;
    }

    /* renamed from: m */
    public static JSONArray m33475m(List<HashMap<String, String>> list) {
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (i < list.size()) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(f23995W, list.get(i).get(f23995W));
                jSONObject.put(f23994V, list.get(i).get(f23994V));
                jSONObject.put(f23996X, list.get(i).get(f23996X));
                jSONArray.put(jSONObject);
                i++;
            } catch (Exception unused) {
            }
        }
        return jSONArray;
    }

    /* renamed from: n */
    public static int m33476n(String str) {
        int i = 0;
        while (true) {
            String[] strArr = f24001b;
            if (i >= strArr.length) {
                return 1;
            }
            if (str.equalsIgnoreCase(strArr[i])) {
                return i;
            }
            i++;
        }
    }

    /* renamed from: o */
    public static String m33477o(String str) {
        return str != null ? str.replaceAll("[()\\s-]+", "") : "";
    }

    /* renamed from: p */
    public static String m33478p(String str, int i, int i2) {
        StringBuilder sb;
        String str2;
        switch (i) {
            case 2:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":B";
                break;
            case 3:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":C";
                break;
            case 4:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":D";
                break;
            case 5:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":E";
                break;
            case 6:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":F";
                break;
            case 7:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":G";
                break;
            case 8:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":H";
                break;
            case 9:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":I";
                break;
            case 10:
                sb = new StringBuilder();
                sb.append(str);
                sb.append("!A");
                sb.append(i2);
                str2 = ":J";
                break;
            default:
                return "";
        }
        sb.append(str2);
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: q */
    public static boolean m33479q(String str) {
        Pattern compile = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (str == null) {
            return false;
        }
        return compile.matcher(str).matches();
    }

    /* renamed from: r */
    public static boolean m33480r(long j, long j2) {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        if (j == 0) {
            return false;
        }
        long j3 = (timeInMillis - j) / 1000;
        long j4 = j3 / 60;
        return j3 >= j2;
    }
}
