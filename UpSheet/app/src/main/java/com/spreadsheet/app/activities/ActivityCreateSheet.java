package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.chip.Chip;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.p067v4.Sheets;
import com.google.firebase.database.C4524a;
import com.google.firebase.database.C4527b;
import com.google.firebase.database.C4529d;
import com.google.firebase.database.C4534g;
import com.google.firebase.database.C4549p;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7104g;
import com.spreadsheet.app.adapters.C7105h;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import p099d.p119h.p120e.C4901a;
import p153e.p174d.p176b.p190c.p217h.C6386c;
import p153e.p174d.p176b.p190c.p217h.C6394h;
import p153e.p300e.p301a.p302a.C7557b;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivityCreateSheet extends C0067c implements View.OnClickListener, AdapterView.OnItemSelectedListener, C7598a {

    /* renamed from: A */
    String f24227A = "";

    /* renamed from: B */
    String f24228B = "";

    /* renamed from: C */
    int f24229C = 0;

    /* renamed from: D */
    C6879h f24230D = C6879h.m33499c();

    /* renamed from: E */
    C4529d f24231E;

    /* renamed from: F */
    C4529d f24232F;

    /* renamed from: G */
    List<View> f24233G = new ArrayList();

    /* renamed from: H */
    List<String> f24234H;

    /* renamed from: I */
    List<C7557b> f24235I;

    /* renamed from: J */
    List<String> f24236J;

    /* renamed from: K */
    List<Integer> f24237K;

    /* renamed from: L */
    C6872a f24238L;

    /* renamed from: M */
    C7562g f24239M;

    /* renamed from: N */
    C7590d f24240N;

    /* renamed from: O */
    int f24241O;

    /* renamed from: P */
    C6875d f24242P;

    /* renamed from: Q */
    C7591e f24243Q;

    /* renamed from: R */
    C7596g f24244R;

    /* renamed from: S */
    C7560e f24245S;

    /* renamed from: T */
    C7560e f24246T;
    @BindView(2131296348)
    Button buttonAddColumn;
    @BindView(2131296356)
    Button buttonCreateSheet;
    @BindView(2131296361)
    Button buttonRemoveColumn;
    @BindView(2131296461)
    EditText editSheetName;
    @BindView(2131296565)
    LinearLayout layoutColumns;
    @BindView(2131296783)
    Spinner spinnerColumns;
    @BindView(2131296929)
    Toolbar toolbarCreateSheet;

    /* renamed from: u */
    List<String> f24247u = new ArrayList();

    /* renamed from: v */
    List<HashMap<String, String>> f24248v = new ArrayList();

    /* renamed from: w */
    List<HashMap<String, String>> f24249w = new ArrayList();

    /* renamed from: x */
    String f24250x = "";

    /* renamed from: y */
    String f24251y = "";

    /* renamed from: z */
    int f24252z = 0;

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$a */
    class C6941a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24253c;

        C6941a(Dialog dialog) {
            this.f24253c = dialog;
        }

        public void onClick(View view) {
            this.f24253c.dismiss();
            C6873b.f24021l = true;
            ActivityCreateSheet.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$b */
    class C6942b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24255c;

        C6942b(ActivityCreateSheet activityCreateSheet, Dialog dialog) {
            this.f24255c = dialog;
        }

        public void onClick(View view) {
            this.f24255c.dismiss();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$c */
    class C6943c implements C6386c<Void> {

        /* renamed from: a */
        final /* synthetic */ C7560e f24256a;

        C6943c(C7560e eVar) {
            this.f24256a = eVar;
        }

        /* renamed from: b */
        public void mo7656b(C6394h<Void> hVar) {
            if (!hVar.mo23043r()) {
                return;
            }
            if (!ActivityCreateSheet.this.getIntent().hasExtra("spreadsheet")) {
                ActivityCreateSheet activityCreateSheet = ActivityCreateSheet.this;
                activityCreateSheet.f24246T = this.f24256a;
                activityCreateSheet.m33607H0();
            } else if (ActivityCreateSheet.this.m33604E0()) {
                ActivityCreateSheet activityCreateSheet2 = ActivityCreateSheet.this;
                activityCreateSheet2.f24241O = 0;
                activityCreateSheet2.m33619U0(activityCreateSheet2.f24236J.get(0));
            } else {
                ActivityCreateSheet activityCreateSheet3 = ActivityCreateSheet.this;
                C7591e eVar = activityCreateSheet3.f24243Q;
                String sheetId = activityCreateSheet3.f24245S.getSheetId();
                ActivityCreateSheet activityCreateSheet4 = ActivityCreateSheet.this;
                C7562g gVar = activityCreateSheet4.f24239M;
                eVar.mo25644j(sheetId, C7562g.KEY_SPREADSHEET_NAME, activityCreateSheet4.f24250x);
                ActivityCreateSheet activityCreateSheet5 = ActivityCreateSheet.this;
                activityCreateSheet5.f24243Q.mo25647m(activityCreateSheet5.f24227A);
                ActivityCreateSheet activityCreateSheet6 = ActivityCreateSheet.this;
                activityCreateSheet6.f24239M.isUpdated = true;
                activityCreateSheet6.f24242P.mo24211a();
                Intent intent = new Intent();
                intent.putExtra("sheet", ActivityCreateSheet.this.f24245S);
                intent.putExtra("sheetname", ActivityCreateSheet.this.editSheetName.getText().toString());
                intent.putExtra("isCreated", true);
                ActivityCreateSheet.this.setResult(-1, intent);
                ActivityCreateSheet.this.finish();
            }
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$d */
    class C6944d implements View.OnClickListener {
        C6944d() {
        }

        public void onClick(View view) {
            ActivityCreateSheet.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$e */
    class C6945e implements AdapterView.OnItemSelectedListener {
        C6945e() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            ActivityCreateSheet activityCreateSheet = ActivityCreateSheet.this;
            activityCreateSheet.f24251y = activityCreateSheet.f24247u.get(i);
            ActivityCreateSheet activityCreateSheet2 = ActivityCreateSheet.this;
            activityCreateSheet2.m33617S0(activityCreateSheet2.f24251y);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$f */
    class C6946f implements C4549p {

        /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$f$a */
        class C6947a implements Comparator<C7557b> {
            C6947a(C6946f fVar) {
            }

            /* renamed from: a */
            public int compare(C7557b bVar, C7557b bVar2) {
                return String.valueOf(bVar.getColNum()).compareTo(String.valueOf(bVar2.getColNum()));
            }
        }

        C6946f() {
        }

        /* renamed from: a */
        public void mo18610a(C4527b bVar) {
            ActivityCreateSheet.this.f24242P.mo24211a();
        }

        /* renamed from: b */
        public void mo18611b(C4524a aVar) {
            ActivityCreateSheet.this.f24235I.clear();
            for (C4524a h : aVar.mo18573d()) {
                ActivityCreateSheet.this.f24235I.add((C7557b) h.mo18577h(C7557b.class));
            }
            Collections.sort(ActivityCreateSheet.this.f24235I, new C6947a(this));
            ActivityCreateSheet.this.f24242P.mo24211a();
            ActivityCreateSheet activityCreateSheet = ActivityCreateSheet.this;
            activityCreateSheet.m33617S0(ActivityCreateSheet.this.f24235I.size() + "");
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$g */
    class C6948g implements TextWatcher {

        /* renamed from: c */
        final /* synthetic */ FlexboxLayout f24261c;

        /* renamed from: d */
        final /* synthetic */ EditText f24262d;

        C6948g(FlexboxLayout flexboxLayout, EditText editText) {
            this.f24261c = flexboxLayout;
            this.f24262d = editText;
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String charSequence2 = charSequence.toString();
            if (charSequence2.contains(",") && charSequence2.length() > 1) {
                ActivityCreateSheet.this.m33602C0(charSequence2.replace(",", ""), this.f24261c, this.f24262d);
            } else if (!charSequence2.contains(",")) {
                return;
            }
            this.f24262d.setText("");
            ActivityCreateSheet.this.m33618T0(this.f24262d, this.f24261c);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$h */
    class C6949h implements TextView.OnEditorActionListener {

        /* renamed from: a */
        final /* synthetic */ EditText f24264a;

        /* renamed from: b */
        final /* synthetic */ FlexboxLayout f24265b;

        C6949h(EditText editText, FlexboxLayout flexboxLayout) {
            this.f24264a = editText;
            this.f24265b = flexboxLayout;
        }

        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (i != 6) {
                return false;
            }
            String obj = this.f24264a.getText().toString();
            if (!obj.contains(",") || obj.length() <= 1) {
                if (!obj.contains(",")) {
                    if (obj.equals("")) {
                        return false;
                    }
                }
                this.f24264a.setText("");
                ActivityCreateSheet.this.m33618T0(this.f24264a, this.f24265b);
                return false;
            }
            obj = obj.replace(",", "");
            ActivityCreateSheet.this.m33602C0(obj, this.f24265b, this.f24264a);
            this.f24264a.setText("");
            ActivityCreateSheet.this.m33618T0(this.f24264a, this.f24265b);
            return false;
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$i */
    class C6950i implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ View f24267c;

        C6950i(View view) {
            this.f24267c = view;
        }

        public void onClick(View view) {
            ActivityCreateSheet.this.m33615Q0(this.f24267c);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$j */
    class C6951j implements AdapterView.OnItemSelectedListener {

        /* renamed from: c */
        final /* synthetic */ FlexboxLayout f24269c;

        C6951j(FlexboxLayout flexboxLayout) {
            this.f24269c = flexboxLayout;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
            int i2;
            FlexboxLayout flexboxLayout;
            if (((String) ActivityCreateSheet.this.f24248v.get(i).get("Type")).equals(C6873b.f23993U)) {
                flexboxLayout = this.f24269c;
                i2 = 0;
            } else {
                flexboxLayout = this.f24269c;
                i2 = 8;
            }
            flexboxLayout.setVisibility(i2);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$k */
    class C6952k implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ FlexboxLayout f24271c;

        /* renamed from: d */
        final /* synthetic */ Chip f24272d;

        /* renamed from: e */
        final /* synthetic */ EditText f24273e;

        C6952k(FlexboxLayout flexboxLayout, Chip chip, EditText editText) {
            this.f24271c = flexboxLayout;
            this.f24272d = chip;
            this.f24273e = editText;
        }

        public void onClick(View view) {
            this.f24271c.removeView(this.f24272d);
            ActivityCreateSheet.this.m33618T0(this.f24273e, this.f24271c);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityCreateSheet$l */
    class C6953l implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24275c;

        C6953l(ActivityCreateSheet activityCreateSheet, Dialog dialog) {
            this.f24275c = dialog;
        }

        public void onClick(View view) {
            this.f24275c.dismiss();
        }
    }

    public ActivityCreateSheet() {
        new ArrayList();
        this.f24234H = new ArrayList();
        this.f24235I = new ArrayList();
        this.f24236J = new ArrayList();
        this.f24237K = new ArrayList();
        this.f24238L = C6872a.m33458a();
        this.f24239M = C7562g.getInstance();
        this.f24240N = C7590d.m35558a();
        this.f24241O = -1;
        this.f24242P = C6875d.m33482b();
        this.f24243Q = C7591e.m35564f();
        this.f24244R = C7596g.m35606z();
    }

    /* access modifiers changed from: private */
    /* renamed from: C0 */
    public void m33602C0(String str, FlexboxLayout flexboxLayout, EditText editText) {
        Chip chip = new Chip(this);
        chip.setText(str);
        chip.setCloseIconVisible(true);
        chip.setTextColor(-1);
        chip.setCloseIconTint(ColorStateList.valueOf(C4901a.m25757d(this, R.color.white)));
        chip.setChipBackgroundColor(ColorStateList.valueOf(C4901a.m25757d(this, R.color.primary_lite)));
        chip.setOnCloseIconClickListener(new C6952k(flexboxLayout, chip, editText));
        flexboxLayout.addView(chip, flexboxLayout.getChildCount() - 1);
        FlexboxLayout.C1647a aVar = (FlexboxLayout.C1647a) chip.getLayoutParams();
        aVar.height = -2;
        aVar.width = -2;
        aVar.setMargins(5, 0, 5, 0);
        chip.setLayoutParams(aVar);
    }

    /* renamed from: D0 */
    private void m33603D0() {
        View inflate = getLayoutInflater().inflate(R.layout.row_add_columns, (ViewGroup) null, false);
        EditText editText = (EditText) inflate.findViewById(R.id.edit_column_title);
        AppCompatSpinner appCompatSpinner = (AppCompatSpinner) inflate.findViewById(R.id.spinner_column_type);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.image_remove_column);
        EditText editText2 = (EditText) inflate.findViewById(R.id.edit_options);
        FlexboxLayout flexboxLayout = (FlexboxLayout) inflate.findViewById(R.id.flexbox_options);
        editText2.addTextChangedListener(new C6948g(flexboxLayout, editText2));
        editText2.setOnEditorActionListener(new C6949h(editText2, flexboxLayout));
        if (getIntent().hasExtra("spreadsheet")) {
            imageView.setVisibility(View.GONE);
        } else if (this.layoutColumns.getChildCount() >= 2) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setOnClickListener(new C6950i(inflate));
        }
        editText.setSingleLine(true);
        appCompatSpinner.setAdapter((SpinnerAdapter) new C7105h(this, this.f24248v));
        appCompatSpinner.setOnItemSelectedListener(new C6951j(flexboxLayout));
        this.layoutColumns.addView(inflate);
        mo24312V0();
    }

    /* access modifiers changed from: private */
    /* renamed from: E0 */
    public boolean m33604E0() {
        if (this.f24235I.size() != this.f24249w.size()) {
            return true;
        }
        for (int i = 0; i < this.f24249w.size(); i++) {
            if (!((String) this.f24249w.get(i).get(C6873b.f23994V)).equals(this.f24235I.get(i).getColumnName()) || !((String) this.f24249w.get(i).get(C6873b.f23995W)).equals(this.f24235I.get(i).getColumnType()) || !((String) this.f24249w.get(i).get(C6873b.f23996X)).equals(this.f24235I.get(i).getColumnData())) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: F0 */
    private boolean m33605F0() {
        return !((C7560e) getIntent().getSerializableExtra("spreadsheet")).sheetName.equals(this.f24250x);
    }

    /* renamed from: G0 */
    private boolean m33606G0() {
        Toast makeText;
        this.f24234H.clear();
        this.f24249w.clear();
        int i = 0;
        boolean z = true;
        while (true) {
            if (i >= this.f24233G.size()) {
                break;
            }
            View view = this.f24233G.get(i);
            CustomTextView customTextView = (CustomTextView) view.findViewById(R.id.text_column_number);
            EditText editText = (EditText) view.findViewById(R.id.edit_column_title);
            Spinner spinner = (Spinner) view.findViewById(R.id.spinner_column_type);
            FlexboxLayout flexboxLayout = (FlexboxLayout) view.findViewById(R.id.flexbox_options);
            String str = "";
            if (editText.getText().toString().equals(str)) {
                z = false;
            }
            if (spinner.getSelectedItemPosition() == 0) {
                z = false;
            }
            if (z) {
                if (((String) this.f24248v.get(spinner.getSelectedItemPosition()).get("Type")).equals(C6873b.f23993U) && flexboxLayout.getChildCount() <= 1) {
                    makeText = Toast.makeText(this, "Enter options for dropdown for column " + i, 0);
                    break;
                }
                if (flexboxLayout.getChildCount() > 1) {
                    for (int i2 = 0; i2 < flexboxLayout.getChildCount() - 1; i2++) {
                        Chip chip = (Chip) flexboxLayout.getChildAt(i2);
                        if (i2 == 0) {
                            str = chip.getText().toString();
                        } else {
                            str = str + "," + chip.getText().toString();
                        }
                    }
                }
                HashMap hashMap = new HashMap();
                hashMap.put(C6873b.f23996X, str);
                hashMap.put(C6873b.f23994V, editText.getText().toString());
                hashMap.put(C6873b.f23995W, this.f24248v.get(spinner.getSelectedItemPosition()).get("Type"));
                this.f24249w.add(hashMap);
                this.f24234H.add(editText.getText().toString());
                i++;
            } else {
                makeText = Toast.makeText(this, i + R.string.enter_all_details_for_column, 0);
                break;
            }
        }
        makeText.show();
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: H0 */
    public void m33607H0() {
        m33611M0(this.f24239M.sheet1);
        C7596g gVar = this.f24244R;
        gVar.mo25666d(this.f24227A, gVar.mo25686x(this.f24252z, 0, this.f24234H));
    }

    /* renamed from: I0 */
    private void m33608I0() {
        if (this.f24238L.mo24207d()) {
            C7596g gVar = this.f24244R;
            gVar.mo25670h(gVar.mo25680r(this.f24250x));
            return;
        }
        this.f24242P.mo24211a();
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* renamed from: K0 */
    private void m33609K0() {
        if (this.f24238L.mo24207d()) {
            this.f24244R.mo25679q(this.f24227A);
            return;
        }
        this.f24242P.mo24211a();
        Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
    }

    /* renamed from: L0 */
    private void m33610L0() {
        List<C7557b> columnsList = this.f24245S.getColumnsList();
        this.f24235I = columnsList;
        if (columnsList.size() > 0) {
            m33617S0(this.f24235I.size() + "");
        } else if (this.f24238L.mo24207d()) {
            this.f24232F.mo18590g(this.f24245S.getSheetId()).mo18606b(new C6946f());
        } else {
            this.f24242P.mo24211a();
            Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: M0 */
    private String m33611M0(String str) {
        String str2;
        StringBuilder sb;
        switch (this.f24249w.size()) {
            case 2:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:B1";
                break;
            case 3:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:C1";
                break;
            case 4:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:D1";
                break;
            case 5:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:E1";
                break;
            case 6:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:F1";
                break;
            case 7:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:G1";
                break;
            case 8:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:H1";
                break;
            case 9:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:I1";
                break;
            case 10:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:J1";
                break;
            case 11:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:K1";
                break;
            case 12:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:L1";
                break;
            case 13:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:M1";
                break;
            case 14:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:N1";
                break;
            case 15:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:O1";
                break;
            case 16:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:P1";
                break;
            case 17:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:Q1";
                break;
            case 18:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:R1";
                break;
            case 19:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:S1";
                break;
            case 20:
                sb = new StringBuilder();
                sb.append(str);
                str2 = "!A1:T1";
                break;
            default:
                return "";
        }
        sb.append(str2);
        return sb.toString();
    }

    /* renamed from: N0 */
    private void m33612N0() {
        this.f24238L.mo24206c(this);
        this.f24228B = this.f24230D.mo24222b(C6873b.f24024o);
        this.f24247u.add(String.valueOf(getResources().getString(R.string.number_of_columns)));
        this.f24247u.add("1");
        this.f24247u.add("2");
        this.f24247u.add("3");
        this.f24247u.add("4");
        this.f24247u.add("5");
        this.f24247u.add("6");
        this.f24247u.add("7");
        this.f24247u.add("8");
        this.f24247u.add("9");
        this.f24247u.add("10");
        this.f24247u.add("11");
        this.f24247u.add("12");
        this.f24247u.add("13");
        this.f24247u.add("14");
        this.f24247u.add("15");
        this.f24247u.add("16");
        this.f24247u.add("17");
        this.f24247u.add("18");
        this.f24247u.add("19");
        this.f24247u.add("20");
        this.f24248v = C6873b.m33474l(this);
        this.f24240N.mo25637b(this);
        this.f24232F = C4534g.m24100b().mo18599d(C6873b.f23979G);
        this.f24231E = C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.toolbarCreateSheet.setTitle((int) R.string.new_spreadsheet);
        this.toolbarCreateSheet.setTitleTextColor(getResources().getColor(R.color.black));
        this.toolbarCreateSheet.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarCreateSheet.setNavigationOnClickListener(new C6944d());
        new ArrayAdapter(this, 17367048, this.f24247u);
        this.spinnerColumns.setAdapter(new C7104g(this, this.f24247u));
        this.spinnerColumns.setOnItemSelectedListener(new C6945e());
        m33613O0(this.f24230D.mo24222b(C6873b.f24025p));
        this.f24245S = new C7560e();
        if (getIntent().hasExtra("spreadsheet")) {
            C7560e eVar = (C7560e) getIntent().getSerializableExtra("spreadsheet");
            this.f24245S = eVar;
            this.editSheetName.setText(eVar.getSheetName());
            EditText editText = this.editSheetName;
            editText.setSelection(editText.length());
            this.buttonCreateSheet.setText(R.string.update_spreadsheet);
            this.spinnerColumns.setVisibility(8);
            this.f24227A = this.f24245S.getSheetId();
            this.f24242P.mo24213d("");
            this.toolbarCreateSheet.setTitle((int) R.string.update_spreadsheet);
            m33614P0();
        } else {
            m33603D0();
            m33603D0();
        }
        this.buttonAddColumn.setVisibility(0);
        this.buttonRemoveColumn.setVisibility(8);
        this.spinnerColumns.setVisibility(8);
    }

    /* renamed from: O0 */
    private void m33613O0(String str) {
        try {
            new Sheets.Builder(AndroidHttp.newCompatibleTransport(), JacksonFactory.getDefaultInstance(), new GoogleCredential().setAccessToken(this.f24230D.mo24222b(C6873b.f24030u))).setApplicationName("Android spreadsheet client").build();
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }
    }

    /* renamed from: P0 */
    private void m33614P0() {
        this.f24236J.clear();
        this.f24237K.clear();
        for (int i = 0; i < this.f24239M.sheetIdList.size(); i++) {
            this.f24236J.add(this.f24239M.sheetTitleList.get(i));
            this.f24237K.add(this.f24239M.sheetIdList.get(i));
        }
        m33610L0();
    }

    /* access modifiers changed from: private */
    /* renamed from: Q0 */
    public void m33615Q0(View view) {
        this.layoutColumns.removeView(view);
        mo24312V0();
    }

    /* renamed from: R0 */
    private void m33616R0() {
        for (int i = 0; i < this.f24233G.size(); i++) {
            View view = this.f24233G.get(i);
            CustomTextView customTextView = (CustomTextView) view.findViewById(R.id.text_column_number);
            Spinner spinner = (Spinner) view.findViewById(R.id.spinner_column_type);
            EditText editText = (EditText) view.findViewById(R.id.edit_options);
            FlexboxLayout flexboxLayout = (FlexboxLayout) view.findViewById(R.id.flexbox_options);
            C7557b bVar = this.f24235I.get(i);
            ((EditText) view.findViewById(R.id.edit_column_title)).setText(bVar.getColumnName());
            String replace = bVar.getColumnType().replace(" ", "");
            int i2 = 0;
            for (int i3 = 0; i3 < this.f24248v.size(); i3++) {
                if (((String) this.f24248v.get(i3).get("Type")).equalsIgnoreCase(replace)) {
                    i2 = i3;
                }
            }
            spinner.setSelection(i2);
            new ArrayList();
            String columnData = bVar.getColumnData();
            if (columnData != null && columnData.contains(",")) {
                String[] split = columnData.split(",");
                for (String C0 : split) {
                    m33602C0(C0, flexboxLayout, editText);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: S0 */
    public void m33617S0(String str) {
        if (str.equals(String.valueOf(getResources().getString(R.string.number_of_columns)))) {
            this.layoutColumns.removeAllViews();
            this.f24233G.clear();
        } else {
            this.layoutColumns.removeAllViews();
            this.f24233G.clear();
            int parseInt = Integer.parseInt(str);
            this.f24229C = parseInt;
            for (int i = 0; i < parseInt; i++) {
                m33603D0();
            }
        }
        if (getIntent().hasExtra("spreadsheet")) {
            m33616R0();
        }
        this.f24242P.mo24211a();
    }

    /* access modifiers changed from: private */
    /* renamed from: T0 */
    public void m33618T0(EditText editText, FlexboxLayout flexboxLayout) {
        editText.setHint(flexboxLayout.getChildCount() > 1 ? "" : "Add options here");
    }

    /* access modifiers changed from: private */
    /* renamed from: U0 */
    public void m33619U0(String str) {
        String M0 = m33611M0(str);
        C7596g gVar = this.f24244R;
        String str2 = this.f24227A;
        gVar.mo25668f(str2, M0, gVar.mo25675m(str2, M0, this.f24234H));
    }

    /* renamed from: W0 */
    private void m33620W0() {
        this.f24240N.mo25638c("UPDATE_SPREADSHEET", "UPDATE_SPREADSHEET");
        Calendar instance = Calendar.getInstance();
        String str = instance.getTimeInMillis() + "";
        int size = this.f24233G.size();
        this.f24229C = size;
        this.f24245S.setColCount(size);
        String str2 = getIntent().hasExtra("Pro") ? "CustomPro" : "Custom";
        JSONArray m = C6873b.m33475m(this.f24249w);
        C7560e eVar = new C7560e(this.f24227A, this.f24250x, this.f24229C, str, "0", str2, false, "", "", m.toString());
        eVar.setColumnsList(C6873b.m33468f(m));
        this.f24243Q.mo25646l(this.f24227A, C7562g.KEY_COL_LIST, C6873b.m33468f(m));
        this.f24243Q.mo25645k(this.f24227A, C7562g.KEY_COL_LIST, m.toString());
        this.f24231E.mo18590g(this.f24228B).mo18590g(this.f24227A).mo18595k(eVar).mo23027b(new C6943c(eVar));
    }

    /* renamed from: H */
    public void mo24237H(String str, String str2) {
        String[] split = str.split(":");
        str2.hashCode();
        char c = 65535;
        switch (str2.hashCode()) {
            case -1788871435:
                if (str2.equals("updateSpreadSheetTitle")) {
                    c = 0;
                    break;
                }
                break;
            case -1422526087:
                if (str2.equals("addRow")) {
                    c = 1;
                    break;
                }
                break;
            case -1363989200:
                if (str2.equals("createSpreadsheet")) {
                    c = 2;
                    break;
                }
                break;
            case -452548289:
                if (str2.equals("getAllSheets")) {
                    c = 3;
                    break;
                }
                break;
            case -358721489:
                if (str2.equals("deleteRow")) {
                    c = 4;
                    break;
                }
                break;
            case -351206292:
                if (str2.equals("addHeaderRow")) {
                    c = 5;
                    break;
                }
                break;
            case 323754608:
                if (str2.equals("formatRows")) {
                    c = 6;
                    break;
                }
                break;
            case 1594339684:
                if (str2.equals("formatHeader")) {
                    c = 7;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                this.f24242P.mo24211a();
                break;
        }
        if (split[0].equals("com.android.volley.NoConnectionError")) {
            Toast.makeText(this, getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: J0 */
    public void mo24311J0(String str) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_free_limits);
        dialog.getWindow().setLayout(-2, -2);
        if (!isFinishing()) {
            dialog.show();
        }
        ((TextView) dialog.findViewById(R.id.text_free_limits_dialog_msg)).setText(str);
        ((Button) dialog.findViewById(R.id.button_free_limits_dialog_close)).setOnClickListener(new C6953l(this, dialog));
        ((Button) dialog.findViewById(R.id.button_free_limits_go_premium)).setOnClickListener(new C6941a(dialog));
        ((ImageView) dialog.findViewById(R.id.image_close_free_limits_dialog)).setOnClickListener(new C6942b(this, dialog));
    }

    /* renamed from: V0 */
    public void mo24312V0() {
        this.f24233G.clear();
        for (int i = 0; i < this.layoutColumns.getChildCount(); i++) {
            this.f24233G.add(this.layoutColumns.getChildAt(i));
        }
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_create_sheet);
        ButterKnife.bind((Activity) this);
        this.f24230D.mo24225f(this);
        this.f24242P.mo24212c(this);
        this.f24243Q.mo25642h(this, this);
        this.f24244R.mo25662C(this, this);
        m33612N0();
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @OnClick({2131296356, 2131296348, 2131296361})
    public void onViewClicked(View view) {
        int i;
        Toast toast;
        int id = view.getId();
        if (id == R.id.button_add_column) {
            if (this.f24230D.mo24221a(C6873b.f24017j)) {
                if (this.layoutColumns.getChildCount() >= 20) {
                    i = R.string.you_can_add_only_20_columns;
                }
            } else if (this.layoutColumns.getChildCount() >= 10) {
                mo24311J0("Add upto 20 columns with UpSheet Premium");
                return;
            }
            m33603D0();
            return;
        } else if (id == R.id.button_create_sheet) {
            String obj = this.editSheetName.getText().toString();
            this.f24250x = obj;
            if (obj.equals("")) {
                i = R.string.enter_sheet_name;
            } else if (this.f24233G.size() <= 1) {
                i = R.string.there_should_be_atlease_2_columns;
            } else if (m33606G0()) {
                this.f24242P.mo24213d("");
                if (!getIntent().hasExtra("spreadsheet")) {
                    m33608I0();
                    return;
                } else if (!this.f24238L.mo24207d()) {
                    this.f24242P.mo24211a();
                    toast = Toast.makeText(this, getResources().getString(R.string.internet_check), 0);
                    toast.show();
                } else if (m33605F0()) {
                    C7596g gVar = this.f24244R;
                    gVar.mo25664R(this.f24227A, gVar.mo25660A(this.f24250x));
                    return;
                } else if (m33604E0()) {
                    m33620W0();
                    return;
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("sheet", this.f24245S);
                    intent.putExtra("isCreated", true);
                    setResult(-1, intent);
                    finish();
                    return;
                }
            } else {
                return;
            }
        } else {
            return;
        }
        toast = Toast.makeText(this, i, 0);
        toast.show();
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d7, code lost:
        r9.putExtra("spreadsheet", r8.f24246T);
        startActivity(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00df, code lost:
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x012e, code lost:
        m33619U0(r8.f24236J.get(r8.f24241O));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x015e, code lost:
        r9.putExtra("sheet", r8.f24245S);
        r9.putExtra("sheetname", r8.editSheetName.getText().toString());
        r9.putExtra("isCreated", true);
        setResult(-1, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0178, code lost:
        m33620W0();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return;
     */
    /* renamed from: s */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24244s(String r9, String r10) {
        /*
            r8 = this;
            r10.hashCode()
            int r9 = r10.hashCode()
            r0 = -1
            r1 = 0
            r2 = 1
            switch(r9) {
                case -1788871435: goto L_0x0051;
                case -1422526087: goto L_0x0046;
                case -1363989200: goto L_0x003b;
                case -452548289: goto L_0x0030;
                case -351206292: goto L_0x0025;
                case 323754608: goto L_0x001a;
                case 635300543: goto L_0x000f;
                default: goto L_0x000d;
            }
        L_0x000d:
            r9 = -1
            goto L_0x005b
        L_0x000f:
            java.lang.String r9 = "addFormattedRow"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x0018
            goto L_0x000d
        L_0x0018:
            r9 = 6
            goto L_0x005b
        L_0x001a:
            java.lang.String r9 = "formatRows"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x0023
            goto L_0x000d
        L_0x0023:
            r9 = 5
            goto L_0x005b
        L_0x0025:
            java.lang.String r9 = "addHeaderRow"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x002e
            goto L_0x000d
        L_0x002e:
            r9 = 4
            goto L_0x005b
        L_0x0030:
            java.lang.String r9 = "getAllSheets"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x0039
            goto L_0x000d
        L_0x0039:
            r9 = 3
            goto L_0x005b
        L_0x003b:
            java.lang.String r9 = "createSpreadsheet"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x0044
            goto L_0x000d
        L_0x0044:
            r9 = 2
            goto L_0x005b
        L_0x0046:
            java.lang.String r9 = "addRow"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x004f
            goto L_0x000d
        L_0x004f:
            r9 = 1
            goto L_0x005b
        L_0x0051:
            java.lang.String r9 = "updateSpreadSheetTitle"
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x005a
            goto L_0x000d
        L_0x005a:
            r9 = 0
        L_0x005b:
            java.lang.String r10 = "isCreated"
            java.lang.String r3 = "sheetname"
            java.lang.String r4 = "sheet"
            r5 = 2131820868(0x7f110144, float:1.9274463E38)
            java.lang.String r6 = "spreadsheet"
            r7 = 2131820867(0x7f110143, float:1.9274461E38)
            switch(r9) {
                case 0: goto L_0x0178;
                case 1: goto L_0x0101;
                case 2: goto L_0x00ee;
                case 3: goto L_0x00e9;
                case 4: goto L_0x00e4;
                case 5: goto L_0x00ba;
                case 6: goto L_0x006e;
                default: goto L_0x006c;
            }
        L_0x006c:
            goto L_0x017b
        L_0x006e:
            int r9 = r8.f24241O
            if (r9 != r0) goto L_0x0090
            android.widget.Toast r9 = android.widget.Toast.makeText(r8, r7, r1)
            r9.show()
            e.e.a.a.g r9 = r8.f24239M
            java.util.List r9 = r9.getSheetsList()
            e.e.a.a.e r10 = r8.f24246T
            r9.add(r10)
            e.e.a.a.g r9 = r8.f24239M
            r9.isUpdated = r2
            android.content.Intent r9 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivitySheetDetails> r10 = com.spreadsheet.app.activities.ActivitySheetDetails.class
            r9.<init>(r8, r10)
            goto L_0x00d7
        L_0x0090:
            int r9 = r9 + r2
            r8.f24241O = r9
            java.util.List<java.lang.String> r6 = r8.f24236J
            int r6 = r6.size()
            if (r9 >= r6) goto L_0x009d
            goto L_0x012e
        L_0x009d:
            android.widget.Toast r9 = android.widget.Toast.makeText(r8, r5, r1)
            r9.show()
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r9 = r8.f24249w
            org.json.JSONArray r9 = com.spreadsheet.app.Utils.C6873b.m33475m(r9)
            e.e.a.a.e r1 = r8.f24245S
            java.util.List r9 = com.spreadsheet.app.Utils.C6873b.m33468f(r9)
            r1.setColumnsList(r9)
            android.content.Intent r9 = new android.content.Intent
            r9.<init>()
            goto L_0x015e
        L_0x00ba:
            android.widget.Toast r9 = android.widget.Toast.makeText(r8, r7, r1)
            r9.show()
            e.e.a.a.g r9 = r8.f24239M
            java.util.List r9 = r9.getSheetsList()
            e.e.a.a.e r10 = r8.f24246T
            r9.add(r10)
            e.e.a.a.g r9 = r8.f24239M
            r9.isUpdated = r2
            android.content.Intent r9 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivitySheetDetails> r10 = com.spreadsheet.app.activities.ActivitySheetDetails.class
            r9.<init>(r8, r10)
        L_0x00d7:
            e.e.a.a.e r10 = r8.f24246T
            r9.putExtra(r6, r10)
            r8.startActivity(r9)
        L_0x00df:
            r8.finish()
            goto L_0x017b
        L_0x00e4:
            r8.m33609K0()
            goto L_0x017b
        L_0x00e9:
            r8.m33614P0()
            goto L_0x017b
        L_0x00ee:
            e.e.a.d.d r9 = r8.f24240N
            java.lang.String r10 = "CREATE_SPREADSHEET"
            r9.mo25638c(r10, r10)
            e.e.a.a.g r9 = r8.f24239M
            java.lang.String r10 = r9.spreadsheetId
            r8.f24227A = r10
            int r9 = r9.sheetId
            r8.f24252z = r9
            goto L_0x0178
        L_0x0101:
            int r9 = r8.f24241O
            if (r9 != r0) goto L_0x0123
            android.widget.Toast r9 = android.widget.Toast.makeText(r8, r7, r1)
            r9.show()
            e.e.a.a.g r9 = r8.f24239M
            java.util.List r9 = r9.getSheetsList()
            e.e.a.a.e r10 = r8.f24246T
            r9.add(r10)
            e.e.a.a.g r9 = r8.f24239M
            r9.isUpdated = r2
            android.content.Intent r9 = new android.content.Intent
            java.lang.Class<com.spreadsheet.app.activities.ActivitySheetDetails> r10 = com.spreadsheet.app.activities.ActivitySheetDetails.class
            r9.<init>(r8, r10)
            goto L_0x00d7
        L_0x0123:
            int r9 = r9 + r2
            r8.f24241O = r9
            java.util.List<java.lang.String> r6 = r8.f24236J
            int r6 = r6.size()
            if (r9 >= r6) goto L_0x013c
        L_0x012e:
            java.util.List<java.lang.String> r9 = r8.f24236J
            int r10 = r8.f24241O
            java.lang.Object r9 = r9.get(r10)
            java.lang.String r9 = (java.lang.String) r9
            r8.m33619U0(r9)
            goto L_0x017b
        L_0x013c:
            android.widget.Toast r9 = android.widget.Toast.makeText(r8, r5, r1)
            r9.show()
            java.util.List<java.util.HashMap<java.lang.String, java.lang.String>> r9 = r8.f24249w
            org.json.JSONArray r9 = com.spreadsheet.app.Utils.C6873b.m33475m(r9)
            e.e.a.a.e r1 = r8.f24245S
            java.util.List r9 = com.spreadsheet.app.Utils.C6873b.m33468f(r9)
            r1.setColumnsList(r9)
            e.e.a.a.g r9 = r8.f24239M
            java.util.List<java.lang.String> r1 = r8.f24234H
            r9.setHeaderColumns(r1)
            android.content.Intent r9 = new android.content.Intent
            r9.<init>()
        L_0x015e:
            e.e.a.a.e r1 = r8.f24245S
            r9.putExtra(r4, r1)
            android.widget.EditText r1 = r8.editSheetName
            android.text.Editable r1 = r1.getText()
            java.lang.String r1 = r1.toString()
            r9.putExtra(r3, r1)
            r9.putExtra(r10, r2)
            r8.setResult(r0, r9)
            goto L_0x00df
        L_0x0178:
            r8.m33620W0()
        L_0x017b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.spreadsheet.app.activities.ActivityCreateSheet.mo24244s(java.lang.String, java.lang.String):void");
    }
}
