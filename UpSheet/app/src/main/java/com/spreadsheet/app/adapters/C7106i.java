package com.spreadsheet.app.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import java.util.List;
import p153e.p300e.p301a.p302a.C7566k;
import p153e.p300e.p301a.p304c.C7579i;

/* renamed from: com.spreadsheet.app.adapters.i */
public class C7106i extends RecyclerView.C0503g<C7110c> {

    /* renamed from: e */
    Context f24694e;

    /* renamed from: f */
    List<C7566k> f24695f;

    /* renamed from: g */
    C7579i f24696g;

    /* renamed from: com.spreadsheet.app.adapters.i$a */
    class C7107a implements CompoundButton.OnCheckedChangeListener {

        /* renamed from: a */
        final /* synthetic */ C7566k f24697a;

        /* renamed from: b */
        final /* synthetic */ C7110c f24698b;

        C7107a(C7566k kVar, C7110c cVar) {
            this.f24697a = kVar;
            this.f24698b = cVar;
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Resources resources;
            int i;
            C7106i.this.f24696g.mo24369l(Boolean.valueOf(z), this.f24697a);
            CustomTextView customTextView = this.f24698b.f24705u;
            if (z) {
                resources = C7106i.this.f24694e.getResources();
                i = R.color.text_color_grey;
            } else {
                resources = C7106i.this.f24694e.getResources();
                i = R.color.text_color_dark;
            }
            customTextView.setTextColor(resources.getColor(i));
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.i$b */
    class C7108b implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ C7566k f24700c;

        /* renamed from: d */
        final /* synthetic */ int f24701d;

        /* renamed from: com.spreadsheet.app.adapters.i$b$a */
        class C7109a implements PopupMenu.OnMenuItemClickListener {
            C7109a() {
            }

            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_task_delete:
                        C7108b bVar = C7108b.this;
                        C7106i.this.f24696g.mo24363A(bVar.f24700c);
                        return true;
                    case R.id.menu_task_edit:
                        C7108b bVar2 = C7108b.this;
                        C7106i.this.f24696g.mo24364P(bVar2.f24701d, bVar2.f24700c);
                        return true;
                    default:
                        return true;
                }
            }
        }

        C7108b(C7566k kVar, int i) {
            this.f24700c = kVar;
            this.f24701d = i;
        }

        public void onClick(View view) {
            PopupMenu popupMenu = new PopupMenu(C7106i.this.f24694e, view);
            popupMenu.getMenuInflater().inflate(R.menu.menu_task_row, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new C7109a());
            popupMenu.show();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.i$c */
    public class C7110c extends RecyclerView.C0500d0 {

        /* renamed from: t */
        CheckBox f24704t;

        /* renamed from: u */
        CustomTextView f24705u;

        /* renamed from: v */
        ImageView f24706v;

        public C7110c(C7106i iVar, View view) {
            super(view);
            this.f24704t = (CheckBox) view.findViewById(R.id.checkbox_task);
            this.f24705u = (CustomTextView) view.findViewById(R.id.text_row_task);
            this.f24706v = (ImageView) view.findViewById(R.id.image_menu_task);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout_header);
            CustomTextView customTextView = (CustomTextView) view.findViewById(R.id.header_task);
        }
    }

    public C7106i(Context context, List<C7566k> list, C7579i iVar) {
        this.f24694e = context;
        this.f24695f = list;
        this.f24696g = iVar;
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24695f.size();
    }

    /* renamed from: u */
    public void mo3300j(C7110c cVar, int i) {
        boolean z;
        CheckBox checkBox;
        C7566k kVar = this.f24695f.get(i);
        cVar.f24705u.setText(kVar.getTaskName());
        if (kVar.isDone()) {
            cVar.f24705u.setTextColor(this.f24694e.getResources().getColor(R.color.text_color_grey));
            checkBox = cVar.f24704t;
            z = true;
        } else {
            checkBox = cVar.f24704t;
            z = false;
        }
        checkBox.setChecked(z);
        cVar.f24704t.setOnCheckedChangeListener(new C7107a(kVar, cVar));
        cVar.f24706v.setOnClickListener(new C7108b(kVar, i));
    }

    /* renamed from: v */
    public C7110c mo3302l(ViewGroup viewGroup, int i) {
        return new C7110c(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_task, viewGroup, false));
    }
}
