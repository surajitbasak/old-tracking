package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class ActivityAddRow_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityAddRow f24103a;

    /* renamed from: b */
    private View f24104b;

    /* renamed from: c */
    private View f24105c;

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow_ViewBinding$a */
    class C6895a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityAddRow f24106e;

        C6895a(ActivityAddRow_ViewBinding activityAddRow_ViewBinding, ActivityAddRow activityAddRow) {
            this.f24106e = activityAddRow;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24106e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityAddRow_ViewBinding$b */
    class C6896b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityAddRow f24107e;

        C6896b(ActivityAddRow_ViewBinding activityAddRow_ViewBinding, ActivityAddRow activityAddRow) {
            this.f24107e = activityAddRow;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24107e.onViewClicked(view);
        }
    }

    public ActivityAddRow_ViewBinding(ActivityAddRow activityAddRow, View view) {
        this.f24103a = activityAddRow;
        activityAddRow.toolbarAddRow = (Toolbar) C0642c.m3874c(view, R.id.toolbar_add_row, "field 'toolbarAddRow'", Toolbar.class);
        activityAddRow.layoutInputs = (LinearLayout) C0642c.m3874c(view, R.id.layout_inputs, "field 'layoutInputs'", LinearLayout.class);
        View b = C0642c.m3873b(view, R.id.button_submit, "field 'buttonSubmit' and method 'onViewClicked'");
        activityAddRow.buttonSubmit = (Button) C0642c.m3872a(b, R.id.button_submit, "field 'buttonSubmit'", Button.class);
        this.f24104b = b;
        b.setOnClickListener(new C6895a(this, activityAddRow));
        View b2 = C0642c.m3873b(view, R.id.btn_try_again, "field 'btnTryAgain' and method 'onViewClicked'");
        activityAddRow.btnTryAgain = (CustomTextView) C0642c.m3872a(b2, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        this.f24105c = b2;
        b2.setOnClickListener(new C6896b(this, activityAddRow));
        activityAddRow.layoutTryAgain = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", ConstraintLayout.class);
    }

    public void unbind() {
        ActivityAddRow activityAddRow = this.f24103a;
        if (activityAddRow != null) {
            this.f24103a = null;
            activityAddRow.toolbarAddRow = null;
            activityAddRow.layoutInputs = null;
            activityAddRow.buttonSubmit = null;
            activityAddRow.btnTryAgain = null;
            activityAddRow.layoutTryAgain = null;
            this.f24104b.setOnClickListener((View.OnClickListener) null);
            this.f24104b = null;
            this.f24105c.setOnClickListener((View.OnClickListener) null);
            this.f24105c = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
