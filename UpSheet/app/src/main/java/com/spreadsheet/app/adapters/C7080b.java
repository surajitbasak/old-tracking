package com.spreadsheet.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.spreadsheet.app.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p153e.p300e.p301a.p304c.C7572b;

/* renamed from: com.spreadsheet.app.adapters.b */
public class C7080b extends RecyclerView.C0503g<C7083b> implements Filterable {

    /* renamed from: e */
    Context f24615e;

    /* renamed from: f */
    List<HashMap<String, String>> f24616f = new ArrayList();

    /* renamed from: g */
    List<HashMap<String, String>> f24617g = new ArrayList();

    /* renamed from: h */
    C7572b f24618h;

    /* renamed from: i */
    C7084c f24619i;

    /* renamed from: com.spreadsheet.app.adapters.b$a */
    class C7081a implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ int f24620c;

        /* renamed from: com.spreadsheet.app.adapters.b$a$a */
        class C7082a implements PopupMenu.OnMenuItemClickListener {
            C7082a() {
            }

            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_barcode_delete:
                        C7081a aVar = C7081a.this;
                        C7080b bVar = C7080b.this;
                        bVar.f24618h.mo24273w(bVar.f24617g.get(aVar.f24620c));
                        return true;
                    case R.id.menu_barcode_edit:
                        C7081a aVar2 = C7081a.this;
                        C7080b bVar2 = C7080b.this;
                        bVar2.f24618h.mo24268o(bVar2.f24617g.get(aVar2.f24620c));
                        return true;
                    default:
                        return true;
                }
            }
        }

        C7081a(int i) {
            this.f24620c = i;
        }

        public void onClick(View view) {
            PopupMenu popupMenu = new PopupMenu(C7080b.this.f24615e, view);
            popupMenu.getMenuInflater().inflate(R.menu.popup_menu_barcode, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new C7082a());
            popupMenu.show();
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.b$b */
    public class C7083b extends RecyclerView.C0500d0 {

        /* renamed from: t */
        TextView f24623t;

        /* renamed from: u */
        TextView f24624u;

        /* renamed from: v */
        TextView f24625v;

        /* renamed from: w */
        ImageView f24626w;

        public C7083b(C7080b bVar, View view) {
            super(view);
            this.f24623t = (TextView) view.findViewById(R.id.text_barcode);
            this.f24624u = (TextView) view.findViewById(R.id.text_quantity);
            this.f24625v = (TextView) view.findViewById(R.id.text_date);
            this.f24626w = (ImageView) view.findViewById(R.id.menu_barcode);
        }
    }

    /* renamed from: com.spreadsheet.app.adapters.b$c */
    public class C7084c extends Filter {
        public C7084c() {
        }

        /* access modifiers changed from: protected */
        public FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            List<HashMap<String, String>> list = C7080b.this.f24616f;
            ArrayList arrayList = new ArrayList();
            String lowerCase = charSequence.toString().toLowerCase();
            for (int i = 0; i < list.size(); i++) {
                HashMap hashMap = list.get(i);
                if (((String) hashMap.get("Barcode")).toLowerCase().contains(lowerCase)) {
                    arrayList.add(hashMap);
                }
            }
            filterResults.count = arrayList.size();
            filterResults.values = arrayList;
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence charSequence, FilterResults filterResults) {
            C7080b bVar = C7080b.this;
            bVar.f24617g = (List) filterResults.values;
            bVar.mo3298h();
        }
    }

    public C7080b(Context context, List<HashMap<String, String>> list, C7572b bVar) {
        this.f24615e = context;
        this.f24616f = list;
        this.f24617g = list;
        this.f24618h = bVar;
        this.f24619i = new C7084c();
    }

    /* renamed from: c */
    public int mo3293c() {
        return this.f24617g.size();
    }

    public Filter getFilter() {
        return this.f24619i;
    }

    /* renamed from: u */
    public void mo3300j(C7083b bVar, int i) {
        HashMap hashMap = this.f24617g.get(i);
        hashMap.keySet().toArray();
        bVar.f24623t.setText((CharSequence) hashMap.get("Barcode"));
        bVar.f24624u.setText((CharSequence) hashMap.get("Quantity"));
        bVar.f24625v.setText((CharSequence) hashMap.get("Date"));
        bVar.f24626w.setOnClickListener(new C7081a(i));
    }

    /* renamed from: v */
    public C7083b mo3302l(ViewGroup viewGroup, int i) {
        return new C7083b(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_barcode, viewGroup, false));
    }
}
