package com.spreadsheet.app.activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.p002c.C0641b;
import butterknife.p002c.C0642c;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;

public class ActivityTodoDetails_ViewBinding implements Unbinder {

    /* renamed from: a */
    private ActivityTodoDetails f24404a;

    /* renamed from: b */
    private View f24405b;

    /* renamed from: c */
    private View f24406c;

    /* renamed from: d */
    private View f24407d;

    /* renamed from: e */
    private View f24408e;

    /* renamed from: f */
    private View f24409f;

    /* renamed from: g */
    private View f24410g;

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$a */
    class C7002a extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24411e;

        C7002a(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24411e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24411e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$b */
    class C7003b extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24412e;

        C7003b(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24412e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24412e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$c */
    class C7004c extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24413e;

        C7004c(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24413e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24413e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$d */
    class C7005d extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24414e;

        C7005d(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24414e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24414e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$e */
    class C7006e extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24415e;

        C7006e(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24415e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24415e.onViewClicked(view);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityTodoDetails_ViewBinding$f */
    class C7007f extends C0641b {

        /* renamed from: e */
        final /* synthetic */ ActivityTodoDetails f24416e;

        C7007f(ActivityTodoDetails_ViewBinding activityTodoDetails_ViewBinding, ActivityTodoDetails activityTodoDetails) {
            this.f24416e = activityTodoDetails;
        }

        /* renamed from: a */
        public void mo4084a(View view) {
            this.f24416e.onViewClicked(view);
        }
    }

    public ActivityTodoDetails_ViewBinding(ActivityTodoDetails activityTodoDetails, View view) {
        this.f24404a = activityTodoDetails;
        activityTodoDetails.toolbarTodoDetail = (Toolbar) C0642c.m3874c(view, R.id.toolbar_todo_detail, "field 'toolbarTodoDetail'", Toolbar.class);
        View b = C0642c.m3873b(view, R.id.text_add_task, "field 'textAddTask' and method 'onViewClicked'");
        activityTodoDetails.textAddTask = (CustomTextView) C0642c.m3872a(b, R.id.text_add_task, "field 'textAddTask'", CustomTextView.class);
        this.f24405b = b;
        b.setOnClickListener(new C7002a(this, activityTodoDetails));
        activityTodoDetails.recylerTask = (RecyclerView) C0642c.m3874c(view, R.id.recyler_task, "field 'recylerTask'", RecyclerView.class);
        activityTodoDetails.relativeEmptyScreen = (RelativeLayout) C0642c.m3874c(view, R.id.relative_empty_screen, "field 'relativeEmptyScreen'", RelativeLayout.class);
        View b2 = C0642c.m3873b(view, R.id.btn_try_again, "field 'btnTryAgain' and method 'onViewClicked'");
        activityTodoDetails.btnTryAgain = (CustomTextView) C0642c.m3872a(b2, R.id.btn_try_again, "field 'btnTryAgain'", CustomTextView.class);
        this.f24406c = b2;
        b2.setOnClickListener(new C7003b(this, activityTodoDetails));
        activityTodoDetails.layoutTryAgain = (ConstraintLayout) C0642c.m3874c(view, R.id.layout_try_again, "field 'layoutTryAgain'", ConstraintLayout.class);
        View b3 = C0642c.m3873b(view, R.id.button_add_task, "field 'buttonAddTask' and method 'onViewClicked'");
        activityTodoDetails.buttonAddTask = (ImageView) C0642c.m3872a(b3, R.id.button_add_task, "field 'buttonAddTask'", ImageView.class);
        this.f24407d = b3;
        b3.setOnClickListener(new C7004c(this, activityTodoDetails));
        View b4 = C0642c.m3873b(view, R.id.image_close_card, "field 'imageCloseCard' and method 'onViewClicked'");
        activityTodoDetails.imageCloseCard = (ImageView) C0642c.m3872a(b4, R.id.image_close_card, "field 'imageCloseCard'", ImageView.class);
        this.f24408e = b4;
        b4.setOnClickListener(new C7005d(this, activityTodoDetails));
        activityTodoDetails.layoutAddProgress = (RelativeLayout) C0642c.m3874c(view, R.id.layout_add_progress, "field 'layoutAddProgress'", RelativeLayout.class);
        activityTodoDetails.cardAddTask = (CardView) C0642c.m3874c(view, R.id.card_add_task, "field 'cardAddTask'", CardView.class);
        View b5 = C0642c.m3873b(view, R.id.image_todo_menu, "field 'imageTodoMenu' and method 'onViewClicked'");
        activityTodoDetails.imageTodoMenu = (ImageView) C0642c.m3872a(b5, R.id.image_todo_menu, "field 'imageTodoMenu'", ImageView.class);
        this.f24409f = b5;
        b5.setOnClickListener(new C7006e(this, activityTodoDetails));
        View b6 = C0642c.m3873b(view, R.id.button_submit_task, "method 'onViewClicked'");
        this.f24410g = b6;
        b6.setOnClickListener(new C7007f(this, activityTodoDetails));
    }

    public void unbind() {
        ActivityTodoDetails activityTodoDetails = this.f24404a;
        if (activityTodoDetails != null) {
            this.f24404a = null;
            activityTodoDetails.toolbarTodoDetail = null;
            activityTodoDetails.textAddTask = null;
            activityTodoDetails.recylerTask = null;
            activityTodoDetails.relativeEmptyScreen = null;
            activityTodoDetails.btnTryAgain = null;
            activityTodoDetails.layoutTryAgain = null;
            activityTodoDetails.buttonAddTask = null;
            activityTodoDetails.imageCloseCard = null;
            activityTodoDetails.layoutAddProgress = null;
            activityTodoDetails.cardAddTask = null;
            activityTodoDetails.imageTodoMenu = null;
            this.f24405b.setOnClickListener((View.OnClickListener) null);
            this.f24405b = null;
            this.f24406c.setOnClickListener((View.OnClickListener) null);
            this.f24406c = null;
            this.f24407d.setOnClickListener((View.OnClickListener) null);
            this.f24407d = null;
            this.f24408e.setOnClickListener((View.OnClickListener) null);
            this.f24408e = null;
            this.f24409f.setOnClickListener((View.OnClickListener) null);
            this.f24409f = null;
            this.f24410g.setOnClickListener((View.OnClickListener) null);
            this.f24410g = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
