package com.spreadsheet.app.Utils;

import java.util.Comparator;
import p153e.p300e.p301a.p302a.C7560e;

/* renamed from: com.spreadsheet.app.Utils.c */
public class C6874c implements Comparator<C7560e> {
    /* renamed from: a */
    public int compare(C7560e eVar, C7560e eVar2) {
        String date = eVar.getDate();
        String date2 = eVar2.getDate();
        if (date == null || date2 == null) {
            return 0;
        }
        return date.compareTo(date2);
    }
}
