package com.spreadsheet.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.C0065b;
import androidx.appcompat.app.C0067c;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.firebase.database.C4534g;
import com.spreadsheet.app.R;
import com.spreadsheet.app.Utils.C6872a;
import com.spreadsheet.app.Utils.C6873b;
import com.spreadsheet.app.Utils.C6875d;
import com.spreadsheet.app.Utils.C6879h;
import com.spreadsheet.app.Utils.Fonts.CustomTextView;
import com.spreadsheet.app.adapters.C7085c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import p007cn.pedant.SweetAlert.SweetAlertDialog;
import p153e.p300e.p301a.p302a.C7556a;
import p153e.p300e.p301a.p302a.C7560e;
import p153e.p300e.p301a.p302a.C7562g;
import p153e.p300e.p301a.p304c.C7574d;
import p153e.p300e.p301a.p304c.C7580j;
import p153e.p300e.p301a.p305d.C7581a;
import p153e.p300e.p301a.p305d.C7582b;
import p153e.p300e.p301a.p305d.C7590d;
import p153e.p300e.p301a.p305d.C7591e;
import p153e.p300e.p301a.p305d.C7596g;
import p153e.p300e.p301a.p306e.C7598a;

public class ActivityContacts extends C0067c implements C7574d, View.OnClickListener, C7598a, C7580j {

    /* renamed from: A */
    CustomTextView f24191A;

    /* renamed from: B */
    CustomTextView f24192B;

    /* renamed from: C */
    Dialog f24193C;

    /* renamed from: D */
    private String f24194D = "";
    /* access modifiers changed from: private */

    /* renamed from: E */
    public C7560e f24195E;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public String f24196F = "";
    /* access modifiers changed from: private */

    /* renamed from: G */
    public C6875d f24197G = C6875d.m33482b();

    /* renamed from: H */
    private C7582b f24198H = C7582b.m35511n();

    /* renamed from: I */
    C7562g f24199I = C7562g.getInstance();
    /* access modifiers changed from: private */

    /* renamed from: J */
    public C6872a f24200J = C6872a.m33458a();

    /* renamed from: K */
    String[] f24201K = {"android.permission.READ_CONTACTS"};

    /* renamed from: L */
    C7556a f24202L = C7556a.getInstance();

    /* renamed from: M */
    C7581a f24203M = C7581a.m35508c();

    /* renamed from: N */
    ArrayList<HashMap<String, Object>> f24204N = new ArrayList<>();

    /* renamed from: O */
    ArrayList<HashMap<String, Object>> f24205O = new ArrayList<>();

    /* renamed from: P */
    C6879h f24206P = C6879h.m33499c();

    /* renamed from: Q */
    C7591e f24207Q = C7591e.m35564f();

    /* renamed from: R */
    C7596g f24208R = C7596g.m35606z();

    /* renamed from: S */
    C7590d f24209S = C7590d.m35558a();
    @BindView(2131296344)
    CustomTextView btnTryAgain;
    @BindView(2131296605)
    LinearLayout layoutSync;
    @BindView(2131296608)
    ConstraintLayout layoutTryAgain;
    @BindView(2131296728)
    RecyclerView recyclerSheetRows;
    @BindView(2131296902)
    CustomTextView textSync;
    @BindView(2131296928)
    Toolbar toolbarContacts;

    /* renamed from: u */
    private C7085c f24210u;

    /* renamed from: v */
    private List<HashMap<String, String>> f24211v = new ArrayList();

    /* renamed from: w */
    ConstraintLayout f24212w;

    /* renamed from: x */
    ConstraintLayout f24213x;

    /* renamed from: y */
    ConstraintLayout f24214y;

    /* renamed from: z */
    CustomTextView f24215z;

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$a */
    class C6932a implements View.OnClickListener {
        C6932a() {
        }

        public void onClick(View view) {
            ActivityContacts.this.setResult(-1, new Intent());
            ActivityContacts.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$b */
    class C6933b implements DialogInterface.OnClickListener {
        C6933b() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ActivityContacts.this.f24200J.mo24207d()) {
                ActivityContacts activityContacts = ActivityContacts.this;
                activityContacts.f24208R.mo25662C(activityContacts, activityContacts);
                ActivityContacts.this.f24197G.mo24213d("");
                ActivityContacts activityContacts2 = ActivityContacts.this;
                activityContacts2.f24208R.mo25673k(activityContacts2.f24195E.getSheetId());
                return;
            }
            ActivityContacts activityContacts3 = ActivityContacts.this;
            Toast.makeText(activityContacts3, activityContacts3.getResources().getString(R.string.internet_check), 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$c */
    class C6934c implements DialogInterface.OnClickListener {
        C6934c(ActivityContacts activityContacts) {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$d */
    class C6935d implements SweetAlertDialog.OnSweetClickListener {
        C6935d() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityContacts.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$e */
    class C6936e implements SweetAlertDialog.OnSweetClickListener {
        C6936e() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityContacts.this.f24197G.mo24213d("");
            sweetAlertDialog.dismiss();
            ActivityContacts activityContacts = ActivityContacts.this;
            activityContacts.f24207Q.mo25639d(activityContacts.f24196F);
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$f */
    class C6937f implements SweetAlertDialog.OnSweetClickListener {
        C6937f() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            ActivityContacts.this.finish();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$g */
    class C6938g implements SweetAlertDialog.OnSweetClickListener {
        C6938g() {
        }

        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
            ActivityContacts.this.m33581D0();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$h */
    class C6939h implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ EditText f24222c;

        /* renamed from: d */
        final /* synthetic */ Dialog f24223d;

        C6939h(EditText editText, Dialog dialog) {
            this.f24222c = editText;
            this.f24223d = dialog;
        }

        public void onClick(View view) {
            String obj = this.f24222c.getText().toString();
            if (!obj.equals("")) {
                this.f24223d.dismiss();
                ActivityContacts.this.f24197G.mo24213d("");
                ActivityContacts activityContacts = ActivityContacts.this;
                activityContacts.f24199I.updatedSpreadsheetTitle = obj;
                activityContacts.f24208R.mo25664R(activityContacts.f24196F, ActivityContacts.this.f24208R.mo25660A(obj));
                return;
            }
            Toast.makeText(ActivityContacts.this, "Spreadsheet name cannot be empty!", 0).show();
        }
    }

    /* renamed from: com.spreadsheet.app.activities.ActivityContacts$i */
    class C6940i implements View.OnClickListener {

        /* renamed from: c */
        final /* synthetic */ Dialog f24225c;

        C6940i(ActivityContacts activityContacts, Dialog dialog) {
            this.f24225c = dialog;
        }

        public void onClick(View view) {
            this.f24225c.dismiss();
        }
    }

    /* renamed from: A0 */
    private void m33578A0(List<List<String>> list) {
        String str = "sheet1!A2:E" + (list.size() + 2) + "";
        C7582b bVar = this.f24198H;
        String str2 = this.f24196F;
        bVar.mo25603c(str2, str, bVar.mo25608h(str2, str, list));
    }

    /* renamed from: B0 */
    private void m33579B0() {
        this.f24204N = this.f24202L.getContactList();
        ArrayList<HashMap<String, Object>> emailList = this.f24202L.getEmailList();
        this.f24205O = emailList;
        m33583F0(this.f24204N, emailList);
    }

    /* renamed from: C0 */
    private void m33580C0(String str) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:" + str));
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: D0 */
    public void m33581D0() {
        ConstraintLayout constraintLayout;
        int i = 8;
        if (this.f24200J.mo24207d()) {
            this.f24197G.mo24213d("");
            this.f24198H.mo25610j(this.f24196F, "Sheet1!A1:E");
            constraintLayout = this.layoutTryAgain;
        } else {
            this.layoutSync.setVisibility(8);
            constraintLayout = this.layoutTryAgain;
            i = 0;
        }
        constraintLayout.setVisibility(i);
    }

    /* renamed from: E0 */
    private void m33582E0() {
        C4534g.m24100b().mo18599d(C6873b.f23978F);
        this.f24197G.mo24212c(this);
        this.f24208R.mo25662C(this, this);
        this.f24209S.mo25637b(this);
        this.f24198H.mo25614o(this, this);
        this.f24200J.mo24206c(this);
        this.f24206P.mo24225f(this);
        C7560e eVar = (C7560e) getIntent().getSerializableExtra("spreadsheet");
        this.f24195E = eVar;
        this.f24196F = eVar.getSheetId();
        this.f24206P.mo24222b(C6873b.f24024o);
        this.toolbarContacts.setTitle((CharSequence) this.f24195E.getSheetName());
        this.toolbarContacts.setTitleTextColor(getResources().getColor(R.color.black));
        mo276r0(this.toolbarContacts);
        this.toolbarContacts.setNavigationIcon((int) R.drawable.ic_back);
        this.toolbarContacts.setNavigationOnClickListener(new C6932a());
        Dialog dialog = new Dialog(this);
        this.f24193C = dialog;
        dialog.requestWindowFeature(1);
        this.f24193C.setContentView(R.layout.dialog_call_number);
        this.f24212w = (ConstraintLayout) this.f24193C.findViewById(R.id.layout_number1);
        this.f24213x = (ConstraintLayout) this.f24193C.findViewById(R.id.layout_number2);
        this.f24214y = (ConstraintLayout) this.f24193C.findViewById(R.id.layout_number3);
        this.f24215z = (CustomTextView) this.f24193C.findViewById(R.id.text_number1);
        this.f24191A = (CustomTextView) this.f24193C.findViewById(R.id.text_number2);
        this.f24192B = (CustomTextView) this.f24193C.findViewById(R.id.text_number3);
        this.f24212w.setOnClickListener(this);
        this.f24213x.setOnClickListener(this);
        this.f24214y.setOnClickListener(this);
        this.textSync.setOnClickListener(this);
        this.btnTryAgain.setOnClickListener(this);
        this.recyclerSheetRows.setLayoutManager(new LinearLayoutManager(this, 1, false));
        m33581D0();
    }

    /* renamed from: F0 */
    private void m33583F0(ArrayList<HashMap<String, Object>> arrayList, ArrayList<HashMap<String, Object>> arrayList2) {
        ArrayList arrayList3 = new ArrayList();
        for (int i = 0; i < arrayList.size(); i++) {
            HashMap hashMap = arrayList.get(i);
            int i2 = 0;
            while (true) {
                if (i2 >= arrayList2.size()) {
                    break;
                }
                HashMap hashMap2 = arrayList2.get(i2);
                if (arrayList.get(i).containsValue(arrayList2.get(i2).get(C7556a.KEY_NAME))) {
                    String str = (String) hashMap2.get(C7556a.KEY_EMAIL);
                    hashMap.put(C7556a.KEY_EMAIL, arrayList2.get(i2).get(C7556a.KEY_EMAIL));
                    break;
                }
                hashMap.put(C7556a.KEY_EMAIL, "");
                i2++;
            }
            arrayList3.add(hashMap);
        }
        this.f24202L.setContactList(arrayList3);
        m33585I0();
    }

    /* renamed from: G0 */
    private void m33584G0() {
        if (!this.f24200J.mo24205b(this.f24201K)) {
            this.f24200J.mo24208e(this.f24201K);
        } else if (this.f24200J.mo24207d()) {
            this.f24197G.mo24213d("");
            this.f24203M.mo25599a(this);
            this.f24203M.mo25600b(this);
            m33579B0();
        } else {
            Toast.makeText(this, R.string.internet_check, 0).show();
        }
    }

    /* renamed from: I0 */
    private void m33585I0() {
        ArrayList<HashMap<String, Object>> contactList = this.f24202L.getContactList();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < contactList.size(); i++) {
            new HashMap();
            HashMap hashMap = contactList.get(i);
            ArrayList arrayList2 = new ArrayList();
            if (hashMap.containsKey(C7556a.KEY_NAME)) {
                arrayList2.add(hashMap.get(C7556a.KEY_NAME).toString());
            } else {
                arrayList2.add("");
            }
            if (hashMap.containsKey(C7556a.KEY_CONTACT_NUMBER)) {
                arrayList2.add(hashMap.get(C7556a.KEY_CONTACT_NUMBER).toString());
            } else {
                arrayList2.add("");
            }
            if (hashMap.containsKey(C7556a.KEY_CONTACT_NUMBER2)) {
                arrayList2.add(hashMap.get(C7556a.KEY_CONTACT_NUMBER2).toString());
            } else {
                arrayList2.add("");
            }
            if (hashMap.containsKey(C7556a.KEY_CONTACT_NUMBER3)) {
                arrayList2.add(hashMap.get(C7556a.KEY_CONTACT_NUMBER3).toString());
            } else {
                arrayList2.add("");
            }
            if (!hashMap.containsKey(C7556a.KEY_EMAIL) || hashMap.get(C7556a.KEY_EMAIL).equals("")) {
                arrayList2.add("");
            } else {
                arrayList2.add(hashMap.get(C7556a.KEY_EMAIL).toString());
            }
            arrayList.add(arrayList2);
        }
        m33578A0(arrayList);
    }

    /* renamed from: J0 */
    private void m33586J0() {
        C7085c cVar = new C7085c(this, this.f24211v, this);
        this.f24210u = cVar;
        this.recyclerSheetRows.setAdapter(cVar);
    }

    /* renamed from: K0 */
    private void m33587K0() {
        this.f24197G.mo24211a();
        SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("Something Went Wrong!").setConfirmButton("Try Again", (SweetAlertDialog.OnSweetClickListener) new C6938g()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C6937f());
        cancelButton.setCancelable(false);
        cancelButton.setCanceledOnTouchOutside(false);
        cancelButton.show();
    }

    /* renamed from: H */
    public void mo24237H(String str, String str2) {
        str2.hashCode();
        char c = 65535;
        switch (str2.hashCode()) {
            case -2022976223:
                if (str2.equals("deleteSpreadsheet")) {
                    c = 0;
                    break;
                }
                break;
            case -561789226:
                if (str2.equals("Access_Denied")) {
                    c = 1;
                    break;
                }
                break;
            case 281689075:
                if (str2.equals("File_Not_Found")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.f24209S.mo25638c("DELETE_SPREADSHEET", "DELETE_SPREADSHEET");
                this.f24207Q.mo25642h(this, this);
                this.f24207Q.mo25639d(this.f24195E.getSheetId());
                return;
            case 1:
                this.f24199I.shouldRequestAuth = true;
                this.f24197G.mo24211a();
                finish();
                return;
            case 2:
                this.f24197G.mo24211a();
                SweetAlertDialog cancelButton = new SweetAlertDialog(this, 3).setTitleText("File not found!").setContentText("You have deleted this sheet\nfrom Drive!").setConfirmButton("Remove from list", (SweetAlertDialog.OnSweetClickListener) new C6936e()).setCancelButton("Exit", (SweetAlertDialog.OnSweetClickListener) new C6935d());
                cancelButton.setCancelable(false);
                cancelButton.setCanceledOnTouchOutside(false);
                cancelButton.show();
                return;
            default:
                m33587K0();
                return;
        }
    }

    /* renamed from: H0 */
    public void mo24298H0() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_add_new_sheet);
        EditText editText = (EditText) dialog.findViewById(R.id.edit_sheet_name);
        Button button = (Button) dialog.findViewById(R.id.button_add_sheet);
        editText.setText(this.f24195E.getSheetName());
        ((CustomTextView) dialog.findViewById(R.id.text_spreadsheet_title)).setText("Rename Spreadsheet");
        button.setText("Update");
        button.setOnClickListener(new C6939h(editText, dialog));
        ((ImageView) dialog.findViewById(R.id.image_close)).setOnClickListener(new C6940i(this, dialog));
        dialog.show();
    }

    /* renamed from: L0 */
    public void mo24299L0() {
        this.f24197G.mo24211a();
        Toast.makeText(this, "Spreadsheet Renamed!", 0).show();
        this.f24207Q.mo25644j(this.f24196F, C7562g.KEY_SPREADSHEET_NAME, this.f24199I.updatedSpreadsheetTitle);
        this.f24207Q.mo25647m(this.f24196F);
        this.toolbarContacts.setTitle((CharSequence) this.f24199I.updatedSpreadsheetTitle);
        this.f24199I.isUpdated = true;
    }

    /* renamed from: d */
    public void mo24300d(HashMap<String, String> hashMap, int i) {
        Intent intent = new Intent("android.intent.action.SENDTO");
        intent.setData(Uri.parse("mailto:"));
        Intent intent2 = new Intent("android.intent.action.SEND");
        intent2.putExtra("android.intent.extra.EMAIL", new String[]{(String) this.f24211v.get(i).get("Email")});
        intent2.setSelector(intent);
        startActivity(intent2);
    }

    public void onClick(View view) {
        CustomTextView customTextView;
        int id = view.getId();
        if (id == R.id.btn_try_again) {
            m33581D0();
        } else if (id != R.id.text_sync) {
            switch (id) {
                case R.id.layout_number1:
                    this.f24193C.dismiss();
                    customTextView = this.f24215z;
                    break;
                case R.id.layout_number2:
                    this.f24193C.dismiss();
                    customTextView = this.f24191A;
                    break;
                case R.id.layout_number3:
                    this.f24193C.dismiss();
                    customTextView = this.f24192B;
                    break;
                default:
                    return;
            }
            String charSequence = customTextView.getText().toString();
            this.f24194D = charSequence;
            m33580C0(charSequence);
        } else {
            m33584G0();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_contacts);
        ButterKnife.bind((Activity) this);
        m33582E0();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_delete_spreadsheet) {
            C0065b.C0066a aVar = new C0065b.C0066a(this);
            aVar.mo236g(getResources().getString(R.string.do_you_want_to_delete) + " '" + this.f24195E.getSheetName() + "' " + getResources().getString(R.string.file_permanently));
            aVar.mo240k(getResources().getString(R.string.yes), new C6933b());
            aVar.mo237h(getResources().getString(R.string.no), new C6934c(this));
            aVar.mo243n();
        } else if (itemId == R.id.menu_rename_spreadsheet) {
            mo24298H0();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 1) {
            m33584G0();
        }
    }

    /* renamed from: s */
    public void mo24244s(String str, String str2) {
        str2.hashCode();
        char c = 65535;
        switch (str2.hashCode()) {
            case -2022976223:
                if (str2.equals("deleteSpreadsheet")) {
                    c = 0;
                    break;
                }
                break;
            case -1809886158:
                if (str2.equals("getAllSpreadsheets")) {
                    c = 1;
                    break;
                }
                break;
            case -1788871435:
                if (str2.equals("updateSpreadSheetTitle")) {
                    c = 2;
                    break;
                }
                break;
            case -1033874466:
                if (str2.equals("getAllContacts")) {
                    c = 3;
                    break;
                }
                break;
            case 227556787:
                if (str2.equals("addAllContacts")) {
                    c = 4;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.f24209S.mo25638c("DELETE_SPREADSHEET", "DELETE_SPREADSHEET");
                this.f24207Q.mo25639d(this.f24195E.getSheetId());
                return;
            case 1:
                this.f24197G.mo24211a();
                this.f24199I.isUpdated = true;
                finish();
                return;
            case 2:
                this.f24195E.setSheetName(this.f24199I.updatedSpreadsheetTitle);
                mo24299L0();
                return;
            case 3:
                this.f24197G.mo24211a();
                this.f24197G.mo24211a();
                List<HashMap<String, String>> allRowsList = this.f24199I.getAllRowsList();
                this.f24211v = allRowsList;
                if (allRowsList.isEmpty()) {
                    this.recyclerSheetRows.setVisibility(8);
                    this.layoutSync.setVisibility(0);
                } else {
                    this.recyclerSheetRows.setVisibility(0);
                    this.layoutSync.setVisibility(8);
                }
                m33586J0();
                return;
            case 4:
                this.f24199I.isUpdated = true;
                this.f24207Q.mo25647m(this.f24196F);
                m33581D0();
                return;
            default:
                return;
        }
    }

    /* renamed from: t */
    public void mo24304t(HashMap<String, String> hashMap, int i) {
        HashMap hashMap2 = this.f24211v.get(i);
        if (!hashMap2.containsKey("Contact No1") || ((String) hashMap2.get("Contact No1")).equals("")) {
            this.f24212w.setVisibility(8);
        } else {
            this.f24212w.setVisibility(0);
            this.f24215z.setText((CharSequence) hashMap2.get("Contact No1"));
        }
        if (!hashMap2.containsKey("Contact No2") || ((String) hashMap2.get("Contact No2")).equals("")) {
            this.f24213x.setVisibility(8);
        } else {
            this.f24191A.setText((CharSequence) hashMap2.get("Contact No2"));
            this.f24213x.setVisibility(0);
        }
        if (!hashMap2.containsKey("Contact No3") || ((String) hashMap2.get("Contact No3")).equals("")) {
            this.f24214y.setVisibility(8);
        } else {
            this.f24192B.setText((CharSequence) hashMap2.get("Contact No3"));
            this.f24214y.setVisibility(0);
        }
        this.f24193C.show();
    }

    /* renamed from: u */
    public void mo24305u() {
    }
}
